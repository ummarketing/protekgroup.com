<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Варианты исполнения");
?><section class="section section--top0">
<div class="container">
	<h1 class="section__title">Варианты исполнения</h1>
	<div class="news_article_container">
		<p>
		</p>
		<h1>Лотки и подложки</h1>
		<p>
			 На нашем производстве возможно изготовление любого типа лотков и подложек
		</p>
		<table cellpadding="0" cellspacing="20" style="table-layout: fixed;">
		<tbody>
		<tr>
			<td>
				<h3 class="rtecenter">
				С технологией "Коатинг" </h3>
 <img src="/sites/default/files/images/img-v1.png" alt="" class="img-fluid">
			</td>
			<td>
				<h3 class="rtecenter">
				С влаговпитывающими свойствами </h3>
 <img src="/sites/default/files/images/img-v2.png" alt="" class="img-fluid">
			</td>
		</tr>
		<tr>
			<td>
				<h3 class="rtecenter">
				С металлизацией </h3>
 <img src="/sites/default/files/images/img-v3.png" alt="" class="img-fluid">
			</td>
			<td>
				<h3 class="rtecenter">
				Со сплошной печатью </h3>
 <img src="/sites/default/files/images/img-v4.png" alt="" class="img-fluid">
			</td>
		</tr>
		<tr>
			<td>
				<h3 class="rtecenter">
				С нанесением логотипа </h3>
 <img src="/sites/default/files/images/img-v5.png" alt="" class="img-fluid">
			</td>
			<td>
				<h3 class="rtecenter">
				Барьерные лотки под запайку </h3>
 <img src="/sites/default/files/images/img-v6.png" alt="" class="img-fluid">
			</td>
		</tr>
		<tr>
			<td>
				<h3 class="rtecenter">
				Без бортиков </h3>
 <img src="/sites/default/files/images/img-v7.png" alt="" class="img-fluid">
			</td>
			<td>
				 &nbsp;
			</td>
		</tr>
		</tbody>
		</table>
		<h1>Упаковка для тортов</h1>
		<p>
			 На нашем производстве возможно применение данных технологий на всех видах упаковки для тортов и пирогов:
		</p>
		<table cellpadding="0" cellspacing="20" style="table-layout: fixed;">
		<tbody>
		<tr>
			<td>
				<h3 class="rtecenter">
				Шипы </h3>
 <img src="/sites/default/files/images/img-v8.png" alt="" class="img-fluid">
			</td>
			<td>
				<h3 class="rtecenter">
				Вентиляция </h3>
 <img src="/sites/default/files/images/img-v9.png" alt="" class="img-fluid">
			</td>
		</tr>
		<tr>
			<td>
				<h3 class="rtecenter">
				Цветное дно </h3>
 <img src="/sites/default/files/images/img-v10.png" alt="" class="img-fluid">
			</td>
			<td>
				<h3 class="rtecenter">
				Тиснение логотипа </h3>
 <img src="/sites/default/files/images/img-v11.png" alt="" class="img-fluid">
			</td>
		</tr>
		</tbody>
		</table>
		<p>
		</p>
	</div>
</div>
 </section><br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>