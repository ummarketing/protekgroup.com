<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? IncludeTemplateLangFile(__FILE__); ?>
    <!DOCTYPE html>
    <html lang="cn">
    <head>
        <meta charset="utf-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible"
              content="IE=edge">
        <title><? $APPLICATION->ShowTitle(); ?></title>

        <? $APPLICATION->ShowMeta("keywords");
        $APPLICATION->ShowMeta("description");
        $APPLICATION->SetAdditionalCss(DEFAULT_TEMPLATE_PATH . "/css/styles.css");
        $APPLICATION->SetAdditionalCss(DEFAULT_TEMPLATE_PATH . "/css/template_styles.css");
        $APPLICATION->AddHeadScript(DEFAULT_TEMPLATE_PATH . "/js/scripts.min.js");
        $APPLICATION->AddHeadScript("//api-maps.yandex.ru/2.1/?lang=en_US");
        $APPLICATION->AddHeadScript(DEFAULT_TEMPLATE_PATH . "/js/main.js");
        $APPLICATION->ShowHead(); ?>
        <meta property="og:title"
              content="<? $APPLICATION->ShowTitle() ?>"/>
        <meta property="og:type"
              content="<? $APPLICATION->ShowProperty("og_type", "website") ?>"/>
        <meta property="og:url"
              content="<? $APPLICATION->ShowProperty("canonical", $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . $APPLICATION->GetCurDir()) ?>"/>
        <meta property="og:image"
              content="<? $APPLICATION->ShowProperty("og_image", $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . DEFAULT_TEMPLATE_PATH . "/img/logo.svg") ?>"/>
        <meta property="og:description"
              content="<? $APPLICATION->ShowProperty("description") ?>"/>
        <? $APPLICATION->IncludeFile("/include/canonical.php") ?>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script data-skip-moving="true"
                type="text/javascript">
            const SITE_ID = "<?=SITE_ID?>";
        </script>
        <script data-skip-moving="true"
                type="text/javascript">!function () {
                var t = document.createElement("script");
                t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?162", t.onload = function () {
                    VK.Retargeting.Init("VK-RTRG-410429-1EMdi"), VK.Retargeting.Hit()
                }, document.head.appendChild(t)
            }();</script>
        <noscript>
            <img src="https://vk.com/rtrg?p=VK-RTRG-410429-1EMdi"
                 style="position:fixed; left:-999px;"
                 alt=""/>
        </noscript>
        <!-- Facebook Pixel Code -->
        <script data-skip-moving="true">
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '1255896814615053');
            fbq('track', 'PageView');
        </script>
        <noscript>
            <img height="1"
                 width="1"
                 style="display:none"
                 src="https://www.facebook.com/tr?id=1255896814615053&ev=PageView&noscript=1"
            />
        </noscript>
        <!-- End Facebook Pixel Code -->



        <!-- ASSETS -->
        <link rel="stylesheet" href="/local/assets/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="/local/assets/css/fancybox.min.css">
        <link rel="stylesheet" href="/local/assets/css/grid.css">
        <link rel="stylesheet" href="/local/assets/css/style_new.css">
        <!-- /ASSETS -->
    </head>
<body>
<? $APPLICATION->ShowPanel(); ?>
    <header class="header">
        <div class="header__submenu hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__submenu-wrap">
                            <div class="header__submenu-lang">
                                <span><?= GetMessage("PROTEK_TEMPLATE_LANGUAGE") ?></span>
                                <a href="/">RU</a>
                                <a href="/en/">EN</a>
                                <a href="/de/">DE</a>
                                <span class="active">CN</span>
                                <a href="/es/">ES</a>
                            </div>

                            <div class="header__submenu-links">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "protek_top_menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "left",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(0 => "",),
                                        "MENU_CACHE_TIME" => "36000",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "first",
                                        "USE_EXT" => "N"
                                    )
                                ); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?
        $arComponentOutput = $APPLICATION->IncludeComponent(
            "cyberrex:city.selection",
            ".default",
            array(
                "ELEMENT_ID" => "74",
                "IBLOCK_ID" => "30",
                "IBLOCK_TYPE" => "content_cn",
                "COMPONENT_TEMPLATE" => ".default",
                "BTN_EDIT" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_NO"),
                "BTN_CLOSE" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_YES"),
                "POPUP_LABEL" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_CHOOSE_CLOSEST"),
                "QUESTION_TEXT" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_QUESTION"),
                "BLOCKED" => GetMessage("PROTEK_TEMPLATE_CITY_CHANGING_BLOCK"),
            ),
            false
        );
        global $arCity;
        $arCity = array(
            'ID' => $arComponentOutput['CURRENT_CITY']['ID'],
            'NAME' => $arComponentOutput['CURRENT_CITY']['NAME'],
            'ADDRESS' => $arComponentOutput['CURRENT_CITY']['ADDRESS'],
            'EMAIL' => $arComponentOutput['CURRENT_CITY']['EMAIL'],
            'PHONE' => $arComponentOutput['CURRENT_CITY']['PHONE'],
            'TIME' => $arComponentOutput['CURRENT_CITY']['TIME'],
            'LOCATION' => $arComponentOutput['CURRENT_CITY']['LOCATION'],
        );
        unset($arComponentOutput);
        ?>



        <!-- NEW MOBILE MENU -->
        <? include ($_SERVER['DOCUMENT_ROOT'].'/local/include_area/mobile_menu.php'); ?>
        <!-- /NEW MOBILE MENU -->



        <div class="header__content hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-3 col-xl-4">
                        <div class="header__mobmenu">
                            <div class="header__mobmenu-nav">
                                <button class="header__mobmenu-phone"
                                        type="button">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 348.077 348.077"
                                         xml:space="preserve"><path
                                                d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"/></svg>
                                </button>
                                <button class="header__mobmenu-search"
                                        type="button">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 56.966 56.966"
                                         xml:space="preserve"><path
                                                d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23 s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92 c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17 s-17-7.626-17-17S14.61,6,23.984,6z"/></svg>
                                </button>
                                <a href="#modal-cart"
                                   class="header__mobmenu-cart open-modal">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 260.293 260.293"
                                         xml:space="preserve"><path
                                                d="M258.727,57.459c-1.42-1.837-3.612-2.913-5.934-2.913H62.004l-8.333-32.055c-0.859-3.306-3.843-5.613-7.259-5.613H7.5 c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h33.112l8.333,32.055c0,0.001,0,0.001,0.001,0.002l29.381,112.969 c0.859,3.305,3.843,5.612,7.258,5.612h137.822c3.415,0,6.399-2.307,7.258-5.612l29.385-112.971 C260.636,61.687,260.147,59.295,258.727,57.459z M117.877,167.517H91.385l-5.892-22.652h32.384V167.517z M117.877,129.864H81.592 l-5.895-22.667h42.18V129.864z M117.877,92.197H71.795l-5.891-22.651h51.973V92.197z M176.119,167.517h-43.242v-22.652h43.242 V167.517z M176.119,129.864h-43.242v-22.667h43.242V129.864z M176.119,92.197h-43.242V69.546h43.242V92.197z M217.609,167.517 h-26.49v-22.652h32.382L217.609,167.517z M227.403,129.864h-36.284v-22.667h42.18L227.403,129.864z M237.201,92.197h-46.081V69.546 h51.974L237.201,92.197z"/>
                                        <path d="M105.482,188.62c-15.106,0-27.396,12.29-27.396,27.395c0,15.108,12.29,27.4,27.396,27.4 c15.105,0,27.395-12.292,27.395-27.4C132.877,200.91,120.588,188.62,105.482,188.62z M105.482,228.415 c-6.835,0-12.396-5.563-12.396-12.4c0-6.835,5.561-12.395,12.396-12.395c6.834,0,12.395,5.561,12.395,12.395 C117.877,222.853,112.317,228.415,105.482,228.415z"/>
                                        <path d="M203.512,188.62c-15.104,0-27.392,12.29-27.392,27.395c0,15.108,12.288,27.4,27.392,27.4 c15.107,0,27.396-12.292,27.396-27.4C230.908,200.91,218.618,188.62,203.512,188.62z M203.512,228.415 c-6.833,0-12.392-5.563-12.392-12.4c0-6.835,5.559-12.395,12.392-12.395c6.836,0,12.396,5.561,12.396,12.395 C215.908,222.853,210.347,228.415,203.512,228.415z"/></svg>
                                    <span class="cart-btn-wrap-basket"><?= (count($_SESSION['CYBER_BASKET_' . SITE_ID]) > 0 ? count($_SESSION['CYBER_BASKET_' . SITE_ID]) : "") ?></span>
                                </a>
                            </div>
                            <div class="header__mob_fav_comp">
                                <a href="<?= SITE_DIR ?>catalog/compare.php"><?= GetMessage('PROTEK_TEMPLATE_COMPARE') ?>
                                    <span class="cart-btn-wrap-compare"><? if (isset($_SESSION[COMPARE_NAME . SITE_ID])) {
                                            if (reset($_SESSION[COMPARE_NAME . SITE_ID])) {
                                                $arCompare = reset($_SESSION[COMPARE_NAME . SITE_ID]);
                                                if (count($arCompare['ITEMS']) > 0) {
                                                    echo "(" . count($arCompare['ITEMS']) . ")";
                                                }
                                            }
                                        } ?></span></a>
                                <a href="<?= SITE_DIR ?>catalog/favorites/"><?= GetMessage('PROTEK_TEMPLATE_FAVORITES') ?>
                                    <span class="cart-btn-wrap-favorites"><? if (isset($_SESSION[FAVORITES_NAME . SITE_ID])) {
                                            if (count($_SESSION[FAVORITES_NAME . SITE_ID]) > 0) {
                                                echo "(" . count($_SESSION[FAVORITES_NAME . SITE_ID]) . ")";
                                            }
                                        } ?></span></a>
                            </div>
                            <? $APPLICATION->IncludeComponent("bitrix:search.form", "protek_search_input", Array(
                                "PAGE" => "#SITE_DIR#search/",
                                "SEARCH_CSS_CLASS" => "header__mobsearch",
                                "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
                            ),
                                false
                            ); ?>

                            <div class="header__mobcontent">
                                <div class="header__contacts">
                                    <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                                       class="header__contacts-phone cyber_location_string phone">
                                        <svg version="1.1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px"
                                             y="0px"
                                             viewBox="0 0 348.077 348.077"
                                             xml:space="preserve"><path
                                                    d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"></path></svg>
                                        <span class="ya-phone"><?= $arCity['PHONE']; ?></span>
                                    </a>
                                    <div class="header__contacts-city">
                                        <svg version="1.1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px"
                                             y="0px"
                                             viewBox="0 0 80 80"
                                             xml:space="preserve"><path
                                                    d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path></svg>
                                        <span><?= GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_TITLE") ?></span>
                                        <a href="#modal-location"
                                           class="open-modal cyber_location_name"><?= $arCity['NAME'] ?></a>
                                    </div>
                                    <a href="mailto:<?= $arCity['EMAIL'] ?>"
                                       class="header__contacts-mail cyber_location_string email">
                                        <svg version="1.1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px"
                                             y="0px"
                                             viewBox="0 0 14 14"
                                             xml:space="preserve"><path
                                                    d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                                            <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/>
                                            <polygon
                                                    points="0,2.878 0,11.186 4.833,7.079"/>
                                            <polygon
                                                    points="9.167,7.079 14,11.186 14,2.875"/>
									</svg>
                                        <span><?= $arCity['EMAIL'] ?></span></a>
                                </div>

                                <div class="header__btns">
                                    <a href="#modal-call"
                                       class="header__btn open-modal"><?= GetMessage("PROTEK_TEMPLATE_CALLBACK") ?></a>
                                    <a href="#modal-request"
                                       class="header__btn open-modal"><?= GetMessage("PROTEK_TEMPLATE_ORDER_BUTTON") ?></a>
                                </div>

                                <div class="header__social">
                                    <div class="header__social-list">
                                        <span><?= GetMessage("PROTEK_TEMPLATE_SOCIAL_TITLE") ?></span>
                                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/socnet.php"), false); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="header__logo">
                            <a href="/cn/">
                                <svg version="1.1"
                                     xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     x="0px"
                                     y="0px"
                                     viewBox="0 0 443.2 155.5"
                                     style="enable-background:new 0 0 443.2 155.5;"
                                     xml:space="preserve">
									<style type="text/css">
                                        .cyber_super_class_for_svg0 {
                                            fill: #45A2D0
                                        }

                                        .cyber_super_class_for_svg1 {
                                            fill: #595651
                                        }
                                    </style>
                                    <path class="cyber_super_class_for_svg0"
                                          d="M197.8,59.7c-3.3,1.8-6.9,2.5-11.7,2.5h-14.6v17.8h-6.2V36.4h21.5c11.7,0,17.1,4.4,17.1,13.1
									C204,53.9,201.8,57.2,197.8,59.7 M186.8,42.2h-15.7v14.6h16c2.9,0,5.5-0.7,7.6-1.8c2.2-1.5,3.3-2.9,3.3-5.1
									C197.8,44.4,194.1,42.2,186.8,42.2"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="M244.4,58.6c3.3,2.2,5.1,5.1,5.1,9.5v11.7h-6.2V68.1c0-4.4-3.6-6.6-10.6-6.6h-15.7v18.2h-6.2V36.4h22.2
									c11.3,0,16.8,4,16.8,12C250.2,53.2,248.4,56.4,244.4,58.6 M233.5,42.2h-16.4v14.2h15.3c3.3,0,6.2-0.7,8-1.8c2.2-1.5,3.3-3.3,3.3-5.8
									C244,44.4,240.4,42.2,233.5,42.2"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="M299.4,74.3c-4.4,4-10.2,5.8-17.8,5.8s-13.5-1.8-17.8-5.8c-4.4-4-6.6-9.5-6.6-16c0-6.9,2.2-12,6.6-16
									c4.4-3.6,10.2-5.5,17.8-5.5s13.5,1.8,17.8,5.5c4.4,4,6.6,9.1,6.6,16C305.9,65.2,303.7,70.3,299.4,74.3 M295,46.2
									c-3.3-2.9-7.6-4-13.5-4s-10.2,1.5-13.5,4c-3.3,2.9-4.7,6.6-4.7,11.7s1.5,9.1,4.7,12c3.3,2.9,7.6,4.4,13.5,4.4s10.2-1.5,13.5-4.4
									s4.7-6.9,4.7-12C299.7,53.2,298.3,49.2,295,46.2"/>
                                    <polygon
                                            class="cyber_super_class_for_svg0"
                                            points="332.5,42.2 332.5,79.7 326.3,79.7 326.3,42.2 309.9,42.2 309.9,36.4 349.3,36.4 349.3,42.2 "/>
                                    <polygon
                                            class="cyber_super_class_for_svg0"
                                            points="354.7,79.7 354.7,36.4 390.4,36.4 390.4,42.2 360.9,42.2 360.9,54.6 387.1,54.6 387.1,60.1 360.9,60.1
									360.9,74.3 390.8,74.3 390.8,79.7 "/>
                                    <polygon
                                            class="cyber_super_class_for_svg0"
                                            points="434.5,79.7 410.8,59 403.1,64.5 403.1,79.7 397,79.7 397,36.4 403.1,36.4 403.1,57.5 433,36.4
									443.2,36.4 415.9,55.3 443.2,79.7 "/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M188.3,123.8h-12.7c-7.3,0-10.6-1.1-10.6-8.7v-7.6c0-6.6,3.3-8.7,8-8.7h17.1c4,0,7.3,1.5,7.3,5.5v1.8h-3.3v-1.5
									c0-2.9-1.1-3.6-5.8-3.6h-13.8c-5.8,0-6.9,1.1-6.9,6.6v8c0,4.7,1.8,5.8,6.9,5.8h12.7c5.5,0,6.6-0.7,6.6-4.4v-2.5h-13.5v-2.5H197v4.7
									C197.4,122.7,194.5,123.8,188.3,123.8"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M257.9,123.8c-4.4,0-4.4-2.5-4.4-5.8v-1.1c0-3.3-0.4-4.4-4-4.4h-19.3v10.9h-3.3V98.7h23.7
									c5.5,0,6.6,3.3,6.6,6.6c0,3.6-1.5,5.8-4,6.2l0,0c2.5,0,3.6,1.5,3.6,4.7v3.3c0,1.1,0,1.8,1.8,1.8h0.7v2.5
									C259.3,123.8,257.9,123.8,257.9,123.8z M249.1,101.2h-18.9v9.1H248c4.4,0,5.8-0.4,5.8-4.4C254.2,102.7,253.1,101.2,249.1,101.2"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M312.5,123.8h-14.6c-6.6,0-9.1-2.2-9.1-8v-8.7c0-6.2,2.2-8.4,9.1-8.4h14.6c6.9,0,9.1,2.2,9.1,8.4v8.7
									C321.2,121.6,319,123.8,312.5,123.8 M318.3,107.1c0-4.7-1.1-5.8-5.8-5.8h-14.6c-4.7,0-5.8,1.1-5.8,5.8v8.7c0,5.1,1.8,5.5,5.8,5.5
									h14.6c4,0,5.8-0.7,5.8-5.5V107.1z"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M371.8,123.8H362c-6.2,0-10.9,0-10.9-7.3V98.7h2.9v17.8c0,4,1.1,4.7,7.6,4.7h9.8c5.5,0,7.6,0,7.6-4.7V98.7h2.9
									v17.8C382.8,123.4,378.4,123.8,371.8,123.8"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M436.6,114h-18.9v9.8h-3.3V99h22.2c5.5,0,6.6,3.3,6.6,7.6C443.2,108.5,443.2,114,436.6,114 M435.6,101.6h-17.8
									v10.2h16.4c4,0,6.2,0,6.2-4.7C440.3,102,439.2,101.6,435.6,101.6"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="M98.4,78.7c0,0-19.3-36.4-43.3-22.6S9.1,84.5,33.5,108.9c0,0-12.4-7.6-20.8-12.4C4,91.8-12.3,83,21.2,61.5
									C54.7,40.4,54.7,39.3,66,39.3C76.9,39.7,95.8,59,98.4,78.7"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M75.4,106.3c0,0,37.5-17.5,24.8-41.9C87.1,40.1,73.2,17.8,47.4,41.1c0,0,8.4-11.7,13.1-20
									c5.1-8.4,14.9-24.4,35,9.8c20,34.2,21.1,33.9,20.8,44.8C115.5,86.7,95.1,104.5,75.4,106.3"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="M73.2,58.6c0,0-37.9,17.1-25.1,41.5c12.7,24.4,26.6,47,52.4,24c0,0-8.4,11.7-13.5,20c-5.1,8.4-15.3,24-35-10.2
									s-20.8-34.2-20-45.2C32.8,77.6,53.6,60.1,73.2,58.6"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="M49.6,82.7c0,0,17.1,37.5,41.9,25.1s47.7-25.9,24.8-51.7c0,0,11.7,8.4,20,13.5c8.4,5.1,24.4,14.9-10.6,34.2
									s-35,20.4-45.9,19.7C68.5,122.7,51,102.3,49.6,82.7"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="M145.3,12.7c-0.7,3.6-1.8,6.9-3.6,10.2c-2.2,3.6-4.7,7.3-8,10.2c-3.3,3.3-6.6,6.2-10.6,8.7
									c-1.5,1.1-2.5,1.8-4,2.5h-0.4l0,0c0-0.7,0-1.5,0.4-2.2c0.4-2.5,1.5-4.7,3.3-6.6c1.5-1.8,3.3-3.6,5.1-5.1c2.2-1.8,4-3.6,5.8-5.5
									c2.5-2.5,5.1-5.5,7.3-8.4c1.5-1.8,2.9-4,4.4-6.2l0,0l0,0C145.7,11.3,145.7,12,145.3,12.7 M150.1,2.9c0-0.7,0-1.8-0.4-2.5V0
									c0,0-0.4,0-0.4,0.4c-0.4,0.4-0.4,0.7-0.7,0.7c-1.1,1.1-2.2,2.2-3.3,2.9c-2.9,2.2-6.2,4-9.5,5.8c-2.9,1.5-5.8,2.9-8.7,4.7
									c-2.5,1.5-5.1,2.9-6.9,5.1c-4.7,4.4-7.3,9.5-7.3,16c0,2.2,0,4.4,0.7,6.6c0.4,1.1,0.7,2.5,0.7,4c0.4,1.8,0,3.3-0.7,4.7
									c0,0.4-0.4,0.7-0.4,1.1l2.5,3.6c0.4-1.8,1.1-3.6,2.5-5.1c0.4,0,0.4-0.4,0.7-0.4c0.4,0,0.7,0,1.1,0c4.4-0.4,8.4-1.1,12.4-2.9
									c2.9-1.5,5.5-2.9,7.6-5.5c3.3-3.3,5.5-7.3,6.9-11.3c1.5-4,2.2-7.6,2.5-12C150.4,13.1,150.4,8,150.1,2.9"
                                          fill-opacity="0"/>
								</svg>
                            </a>
                        </div>
                    </div>

                    <div class="col-12 col-md-12 col-lg-9 col-xl-8">
                        <div class="header__content-wrap">
                            <div class="header__contacts">
                                <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                                   class="header__contacts-phone cyber_location_string phone">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 348.077 348.077"
                                         xml:space="preserve"><path
                                                d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"></path></svg>
                                    <span class="ya-phone"><?= $arCity['PHONE']; ?></span>
                                </a>
                                <div class="header__contacts-city">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 80 80"
                                         xml:space="preserve"><path
                                                d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path></svg>
                                    <span><?= GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_TITLE") ?></span>
                                    <a href="#modal-location"
                                       class="open-modal cyber_location_name"><?= $arCity['NAME'] ?></a>
                                </div>
                                <a href="mailto:<?= $arCity['EMAIL'] ?>"
                                   class="header__contacts-mail cyber_location_string email">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 14 14"
                                         xml:space="preserve"><path
                                                d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                                        <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/>
                                        <polygon
                                                points="0,2.878 0,11.186 4.833,7.079"/>
                                        <polygon
                                                points="9.167,7.079 14,11.186 14,2.875"/>
								</svg>
                                    <span><?= $arCity['EMAIL'] ?></span></a>
                            </div>

                            <div class="header__btns">
                                <a href="#modal-call"
                                   class="header__btn open-modal"><?= GetMessage("PROTEK_TEMPLATE_CALLBACK") ?></a>
                                <a href="#modal-request"
                                   class="header__btn open-modal"><?= GetMessage("PROTEK_TEMPLATE_ORDER_BUTTON") ?></a>
                            </div>

                            <div class="header__social">
                                <div class="header__lang">
                                    <a href="/">RU</a>
                                    <a href="/en/">EN</a>
                                    <a href="/de/">DE</a>
                                    <span class="active">CN</span>
                                    <a href="/es/">ES</a>
                                </div>

                                <div class="header__social-list">
                                    <span><?= GetMessage("PROTEK_TEMPLATE_SOCIAL_TITLE") ?></span>
                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/socnet.php"), false); ?>
                                </div>
                            </div>
                        </div>
                        <? $APPLICATION->IncludeComponent("bitrix:search.form", "protek_search_input", Array(
                            "PAGE" => "#SITE_DIR#search/",
                            "SEARCH_CSS_CLASS" => "header__search",
                            "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
                        ),
                            false
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="nav nav--head hidden-xs">
        <div class="container nav__wrap">
            <div class="row">
                <div class="col-12">
                    <div class="nav__content">
                        <button class="nav__btn nav__btn--head"
                                type="button"
                                data-toggle="collapse"
                                href="#collapsed_mobile_menu"
                                aria-expanded="false"
                                aria-controls="collapse0">
                            <svg version="1.1"
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 x="0px"
                                 y="0px"
                                 viewBox="0 0 92.833 92.833"
                                 xml:space="preserve"><path
                                        d="M89.834,1.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V4.75 C92.834,3.096,91.488,1.75,89.834,1.75z"/>
                                <path d="M89.834,36.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V39.75 C92.834,38.096,91.488,36.75,89.834,36.75z"/>
                                <path d="M89.834,71.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V74.75 C92.834,73.095,91.488,71.75,89.834,71.75z"/></svg>
                            <span data-toggle-caption="<?= GetMessage("PROTEK_TEMPLATE_CLOSE_MENU") ?>"><?= GetMessage("PROTEK_TEMPLATE_OPEN_MENU") ?></span>
                        </button>

                        <div class="collapse nav__mob"
                             id="collapsed_mobile_menu">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "protek_mobile_menu",
                                Array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "2",
                                    "MENU_CACHE_GET_VARS" => array(0 => "",),
                                    "MENU_CACHE_TIME" => "36000",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "top",
                                    "USE_EXT" => "Y",
                                    "MENU_SHOW_LESS" => GetMessage("PROTEK_TEMPLATE_SHOW_LESS"),
                                )
                            ); ?>
                            <div class="nav__submenu">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "protek_top_menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "left",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(0 => "",),
                                        "MENU_CACHE_TIME" => "36000",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "first",
                                        "USE_EXT" => "N"
                                    )
                                ); ?>
                            </div>
                        </div>

                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "protek_main_menu",
                            Array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "left",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(0 => "",),
                                "MENU_CACHE_TIME" => "36000",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "top",
                                "USE_EXT" => "N"
                            )
                        ); ?>

                    </div>
                </div>
            </div>
        </div>
        <? $APPLICATION->IncludeComponent("bitrix:search.form", "protek_search_input", Array(
            "PAGE" => "#SITE_DIR#search/",
            "SEARCH_CSS_CLASS" => "nav__search",
            "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
            "SEARCH_USE_CONTAINER" => "Y",
        ),
            false
        ); ?>
    </div>
<? if ($APPLICATION->GetCurPage() != SITE_DIR) { ?>
    <div class="section section--bottom0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "main",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => SITE_ID,
                            "START_FROM" => "0"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
<? } ?>