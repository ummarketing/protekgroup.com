<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var $arResult array
 * @var $arParams array
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @var $component CBitrixComponent
 * @var $this CBitrixComponentTemplate
 * @var $city
 */

$this->setFrameMode(true);
$frame = $this->createFrame()->begin("");
$chosenCity = '';
if(isset($arResult["USER_CHOICE"])){
	$chosenCity = $arResult["USER_CHOICE"]["CITY"]["NAME"];
}else{
	$chosenCity = $arResult["AUTO_AUTODETECTION"]["C_NAME"];
}

?>
<div id="modal-location" class="zoom-anim-dialog mfp-hide modal modal--location">
	<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
	<div class="modal__content">
		<h6 class="modal__title"><?= str_ireplace("%CITY%", $chosenCity, $arParams["CITY_QUESTION"]) ?></h6>

		<div class="modal__btns">
			<a href="#" class="modal__btn modal__btn--green"><?=$arParams["CITY_YES"]?></a>
			<a href="#modal-city" class="modal__btn modal__btn--grey open-modal"><?=$arParams["CITY_SELECT_CITY"]?></a>
		</div>
	</div>
</div>
<div id="modal-city" class="zoom-anim-dialog mfp-hide modal modal--city">
	<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
	<div class="modal__content">
		<h6 class="modal__title"><?=$arParams["CITY_CHOOSE_CLOSEST"]?></h6>

		<div class="row">
			<div class="col-12 col-md-6 col-lg-3">
				<span class="modal__abc">А - И</span>
				<ul class="modal__city">
					<li><a href="#">Архангельск</a></li>
					<li><a href="#">Астрахань</a></li>
					<li><a href="#">Баку, Азербайджан</a></li>
					<li><a href="#">Берлин, Германия</a></li>
					<li><a href="#">Бишкек, Киргизия</a></li>
					<li><a href="#">Брянск</a></li>
					<li><a href="#">Варшава, Польша</a></li>
				</ul>
			</div>

			<div class="col-12 col-md-6 col-lg-3">
				<span class="modal__abc">К - Т</span>
				<ul class="modal__city">
					<li><a href="#">Йошкар-Ола</a></li>
					<li><a href="#">Цюрих, Швейцария</a></li>
					<li><a href="#">Осло, Норвегия</a></li>
					<li><a href="#">Рим, Италия</a></li>
					<li><a href="#">Астана, Казахстан</a></li>
					<li><a href="#">Пекин, Китай</a></li>
					<li><a href="#">Токио, Япония</a></li>
				</ul>
			</div>

			<div class="col-12 col-md-6 col-lg-3">
				<span class="modal__abc">У - Ч</span>
				<ul class="modal__city">
					<li><a href="#">Владимир</a></li>
					<li><a href="#">Ростов-на-Дону</a></li>
					<li><a href="#">Ереван, Армения</a></li>
					<li><a href="#">Тбилиси, Грузия</a></li>
					<li><a href="#">Сидней, Австралия</a></li>
					<li><a href="#">Казань</a></li>
					<li><a href="#">Мюнхен, Германия</a></li>
				</ul>
			</div>

			<div class="col-12 col-md-6 col-lg-3">
				<span class="modal__abc">Ш - Я</span>
				<ul class="modal__city">
					<li><a href="#">Махачкала</a></li>
					<li><a href="#">Барселона, Испания</a></li>
					<li><a href="#">Лиссабон, Португалия</a></li>
					<li><a href="#">Нью-Йорк, США</a></li>
					<li><a href="#">Хельсинки, Финляндия</a></li>
					<li><a href="#">Амстердам, Нидерланды</a></li>
					<li><a href="#">Кипр, Греция</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?$frame->end();?>