<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"CITY_QUESTION" => Array(
		"NAME" => "Строка с вопросом о филиале. Имя города заменяется по маске %CITY% (Например: Ближайший филиал в городе %CITY%?)",
		"TYPE" => "STRING",
	),
	"CITY_YES" => Array(
		"NAME" => "Утвердительный ответ",
		"TYPE" => "STRING",
	),
	"CITY_SELECT_CITY" => Array(
		"NAME" => "Отрицательный ответ, переход к выбору филиала",
		"TYPE" => "STRING",
	),
);

?>
