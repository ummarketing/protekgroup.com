<?
if ($arResult){
    //-------- Сжатие изображений ---------
    foreach ($arResult['PROPERTIES']['PHOTO_LIST']['VALUE'] as $prop_id => $prop_val){
        if (!empty($prop_val)){
            $img = CFile::ResizeImageGet($prop_val, array("width" => 360, "height" => 360), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 80 );
            $arResult['PHOTOS']['SRC_XS'][] = $img['src'];
            $arResult['PHOTOS']['SRC_LG'][] = CFile::GetPath($prop_val);
        }
    }
    //-------- /Сжатие изображений ---------
}
?>