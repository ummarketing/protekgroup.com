<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<h1 class="section__title">
    <?= $arResult['NAME']; ?>
</h1>



<div class="faq__detail-block">
    <div class="faq-item__answer">
        <?= $arResult['DETAIL_TEXT']; ?>
    </div>

    <? if (!empty($arResult['PHOTOS'])): ?>
        <div class="faq-answer__media">
            <div class="row">
                <? foreach ($arResult['PHOTOS']['SRC_XS'] as $photo_key => $photo_val): ?>
                    <div class="col-xs-6 col-sm-3">
                        <div class="faq-media__item">
                            <div class="faq-media__img"
                                 style="background-image: url(<?= $photo_val; ?>)"
                                 data-fancybox="gallery_<?= $arItem['ID']; ?>"
                                 href="<?= $arResult['PHOTOS']['SRC_LG'][$photo_key]; ?>"
                                 data-caption="<?= $arItem['NAME']; ?>">
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    <? endif ?>
</div>