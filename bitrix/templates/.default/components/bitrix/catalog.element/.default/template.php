<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */



$this->setFrameMode(true);
$APPLICATION->AddHeadScript('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$APPLICATION->AddHeadScript('//yastatic.net/share2/share.js');
ob_start();
$mainId = $this->GetEditAreaId($arResult['ID']);

$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$actualItem = $arResult;
$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;

$arMessages['ADD_TO_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arMessages['ADD_TO_CART'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arMessages['ADD_TO_FAVORITES'] = $arParams['~BUTTON_ADD_TO_FAVORITES'];
$arMessages['DELETE_FROM_COMPARE'] = $arParams['~BUTTON_DELETE_FROM_COMPARE'];
$arMessages['DELETE_FROM_FAVORITES'] = $arParams['~BUTTON_DELETE_FROM_FAVORITES'];

?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.min.css" rel="stylesheet" />

<script data-skip-moving="true">
var basketData = {
	basketTemplateUrl : "<?=$arResult['~ADD_URL_TEMPLATE']?>",
	quantity : "<?=$arResult['ORIGINAL_PARAMETERS']['PRODUCT_QUANTITY_VARIABLE']?>",
	compareAddTemplate : "<?=$arResult['~COMPARE_URL_TEMPLATE']?>",
	compareDeleteTemplate : "<?=$arResult['~COMPARE_DELETE_URL_TEMPLATE']?>",
	compareAddMessage : "<?=$arMessages['ADD_TO_COMPARE']?>",
	compareRemoveMessage : "<?=$arMessages['DELETE_FROM_COMPARE']?>",
	favoritesAddMessage : "<?=$arMessages['ADD_TO_FAVORITES']?>",
	favoritesRemoveMessage : "<?=$arMessages['DELETE_FROM_FAVORITES']?>",
	favoritesName : "<?=FAVORITES_NAME.SITE_ID?>"
};
</script>

<section class="section section--top0" id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">

    <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
        <meta itemprop="ratingValue" content="5">
        <meta itemprop="bestRating" content="5"/>
        <meta itemprop="worstRating" content="1"/>
        <meta itemprop="ratingCount" content="10">
        <meta itemprop="reviewCount" content="5">
    </div>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title" itemprop="name"><?=$name?></h1>
			</div>
		</div>

		<div class="row">
			<div class="col-12 col-lg-6 order-lg-1 col-xl-7">
                <?if(!empty($arResult['PROPERTIES']['LABEL']['VALUE'])){?>
                    <span class="category_item_lable"><img src="<?=$arResult['PROPERTIES']['LABEL']['VALUE']?>" /></span>
                <?}?>
				<div class="item-slider owl-carousel">
				<?foreach($arResult[$arResult['ORIGINAL_PARAMETERS']['ADD_PICT_PROP']] as $pick){?>
					<div class="item-slider__item">
						<?php
						$arWaterMark = Array(
							array(
								"name"        => "watermark",
								"position"    => "center",
								"type"        => "image",
								"size"        => "real",
								"file"        => WATERMARK_PATH,
								"fill"        => "exact",
								"alpha_level" => 26
							)
						);
						$arPhotoSmall = CFile::ResizeImageGet($pick['ID'], array('width'  => 670, 'height' => 446), BX_RESIZE_IMAGE_EXACT, false, $arWaterMark, 95);
						$arPhotoLg    = CFile::ResizeImageGet($pick['ID'], array('width'  => 1600, 'height' => 900), BX_RESIZE_IMAGE_PROPORTIONAL, false, $arWaterMark, 95); ?>
						<a href="<?= $arPhotoLg['src'] ?>" data-fancybox="gallery" rel="group" data-caption="<?= $name ?>">

							<img src="<?=$arPhotoSmall['src']?>" alt="<?=$alt?>" title="<?= $name ?>" itemprop="image">
						</a>
					</div>
				<?}?>
				<?if(!empty($arResult['PROPERTIES']['VIDEO']['VALUE'])){
					foreach($arResult['PROPERTIES']['VIDEO']['VALUE'] as $video){?>
					<div class="item-slider__item">
						<iframe src="<?=$video?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				<?	}
				}?>
				</div>
			</div>

			<div class="col-12 order-lg-3">
				<div class="item-thumbnails-wrap">
					<div class="item-thumbnails owl-carousel">
					<?foreach($arResult[$arResult['ORIGINAL_PARAMETERS']['ADD_PICT_PROP']] as $pick){?>
						<div class="item-thumbnails__item">
							<img src="<?
							$arWaterMark = Array(
								array(
									"name" => "watermark",
									"position" => "center",
									"type" => "image",
									"size" => "medium",
									"file" => WATERMARK_PATH,
									"fill" => "exact",
									"alpha_level" => 26
								)
							);
							$arPhotoSmall = CFile::ResizeImageGet(
								$pick['ID'],
								array(
									'width'=>420,
									'height'=>320
								),
								BX_RESIZE_IMAGE_EXACT,
								false,
								$arWaterMark,
								75
							);
							echo $arPhotoSmall['src'];?>" alt="<?=$alt?>" title="<?= $name; ?>">
						</div>
					<?}?>
					<?if(!empty($arResult['PROPERTIES']['VIDEO']['VALUE'])){
						foreach($arResult['PROPERTIES']['VIDEO']['VALUE'] as $video){?>
						<div class="item-thumbnails__item">
							<iframe src="<?=$video?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					<?	}
					}?>
					</div>
					<?if($arResult['MORE_PHOTO_COUNT'] > 1){?>
					<button class="item-thumbnails-nav__btn item-thumbnails-nav__btn--prev" type="button">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
					</button>
					<button class="item-thumbnails-nav__btn item-thumbnails-nav__btn--next" type="button">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
					</button>
					<?}?>
				</div>
			</div>

			<div class="col-12 col-lg-6 order-lg-2 col-xl-5">
				<div class="item cyberItemContainer">
					<div class="item__title"><?=$arParams['ITEM_INFO']?></div>

					<ul class="item__links item__links--mob">
						<li><a href="#collapse-item1"><?=$arParams['ITEM_INFO_PROPS']?></a></li>
						<li><a href="#collapse-item4"><?=$arParams['ITEM_INFO_DELIVERY']?></a></li>
						<?if(!empty($arResult['PREVIEW_TEXT'])){?><li><a href="#collapse-item2"><?=$arParams['ITEM_INFO_DESCR']?></a></li><?}?>
						<?if(!empty($arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT'])){?><li><a href="#collapse-item6"><?=$arParams['ITEM_INFO_INSTRUCTION']?></a></li><?}?>
						<?if(!empty($arParams['REVIEW_IBLOCK_ID'])){?><li><a href="#collapse-item7"><?=$arParams['ITEM_INFO_REVIEWS']?></a></li><?}?>
					</ul>

					<ul class="item__links item__links--desk">
						<li><a href="#tab1"><?=$arParams['ITEM_INFO_PROPS']?></a></li>
						<li><a href="#tab4"><?=$arParams['ITEM_INFO_DELIVERY']?></a></li>
						<?if(!empty($arResult['PREVIEW_TEXT'])){?><li><a href="#tab2"><?=$arParams['ITEM_INFO_DESCR']?></a></li><?}?>
						<?if(!empty($arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT'])){?><li><a href="#tab6"><?=$arParams['ITEM_INFO_INSTRUCTION']?></a></li><?}?>
						<?if(!empty($arParams['REVIEW_IBLOCK_ID'])){?><li><a href="#tab7"><?=$arParams['ITEM_INFO_REVIEWS']?></a></li><?}?>
					</ul>
					<?if(!empty($arResult['PRICES'])){?>
					<div class="item__quantity">
						<span><?=$arParams['BOX_QUANTITY']?></span>

						<div class="cart__item-quantity">
							<button type="button" class="quantity-control minus">-</button>
							<input type="text" class="quantity" value="1" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>">
							<button type="button" class="quantity-control plus">+</button>
						</div>
					</div>

					<?if($arResult['PROPERTIES']['MIN_BATCH']['~VALUE']['TEXT']){?>
					<div class="item__min-batch">* Минимальная партия: <?= $arResult['PROPERTIES']['MIN_BATCH']['VALUE'] ?></div>
					<?}?>
					<?}?>
					<div class="item__action">
						<?if(!empty($arResult['PRICES'])){?><button class="item__add add2basket_btn catalog-item__btn catitem__add" data-cyber_id="<?=$arResult['ID']?>" type="button" onclick="yaCounter30699088.reachGoal('goal14'); return true;">
                            <span data-cart="add_to_cart" class="active">
                                <?=$arMessages['ADD_TO_CART']?>
                            </span>
                            <span data-cart="in_cart" class="btn_in_cart">
                                <img src="/local/assets/img/check_icon.svg" alt=""> В корзине
                            </span>
                            </button>
                        <?}?>
						<?if(!empty($_SESSION[FAVORITES_NAME.SITE_ID][$arResult['ID']])){?>
							<button class="item__favorites in-favorites add2favorites_btn" data-cyber_id="<?=$arResult['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path></svg><span><?=$arMessages['DELETE_FROM_FAVORITES']?></span></button>
						<?}else{?>
							<button class="item__favorites add2favorites_btn" data-cyber_id="<?=$arResult['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path></svg><span><?=$arMessages['ADD_TO_FAVORITES']?></span></button>
						<?}?>
						<?if(!empty($_SESSION[$arResult['ORIGINAL_PARAMETERS']['COMPARE_NAME']][$arResult['ORIGINAL_PARAMETERS']['IBLOCK_ID']]['ITEMS'][$arResult['ID']])){?>
							<button class="item__compare add2compare_btn in-compare" data-cyber_id="<?=$arResult['ID']?>" type="button" onclick="yaCounter30699088.reachGoal('goal15'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve"><path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path><path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path><path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path><path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path><path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path></svg><span><?=$arMessages['DELETE_FROM_COMPARE']?></span></button>
						<?}else{?>
						<button class="item__compare add2compare_btn" data-cyber_id="<?=$arResult['ID']?>" type="button" onclick="yaCounter30699088.reachGoal('goal15'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve"><path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path><path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path><path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path><path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path><path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path></svg><span><?=$arMessages['ADD_TO_COMPARE']?></span></button>
						<?}?>
						<?if(!empty($arResult['PROPERTIES']['DOWNLOAD_PRISE']['VALUE'])){?>
						<a class="item__download" href="<?=CFile::GetPath($arResult['PROPERTIES']['DOWNLOAD_PRISE']['VALUE'])?>" download><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path d="M498.966,339.946c-7.197,0-13.034,5.837-13.034,13.034v49.804c0,28.747-23.388,52.135-52.135,52.135H78.203 c-28.747,0-52.135-23.388-52.135-52.135V352.98c0-7.197-5.835-13.034-13.034-13.034C5.835,339.946,0,345.782,0,352.98v49.804 c0,43.121,35.082,78.203,78.203,78.203h355.594c43.121,0,78.203-35.082,78.203-78.203V352.98 C512,345.782,506.165,339.946,498.966,339.946z"></path><path d="M419.833,391.3H92.167c-7.197,0-13.034,5.837-13.034,13.034s5.835,13.034,13.034,13.034h327.665 c7.199,0,13.034-5.835,13.034-13.034C432.866,397.137,427.031,391.3,419.833,391.3z"></path><path d="M387.919,207.93c-4.795-5.367-13.034-5.834-18.404-1.038l-100.482,89.765V44.048c0-7.197-5.835-13.034-13.034-13.034 c-7.197,0-13.034,5.835-13.034,13.034v252.609l-100.482-89.764c-5.367-4.796-13.607-4.328-18.404,1.038 c-4.794,5.369-4.331,13.609,1.037,18.404l109.174,97.527c6.187,5.529,13.946,8.292,21.708,8.292 c7.759,0,15.519-2.763,21.708-8.289l109.174-97.53C392.25,221.537,392.714,213.297,387.919,207.93z"></path></svg>Скачать коммерческое предложение</a>
						<?}?>
						<?if (SITE_ID=="s1"): ?>
    						<a href="<?if ($arResult['PROPERTIES']['LINK_SHOP']['VALUE']): ?><?=$arResult['PROPERTIES']['LINK_SHOP']['VALUE'];?><?else:?>https://protekgroup.shop/catalog/<?endif;?>" class="link-shop">Купить в интернет магазине от 1 упаковки</a>
						<?endif ?>			
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="item-accordion" id="accordion">
					<div class="card">
						<button type="button" data-toggle="collapse" data-target="#collapse-item1" aria-expanded="true" aria-controls="collapse-item1"><?=$arParams['ITEM_INFO_PROPS']?></button>

						<div id="collapse-item1" class="collapse show" data-parent="#accordion">
							<div class="item-container">
								<div class="row">
									<?$count = ceil(count($arResult['DISPLAY_PROPERTIES']) / 2);
									foreach(array_chunk($arResult['DISPLAY_PROPERTIES'], $count) as $arProps){?>
									<div class="col-12 col-lg-6">
										<ul class="item-list">
											<?foreach($arProps as $arProp){
												if($arProp['CODE'] == 'COLOR'){?>
												<li class="colors">
													<span><?=$arProp['NAME']?></span>
													<span>
														<?foreach($arProp['COLORS_VALUE'] as $color){?>
														<div class="color" title="<?=$color['NAME']?>" style="<?=(!empty($color['FILE']) ? "background-image:url('" . $color['FILE'] . "')" : "background:" . $color['COLOR'])?>"></div>
														<?}?>
													</span>
												</li>
												<?}elseif($arProp['PROPERTY_TYPE'] == "G"){?>
												<li class="prop_row">
													<span><?=$arProp['NAME']?></span>
													<span><?=(is_array($arProp['DISPLAY_VALUE']) ? implode("", $arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE'])?></span>
												</li>
												<?}else{?>
											<li>
												<span><?=$arProp['NAME']?></span>
												<span><?=(is_array($arProp['DISPLAY_VALUE']) ? implode(", ", $arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE'])?></span>
											</li>
												<?}
											}?>
										</ul>
									</div>
									<?}?>
								</div>
							</div>
							<?if(isset($arResult['PACK']) || isset($arResult['PACK_TYPE'])){?>
							<div class="item-container">
								<div class="row">
									<?if(isset($arResult['PACK'])){?>
									<div class="col-12 col-lg-6">
										<span class="item-title"><?=$arParams['ITEMS_PACK_PROPS']?>:</span>
										<ul class="item-list">
											<?foreach($arResult['PACK'] as $prop){?>
												<li>
													<span><?=$prop['NAME']?></span>
													<span><?=$prop['DISPLAY_VALUE']?></span>
												</li>
											<?}?>
										</ul>
									</div>
									<?}
									if(isset($arResult['PACK_TYPE'])){?>
									<div class="col-12 col-lg-6">
										<span class="item-title"><?=$arParams['ITEMS_PACK_VOLUME']?>:</span>
										<ul class="item-list">
											<li>
												<span><?=$arParams['~ITEMS_PACK_VOLUME_TEXT']?></span>
												<span><?=$arResult['PACK_TYPE']['UF_LENGTH'] . "x" . $arResult['PACK_TYPE']['UF_WIDTH'] . "x" . $arResult['PACK_TYPE']['UF_HEIGHT']?></span>
											</li>
										</ul>
									</div>
									<?}?>
									<div class="col-12">
										<span class="item-title"><?=$arParams['ITEMS_SHARE']?>:</span>

										<div class="item-social">
											<div class="ya-share2" data-services="vkontakte,facebook,twitter,telegram"></div>
										</div>
									</div>
								</div>
							</div>
							<?}?>
						</div>
					</div>
					<?if(!empty($arResult['PREVIEW_TEXT'])){?>
					<div class="card">
						<button type="button" data-toggle="collapse" data-target="#collapse-item2" aria-expanded="false" aria-controls="collapse-item2"><?=$arParams['ITEM_INFO_DESCR']?></button>

						<div id="collapse-item2" class="collapse" data-parent="#accordion">
							<div class="item-container news_article_container"><?=$arResult['PREVIEW_TEXT']?></div>
						</div>
					</div>
					<?}?>
					<div class="card">
						<button type="button" data-toggle="collapse" data-target="#collapse-item4" aria-expanded="false" aria-controls="collapse-item4"><?=$arParams['ITEM_INFO_DELIVERY']?></button>

						<div id="collapse-item4" class="collapse" data-parent="#accordion">
							<div class="item-container news_article_container"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/catalog/delivery.php"), $component);?></div>
						</div>
					</div>

					<?if(!empty($arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT'])){?>
					<div class="card">
						<button type="button" data-toggle="collapse" data-target="#collapse-item6" aria-expanded="false" aria-controls="collapse-item6"><?=$arParams['ITEM_INFO_INSTRUCTION']?></button>

						<div id="collapse-item6" class="collapse" data-parent="#accordion">
							<div class="item-container news_article_container"><?=$arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT']?></div>
						</div>
					</div>
					<?}?>
					<?if(!empty($arParams['REVIEW_IBLOCK_ID'])){?>
					<div class="card">
						<button type="button" data-toggle="collapse" data-target="#collapse-item7" aria-expanded="false" aria-controls="collapse-item7"><?=$arParams['ITEM_INFO_REVIEWS']?></button>

						<div id="collapse-item7" class="collapse" data-parent="#accordion">
							<div class="item-container news_article_container">
							<?if(!empty($arResult['REVIEWS'])){?>
								<div class="reviews_list">
								<?foreach($arResult['REVIEWS'] as $arReview){
									?><div class="review">
										<span class="review_name"><?=$arReview['NAME']?></span>
										<blockquote class="review_text"><?=$arReview['PREVIEW_TEXT']?></blockquote>
										<?if(!empty($arReview['DETAIL_TEXT'])){?>
											<span class="review_name answer"><?=SITE_NAME?></span>
											<blockquote class="review_answer"><?=$arReview['DETAIL_TEXT']?></blockquote>
										<?}?>
									</div><?
								}?>
								</div>
							<?}?>
								<div class="review_form_wrap"></div>
							</div>
						</div>
					</div>
					<?}?>
                    <div class="card">
                        <button type="button" data-toggle="collapse" data-target="#collapse-item8" aria-expanded="false" aria-controls="collapse-item8">Сертификаты</button>

                        <div id="collapse-item8" class="collapse" data-parent="#accordion">
                            <div class="item-container news_article_container"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/catalog/certificates_mobile.php"), $component);?></div>
                        </div>
                    </div>
				</div>

				<div class="item-tabs" id="item-tabs">
					<ul class="nav section__nav section__nav--item" role="tablist">
						<li>
							<a class="active" id="tab-1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="true"><?=$arParams['ITEM_INFO_PROPS']?></a>
						</li>
						<?if(!empty($arResult['PREVIEW_TEXT'])){?>
						<li>
							<a class="" id="tab-2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="false"><?=$arParams['ITEM_INFO_DESCR']?></a>
						</li>
						<?}?>
						<li>
							<a class="" id="tab-4" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="false"><?=$arParams['ITEM_INFO_DELIVERY']?></a>
						</li>
						<?if(!empty($arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT'])){?>
						<li>
							<a class="" id="tab-6" data-toggle="tab" href="#tab6" role="tab" aria-controls="tab6" aria-selected="false"><?=$arParams['ITEM_INFO_INSTRUCTION']?></a>
						</li>
						<?}?>
						<?if(!empty($arParams['REVIEW_IBLOCK_ID'])){?>
						<li>
							<a class="" id="tab-7" data-toggle="tab" href="#tab7" role="tab" aria-controls="tab7" aria-selected="false"><?=$arParams['ITEM_INFO_REVIEWS']?></a>
						</li>
						<?}?>
                        <li>
                            <a class="" id="tab-8" data-toggle="tab" href="#tab8" role="tab" aria-controls="tab8" aria-selected="false">Сертификаты</a>
                        </li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab-1">
							<div class="item-container title-char">
								<h2><?=Loc::getMessage('CT_BCE_CHARACTERISTICS')?>: <?=$arResult['NAME']?></h2>
							</div>
							<div class="item-container">
								<div class="row">
								<?$count = ceil(count($arResult['DISPLAY_PROPERTIES']) / 2);
									foreach(array_chunk($arResult['DISPLAY_PROPERTIES'], $count) as $arProps){?>
									<div class="col-12 col-lg-6">
										<ul class="item-list">
											<?foreach($arProps as $arProp){
												if($arProp['CODE'] == 'COLOR'){?>
												<li class="colors">
													<span><?=$arProp['NAME']?></span>
													<span>
														<?foreach($arProp['COLORS_VALUE'] as $color){?>
														<div class="color" title="<?=$color['NAME']?>" style="<?=(!empty($color['FILE']) ? "background-image:url('" . $color['FILE'] . "')" : "background:" . $color['COLOR'])?>"></div>
														<?}?>
													</span>
												</li>
												<?}elseif($arProp['PROPERTY_TYPE'] == "G"){?>
												<li class="prop_row">
													<span><?=$arProp['NAME']?></span>
													<span><?=(is_array($arProp['DISPLAY_VALUE']) ? implode("", $arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE'])?></span>
												</li>
												<?}else{?>
											<li>
												<span><?=$arProp['NAME']?></span>
												<span><?=(is_array($arProp['DISPLAY_VALUE']) ? implode(", ", $arProp['DISPLAY_VALUE']) : $arProp['DISPLAY_VALUE'])?></span>
											</li>
												<?}
											}?>
										</ul>
									</div>
									<?}?>
								</div>
							</div>
							<?if(isset($arResult['PACK']) || isset($arResult['PACK_TYPE'])){?>
							<div class="item-container">
								<div class="row">
									<?if(isset($arResult['PACK'])){?>
									<div class="col-12 col-lg-6">
										<span class="item-title"><?=$arParams['ITEMS_PACK_PROPS']?>:</span>
										<ul class="item-list">
											<?foreach($arResult['PACK'] as $prop){?>
												<li>
													<span><?=$prop['NAME']?></span>
													<span><?=$prop['DISPLAY_VALUE']?></span>
												</li>
											<?}?>
										</ul>
									</div>
									<?}
									if(isset($arResult['PACK_TYPE'])){?>
									<div class="col-12 col-lg-6">
										<span class="item-title"><?=$arParams['ITEMS_PACK_VOLUME']?>:</span>
										<ul class="item-list">
											<li>
												<span><?=$arParams['~ITEMS_PACK_VOLUME_TEXT']?></span>
												<span><?=$arResult['PACK_TYPE']['UF_LENGTH'] . "x" . $arResult['PACK_TYPE']['UF_WIDTH'] . "x" . $arResult['PACK_TYPE']['UF_HEIGHT']?></span>
											</li>
										</ul>
									</div>
									<?}?>
									<div class="col-12">
										<span class="item-title"><?=$arParams['ITEMS_SHARE']?>:</span>

										<div class="item-social">
											<div class="ya-share2" data-services="vkontakte,facebook,twitter,telegram"></div>
										</div>
									</div>
								</div>
							</div>
							<?}?>
						</div>
						<?if(!empty($arResult['PREVIEW_TEXT'])){?>
						<div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab-2">
							<div class="item-container news_article_container" itemprop="description">
                                <?=$arResult['PREVIEW_TEXT']?>
                            </div>
						</div>
						<?}?>

						<div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="tab-4">
							<div class="item-container news_article_container">
								<!--noindex-->
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/catalog/delivery.php"), $component);?>
								<!--/noindex-->
							</div>
						</div>
						<?if(!empty($arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT'])){?>
						<div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="tab-6">
							<div class="item-container news_article_container"><?=$arResult['PROPERTIES']['INSTRUCTION']['~VALUE']['TEXT']?></div>
						</div>
						<?}?>
						<?if(!empty($arParams['REVIEW_IBLOCK_ID'])){?>
						<div class="tab-pane fade" id="tab7" role="tabpanel" aria-labelledby="tab-7">
							<div class="item-container news_article_container">
							<?if(!empty($arResult['REVIEWS'])){?>
								<div class="reviews_list">
								<?foreach($arResult['REVIEWS'] as $arReview){
									?><div class="review">
										<span class="review_name"><?=$arReview['NAME']?></span>
										<blockquote class="review_text"><?=$arReview['PREVIEW_TEXT']?></blockquote>
										<?if(!empty($arReview['DETAIL_TEXT'])){?>
											<span class="review_name answer"><?=SITE_NAME?></span>
											<blockquote class="review_answer"><?=$arReview['DETAIL_TEXT']?></blockquote>
										<?}?>
									</div><?
								}?>
								</div>
							<?}?><div class="review_form_wrap">#REVIEWS_CONTAINER#</div></div>
						</div>
						<?}?>
                        <div class="tab-pane fade" id="tab8" role="tabpanel" aria-labelledby="tab-8">
                            <div class="item-container news_article_container">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/catalog/certificates.php"), $component);?>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?if($arResult['RELEVANT_ITEMS']){?>
<section class="section section--grey">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title2"><?=$arParams['ITEMS_RELEVANT']?></h2>
			</div>

			<div class="col-12">
				<div class="products-slider owl-carousel">
					<?foreach($arResult['RELEVANT_ITEMS'] as $arItem){?>
					<div class="product">
						<a class="product__img" href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?
						$arWaterMark = Array(
							array(
								"name" => "watermark",
								"position" => "center",
								"type" => "image",
								"size" => "medium",
								"file" => WATERMARK_PATH,
								"fill" => "exact",
								"alpha_level" => 26
							)
						);
						$arPhotoSmall = CFile::ResizeImageGet(
							$arItem['~PREVIEW_PICTURE'],
							array(
								'width'=>660,
								'height'=>440
							),
							BX_RESIZE_IMAGE_EXACT,
							false,
							$arWaterMark,
							80
						);
						echo $arPhotoSmall['src'];?>" alt="<?=$arItem['NAME']?>" title="<?= $arItem['NAME']; ?>"></a>
						<div class="product__content">
							<span class="product__title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></span>
						</div>
					</div>
					<?}?>
				</div>
				<?if(count($arResult['RELEVANT_ITEMS']) > 2){?>
				<div class="products-nav">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<button class="products-nav__btn products-nav__btn--prev" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
								</button>
								<button class="products-nav__btn products-nav__btn--next" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
								</button>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</section>
<?}?>
<?if($arResult['SIMILAR_ITEMS']){?>
<section class="section <?=(!empty($arResult['DETAIL_TEXT']) ? "section--bottom0" : "")?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title2"><?=$arParams['ITEMS_SAME']?></h2>
			</div>

			<div class="col-12">
				<div class="products-slider owl-carousel">
					<?foreach($arResult['SIMILAR_ITEMS'] as $arItem){?>
					<div class="product">
						<a class="product__img" href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?
						$arWaterMark = Array(
							array(
								"name" => "watermark",
								"position" => "center",
								"type" => "image",
								"size" => "medium",
								"file" => WATERMARK_PATH,
								"fill" => "exact",
								"alpha_level" => 26
							)
						);
						$arPhotoSmall = CFile::ResizeImageGet(
							$arItem['~PREVIEW_PICTURE'],
							array(
								'width'=>660,
								'height'=>440
							),
							BX_RESIZE_IMAGE_EXACT,
							false,
							$arWaterMark,
							80
						);
						echo $arPhotoSmall['src'];?>" alt="<?=$arItem['NAME']?>" title="<?= $arItem['NAME']; ?>"></a>
						<div class="product__content">
							<span class="product__title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></span>
						</div>
					</div>
					<?}?>
				</div>
				<?if(count($arResult['SIMILAR_ITEMS']) > 2){?>
				<div class="products-nav">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<button class="products-nav__btn products-nav__btn--prev" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
								</button>
								<button class="products-nav__btn products-nav__btn--next" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
								</button>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</section>
<?}?>
<?if(!empty($arResult['DETAIL_TEXT'])){?>
<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="news_article_container">
					<?=$arResult['DETAIL_TEXT']?>
				</div>
			</div>
		</div>
	</div>
</section>
<?}?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js"></script>

<?
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean()?>