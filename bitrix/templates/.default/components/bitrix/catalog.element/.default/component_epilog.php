<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;

$APPLICATION->SetPageProperty("no_sticky_menu", "Y"); //SWITCH OFF STICKY MENU

$GLOBALS["PARAMS"] = $arParams;
$GLOBALS["REVIEW_ID"] = $arResult["ID"];

echo preg_replace_callback(
	"/#REVIEWS_CONTAINER#/is".BX_UTF_PCRE_MODIFIER,
	function(){
		
		ob_start();
		$GLOBALS["APPLICATION"]->IncludeComponent(
			"cyberrex:iblock.element.add.form", 
			".default", 
			array(
				"IBLOCK_ID" => $GLOBALS["PARAMS"]['REVIEW_IBLOCK_ID'],
				"IBLOCK_TYPE" => "rewiews",
				"USER_MESSAGE_ADD" => $GLOBALS["PARAMS"]['REVIEW_ADD_MESSAGE'],
				"BUTTON_TEXT" => $GLOBALS["PARAMS"]['REVIEW_ADD_BUTTON'],
				"USE_CAPTCHA" => "Y",
				"REVIEWED_ELEMENT" => $GLOBALS["REVIEW_ID"],
				"NAME_CAPTION" => $GLOBALS["PARAMS"]['REVIEW_NAME'],
				"TEXT_CAPTION" => $GLOBALS["PARAMS"]['REVIEW_TEXT'],
				"REQUIRED_NOTE" => $GLOBALS["PARAMS"]['REVIEW_REQUIRED'],
			)
		);
		$retrunStr = @ob_get_contents();
		ob_get_clean();
		return $retrunStr;
	}, 
	$arResult["CACHED_TPL"]
);
unset($GLOBALS["PARAMS"], $GLOBALS["REVIEW_ID"]);
?>