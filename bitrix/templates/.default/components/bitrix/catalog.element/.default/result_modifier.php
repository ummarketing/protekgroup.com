<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

if($arResult['DISPLAY_PROPERTIES']['PACK_TYPE']){
	$hlblock = HL\HighloadBlockTable::getById(9)->fetch();
	   $entity = HL\HighloadBlockTable::compileEntity($hlblock);
	   $entityClass = $entity->getDataClass();

	   $res = $entityClass::getList(array(
		   'select' => array('*'),
		   'filter' => array('UF_XML_ID' => $arResult['DISPLAY_PROPERTIES']['PACK_TYPE']['VALUE'])
	   ));

	$row = $res->fetch();
	$arResult['PACK_TYPE'] = $row;
	unset($arResult['DISPLAY_PROPERTIES']['PACK_TYPE'], $arResult['DISPLAY_PROPERTIES']['PACK_VOLUME']);
}


if($arResult['DISPLAY_PROPERTIES']['COLOR']){
	$rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>$arResult['DISPLAY_PROPERTIES']['COLOR']['USER_TYPE_SETTINGS']['TABLE_NAME'])));
	if(($hldata = $rsData->fetch())){
		$hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
		$hlDataClass = $hldata['NAME'].'Table';
	}

	$res = $hlDataClass::getList(array(
	   'select' => array('*'),
	   'filter' => array('UF_XML_ID' => $arResult['DISPLAY_PROPERTIES']['COLOR']['VALUE'])
   ));
	while($row = $res->fetch()){
		$arResult['DISPLAY_PROPERTIES']['COLOR']['COLORS_VALUE'][] = array(
			'COLOR' => $row['UF_COLOR'],
			'NAME' => $row['UF_NAME'],
			'FILE' => CFile::GetPath($row['UF_FILE'])
		);
	}
}
if(!empty($arResult['PROPERTIES']['RELEVANT_ITEMS']['VALUE'])){
	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $arResult['PROPERTIES']['RELEVANT_ITEMS']['VALUE']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arResult['RELEVANT_ITEMS'][] = $arFields;
	}
	unset($arResult['PROPERTIES']['RELEVANT_ITEMS'], $arResult['DISPLAY_PROPERTIES']['RELEVANT_ITEMS']);
}
if(!empty($arResult['PROPERTIES']['SIMILAR_ITEMS']['VALUE'])){
	$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "ID" => $arResult['PROPERTIES']['SIMILAR_ITEMS']['VALUE']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arResult['SIMILAR_ITEMS'][] = $arFields;
	}
	unset($arResult['PROPERTIES']['SIMILAR_ITEMS'], $arResult['DISPLAY_PROPERTIES']['SIMILAR_ITEMS']);
}
foreach($arResult['DISPLAY_PROPERTIES'] as $key => $prop){
	if(substr($key, 0, 5) == "PACK_" && !empty($prop['DISPLAY_VALUE'])){
		$arResult['PACK'][substr($key, 5)] = $prop;
		unset($arResult['DISPLAY_PROPERTIES'][$key]);
	}
	
}

$arTempMorePhotos = array();
foreach($arResult['MORE_PHOTO'] as $photo){
	$arTempMorePhotos[$photo['ID']] = $photo;
}
$arResult['MORE_PHOTO'] = $arTempMorePhotos;

if(!empty($arParams['REVIEW_IBLOCK_ID'])){
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_TEXT", "PREVIEW_TEXT");
	$arFilter = Array("IBLOCK_ID"=>IntVal($arParams['REVIEW_IBLOCK_ID']), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_REVIEWED_ELEMENT" => $arResult['ID']);
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()){
		$arResult["REVIEWS"][] = $ob->GetFields();
	}
}

if (!empty($arResult['PROPERTIES']['LABEL']['VALUE'])) {
    $rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>$arResult['PROPERTIES']['LABEL']['USER_TYPE_SETTINGS']['TABLE_NAME'])));
    if(($hldata = $rsData->fetch())){
        $hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
        $hlDataClass = $hldata['NAME'].'Table';
    }


    $res = $hlDataClass::getList(array(
        'filter' => array(
            'UF_XML_ID' => $arResult['PROPERTIES']['LABEL']['VALUE'],
        ),
        'select' => array("*"),
    ));

    while($row = $res->fetch()){
        $arResult['PROPERTIES']['LABEL']['VALUE'] = CFile::GetPath($row['UF_FILE']);
    }
}

$this->__component->SetResultCacheKeys(array("CACHED_TPL"));