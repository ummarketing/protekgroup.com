$(document).ready(function(){
	/*==============================
	Item
	==============================*/
	$('.item__links--mob a').on('click', function(e) {
		e.preventDefault();
		var target = $('[data-toggle="collapse"][data-target="' + this.hash + '"]');
		target.trigger('click');

		$('html, body').animate({
			scrollTop: $('#accordion').offset().top -20
		}, 500);

	});

	$('.item__links--desk a').on('click', function(e) {
		e.preventDefault();
		var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
		target.trigger('click');

		$('html, body').animate({
			scrollTop: $('#item-tabs').offset().top -200
		}, 500);
	});

	/*==============================
	Thumbnail slider
	==============================*/
	var sync1 = $(".item-slider");
	var sync2 = $(".item-thumbnails");
	var syncedSecondary = true;
	sync1.owlCarousel({
		mouseDrag: false,
		touchDrag: false,
		dots: false,
		loop: false,
		autoplay: false,
		smartSpeed: 600,
		margin: 30,
		items: 1,
	}).on('changed.owl.carousel', syncPosition);

	sync2.on('initialized.owl.carousel', function() {
		sync2.find(".owl-item").eq(0).addClass("current");
	}).owlCarousel({
		mouseDrag: false,
		dots: true,
		autoplay: false,
		smartSpeed: 600,
		loop: false,
		margin: 10,
		items: 2,
		slideBy: 1,
		responsive : {
			768 : {
				items: 4,
				margin: 30,
			},
			1200 : {
				items: 4,
				margin: 30,
				slideBy: 1,
				dots: false,
			},
		}
	}).on('changed.owl.carousel', syncPosition2);

	function syncPosition(el) {
		//if you set loop to false, you have to restore this next line
		 var current = el.item.index;

		// if you disable loop you have to comment this block
		//var count = el.item.count - 1;
		//var current = Math.round(el.item.index - (el.item.count / 2) - .5);

		//if (current < 0) {
		//    current = count;
		//}
		//if (current > count) {
		//    current = 0;
		//}

		//end block

		sync2
		    .find(".owl-item")
		    .removeClass("current")
		    .eq(current)
		    .addClass("current");
		var onscreen = sync2.find('.owl-item.active').length - 1;
		var start = sync2.find('.owl-item.active').first().index();
		var end = sync2.find('.owl-item.active').last().index();

		if (current > end) {
		    sync2.data('owl.carousel').to(current, 100, true);
		}
		if (current < start) {
		    sync2.data('owl.carousel').to(current - onscreen, 100, true);
		}
	}

	function syncPosition2(el) {
		if (syncedSecondary) {
			var number = el.item.index;
			sync1.data('owl.carousel').to(number, 100, true);
		}
	}

	sync2.on("click", ".owl-item", function(e) {
		e.preventDefault();
		var number = $(this).index();
		sync1.data('owl.carousel').to(number, 300, true);
	});

	$('.item-thumbnails-nav__btn--next').on('click', function() {
		sync1.trigger('next.owl.carousel');
		sync2.trigger('next.owl.carousel');
	});
	$('.item-thumbnails-nav__btn--prev').on('click', function() {
		sync1.trigger('prev.owl.carousel');
		sync2.trigger('prev.owl.carousel');
	});
	$('.products-slider').each(function(){
		$(this).owlCarousel({
			mouseDrag: $(this).find('.product').length > 2,
			touchDrag: $(this).find('.product').length > 2,
			dots: true,
			loop: $(this).find('.product').length > 2,
			autoplay: true,
			smartSpeed: 600,
			margin: 30,
			autoplayTimeout: 5000,
			autoplayHoverPause: true,
			responsive : {
				0 : {
					items: 1,
				},
				768 : {
					items: 2,
				},
				992 : {
					items: 3,
				},
				1200 : {
					items: 3,
					dots: false,
				},
			}
		});	
	});
	
	$('.products-nav__btn--next').on('click', function() {
		let slider = $(this).parents('.products-nav').siblings('.products-slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('next.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	$('.products-nav__btn--prev').on('click', function() {
		let slider = $(this).parents('.products-nav').siblings('.products-slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('prev.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	
	if (window.innerWidth < 1200) {
		$('#collapse-item7 .review_form_wrap').html($('#tab7 .review_form_wrap').html());
		$('#tab7 .review_form_wrap').html("");
	}
	$(window).resize(function() {
		if (window.innerWidth < 1200) {
			if(!$('#collapse-item7 .review_form_wrap').html()){
				$('#collapse-item7 .review_form_wrap').html($('#tab7 .review_form_wrap').html());
				$('#tab7 .review_form_wrap').html("");
			}
		}else{
			if(!$('#tab7 .review_form_wrap').html()){
				$('#tab7 .review_form_wrap').html($('#collapse-item7 .review_form_wrap').html());
				$('#collapse-item7 .review_form_wrap').html("");
			}
		}
	});

	lightbox.option({
		'alwaysShowNavOnTouchDevices': true,
		'albumLabel': '%1 / %2',
		'resizeDuration': 200
	})
});