<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>

<?
$previousLevel = 0;
$linknumber = 0;
$container = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat('<button class="nav__roll" type="button" data-toggle="collapse" href="#menu_' . $container . '" aria-expanded="false">'.($arParams["MENU_SHOW_LESS"] ? $arParams["MENU_SHOW_LESS"] : "Свернуть").'</button></div>', ($previousLevel - $arItem["DEPTH_LEVEL"]));
		$container = 0;?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):
		$container = $linknumber;?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<?if ($arItem["SELECTED"]){?>
			<a class="nav__mob-link" data-toggle="collapse" href="#menu_<?=$linknumber?>" role="button" aria-expanded="true"><?=$arItem["TEXT"]?> <span></span><span></span></a>
				<div class="nav__collapse collapse show" id="menu_<?=$linknumber?>">
					<a class="nav__sublink" href="<?=$arItem["LINK"]?>"><img style="width:26px; height:26px" src="<?=(!empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><?=$arItem["TEXT"]?></a>
			<?}else{?>
			<a class="nav__mob-link" data-toggle="collapse" href="#menu_<?=$linknumber?>" role="button" aria-expanded="false"><?=$arItem["TEXT"]?> <span></span><span></span></a>
				<div class="nav__collapse collapse" id="menu_<?=$linknumber?>">
					<a class="nav__sublink" href="<?=$arItem["LINK"]?>"><img style="width:26px; height:26px" src="<?=(!empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><?=$arItem["TEXT"]?></a>
			<?}?>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<a class="nav__mob-link" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
			<?else:?>
				<a class="nav__sublink" href="<?=$arItem["LINK"]?>"><img style="width:26px; height:26px" src="<?=(!empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><?=$arItem["TEXT"]?></a>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?$linknumber++;
endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div>", ($previousLevel-1) );?>
<?endif?>

<?endif?>