<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<div id="modal-map" class="zoom-anim-dialog mfp-hide modal">
	<div class="modal__content">
		<div class="modal__map">
<?
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat('</div>', ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<?if ($arItem["SELECTED"] && $arItem["LINK"] == $APPLICATION->GetCurPage()){?>
			<span class="modal__link selected"><?=$arItem["TEXT"]?></span>
				<div class="modal__list">
			<?}else{?>
			<a href="<?=$arItem["LINK"]?>" class="modal__link"><?=$arItem["TEXT"]?></a>
				<div class="modal__list">
			<?}?>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<?if ($arItem["SELECTED"] && $arItem["LINK"] == $APPLICATION->GetCurPage()){?>
				<span class="modal__link selected"><?=$arItem["TEXT"]?></span>
				<?}else{?>
				<a href="<?=$arItem["LINK"]?>" class="modal__link"><?=$arItem["TEXT"]?></a>
				<?}?>
			<?else:?>
				<?if ($arItem["SELECTED"] && $arItem["LINK"] == $APPLICATION->GetCurPage()){?>
				<span class="selected">• <?=$arItem["TEXT"]?></span>
				<?}else{?>
				<a href="<?=$arItem["LINK"]?>">• <?=$arItem["TEXT"]?></a>
				<?}?>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</div>", ($previousLevel-1) );?>
<?endif?>
		</div>
	</div>
</div>
<?endif?>