<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<div class="nav__desk">

<?
$count = 0;
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
	if($count > 0){
		?><span></span><?
	}
	if($arItem["SELECTED"] && $arItem["LINK"] == $APPLICATION->GetCurPage()):?>
		<span class="selected"><?=$arItem["TEXT"]?></span>
	<?else:?>
		<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
	<?endif?>
	
<?
$count++;
endforeach?>
</div>
<?endif?>