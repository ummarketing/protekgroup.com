<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? global $arCity; ?>


<div class="header__contacts-city">
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve"><path d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path>
    </svg>
    <span><?= GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_TITLE") ?></span>
    <a href="#modal-location" class="open-modal cyber_location_name">
        <?= $arCity['NAME'] ?>
    </a>
</div>



<?if (!empty($arResult)):
	foreach($arResult as $arItem):
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
			continue;
		if($arItem["SELECTED"] && $arItem["LINK"] == $APPLICATION->GetCurPage()):?>
			<span class="selected"><?=$arItem["TEXT"]?></span>
		<?else:?>
			<a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
		<?endif;
	endforeach;
endif?>