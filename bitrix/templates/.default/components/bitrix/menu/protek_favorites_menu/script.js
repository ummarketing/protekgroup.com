$(document).ready(function(){
	let initCounter = $('.category-menu--favorites img.img-svg').length - 1;
	
	// Replace all SVG images with inline SVG
	$('.category-menu--favorites img.img-svg').each(function(){
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = $(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
			if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
			}

			// Replace image with new SVG
			$img.replaceWith($svg);

			init_side(initCounter);
		}, 'xml');

	});
	
	$(window).resize(function() {
		if (window.innerWidth >= 992) {
			$('.category-menu-btn').attr('aria-expanded', 'true');
			$('.category-menu-content').addClass('show');
			$('.category-menu-btn').on('click', function (e) {
				e.stopPropagation();
				e.preventDefault();
			});
		}else{
			$('.category-menu-btn').unbind("click");
			$('.category-menu-btn').attr('aria-expanded', 'false');
			$('.category-menu-content').removeClass('show');
		}
	});
	
	function init_side(c){
		if(c != 0){
			initCounter--;
		}else{
			if (window.innerWidth >= 992) {
				$('.category-menu-btn').attr('aria-expanded', 'true');
				$('.category-menu-content').addClass('show');
				$('.category-menu-btn').on('click', function (e) {
					e.stopPropagation();
					e.preventDefault();
				});
			}else{
				$('.category-menu-btn').unbind("click");
				$('.category-menu-btn').attr('aria-expanded', 'false');
				$('.category-menu-content').removeClass('show');
			}
		}
	}
});