<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
<div class="sidemenu">
	<img class="sidemenu__logo" src="<?=DEFAULT_TEMPLATE_PATH?>/img/logo--small.png" alt="<?=SITE_NAME?>">
	<?if(count($arResult) > 4){?><button class="sidemenu__prev sidemenu__prev--hidden" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></svg> <?=$arParams['~TOP_CAPTION']?></button><?}?>
	<div class="sidemenu__wrap swiper-container">
		<div class="sidemenu__list swiper-wrapper">
	<?foreach($arResult as $arItem){
		if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
			continue;
		if($arItem["SELECTED"] && $arItem["LINK"] == $APPLICATION->GetCurPage()){?>
			<span class="swiper-slide selected"><img style="width:26px; height:26px" src="<?=(!empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><span><?=$arItem["TEXT"]?></span></span>
		<?}else{?>
			<a class="swiper-slide" href="<?=$arItem["LINK"]?>"><img style="width:26px; height:26px" src="<?=(!empty($arItem['PARAMS']['ICON']) ? $arItem['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><span><?=$arItem["TEXT"]?></span></a>
		<?}
	}?>
		</div>
	</div>
	<?if(count($arResult) > 4){?>
	<button class="sidemenu__btn" type="button"><?=$arParams['~MORE_CAPTION']?> <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></svg></button>
	<?}?>
</div>
<?}?>