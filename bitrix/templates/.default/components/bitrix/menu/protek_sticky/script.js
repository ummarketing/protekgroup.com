$(document).ready(function(){
	let initCounter = $('.sidemenu img.img-svg').length;
	
	// Replace all SVG images with inline SVG
	$('.sidemenu img.img-svg').each(function(){
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		$.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = $(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');

			// Check if the viewport is set, if the viewport is not set the SVG wont't scale.
			if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
				$svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
			}

			// Replace image with new SVG
			$img.replaceWith($svg);
			
			init_side(initCounter);
		}, 'xml');

	});
	/*==============================
	Sidemenu
	==============================*/
	var mySwiperMenu = new Swiper ('.sidemenu__wrap', {
		loop: false,
		speed: 600,
		slidesPerView: 4,
		slidesPerGroup: 4,
		direction: 'vertical',
		// allowTouchMove: false,
		navigation: {
			nextEl: '.sidemenu__btn',
			prevEl: '.sidemenu__prev',
		},
		on: {
			init: function() {
				init_side(initCounter);
			},
		},
	});

	mySwiperMenu.on('reachEnd', function () {
		$('.sidemenu__btn').toggleClass('sidemenu__btn--hidden');
	});

	mySwiperMenu.on('reachBeginning', function () {
		$('.sidemenu__logo').removeClass('sidemenu__logo--hidden');
		$('.sidemenu__prev').addClass('sidemenu__prev--hidden');
	});

	mySwiperMenu.on('slideNextTransitionStart', function () {
		if( ! $('.sidemenu__logo').hasClass('sidemenu__logo--hidden') ) {
			$('.sidemenu__logo').addClass('sidemenu__logo--hidden');
		}

		if( $('.sidemenu__prev').hasClass('sidemenu__prev--hidden') ) {
			$('.sidemenu__prev').removeClass('sidemenu__prev--hidden');
		}
	});

	mySwiperMenu.on('slidePrevTransitionStart', function () {
		if( $('.sidemenu__btn').hasClass('sidemenu__btn--hidden') ) {
			$('.sidemenu__btn').removeClass('sidemenu__btn--hidden');
		}
	});
	
	function init_side(c){
		if(c != 0){
			initCounter--;
		}else{
			$('.sidemenu').addClass('side_initialized');
		}
	}
});