<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)){?>
<div class="category-menu dropdown">
	<button class="category-menu-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 92.833 92.833" style="enable-background:new 0 0 92.833 92.833;" xml:space="preserve"><path d="M89.834,1.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V4.75 C92.834,3.096,91.488,1.75,89.834,1.75z"/><path d="M89.834,36.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V39.75 C92.834,38.096,91.488,36.75,89.834,36.75z"/><path d="M89.834,71.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V74.75 C92.834,73.095,91.488,71.75,89.834,71.75z"/></svg><?=$arParams['MENU_TITLE']?></button>
	<div class="category-menu-content dropdown-menu" role="menu">
		<div class="category-menu-content__list">
	<?
	$count = 5;
	if(count($arResult) <= $count){
		$count = count($arResult);
	}
	for($i = 0; $i < $count; $i++){
		if($arParams["MAX_LEVEL"] == 1 && $arResult[$i]["DEPTH_LEVEL"] > 1) 
			continue;
		if($arResult[$i]["SELECTED"] && $arResult[$i]["LINK"] == $APPLICATION->GetCurPage()){?>
			<span class="selected"><img style="width:26px; height:26px" src="<?=(!empty($arResult[$i]['PARAMS']['ICON']) ? $arResult[$i]['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><span><?=$arResult[$i]["TEXT"]?></span></span>
		<?}else{?>
			<a href="<?=$arResult[$i]["LINK"]?>"><img style="width:26px; height:26px" src="<?=(!empty($arResult[$i]['PARAMS']['ICON']) ? $arResult[$i]['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><span><?=$arResult[$i]["TEXT"]?></span></a>
		<?}
	}?>
		<?if(count($arResult) > $count){?>
			<div class="collapse" id="collapseCatMenu">
			<?for($i = $count; $i < count($arResult); $i++){
				if($arParams["MAX_LEVEL"] == 1 && $arResult[$i]["DEPTH_LEVEL"] > 1) 
					continue;
				if($arResult[$i]["SELECTED"] && $arResult[$i]["LINK"] == $APPLICATION->GetCurPage()){?>
					<span class="selected"><img style="width:26px; height:26px" src="<?=(!empty($arResult[$i]['PARAMS']['ICON']) ? $arResult[$i]['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><span><?=$arResult[$i]["TEXT"]?></span></span>
				<?}else{?>
					<a href="<?=$arResult[$i]["LINK"]?>"><img style="width:26px; height:26px" src="<?=(!empty($arResult[$i]['PARAMS']['ICON']) ? $arResult[$i]['PARAMS']['ICON'] : DEFAULT_TEMPLATE_PATH."/img/plus.svg")?>" class="img-svg" /><span><?=$arResult[$i]["TEXT"]?></span></a>
				<?}
			}?>
			</div>
		<?}?>
		</div>
		<a href="#collapseCatMenu" class="category-menu-collapse category-menu-collapse--dropdown" data-toggle="collapse" data-target="#collapseCatMenu" aria-expanded="false" aria-controls="collapseCatMenu"><span class="category-menu-collapse--dropdown_more"><?=$arParams['MENU_MORE']?></span><span class="category-menu-collapse--dropdown_less"><?=$arParams['MENU_LESS']?></span> <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></svg></a>
		<div class="category-menu-content__phone">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/catalog/menu.php"), false);?>
		</div>
	</div>
</div>
<?}?>