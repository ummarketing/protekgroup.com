<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$arMessages['ADD_TO_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arMessages['ADD_TO_CART'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arMessages['ADD_TO_FAVORITES'] = $arParams['~BUTTON_ADD_TO_FAVORITES'];
$arMessages['DELETE_FROM_COMPARE'] = $arParams['~BUTTON_DELETE_FROM_COMPARE'];
$arMessages['DELETE_FROM_FAVORITES'] = $arParams['~BUTTON_DELETE_FROM_FAVORITES'];

if (isset($arResult['ITEM']))
{
	$item = $arResult['ITEM'];
	$areaId = $arResult['AREA_ID'];
	$itemIds = array(
		'ID' => $areaId,
		'PICT' => $areaId.'_pict',
		'SECOND_PICT' => $areaId.'_secondpict',
		'PICT_SLIDER' => $areaId.'_pict_slider',
		'STICKER_ID' => $areaId.'_sticker',
		'SECOND_STICKER_ID' => $areaId.'_secondsticker',
		'QUANTITY' => $areaId.'_quantity',
		'QUANTITY_DOWN' => $areaId.'_quant_down',
		'QUANTITY_UP' => $areaId.'_quant_up',
		'QUANTITY_MEASURE' => $areaId.'_quant_measure',
		'QUANTITY_LIMIT' => $areaId.'_quant_limit',
		'BUY_LINK' => $areaId.'_buy_link',
		'BASKET_ACTIONS' => $areaId.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $areaId.'_not_avail',
		'SUBSCRIBE_LINK' => $areaId.'_subscribe',
		'COMPARE_LINK' => $areaId.'_compare_link',
		'PRICE' => $areaId.'_price',
		'PRICE_OLD' => $areaId.'_price_old',
		'PRICE_TOTAL' => $areaId.'_price_total',
		'DSC_PERC' => $areaId.'_dsc_perc',
		'SECOND_DSC_PERC' => $areaId.'_second_dsc_perc',
		'PROP_DIV' => $areaId.'_sku_tree',
		'PROP' => $areaId.'_prop_',
		'DISPLAY_PROP_DIV' => $areaId.'_sku_prop',
		'BASKET_PROP_DIV' => $areaId.'_basket_prop',
	);
	$obName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $areaId);
	
	$productTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $item['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $item['NAME'];

	$imgTitle = isset($item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $item['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $item['NAME'];

	
		$actualItem = $item;

		$jsParams = array(
			'PRODUCT_TYPE' => $item['PRODUCT']['TYPE'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_ADD_BASKET_BTN' => false,
			'SHOW_BUY_BTN' => true,
			'SHOW_ABSENT' => true,
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'BIG_DATA' => $item['BIG_DATA'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'VIEW_MODE' => $arResult['TYPE'],
			'USE_SUBSCRIBE' => $showSubscribe,
			'PRODUCT' => array(
				'ID' => $item['ID'],
				'NAME' => $productTitle,
				'DETAIL_PAGE_URL' => $item['DETAIL_PAGE_URL'],
				'PICT' => $item['SECOND_PICT'] ? $item['PREVIEW_PICTURE_SECOND'] : $item['PREVIEW_PICTURE'],
				'CAN_BUY' => $item['CAN_BUY'],
				'CHECK_QUANTITY' => $item['CHECK_QUANTITY'],
				'MAX_QUANTITY' => $item['CATALOG_QUANTITY'],
				'STEP_QUANTITY' => $item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
				'QUANTITY_FLOAT' => is_float($item['ITEM_MEASURE_RATIOS'][$item['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
				'ITEM_PRICE_MODE' => $item['ITEM_PRICE_MODE'],
				'ITEM_PRICES' => $item['ITEM_PRICES'],
				'ITEM_PRICE_SELECTED' => $item['ITEM_PRICE_SELECTED'],
				'ITEM_QUANTITY_RANGES' => $item['ITEM_QUANTITY_RANGES'],
				'ITEM_QUANTITY_RANGE_SELECTED' => $item['ITEM_QUANTITY_RANGE_SELECTED'],
				'ITEM_MEASURE_RATIOS' => $item['ITEM_MEASURE_RATIOS'],
				'ITEM_MEASURE_RATIO_SELECTED' => $item['ITEM_MEASURE_RATIO_SELECTED'],
				'MORE_PHOTO' => $item['MORE_PHOTO'],
				'MORE_PHOTO_COUNT' => $item['MORE_PHOTO_COUNT']
			),
			'BASKET' => array(
				'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'EMPTY_PROPS' => empty($item['PRODUCT_PROPERTIES']),
				'BASKET_URL' => $arParams['~BASKET_URL'],
				'ADD_URL_TEMPLATE' => $arParams['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arParams['~BUY_URL_TEMPLATE']
			),
			'VISUAL' => array(
				'ID' => $itemIds['ID'],
				'PICT_ID' => $item['SECOND_PICT'] ? $itemIds['SECOND_PICT'] : $itemIds['PICT'],
				'PICT_SLIDER_ID' => $itemIds['PICT_SLIDER'],
				'QUANTITY_ID' => $itemIds['QUANTITY'],
				'QUANTITY_UP_ID' => $itemIds['QUANTITY_UP'],
				'QUANTITY_DOWN_ID' => $itemIds['QUANTITY_DOWN'],
				'PRICE_ID' => $itemIds['PRICE'],
				'PRICE_OLD_ID' => $itemIds['PRICE_OLD'],
				'PRICE_TOTAL_ID' => $itemIds['PRICE_TOTAL'],
				'BUY_ID' => $itemIds['BUY_LINK'],
				'BASKET_PROP_DIV' => $itemIds['BASKET_PROP_DIV'],
				'BASKET_ACTIONS_ID' => $itemIds['BASKET_ACTIONS'],
				'NOT_AVAILABLE_MESS' => $itemIds['NOT_AVAILABLE_MESS'],
				'COMPARE_LINK_ID' => $itemIds['COMPARE_LINK'],
				'SUBSCRIBE_ID' => $itemIds['SUBSCRIBE_LINK']
			)
		);

		if ($arParams['DISPLAY_COMPARE'])
		{
			$jsParams['COMPARE'] = array(
				'COMPARE_URL_TEMPLATE' => $arParams['~COMPARE_URL_TEMPLATE'],
				'COMPARE_DELETE_URL_TEMPLATE' => $arParams['~COMPARE_DELETE_URL_TEMPLATE'],
				'COMPARE_PATH' => $arParams['COMPARE_PATH']
			);
		}

		$jsParams['PRODUCT_DISPLAY_MODE'] = $arParams['PRODUCT_DISPLAY_MODE'];
		$jsParams['USE_ENHANCED_ECOMMERCE'] = $arParams['USE_ENHANCED_ECOMMERCE'];
		$jsParams['DATA_LAYER_NAME'] = $arParams['DATA_LAYER_NAME'];
		$jsParams['BRAND_PROPERTY'] = !empty($item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
			? $item['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
			: null;

		$templateData = array(
			'JS_OBJ' => $obName,
			'ITEM' => array(
				'ID' => $item['ID'],
				'IBLOCK_ID' => $item['IBLOCK_ID'],
				'OFFERS_SELECTED' => $item['OFFERS_SELECTED'],
				'JS_OFFERS' => $item['JS_OFFERS']
			)
		);
		?>
	<div class="col-12 col-md-6 col-xl-4" data-entity="item" id="<?=$areaId?>">
		<div class="catitem">
			<a href="<?=$item['DETAIL_PAGE_URL']?>" class="catitem__img">
				<img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['DETAIL_PICTURE']['ALT']?>">
			</a>
			
			<div class="catitem__content">
				<h2 class="catitem__title"><a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a></h2>
				<ul class="catitem__list">
					<?foreach($item['DISPLAY_PROPERTIES'] as $arProp){?>
					<li><?=$arProp['NAME']?>: <?=$arProp['VALUE']?></li>
					<?}?>
				</ul>
				
				<div class="catitem__quantity" data-entity="quantity-block">
					<button type="button" class="catitem__quantity_down" id="<?=$itemIds['QUANTITY_DOWN']?>">-</button>
					<input type="text" value="1" id="<?=$itemIds['QUANTITY']?>" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>">
					<button type="button" class="catitem__quantity_up" id="<?=$itemIds['QUANTITY_UP']?>">+</button>
				</div>

				<div class="catitem__action" data-entity="buttons-block">
					<div id="<?=$itemIds['BASKET_ACTIONS']?>">
						<a class="catitem__add" id="<?=$itemIds['BUY_LINK']?>" href="javascript:void(0)" rel="nofollow">
							<?=$arMessages['ADD_TO_CART']?>
						</a>
					</div>
					<button class="catitem__favorites" type="button" id="<?=$itemIds['COMPARE_LINK']?>"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path></svg><?=$arMessages['ADD_TO_FAVORITES']?></button>
					<label id="<?=$itemIds['COMPARE_LINK']?>">
						<input type="checkbox" data-entity="compare-checkbox">
						<span data-entity="compare-title"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve"><path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path><path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path><path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path><path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path><path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path></svg><?=$arMessages['ADD_TO_COMPARE']?></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<script>
	  var <?=$obName?> = new JCCatalogItem(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
	</script>
	<?
	unset($item, $actualItem, $minOffer, $itemIds, $jsParams);
}