<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<div class="col-12 col-md-6 col-xl-4">
	<div class="catitem" id="<?=$areaId?>">
		<a href="<?=$item['DETAIL_PAGE_URL']?>" class="catitem__img">
			<img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['DETAIL_PICTURE']['ALT']?>">
		</a>
		
		<div class="catitem__content">
			<h2 class="catitem__title"><a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a></h2>
			<ul class="catitem__list">
				<?foreach($item['DISPLAY_PROPERTIES'] as $arProp){?>
				<li><?=$arProp['NAME']?>: <?=$arProp['VALUE']?></li>
				<?}?>
			</ul>
			
			<div class="catitem__quantity" data-entity="quantity-block">
				<button type="button" class="catitem__quantity_down" id="<?=$itemIds['QUANTITY_DOWN']?>">-</button>
				<input type="text" value="1" id="<?=$itemIds['QUANTITY']?>">
				<button type="button" class="catitem__quantity_up" id="<?=$itemIds['QUANTITY_UP']?>">+</button>
			</div>

			<div class="catitem__action" data-entity="buttons-block">
				<div id="<?=$itemIds['BASKET_ACTIONS']?>">
					<a class="catitem__add" id="<?=$itemIds['BUY_LINK']?>" href="javascript:void(0)" rel="nofollow">
						<?=$arMessages['ADD_TO_CART']?>
					</a>
				</div>
				<button class="catitem__favorites" type="button" id="<?=$itemIds['COMPARE_LINK']?>"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path></svg><?=$arMessages['ADD_TO_FAVORITES']?></button>
				<label id="<?=$itemIds['COMPARE_LINK']?>">
					<input type="checkbox" data-entity="compare-checkbox">
					<span data-entity="compare-title"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve"><path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path><path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path><path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path><path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path><path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path></svg><?=$arMessages['ADD_TO_COMPARE']?></span>
				</label>
			</div>
		</div>
	</div>
</div>