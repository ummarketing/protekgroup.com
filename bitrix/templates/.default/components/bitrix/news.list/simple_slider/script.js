$(document).ready(function(){
	$('.slider').owlCarousel({
		mouseDrag: $('.slider__item').length > 1,
		touchDrag: $('.slider__item').length > 1,
		dots: true,
		loop: $('.slider__item').length > 1,
		autoplay: true,
		smartSpeed: 600,
		margin: 0,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		items: 1,
		responsive : {
			1200 : {
				dots: false,
			},
		}
	});

	$('.slider-nav__btn--next').on('click', function() {
		let slider = $(this).parents('.slider-nav').siblings('.slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('next.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	$('.slider-nav__btn--prev').on('click', function() {
		let slider = $(this).parents('.slider-nav').siblings('.slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('prev.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
});