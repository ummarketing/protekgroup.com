<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if(count($arResult["ITEMS"]) > 0){?>
<section class="section <?=($arParams['COLOR'] == "GRAY" ? "section--grey" : "")?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title"><?=$arParams['BLOCK_TITLE']?></h2>
			</div>
			<div class="col-12">
				<div class="posts-slider owl-carousel">
					<?foreach($arResult["ITEMS"] as $arItem){
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
					?>
					<div class="post" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
						<div class="post__img">
							<img src="<?
								$arPhotoSmall = CFile::ResizeImageGet(
									$arItem['~PREVIEW_PICTURE'], 
									array(
										'width'=>280,
										'height'=>185
									), 
									BX_RESIZE_IMAGE_EXACT,
									false,
									70
								);
								echo $arPhotoSmall['src'];	
							?>" alt="">
							<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['PREVIEW_TEXT']?></a>
						</div>
						<span class="post__title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></span>
						<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="post__btn"><?=$arParams['LINK_DEFAULT_TEXT']?></a>
					</div>
					<?}?>
				</div>
				<?if(count($arResult["ITEMS"]) > 1){?>
				<div class="posts-nav">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<button class="posts-nav__btn posts-nav__btn--prev" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
								</button>
								<button class="posts-nav__btn posts-nav__btn--next" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
								</button>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</section>
<?}else{
	?><p class="immediately_error">No elements found</p><?
}?>			