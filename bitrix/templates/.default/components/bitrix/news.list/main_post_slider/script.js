$(document).ready(function(){
	$('.posts-slider').owlCarousel({
		mouseDrag: true,
		touchDrag: true,
		dots: true,
		loop: true,
		autoplay: false,
		smartSpeed: 600,
		margin: 30,
		responsive : {
			0 : {
				items: 1,
			},
			768 : {
				items: 2,
			},
			992 : {
				items: 3,
			},
			1200 : {
				items: 3,
				dots: false,
			},
		}
	});
	$('.posts-nav__btn--next').on('click', function() {
		let slider = $(this).parents('.posts-nav').siblings('.posts-slider');
		$(slider).trigger('next.owl.carousel');
	});
	$('.posts-nav__btn--prev').on('click', function() {
		let slider = $(this).parents('.posts-nav').siblings('.posts-slider');
		$(slider).trigger('prev.owl.carousel');
	});
});