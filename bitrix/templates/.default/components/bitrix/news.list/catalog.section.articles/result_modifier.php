<?
if ($arResult['ITEMS'])
{
    foreach ($arResult['ITEMS'] as $key => &$arItem)
    {
        // --------- Сжатие изображений ------------
        if (!empty($arItem['PREVIEW_PICTURE']['ID']))
        {
            $img_source = $arItem['PREVIEW_PICTURE']['ID'];
            $img        = CFile::ResizeImageGet($img_source, array("width" => 300, "height" => 300), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 80 );
            $arItem['PREVIEW_PICTURE']['SRC'] = $img['src'];
        }
        else
        {
            unset($arResult['ITEMS'][$key]);
        }
        // --------- /Сжатие изображений ------------
    }
}