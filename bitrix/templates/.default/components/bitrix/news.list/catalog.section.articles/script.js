$(document).ready(function () {

    if ($(window).width() > 991)
    {
        $('.catalogSection__articles-list').slick({
            prevArrow: '<button type="button" class="slick-prev-custom"></button>',
            nextArrow: '<button type="button" class="slick-next-custom"></button>',
            arrows: false,
            autoplay: true,
            autoplaySpeed: 60 * 60,
            dotsClass: 'slick-dots slick-dots--white',
            dots: true,
            speed: 600,
            swipeToSlide: true,
            infinite: true,
            slidesToShow: 1,
            centerMode: false,
            responsive: [
                {
                  breakpoint: 991,
                  settings: "unslick"
                }
            ]
        });

        setTimeout(function(){
            $('.catalogSection__articles').fadeIn();
            $('.catalogSection__articles-list').slick('refresh');
        }, 60 * 5);
    }
});