<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->SetAdditionalCSS('/local/assets/plugins/slick/slick/slick.css');
$APPLICATION->AddHeadScript('/local/assets/plugins/slick/slick/slick.min.js');
?>


<? if ($arResult['ITEMS']): ?>
    <div class="catalogSection__articles">
        <div class="catalogSection-articles-title">
            <?= GetMessage('ARTICLES_TITLE'); ?>
        </div>
        <div class="catalogSection__articles-list">
            <? foreach ($arResult['ITEMS'] as $key => $arItem): ?>
                <div class="catalogSection__articles-item">
                    <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                        <img src="<?= $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<?= $arItem['NAME']; ?>">
                    </a>
                    <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>">
                        <?= $arItem['NAME']; ?>
                    </a>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif; ?>
