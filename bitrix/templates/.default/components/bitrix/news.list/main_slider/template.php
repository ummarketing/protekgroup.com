<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if(count($arResult["ITEMS"]) > 0){?>
    <div class="home-wrap">
        <div class="home owl-carousel">
        <?foreach($arResult["ITEMS"] as $arItem){?>
        <div class="home__slide" <?=(!empty($arItem['DISPLAY_PROPERTIES']['COLOR']['VALUE']) ? 'style="background-color: ' . $arItem['DISPLAY_PROPERTIES']['COLOR']['VALUE'] . '"' : '')?>>
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-5">
                        <div class="home__content home__content--first">
                            <?if(isset($arItem['DISPLAY_PROPERTIES']['CAPTION']) && !empty($arItem['DISPLAY_PROPERTIES']['CAPTION']['VALUE']['TEXT'])){
                            ?><div class="home__title"><?=$arItem['DISPLAY_PROPERTIES']['CAPTION']['~VALUE']['TEXT']?></div><?}?>
                            <?if($arItem['PREVIEW_TEXT_TYPE'] == 'HTML'){?>
                            <div><?=$arItem['PREVIEW_TEXT']?></div>
                            <?}else{?>
                            <p class="home__text"><?=$arItem['PREVIEW_TEXT']?></p>
                            <?}?>
                            <?if(isset($arItem['DISPLAY_PROPERTIES']['LINK']) && !empty($arItem['DISPLAY_PROPERTIES']['LINK']['VALUE'])){
                            ?><a href="<?=$arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']?>" target="_blank" class="home__btn"><?
                                if(isset($arItem['DISPLAY_PROPERTIES']['LINK_TEXT']) && !empty($arItem['DISPLAY_PROPERTIES']['LINK_TEXT']['VALUE'])){
                                    echo $arItem['DISPLAY_PROPERTIES']['LINK_TEXT']['VALUE'];
                                }else{
                                    echo $arParams['LINK_DEFAULT_TEXT'];
                                }?></a><?}?>
                        </div>
                    </div>

                    <div class="col-12 col-md-6 col-lg-7">
                        <div class="home__content home__content--last">
                            <?if($arItem['PREVIEW_PICTURE']['SRC']){?><img class="home__img" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>"><?}?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?}?>
        </div>
        <div class="home-wrap__nav">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <button class="home-wrap__btn home-wrap__btn--prev" type="button">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
                        </button>
                        <button class="home-wrap__btn home-wrap__btn--next" type="button">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<? }else{ ?>
    <p class="immediately_error">No elements found</p>
<? } ?>