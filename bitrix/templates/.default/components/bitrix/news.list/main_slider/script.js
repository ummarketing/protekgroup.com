$(document).ready(function(){
	$('.home').owlCarousel({
		mouseDrag: true,
		touchDrag: true,
		dots: true,
		loop: true,
		autoplay: true,
		smartSpeed: 600,
		margin: 0,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		items: 1,
		responsive : {
			1200 : {
				dots: false,
			},
		}
	});

	$('.home-wrap__btn--next').on('click', function() {
		let slider = $(this).parents('.home-wrap__nav').siblings('.home');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('next.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	$('.home-wrap__btn--prev').on('click', function() {
		let slider = $(this).parents('.home-wrap__nav').siblings('.home');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('prev.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
});