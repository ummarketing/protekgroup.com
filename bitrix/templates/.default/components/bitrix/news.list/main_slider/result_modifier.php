<?
if ($arResult['ITEMS']){
    foreach ($arResult['ITEMS'] as $key => &$arItem){

        //--------- Сжатие изображений ----------
        if (!empty($arItem['PREVIEW_PICTURE']['ID'])){
            $img_source = $arItem['PREVIEW_PICTURE']['ID'];
            $filt       = array("width" => 550, "height" => 400);
            $img        = CFile::ResizeImageGet($img_source, $filt, BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 80 );

            if (!empty($img['src'])){
                $arItem['PREVIEW_PICTURE']['SRC'] = $img['src'];
            }
        }
        //--------- /Сжатие изображений ----------
    }
    unset($arItem);
}
?>
