<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<?foreach($arResult['ITEMS'] as $photo){
			$arPhotoSmall = CFile::ResizeImageGet(
				$photo['DETAIL_PICTURE']['ID'], 
				array(
					'width'=>280,
					'height'=>500
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL,
				false,
				70
			);
			?>
			<div class="fancy-gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<a data-fancybox="gallery" data-caption="<?=$photo['NAME']?>" href="<?=$photo['DETAIL_PICTURE']['SRC']?>">
					<img src="<?=$arPhotoSmall['src']?>" alt="<?=$photo['NAME']?>">
				</a>
			</div>
			<?}?>
		</div>
<div class="row">
	<div class="col-12"><?=$arResult["NAV_STRING"]?></div>
</div>
	</div>
</section>