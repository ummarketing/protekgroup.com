<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if(count($arResult["ITEMS"]) > 0){?>
<div class="col-12">
	<?
	function getExtension($filename) {
		return end(explode(".", $filename));
	}
	foreach($arResult["ITEMS"] as $arItem){
		
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		$file = CFile::GetByID($arItem['PROPERTIES']['FILE']['VALUE']);
		$arFile = $file->Fetch();
		
		$fileFormat = strtolower(getExtension($arFile['ORIGINAL_NAME']));
		$fileSize = ceil($arFile['FILE_SIZE'] / 1000000);
		$arIcons = array(
			"doc" => "/img/downloads/doc.svg",
			"docx" => "/img/downloads/doc.svg",
			
			"pdf" => "/img/downloads/pdf.svg",
			
			"xls" => "/img/downloads/xlsx.svg",
			"xlsx" => "/img/downloads/xlsx.svg",
			
			"avi" => "/img/downloads/video.svg",
			"mp4" => "/img/downloads/video.svg",
			"wmv" => "/img/downloads/video.svg",
			"mov" => "/img/downloads/video.svg",
			"mkv" => "/img/downloads/video.svg",

		);
		$defaultIcon = "/img/downloads/picture.svg";
		?>
		<div class="download" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<img src="<?
			if(!empty($arItem['PREVIEW_PICTURE']['SRC'])){
				echo $arItem['PREVIEW_PICTURE']['SRC'];
			}else{
				if(isset($arIcons[$fileFormat])){
					echo SITE_TEMPLATE_PATH . $arIcons[$fileFormat];
				}else{
					echo SITE_TEMPLATE_PATH . $defaultIcon;
				}
			}?>" alt="<?=$arItem['NAME']?>" class="download__img">
			<h3 class="download__title"><?=$arItem['NAME']?> (<?=strtoupper($fileFormat)?>, <?=$fileSize?> <?=$arParams['SIZE_CAPTION']?>)</h3>
			<p class="download__text"><?=$arItem['PREVIEW_TEXT']?></p>
			<a href="<?=CFile::GetPath($arItem['PROPERTIES']['FILE']['VALUE'])?>" class="download__link" download><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path d="M498.966,339.946c-7.197,0-13.034,5.837-13.034,13.034v49.804c0,28.747-23.388,52.135-52.135,52.135H78.203 c-28.747,0-52.135-23.388-52.135-52.135V352.98c0-7.197-5.835-13.034-13.034-13.034C5.835,339.946,0,345.782,0,352.98v49.804 c0,43.121,35.082,78.203,78.203,78.203h355.594c43.121,0,78.203-35.082,78.203-78.203V352.98 C512,345.782,506.165,339.946,498.966,339.946z"></path><path d="M419.833,391.3H92.167c-7.197,0-13.034,5.837-13.034,13.034s5.835,13.034,13.034,13.034h327.665 c7.199,0,13.034-5.835,13.034-13.034C432.866,397.137,427.031,391.3,419.833,391.3z"></path><path d="M387.919,207.93c-4.795-5.367-13.034-5.834-18.404-1.038l-100.482,89.765V44.048c0-7.197-5.835-13.034-13.034-13.034 c-7.197,0-13.034,5.835-13.034,13.034v252.609l-100.482-89.764c-5.367-4.796-13.607-4.328-18.404,1.038 c-4.794,5.369-4.331,13.609,1.037,18.404l109.174,97.527c6.187,5.529,13.946,8.292,21.708,8.292 c7.759,0,15.519-2.763,21.708-8.289l109.174-97.53C392.25,221.537,392.714,213.297,387.919,207.93z"></path></svg><?=$arParams['DOWNLOAD_TEXT']?></a>
		</div>
	<?}?>
</div>
<div class="col-12"><?=$arResult["NAV_STRING"]?></div>
<?}else{
	?><p class="immediately_error">No elements found</p><?
}?>