<?
if ($arResult['ITEMS']){
    foreach ($arResult['ITEMS'] as $key => &$arItem){
        //-------- Сжатие изображений ---------
        foreach ($arItem['PROPERTIES']['PHOTO_LIST']['VALUE'] as $prop_id => $prop_val){
            if (!empty($prop_val)){
                $img = CFile::ResizeImageGet($prop_val, array("width" => 360, "height" => 360), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 80 );
                $arItem['PHOTOS']['SRC_XS'][] = $img['src'];
                $arItem['PHOTOS']['SRC_LG'][] = CFile::GetPath($prop_val);
            }
        }
        //-------- /Сжатие изображений ---------
    }

    unset($arItem);
}
?>