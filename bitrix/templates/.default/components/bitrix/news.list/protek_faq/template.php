<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>




<div class="faq__list">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>


        <? if (!empty($arItem['PREVIEW_TEXT'])): ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="faq__item">
                <div class="faq-item__question" data-faq="question">
                    <?= $arItem['NAME']; ?>
                    <svg width="21" height="13" viewBox="0 0 21 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2 2L10.5 10L19 2" stroke="#58BA47" stroke-width="3"/>
                    </svg>
                </div>
                <div class="faq-item__answer" data-faq="answer">
                    <div class="faq-answer__text">
                        <?= $arItem['PREVIEW_TEXT']; ?>
                    </div>

                    <? if (!empty($arItem['PHOTOS'])): ?>
                        <div class="faq-answer__media">
                            <div class="row">
                                <? foreach ($arItem['PHOTOS']['SRC_XS'] as $photo_key => $photo_val): ?>
                                    <div class="col-xs-6 col-sm-3">
                                        <div class="faq-media__item">
                                            <div class="faq-media__img"
                                                 style="background-image: url(<?= $photo_val; ?>)"
                                                 data-fancybox="gallery_<?= $arItem['ID']; ?>"
                                                 href="<?= $arItem['PHOTOS']['SRC_LG'][$photo_key]; ?>"
                                                 data-caption="<?= $arItem['NAME']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif ?>


                    <? if ($arItem['PROPERTIES']['SHOW_DETAIL']['VALUE'] == 'Y'): ?>
                        <a href="<?= $arItem['DETAIL_PAGE_URL']; ?>" class="faq-answer__link">
                            Читать подробнее
                        </a>
                    <? endif ?>
                </div>
            </div>
        <? endif ?>



    <?endforeach;?>
</div>


<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>


