$(document).ready(function(){
	$('.products-slider').owlCarousel({
		mouseDrag: true,
		touchDrag: true,
		dots: true,
		loop: true,
		autoplay: true,
		smartSpeed: 600,
		margin: 30,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		responsive : {
			0 : {
				items: 1,
			},
			768 : {
				items: 2,
			},
			992 : {
				items: 3,
			},
			1200 : {
				items: 3,
				dots: false,
			},
		}
	});
	$('.products-nav__btn--next').on('click', function() {
		let slider = $(this).parents('.products-nav').siblings('.products-slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('next.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	$('.products-nav__btn--prev').on('click', function() {
		let slider = $(this).parents('.products-nav').siblings('.products-slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('prev.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
});