<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;
		
$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "left";

foreach($arResult['ITEMS'] as $key => $item){
	if($item['CODE'] == "COLOR"){

		//CModule::IncludeModule('iblock');
		CModule::IncludeModule('highloadblock');
		$rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>$item['USER_TYPE_SETTINGS']['TABLE_NAME'])));
		if(($hldata = $rsData->fetch())){
			$hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
			$hlDataClass = $hldata['NAME'].'Table';
		}

		$res = $hlDataClass::getList(array(
			'filter' => array(
				'UF_XML_ID' => array_keys($item['VALUES']),
			), 
			'select' => array("*"),
        ));
		while($row = $res->fetch()){
			$arResult['ITEMS'][$key]['VALUES'][$row['UF_XML_ID']]['COLOR'] = $row['UF_COLOR'];
		}
	}
	if($item['PROPERTY_TYPE'] == "G"){
		foreach($item['VALUES'] as $key1 => $val){
			$arResult['ITEMS'][$key]['VALUES'][$key1]['VALUE'] = preg_replace("/[.]/", "", $val['VALUE']);
		}
	}
}