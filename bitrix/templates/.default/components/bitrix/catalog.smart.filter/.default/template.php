<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="category__menu bx-filter">
    <form name="<? echo $arResult["FILTER_NAME"] . "_form" ?>"
          action="<? echo $arResult["FORM_ACTION"] ?>"
          method="get"
          class="smartfilter">
        <? foreach ($arResult["HIDDEN"] as $arItem) { ?>
            <input type="hidden"
                   name="<? echo $arItem["CONTROL_NAME"] ?>"
                   id="<? echo $arItem["CONTROL_ID"] ?>"
                   value="<? echo $arItem["HTML_VALUE"] ?>"/>
        <? } ?>
        <?
        //not prices
        foreach ($arResult["ITEMS"] as $key => $arItem) {
            if (
            empty($arItem["VALUES"])
            )
                continue;

            if (
                $arItem["DISPLAY_TYPE"] == "A"
                && (
                    $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                )
            )
                continue;
            ?>
            <button class="category__menu-btn"
                    type="button"
                    data-toggle="collapse"
                    data-target="#catmenu_<?= $key ?>"
                    aria-expanded="<?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "true" : "false") ?>"
                    aria-controls="catmenu_<?= $key ?>"><?= $arItem["NAME"] ?>
                <svg version="1.1"
                     xmlns="http://www.w3.org/2000/svg"
                     viewBox="0 0 129 129"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     enable-background="new 0 0 129 129">
                    <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
                </svg>
            </button>
            <div class="category__menu-col bx-filter-parameters-box collapse <?= ($arItem["DISPLAY_EXPANDED"] == "Y" ? "show" : "") ?>"
                 id="catmenu_<?= $key ?>">
                <span class="bx-filter-container-modef"
                      style=""></span>
                <? /*<span class="bx-filter-parameters-box-hint"><?=$arItem["NAME"]?>
							<?if ($arItem["FILTER_HINT"] <> ""):?>
								<i id="item_title_hint_<?echo $arItem["ID"]?>" class="fa fa-question-circle"></i>
								<script type="text/javascript">
									new top.BX.CHint({
										parent: top.BX("item_title_hint_<?echo $arItem["ID"]?>"),
										show_timeout: 10,
										hide_timeout: 200,
										dx: 2,
										preventHide: true,
										min_width: 250,
										hint: '<?= CUtil::JSEscape($arItem["FILTER_HINT"])?>'
									});
								</script>
							<?endif?>
							<i data-role="prop_angle" class="fa fa-angle-<?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>up<?else:?>down<?endif?>"></i>
						</span>*/
                ?>
                <div class="bx-filter-block" data-role="bx_filter_block">
                    <?
                    $arCur = current($arItem["VALUES"]);
                    switch ($arItem["DISPLAY_TYPE"]) {
                        case "B"://NUMBERS
                    case "A"://NUMBERS_WITH_SLIDER
                        ?>
                        <div class="row">
                            <div class="col-12 bx-ui-slider-track-container">
                                <div class="bx-ui-slider-track"
                                     id="drag_track_<?= $key ?>">
                                    <?
                                        $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                                        $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
                                        $value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
                                        $value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
                                        $value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
                                        $value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
                                        $value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                    ?>
                                    <div class="bx-ui-slider-part p1">
                                        <span><?= $value1 ?></span>
                                    </div>
                                    <div class="bx-ui-slider-part p2">
                                        <span><?= $value2 ?></span>
                                    </div>
                                    <div class="bx-ui-slider-part p3">
                                        <span><?= $value3 ?></span>
                                    </div>
                                    <div class="bx-ui-slider-part p4">
                                        <span><?= $value4 ?></span>
                                    </div>
                                    <div class="bx-ui-slider-part p5">
                                        <span><?= $value5 ?></span>
                                    </div>

                                    <div class="bx-ui-slider-pricebar-vd"
                                         style="left: 0;right: 0;"
                                         id="colorUnavailableActive_<?= $key ?>"></div>
                                    <div class="bx-ui-slider-pricebar-vn"
                                         style="left: 0;right: 0;"
                                         id="colorAvailableInactive_<?= $key ?>"></div>
                                    <div class="bx-ui-slider-pricebar-v"
                                         style="left: 0;right: 0;"
                                         id="colorAvailableActive_<?= $key ?>"></div>
                                    <div class="bx-ui-slider-range"
                                         id="drag_tracker_<?= $key ?>"
                                         style="left: 0;right: 0;">
                                        <a class="bx-ui-slider-handle left"
                                           style="left:0;"
                                           href="javascript:void(0)"
                                           id="left_slider_<?= $key ?>"></a>
                                        <a class="bx-ui-slider-handle right"
                                           style="right:0;"
                                           href="javascript:void(0)"
                                           id="right_slider_<?= $key ?>"></a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="new-filter__numbers">
                            <div class="bx-filter-parameters-box-container-block">
                                <div class="bx-filter-input-container left">
                                    <i class="bx-ft-sub"><?= $arParams['FILTER_NUM_FROM'] ?></i>
                                    <input
                                            class="min-price"
                                            type="text"
                                            name="<?= $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                            id="<?= $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                            value="<?= $arItem["VALUES"]["MIN"]["HTML_VALUE"] ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : ''?>"
                                            placeholder="<?= $arItem["VALUES"]["MIN"]["VALUE"] ?>"
                                            size="5"
                                            onkeyup="smartFilter.keyup(this)"
                                    />
                                </div>
                            </div>
                            <div class="bx-filter-parameters-box-container-block">
                                <div class="bx-filter-input-container right">
                                    <i class="bx-ft-sub"><?= $arParams['FILTER_NUM_TO'] ?></i>
                                    <input
                                            class="max-price"
                                            type="text"
                                            name="<?= $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                            id="<?= $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                            value="<?= $arItem["VALUES"]["MAX"]["HTML_VALUE"] ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : ''?>"
                                            placeholder="<?= $arItem["VALUES"]["MAX"]["VALUE"] ?>"
                                            size="5"
                                            onkeyup="smartFilter.keyup(this)"
                                    />
                                </div>
                            </div>
                        </div>
                    <?
                    $arJsParams = array(
                        "leftSlider" => 'left_slider_' . $key,
                        "rightSlider" => 'right_slider_' . $key,
                        "tracker" => "drag_tracker_" . $key,
                        "trackerWrap" => "drag_track_" . $key,
                        "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                        "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                        "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                        "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                        "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                        "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                        "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
                        "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                        "precision" => $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0,
                        "colorUnavailableActive" => 'colorUnavailableActive_' . $key,
                        "colorAvailableActive" => 'colorAvailableActive_' . $key,
                        "colorAvailableInactive" => 'colorAvailableInactive_' . $key,
                    );
                    ?>
                        <script type="text/javascript">
                            BX.ready(function () {
                                window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                            });
                        </script>
                    <?
                    break;
                    case "G"://CHECKBOXES_WITH_PICTURES
                    case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
                    case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
                    ?>
                        <div class="category__colors bx-filter-param-btn-inline">
                            <? foreach ($arItem["VALUES"] as $val => $ar) {
                                ?>
                                <div class="category__color">
                                    <input
                                            style="display: none"
                                            type="checkbox"
                                            name="<?= $ar["CONTROL_NAME"] ?>"
                                            id="<?= $ar["CONTROL_ID"] ?>"
                                            value="<?= $ar["HTML_VALUE"] ?>"
                                        <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                    />
                                    <?
                                    $class = "";
                                    if ($ar["CHECKED"])
                                        $class .= " bx-active";
                                    if ($ar["DISABLED"])
                                        $class .= " disabled";
                                    ?>
                                    <? if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])) {
                                        ?>
                                        <label for="<?= $ar["CONTROL_ID"] ?>"
                                               data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                               title="<?= $ar["VALUE"] ?>"
                                               class="bx-filter-param-label <?= $class ?>"
                                               style="background-image: url('<?= $ar["FILE"]["SRC"] ?>')"
                                               onclick="smartFilter.keyup(BX('<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')); BX.toggleClass(this, 'bx-active');"></label>
                                    <?
                                    } elseif (isset($ar["COLOR"])) {
                                        ?>
                                        <label for="<?= $ar["CONTROL_ID"] ?>"
                                               data-role="label_<?= $ar["CONTROL_ID"] ?>"
                                               title="<?= $ar["VALUE"] ?>"
                                               class="bx-filter-param-label <?= $class ?>"
                                               style="background: <?= $ar["COLOR"] ?>"
                                               onclick="smartFilter.keyup(BX('<?= CUtil::JSEscape($ar["CONTROL_ID"]) ?>')); BX.toggleClass(this, 'bx-active');"></label>
                                    <?
                                    } ?>
                                </div>
                            <?
                            } ?>
                        </div>
                    <?
                    break;
                    default://CHECKBOXES
                    ?>
                    <? foreach ($arItem["VALUES"] as $val => $ar){
                    ?>
                        <div class="category__check">
                            <input
                                    type="checkbox"
                                    value="<? echo $ar["HTML_VALUE"] ?>"
                                    name="<? echo $ar["CONTROL_NAME"] ?>"
                                    id="<? echo $ar["CONTROL_ID"] ?>"
                                <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                    onclick="smartFilter.click(this)"
                            />
                            <label for="<? echo $ar["CONTROL_ID"] ?>"
                                   data-role="label_<?= $ar["CONTROL_ID"] ?>" <? echo $ar["DISABLED"] ? 'disabled' : '' ?>>
                                            <span class="bx-filter-param-text"
                                                  title="<?= $ar["VALUE"]; ?>">
                                                <span class="new-filter__name">
                                                    <?= $ar["VALUE"]; ?>
                                                </span>
                                                <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])): ?>
                                                    (<span data-role="count_<?= $ar["CONTROL_ID"] ?>"><?= $ar["ELEMENT_COUNT"]; ?></span>)
                                                <? endif; ?>
                                            </span>
                            </label>
                        </div>
                    <? } ?>
                    <? } ?>
                </div>

            </div>

        <? } ?>
        <div class="new-filter__footer">
            <button type="submit"
                    class="category__apply new-filter__button"
                    id="set_filter"
                    name="set_filter">
                <?= $arParams['SUBMIT_FILTER'] ?></button>

                <a class="category__apply new-filter__button new-filter__button-white"
                   href="<?= str_replace('filter/clear/apply/', '', $arResult["SEF_DEL_FILTER_URL"]); ?>"
                   target="">
                    <?= (!empty($arParams['RESET_FILTER'])) ? $arParams['RESET_FILTER'] : 'Сбросить фильтр'; ?>
                </a>
            </button>
        </div>

        <div class="bx-filter-popup-result <? echo $arParams["POPUP_POSITION"] ?>"
             id="modef"
             style="display:none;">
            <a href="<? echo $arResult["FILTER_URL"] ?>"
               target="">
                <?= $arParams['SHOW_CAPTION'] ?>
                <?= '<span id="modef_num">' . intval($arResult["ELEMENT_COUNT"]) . '</span>'; ?>
            </a>
        </div>
    </form>
</div>
<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>