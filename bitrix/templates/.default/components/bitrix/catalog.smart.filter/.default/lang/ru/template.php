<?
$MESS ['CT_BCSF_FILTER_TITLE'] = "Подбор параметров";
$MESS ['CT_BCSF_FILTER_FROM'] = "От";
$MESS ['CT_BCSF_FILTER_TO'] = "До";
$MESS ['CT_BCSF_SET_FILTER'] = "Показать";
$MESS ['CT_BCSF_DEL_FILTER'] = "Сбросить";
$MESS ['CT_BCSF_FILTER_COUNT'] = "Выбрано: #ELEMENT_COUNT#";
$MESS ['CT_BCSF_FILTER_SHOW'] = "Показать";
$MESS ['CT_BCSF_FILTER_ALL'] = "Все";
$MESS ['NAME_CATEGORY_FILTER_SIZE'] = "Размеры";
$MESS ['NAME_CATEGORY_FILTER_ABILITIES'] = "Специальные возможности";
$MESS ['NAME_CATEGORY_OTHER_FILTER'] = "Остальные фильтры";

?>