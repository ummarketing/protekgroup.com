<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

?>
<section class="section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title"><?=$arParams['CATALOG_TITLE']?></h2>
			</div>

			<div class="col-12">
				<div class="catalog">
				<?foreach($arResult['SECTIONS'] as $key => $arSection){?>
					<div class="catalog-item" id="<? echo $this->GetEditAreaId($arSection['ID']);?>" data-href="#catalog-modal_<?=$key?>">
						<a href="<?=$arSection['SECTION_PAGE_URL']?>">
							<div class="catalog-item__title">
								<span><?=$arSection['NAME']?></span>
							</div>
							<img class="catalog-item__img" src="<?
							$arWaterMark = Array(
								array(
									"name" => "watermark",
									"position" => "center",
									"type" => "image",
									"size" => "medium",
									"file" => WATERMARK_PATH,
									"fill" => "exact",
									"alpha_level" => 26
								)
							);
							$arPhotoSmall = CFile::ResizeImageGet(
								$arSection['PICTURE']['ID'], 
								array(
									'width'=>300,
									'height'=>220
								), 
								BX_RESIZE_IMAGE_EXACT,
								false,
								$arWaterMark,
								70
							);
							echo $arPhotoSmall['src'];
							?>" alt="<?=$arSection['PICTURE']['ALT']?>">
						</a>
					</div>
				<?}?>
				</div>
			</div>
		</div>
	</div>
</section>