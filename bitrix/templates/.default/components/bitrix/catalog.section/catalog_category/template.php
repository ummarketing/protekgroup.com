<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$arMessages['ADD_TO_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arMessages['ADD_TO_CART'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arMessages['ADD_TO_FAVORITES'] = $arParams['~BUTTON_ADD_TO_FAVORITES'];
$arMessages['DELETE_FROM_COMPARE'] = $arParams['~BUTTON_DELETE_FROM_COMPARE'];
$arMessages['DELETE_FROM_FAVORITES'] = $arParams['~BUTTON_DELETE_FROM_FAVORITES'];

if($_REQUEST['forming']) {
	$_SESSION['CYBER_CATALOG_FORMING'] = htmlspecialchars((string)$_REQUEST['forming']);
}
if($_REQUEST['sorting']) {
	$_SESSION['CYBER_CATALOG_SORTING'] = htmlspecialchars((string)$_REQUEST['sorting']);
}?>
<script data-skip-moving="true">
	var basketData = {
		basketTemplateUrl: "<?= $arResult['~ADD_URL_TEMPLATE'] ?>",
		quantity: "<?= $arResult['ORIGINAL_PARAMETERS']['PRODUCT_QUANTITY_VARIABLE'] ?>",
		compareAddTemplate: "<?= $APPLICATION->GetCurPage() ?>?action=ADD_TO_COMPARE_LIST&id=#ID#",
		compareDeleteTemplate: "<?= $APPLICATION->GetCurPage() ?>?action=DELETE_FROM_COMPARE_LIST&id=#ID#",
		compareAddMessage: "<?= $arMessages['ADD_TO_COMPARE'] ?>",
		compareRemoveMessage: "<?= $arMessages['DELETE_FROM_COMPARE'] ?>",
		favoritesAddMessage: "<?= $arMessages['ADD_TO_FAVORITES'] ?>",
		favoritesRemoveMessage: "<?= $arMessages['DELETE_FROM_FAVORITES'] ?>",
		favoritesName: "<?= FAVORITES_NAME . SITE_ID ?>"
	};
</script>
<div class="category__title">
	<div class="row">
		<form name="sorting-form" id="sorting-form" action="" method="get"></form>
		<div class="col-12">
			<div class="category__filter-wrap">
				<div class="category__sort">
					<span><?= $arParams['CATEGORY_SORT_BY'] ?>:</span>
					<div class="sorting__collapse_container">
						<a data-toggle="collapse" href="#sorting_collapse" role="button" aria-expanded="false" class="category__sort_select">
							<?if($_SESSION['CYBER_CATALOG_SORTING'] == "name"){
							echo $arParams['CATEGORY_SORT_BY_NAME'];
						}elseif($_SESSION['CYBER_CATALOG_SORTING'] == "show_counter"){
							echo $arParams['CATEGORY_SORT_BY_SHOWS'];
						}else{
							echo $arParams['CATEGORY_SORT_BY_FROM'];
						}?>
						</a>
						<div class="sorting__collapse collapse" id="sorting_collapse">
							<button form="sorting-form" class=" <?= ($_SESSION['CYBER_CATALOG_SORTING'] == "name" ? "active" : "") ?>" type="submit" name="sorting" value="name"><?= $arParams['CATEGORY_SORT_BY_NAME'] ?></button>
							<button form="sorting-form" class=" <?= ($_SESSION['CYBER_CATALOG_SORTING'] == "active_from" ? "active" : "") ?>" type="submit" name="sorting" value="active_from"><?= $arParams['CATEGORY_SORT_BY_FROM'] ?></button>
							<button form="sorting-form" class=" <?= ($_SESSION['CYBER_CATALOG_SORTING'] == "show_counter" ? "active" : "") ?>" type="submit" name="sorting" value="show_counter"><?= $arParams['CATEGORY_SORT_BY_SHOWS'] ?></button>
						</div>
					</div>
				</div>

				<div class="category__type">
					<span><?= $arParams['CATEGORY_VIEW'] ?>:</span>
					<button form="sorting-form" class="products-sort__link <?= ($_SESSION['CYBER_CATALOG_FORMING'] == "category_list" ? "active" : "") ?>" type="submit" name="forming" value="category_list"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="39px" height="32px" viewBox="0 0 48.098 36.203" style="enable-background:new 0 0 48.098 36.203;" xml:space="preserve">
							<path d="M45.699,35.567H13.535c-1.324,0-2.398-1.075-2.398-2.399c0-1.328,1.074-2.402,2.398-2.402h32.164 c1.324,0,2.399,1.074,2.399,2.402C48.098,34.492,47.023,35.567,45.699,35.567L45.699,35.567z M45.699,35.567" />
							<path d="M45.699,20.504H13.535c-1.324,0-2.398-1.074-2.398-2.402c0-1.324,1.074-2.399,2.398-2.399h32.164 c1.324,0,2.399,1.075,2.399,2.399C48.098,19.43,47.023,20.504,45.699,20.504L45.699,20.504z M45.699,20.504" />
							<path d="M45.699,5.438H13.535c-1.324,0-2.398-1.075-2.398-2.399c0-1.328,1.074-2.402,2.398-2.402h32.164 c1.324,0,2.399,1.074,2.399,2.402C48.098,4.363,47.023,5.438,45.699,5.438L45.699,5.438z M45.699,5.438" />
							<path d="M6.445,3.223c0,1.781-1.441,3.223-3.222,3.223C1.441,6.446,0,5.004,0,3.223C0,1.442,1.441,0,3.223,0 C5.004,0,6.445,1.442,6.445,3.223L6.445,3.223z M6.445,3.223" />
							<path d="M6.445,18.102c0,1.781-1.441,3.222-3.222,3.222C1.441,21.324,0,19.883,0,18.102c0-1.778,1.441-3.223,3.223-3.223 C5.004,14.879,6.445,16.324,6.445,18.102L6.445,18.102z M6.445,18.102" />
							<path d="M6.445,32.981c0,1.781-1.441,3.222-3.222,3.222C1.441,36.203,0,34.762,0,32.981c0-1.778,1.441-3.223,3.223-3.223 C5.004,29.758,6.445,31.203,6.445,32.981L6.445,32.981z M6.445,32.981" /></svg></button>
					<button form="sorting-form" class="products-sort__link <?= (($_SESSION['CYBER_CATALOG_FORMING'] == "category_big"  || empty($_SESSION['CYBER_CATALOG_FORMING'])) ? "active" : "") ?>" type="submit" name="forming" value="category_big"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewBox="71.163 261.163 271.673 271.673" style="enable-background:new 71.163 261.163 271.673 271.673;" xml:space="preserve">
							<path d="M186.102,261.163H81.612c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C196.551,265.841,191.873,261.163,186.102,261.163z" />
							<path d="M332.388,261.163h-104.49c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C342.837,265.841,338.159,261.163,332.388,261.163z" />
							<path d="M186.102,407.449H81.612c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C196.551,412.127,191.873,407.449,186.102,407.449z" />
							<path d="M332.388,407.449h-104.49c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C342.837,412.127,338.159,407.449,332.388,407.449z" /></svg></button>
					<button form="sorting-form" class="products-sort__link <?= ($_SESSION['CYBER_CATALOG_FORMING'] == "category_small" ? "active" : "") ?>" type="submit" name="forming" value="category_small"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30" height="30" viewBox="0 0 341.333 341.333" style="enable-background:new 0 0 341.333 341.333;" xml:space="preserve">
							<rect x="128" y="128" width="85.333" height="85.333" />
							<rect x="0" y="0" width="85.333" height="85.333" />
							<rect x="128" y="256" width="85.333" height="85.333" />
							<rect x="0" y="128" width="85.333" height="85.333" />
							<rect x="0" y="256" width="85.333" height="85.333" />
							<rect x="256" y="0" width="85.333" height="85.333" />
							<rect x="128" y="0" width="85.333" height="85.333" />
							<rect x="256" y="128" width="85.333" height="85.333" />
							<rect x="256" y="256" width="85.333" height="85.333" /></svg></button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<?if($_SESSION['CYBER_CATALOG_FORMING'] == "category_list"){?>
		<div class="category__grid category__grid--list">
			<div class="row">
				<?
	if (!empty($arResult['ITEMS'])){
		$areaIds = array();
		?>
				<!-- items-container -->
				<?
		foreach ($arResult['ITEMS'] as $item){
			$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
			$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
		?>
				<div class="col-12" id="<?= $areaId ?>">
					<div class="catitem cyberItemContainer">
						<a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="catitem__img">
							<?if(!empty($item['PROPERTIES']['LABEL']['VALUE'])){?>
							<span class="category_item_lable"><img src="<?= $item['PROPERTIES']['LABEL']['VALUE'] ?>" /></span>
							<?}?>
							<img src="<?
						$arWaterMark = Array(
							array(
								" name"=> "watermark",
							"position" => "center",
							"type" => "image",
							"size" => "medium",
							"file" => WATERMARK_PATH,
							"fill" => "exact",
							"alpha_level" => 26
							)
							);
							$arPhotoSmall = CFile::ResizeImageGet(
							$item['PREVIEW_PICTURE']['ID'],
							array(
							'width' => 300,
							'height'=> 250
							),
							BX_RESIZE_IMAGE_EXACT,
							false,
							$arWaterMark,
							90
							);
							echo $arPhotoSmall['src'];?>" alt="<?= $item['DETAIL_PICTURE']['ALT'] ?>">
						</a>

						<div class="catitem__content">
							<div class="catitem__list-wrap">
								<span class="catitem__title"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a></span>
								<ul class="catitem__list">
									<?foreach($item['DISPLAY_PROPERTIES'] as $arProp){?>
                                        <li>
                                            <?= $arProp['NAME'] ?>:
                                            <?= (is_array($arProp['VALUE']) ? implode(", ", $arProp['VALUE']) : $arProp['VALUE']) ?>
                                        </li>
									<?}?>

                                    <?// --------- Свойства раздела -----------?>
                                    <? if (!empty($item['PROPS'])): ?>
                                        <? foreach ($item['PROPS'] as $prop_key => $prop): ?>
                                            <li>
                                                <?= $prop['NAME'].': '.$prop['VALUE']; ?>
                                            </li>
                                        <? endforeach; ?>
                                    <? endif ?>
                                    <?// --------- /Свойства раздела -----------?>
								</ul>
							</div>
							<div class="catitem__list-wrap">
								<?if(!empty($item['PRICES'])){?>
								<div class="catitem__quantity">
									<button type="button" class="catitem__quantity_down quantity-control minus">-</button>
									<input type="text" class="quantity" value="1" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>">
									<button type="button" class="catitem__quantity_up quantity-control plus">+</button>
								</div>
								<?}?>
								<div class="catitem__action">
									<?if(!empty($item['PRICES'])){?>
									<button class="catitem__add add2basket_btn catalog-item__btn" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="_gaq.push(['_trackEvent', 'form', 'user_click10']);yaCounter30699088.reachGoal('goal7'); return true;">
										<span data-cart="add_to_cart" class="active">
											<?= $arMessages['ADD_TO_CART'] ?>
										</span>
										<span data-cart="in_cart" class="btn_in_cart">
											<img src="/local/assets/img/check_icon.svg" alt=""> В корзине
										</span>
									</button>
									<?}?>
									<?if(!empty($_SESSION[FAVORITES_NAME.SITE_ID][$item['ID']])){?>
									<button class="catitem__favorites in-favorites add2favorites_btn" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
											<path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path>
										</svg><span><?= $arMessages['DELETE_FROM_FAVORITES'] ?></span></button>
									<?}else{?>
									<button class="catitem__favorites add2favorites_btn" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
											<path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path>
										</svg><span><?= $arMessages['ADD_TO_FAVORITES'] ?></span></button>
									<?}?>
									<?if(!empty($_SESSION[$arResult['ORIGINAL_PARAMETERS']['COMPARE_NAME']][$arResult['ORIGINAL_PARAMETERS']['IBLOCK_ID']]['ITEMS'][$item['ID']])){?>
									<button class="catitem__compare add2compare_btn in-compare 1" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="yaCounter30699088.reachGoal('goal12'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve">
											<path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path>
											<path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path>
											<path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path>
											<path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path>
											<path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path>
										</svg><span><?= $arMessages['DELETE_FROM_COMPARE'] ?></span></button>
									<?}else{?>
									<button class="catitem__compare add2compare_btn 1" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="yaCounter30699088.reachGoal('goal12'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve">
											<path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path>
											<path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path>
											<path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path>
											<path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path>
											<path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path>
										</svg><span><?= $arMessages['ADD_TO_COMPARE'] ?></span></button>
									<?}?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?}?>
				<!-- items-container -->
				<?
	}?>
			</div>
		</div>

		<?}elseif($_SESSION['CYBER_CATALOG_FORMING'] == "category_small"){?>
		<div class="category__grid category__grid--small">
			<div class="row">
				<?
	if (!empty($arResult['ITEMS'])){
		$areaIds = array();
		?>
				<!-- items-container -->
				<?
		foreach ($arResult['ITEMS'] as $item){
			$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
			$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
		?>
				<div class="col-12 col-md-6 col-xl-4 cyber_tile-container" id="<?= $areaId ?>">
					<div class="catitem cyberItemContainer">
						<a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="catitem__img">
							<?if(!empty($item['PROPERTIES']['LABEL']['VALUE'])){?>
							<span class="category_item_lable"><img src="<?= $item['PROPERTIES']['LABEL']['VALUE'] ?>" /></span>
							<?}?>
							<img src="<?
						$arWaterMark = Array(
							array(
								" name"=> "watermark",
							"position" => "center",
							"type" => "image",
							"size" => "medium",
							"file" => WATERMARK_PATH,
							"fill" => "exact",
							"alpha_level" => 26
							)
							);
							$arPhotoSmall = CFile::ResizeImageGet(
							$item['PREVIEW_PICTURE']['ID'],
							array(
							'width'=>640,
							'height'=>450
							),
							BX_RESIZE_IMAGE_EXACT,
							false,
							$arWaterMark,
							90
							);
							echo $arPhotoSmall['src'];?>" alt="<?= $item['DETAIL_PICTURE']['ALT'] ?>">
						</a>

						<div class="catitem__content">
							<span class="catitem__title"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a></span>
							<ul class="catitem__list">
								<?foreach($item['DISPLAY_PROPERTIES'] as $arProp){?>
                                    <li>
                                        <?= $arProp['NAME'] ?>:
                                        <?= (is_array($arProp['VALUE']) ? implode(", ", $arProp['VALUE']) : $arProp['VALUE']) ?>
                                    </li>
								<?}?>

                                <?// --------- Свойства раздела -----------?>
                                <? if (!empty($item['PROPS'])): ?>
                                    <? foreach ($item['PROPS'] as $prop_key => $prop): ?>
                                        <li>
                                            <?= $prop['NAME'].': '.$prop['VALUE']; ?>
                                        </li>
                                    <? endforeach; ?>
                                <? endif ?>
                                <?// --------- /Свойства раздела -----------?>
							</ul>
							<?if(!empty($item['PRICES'])){?>
							<div class="catitem__quantity">
								<button type="button" class="catitem__quantity_down quantity-control minus">-</button>
								<input type="text" class="quantity" value="1" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>">
								<button type="button" class="catitem__quantity_up quantity-control plus">+</button>
							</div>
							<?}?>
							<div class="catitem__action">
								<?if(!empty($item['PRICES'])){?>
								<button class="catitem__add add2basket_btn catalog-item__btn catalog-item__block" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="_gaq.push(['_trackEvent', 'form', 'user_click10']);yaCounter30699088.reachGoal('goal7'); return true;">
									<span data-cart="add_to_cart" class="active">
										<?= $arMessages['ADD_TO_CART'] ?>
									</span>
									<span data-cart="in_cart" class="btn_in_cart">
										<img src="/local/assets/img/check_icon.svg" alt=""> В корзине
									</span>
								</button>
								<?}?>
								<?if(!empty($_SESSION[FAVORITES_NAME.SITE_ID][$item['ID']])){?>
								<button class="catitem__favorites in-favorites add2favorites_btn" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
										<path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path>
									</svg><span><?= $arMessages['DELETE_FROM_FAVORITES'] ?></span></button>
								<?}else{?>
								<button class="catitem__favorites add2favorites_btn" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
										<path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path>
									</svg><span><?= $arMessages['ADD_TO_FAVORITES'] ?></span></button>
								<?}?>
								<?if(!empty($_SESSION[$arResult['ORIGINAL_PARAMETERS']['COMPARE_NAME']][$arResult['ORIGINAL_PARAMETERS']['IBLOCK_ID']]['ITEMS'][$item['ID']])){?>
								<button class="catitem__compare add2compare_btn in-compare" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="yaCounter30699088.reachGoal('goal12'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve">
										<path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path>
										<path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path>
										<path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path>
										<path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path>
										<path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path>
									</svg><span><?= $arMessages['DELETE_FROM_COMPARE'] ?></span></button>
								<?}else{?>
								<button class="catitem__compare add2compare_btn" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="yaCounter30699088.reachGoal('goal12'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve">
										<path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path>
										<path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path>
										<path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path>
										<path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path>
										<path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path>
									</svg><span><?= $arMessages['ADD_TO_COMPARE'] ?></span></button>
								<?}?>
							</div>
						</div>
					</div>
				</div>
				<?}?>
				<!-- items-container -->
				<?
	}?>
			</div>
		</div>

		<?}else{?>
		<div class="category__grid category__grid--normal">
			<div class="row">
				<?
	if (!empty($arResult['ITEMS'])){
		$areaIds = array();
		?>
				<!-- items-container -->
				<?
		foreach ($arResult['ITEMS'] as $item){
			$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
			$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
		?>
				<div class="col-12 col-md-6 col-xl-4 cyber_tile-container" id="<?= $areaId ?>">
					<div class="catitem cyberItemContainer">
						<a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="catitem__img">
							<?if(!empty($item['PROPERTIES']['LABEL']['VALUE'])){?>
							<span class="category_item_lable"><img src="<?= $item['PROPERTIES']['LABEL']['VALUE'] ?>" /></span>
							<?}?>
							<img src="<?
						$arWaterMark = Array(
							array(
								" name"=> "watermark",
							"position" => "center",
							"type" => "image",
							"size" => "medium",
							"file" => WATERMARK_PATH,
							"fill" => "exact",
							"alpha_level" => 26
							)
							);
							$arPhotoSmall = CFile::ResizeImageGet(
							$item['PREVIEW_PICTURE']['ID'],
							array(
							'width' => 440,
							'height'=> 350
							),
							BX_RESIZE_IMAGE_EXACT,
							false,
							$arWaterMark,
							90
							);
							echo $arPhotoSmall['src'];?>" alt="<?= $item['DETAIL_PICTURE']['ALT'] ?>">
						</a>

						<div class="catitem__content">
							<span class="catitem__title"><a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a></span>
							<ul class="catitem__list">
								<? foreach($item['DISPLAY_PROPERTIES'] as $arProp) { ?>
                                    <li>
                                        <?= $arProp['NAME'] ?>:
                                        <?= (is_array($arProp['VALUE']) ? implode(", ", $arProp['VALUE']) : $arProp['VALUE']) ?>
                                    </li>
								<? } ?>

                                <?// --------- Свойства раздела -----------?>
                                <? if (!empty($item['PROPS'])): ?>
                                    <? foreach ($item['PROPS'] as $prop_key => $prop): ?>
                                        <li>
                                            <?= $prop['NAME'].': '.$prop['VALUE']; ?>
                                        </li>
                                    <? endforeach; ?>
                                <? endif ?>
                                <?// --------- /Свойства раздела -----------?>
							</ul>
							<?if(!empty($item['PRICES'])){?>
							<div class="catitem__quantity">
								<button type="button" class="catitem__quantity_down quantity-control minus">-</button>
								<input type="text" class="quantity" value="1" name="<?= $arParams['PRODUCT_QUANTITY_VARIABLE'] ?>">
								<button type="button" class="catitem__quantity_up quantity-control plus">+</button>
							</div>
							<?}?>
							<div class="catitem__action">
								<?if(!empty($item['PRICES'])){?>
								<button class="catitem__add add2basket_btn catalog-item__btn" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="_gaq.push(['_trackEvent', 'form', 'user_click10']);yaCounter30699088.reachGoal('goal7'); return true;">
									<span data-cart="add_to_cart" class="active">
										<?= $arMessages['ADD_TO_CART'] ?>
									</span>
									<span data-cart="in_cart" class="btn_in_cart">
										<img src="/local/assets/img/check_icon.svg" alt=""> В корзине
									</span>
								</button>
								<?}?>
								<?if(!empty($_SESSION[FAVORITES_NAME.SITE_ID][$item['ID']])){?>
								<button class="catitem__favorites in-favorites add2favorites_btn" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
										<path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path>
									</svg><span><?= $arMessages['DELETE_FROM_FAVORITES'] ?></span></button>
								<?}else{?>
								<button class="catitem__favorites add2favorites_btn" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve">
										<path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path>
									</svg><span><?= $arMessages['ADD_TO_FAVORITES'] ?></span></button>
								<?}?>
								<?if(!empty($_SESSION[$arResult['ORIGINAL_PARAMETERS']['COMPARE_NAME']][$arResult['ORIGINAL_PARAMETERS']['IBLOCK_ID']]['ITEMS'][$item['ID']])){?>
								<button class="catitem__compare add2compare_btn in-compare" data-cyber_id="<?= $item['ID'] ?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve">
										<path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path>
										<path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path>
										<path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path>
										<path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path>
										<path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path>
									</svg><span><?= $arMessages['DELETE_FROM_COMPARE'] ?></span></button>
								<?}else{?>
								<button class="catitem__compare add2compare_btn" data-cyber_id="<?= $item['ID'] ?>" type="button" onclick="yaCounter30699088.reachGoal('goal12'); return true;"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve">
										<path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path>
										<path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path>
										<path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path>
										<path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path>
										<path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path>
									</svg><span><?= $arMessages['ADD_TO_COMPARE'] ?></span></button>
								<?}?>
							</div>
						</div>
					</div>
				</div>
				<?}?>
				<!-- items-container -->
				<?
	}?>
			</div>
		</div>
		<?}?>
	</div>
	<div class="col-12" data-pagination-num="<?= $navParams['NavNum'] ?>">
		<!-- pagination-container -->
		<?= $arResult['NAV_STRING'] ?>
		<!-- pagination-container -->
	</div>
</div>

<?
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'catalog.section');
$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');
?>
<!-- component-end -->