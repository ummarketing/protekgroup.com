<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadScript('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$APPLICATION->AddHeadScript('//yastatic.net/share2/share.js');?>
<section class="section section--top0 section--bottom0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title"><?=$arResult['NAME']?></h1>
			</div>
		</div>
	</div>
</section>
<section class="section section--top0 section--bottom0">
	<div class="container">
		<div class="row">
			<?foreach($arResult['DISPLAY_PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo){
			$arPhotoSmall = CFile::ResizeImageGet(
				$photo, 
				array(
					'width'=>280,
					'height'=>185
				), 
				BX_RESIZE_IMAGE_EXACT,
				false,
				70
			);
			$arPhotoBig = CFile::ResizeImageGet(
				$photo, 
				array(
					'width'=>2000,
					'height'=>2000
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL,
				false,
				80
			);?>
			<div class="fancy-gallery-item col-lg-3 col-md-4 col-sm-6 col-xs-12">
				<a data-fancybox="gallery" data-caption="<?=$arResult['NAME']?>" href="<?=$arPhotoBig['src']?>">
					<img src="<?=$arPhotoSmall['src']?>" alt="<?=$arResult['NAME']?>">
				</a>
			</div>
			<?}?>
		</div>
	</div>
</section>
<div class="article">
	<div class="container">
		<div class="row">
			<div class="col-12 news_article_container">
				<?=$arResult['~DETAIL_TEXT']?>
			</div>

			<div class="col-12">
				<div class="article__share">
					<span><?=$arParams['SHARE_CAPTION']?></span>
				</div>
				<div class="text-center">
					<div class="ya-share2" data-services="vkontakte,facebook,twitter,telegram"></div>
				</div>
			</div>

			<div class="col-12 col-md-6">
				<?if(isset($arResult['PREW_POST'])){?>
				<div class="article__link article__link--prev">
					<a href="<?=$arResult['PREW_POST']['DETAIL_PAGE_URL']?>">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve"><path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554 c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587 c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/></svg>
					</a>
					<div>
						<span><?=$arParams['PREW_CAPTION']?></span>
						<a href="<?=$arResult['PREW_POST']['DETAIL_PAGE_URL']?>"><?=$arResult['PREW_POST']['NAME']?></a>
					</div>
				</div>
				<?}?>
			</div>

			<div class="col-12 col-md-6">
				<?if(isset($arResult['NEXT_POST'])){?>
				<div class="article__link article__link--next">
					<div>
						<span><?=$arParams['NEXT_CAPTION']?></span>
						<a href="<?=$arResult['NEXT_POST']['DETAIL_PAGE_URL']?>"><?=$arResult['NEXT_POST']['NAME']?></a>
					</div>
					<a href="<?=$arResult['NEXT_POST']['DETAIL_PAGE_URL']?>"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve"><path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111 C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587 c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/></svg></a>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</div>