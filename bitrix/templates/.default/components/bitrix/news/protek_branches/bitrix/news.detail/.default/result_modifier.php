<?
if(isset($arResult['DISPLAY_PROPERTIES']['TEAM']['VALUE']) && !empty($arResult['DISPLAY_PROPERTIES']['TEAM']['VALUE'])){
	$arTeam = array();
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID"=>$arResult['PROPERTIES']['TEAM']['LINK_IBLOCK_ID'], "SECTION_ID" => $arResult['PROPERTIES']['TEAM']['VALUE'],"ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()){
		
		$arFields = $ob->GetFields();
		$arFields['PROPERTIES'] = $ob->GetProperties();
		$arTeam[] = $arFields;
	}
	if(count($arTeam) > 0){
		$arResult['TEAM'] = $arTeam;
	}
	unset($arResult['PROPERTIES']['TEAM'], $arResult['DISPLAY_PROPERTIES']['TEAM']);
}
?>