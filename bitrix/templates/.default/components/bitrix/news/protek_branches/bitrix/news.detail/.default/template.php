<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<?if($arResult['PROPERTIES']['FILIAL']['VALUE'] == "Y"){?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title section__title--nomargin"><?=$arResult['NAME']?></h1>
			</div>
		</div>
	</div>
</section>
	<?if(!empty($arResult['PROPERTIES']['BANNERS']['VALUE'])){?>
	<div class="slider-wrap">
		<div class="slider owl-carousel">
		<?foreach($arResult['PROPERTIES']['BANNERS']['VALUE'] as $fileId){?>
			<div class="slider__item">
				<img src="<?
				$arPhotoSmall = CFile::ResizeImageGet(
					$fileId,
					array(
						'width'=>1140,
						'height'=>430
					),
					BX_RESIZE_IMAGE_EXACT,
					false,
					80
				);
				echo $arPhotoSmall['src'];
				?>" alt="<?=$arResult['NAME']?>">
			</div>
		<?}?>
		</div>
		<div class="slider-wrap__title">
			<h2><?=$arResult['NAME']?></h2>
		</div>
		<?if(count($arResult['PROPERTIES']['BANNERS']['VALUE']) > 1){?>
		<div class="slider-nav">
			<button class="slider-nav__btn slider-nav__btn--prev" type="button">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
			</button>
			<button class="slider-nav__btn slider-nav__btn--next" type="button">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
			</button>
		</div>
		<?}?>
	</div>
	<?}?>
	<section class="section <?=(empty($arResult['PROPERTIES']['BANNERS']['VALUE']) ? "section--top0" : "")?>">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="rep">
						<ul class="rep__list">
							<?if(!empty($arResult["PROPERTIES"]["ADDRESS"]["VALUE"])){?><li><span><?=$arResult["PROPERTIES"]["ADDRESS"]["NAME"]?>:</span><p><?=$arResult["PROPERTIES"]["ADDRESS"]["VALUE"]?></p></li><?}?>
									<?if(!empty($arResult["PROPERTIES"]["PHONE"]["VALUE"])){
										?><li><span><?=$arResult["PROPERTIES"]["PHONE"]["NAME"]?>:</span><?
										$count = 0;
										foreach($arResult["PROPERTIES"]["PHONE"]["VALUE"] as $item){
											echo ($count == 0 ? "" : ",&nbsp;");
											?><a href="tel:+<?=preg_replace("/[^0-9]/", '', $item)?>"><?=$item?></a><?
											$count++;
										}?></li><?
									}?>
									<?if(!empty($arResult["PROPERTIES"]["FAX"]["VALUE"])){?><li><span><?=$arResult["PROPERTIES"]["FAX"]["NAME"]?>:</span><p><?=$arResult["PROPERTIES"]["FAX"]["VALUE"]?></p></li><?}?>
									<?if(!empty($arResult["PROPERTIES"]["EMAIL"]["VALUE"])){
										?><li><span><?=$arResult["PROPERTIES"]["EMAIL"]["NAME"]?>:</span><?
										$count = 0;
										foreach($arResult["PROPERTIES"]["EMAIL"]["VALUE"] as $item){
											echo ($count == 0 ? "" : ",&nbsp;");
											?><a href="mailto:+<?=$item?>"><?=$item?></a><?
											$count++;
										}?></li><?
									}?>
									<?if(!empty($arResult["PROPERTIES"]["TIME"]["VALUE"])){?><li><span><?=$arResult["PROPERTIES"]["TIME"]["NAME"]?>:</span><p><?=$arResult["PROPERTIES"]["TIME"]["VALUE"]?></p></li><?}?>
						</ul>
						<?/*if(!empty($arResult["PROPERTIES"]["WAY"]["VALUE"])){*/?><!--
						<div class="rep__content">
							<h2><?/*=$arResult["PROPERTIES"]["WAY"]["NAME"]*/?>:</h2>
							<p><?/*=$arResult["PROPERTIES"]["WAY"]["~VALUE"]["TEXT"]*/?></p>
						</div>
						<?/*}*/?>
						<?/*if(!empty($arResult["PROPERTIES"]["SCHEME"]["VALUE"])){*/?>
						<a href="<?/*=CFile::GetPath($arResult["PROPERTIES"]["SCHEME"]["VALUE"])*/?>" class="rep__download" download><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path d="M498.966,339.946c-7.197,0-13.034,5.837-13.034,13.034v49.804c0,28.747-23.388,52.135-52.135,52.135H78.203 c-28.747,0-52.135-23.388-52.135-52.135V352.98c0-7.197-5.835-13.034-13.034-13.034C5.835,339.946,0,345.782,0,352.98v49.804 c0,43.121,35.082,78.203,78.203,78.203h355.594c43.121,0,78.203-35.082,78.203-78.203V352.98 C512,345.782,506.165,339.946,498.966,339.946z"/><path d="M419.833,391.3H92.167c-7.197,0-13.034,5.837-13.034,13.034s5.835,13.034,13.034,13.034h327.665 c7.199,0,13.034-5.835,13.034-13.034C432.866,397.137,427.031,391.3,419.833,391.3z"/><path d="M387.919,207.93c-4.795-5.367-13.034-5.834-18.404-1.038l-100.482,89.765V44.048c0-7.197-5.835-13.034-13.034-13.034 c-7.197,0-13.034,5.835-13.034,13.034v252.609l-100.482-89.764c-5.367-4.796-13.607-4.328-18.404,1.038 c-4.794,5.369-4.331,13.609,1.037,18.404l109.174,97.527c6.187,5.529,13.946,8.292,21.708,8.292 c7.759,0,15.519-2.763,21.708-8.289l109.174-97.53C392.25,221.537,392.714,213.297,387.919,207.93z"/></svg> <?/*=$arResult["PROPERTIES"]["SCHEME"]["NAME"]*/?></a>
						--><?/*}*/?>
					</div>
				</div>

                <?//--------- Реквизиты ---------?>
                <? if (!empty($arResult['PROPERTIES']['PROPS']['VALUE']['TEXT'])): ?>
                    <div class="col-12">
                        <h2 class="section__title2" style="font-family: unset; margin-bottom: 5px;">
                            Реквизиты:
                        </h2>
                        <div class="rep rep__list">
                            <?= $arResult['PROPERTIES']['PROPS']['~VALUE']['TEXT']; ?>
                        </div>
                    </div>
                <? endif; ?>
                <?//--------- /Реквизиты ---------?>
			</div>
		</div>
	</section>
	<div class="res-address <?=(isset($arResult['TEAM']) ? "" : "section--padding section--top0")?>">
		<?if(!empty($arResult['PROPERTIES']['LOCATION']['VALUE'])){?><div class="res-address__map" id="representation_map"></div><?}?>

		<div class="container">
			<div class="row">
				<div class="col-12">
					<a href="<?=SITE_DIR?>representations/" class="section__btn"><?=$arParams['CHANGE_BRANCH_BUTTON']?></a>
				</div>
			</div>
		</div>
	</div>
    <div class="container">
    <?if(!empty($arResult["PROPERTIES"]["WAY"]["VALUE"])){?>
        <div class="rep__content">
            <h2><?=$arResult["PROPERTIES"]["WAY"]["NAME"]?>:</h2>
            <p><?=$arResult["PROPERTIES"]["WAY"]["~VALUE"]["TEXT"]?></p>
        </div>
    <?}?>
    <?if(!empty($arResult["PROPERTIES"]["SCHEME"]["VALUE"])){?>
        <a href="<?=CFile::GetPath($arResult["PROPERTIES"]["SCHEME"]["VALUE"])?>" class="rep__download" download><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><path d="M498.966,339.946c-7.197,0-13.034,5.837-13.034,13.034v49.804c0,28.747-23.388,52.135-52.135,52.135H78.203 c-28.747,0-52.135-23.388-52.135-52.135V352.98c0-7.197-5.835-13.034-13.034-13.034C5.835,339.946,0,345.782,0,352.98v49.804 c0,43.121,35.082,78.203,78.203,78.203h355.594c43.121,0,78.203-35.082,78.203-78.203V352.98 C512,345.782,506.165,339.946,498.966,339.946z"/><path d="M419.833,391.3H92.167c-7.197,0-13.034,5.837-13.034,13.034s5.835,13.034,13.034,13.034h327.665 c7.199,0,13.034-5.835,13.034-13.034C432.866,397.137,427.031,391.3,419.833,391.3z"/><path d="M387.919,207.93c-4.795-5.367-13.034-5.834-18.404-1.038l-100.482,89.765V44.048c0-7.197-5.835-13.034-13.034-13.034 c-7.197,0-13.034,5.835-13.034,13.034v252.609l-100.482-89.764c-5.367-4.796-13.607-4.328-18.404,1.038 c-4.794,5.369-4.331,13.609,1.037,18.404l109.174,97.527c6.187,5.529,13.946,8.292,21.708,8.292 c7.759,0,15.519-2.763,21.708-8.289l109.174-97.53C392.25,221.537,392.714,213.297,387.919,207.93z"/></svg> <?=$arResult["PROPERTIES"]["SCHEME"]["NAME"]?></a>
    <?}?>
    </div>
	<?if(isset($arResult['TEAM'])){?>
	<section class="section section--padding">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="section__title2"><?=$arParams['TEAM_BRANCH_CAPTION']?></h2>
				</div>
				<?foreach($arResult['TEAM'] as $arItem){?>
				<div class="col-12 col-md-4 col-xl-3">
					<div class="member">
						<img src="<?$arPhotoSmall = CFile::ResizeImageGet(
							$arItem['PREVIEW_PICTURE'],
							array(
								'width'=>300,
								'height'=>300
							),
							BX_RESIZE_IMAGE_EXACT,
							false,
							80
						);
						echo $arPhotoSmall['src'];?>" alt="<?=$arItem['NAME']?>">
						<h3><?=$arItem['NAME']?></h3>
						<p><?=$arItem['PROPERTIES']['POSITION']['VALUE']['TEXT']?></p>
						<?if(isset($arItem['PROPERTIES']['PHONE']) && !empty($arItem['PROPERTIES']['PHONE']['VALUE'])){?><a href="tel:+<?=preg_replace("/[^0-9]/", '', $arItem['PROPERTIES']['PHONE']['VALUE']);?>"><?=$arItem['PROPERTIES']['PHONE']['VALUE']?></a><?}?>
						<?if(isset($arItem['PROPERTIES']['EMAIL']) && !empty($arItem['PROPERTIES']['EMAIL']['VALUE'])){?><a href="mailto:<?=$arItem['PROPERTIES']['EMAIL']['VALUE']?>"><?=$arItem['PROPERTIES']['EMAIL']['VALUE']?></a><?}?>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</section>
	<?}?>
	<script>
	$(document).ready(function(){
		ymaps.ready(function () {
			var mainBranchMap = new ymaps.Map('representation_map', {
					center: [<?=$arResult['PROPERTIES']['LOCATION']['VALUE']?>],
					zoom: 13,
					controls: ["zoomControl"]
				}, {
					searchControlProvider: 'yandex#search'
				})
			,
			mainBranchPlacemark = new ymaps.Placemark([<?=$arResult['PROPERTIES']['LOCATION']['VALUE']?>], {
				hintContent: '<?=$arResult['NAME']?>',
				balloonContent: '<?=$arResult['PROPERTIES']['ADDRESS']['VALUE']?>'
					},{
				iconLayout: 'default#image',
				//iconImageHref: '/bitrix/templates/ru/img/marker.png',
				//iconImageSize: [161, 61],
				//iconImageOffset: [-161, -61]
			});
			mainBranchMap.behaviors.disable('scrollZoom');
			mainBranchMap.geoObjects.add(mainBranchPlacemark);
		});
	});
	</script>
<?} else{
	header( "HTTP/1.1 404 Not Found" );
}?>