<?
$arCoords = $arBranches = $arMain = $arDealers = array();
foreach($arResult['ITEMS'] as $key => $arItem){
	if($arItem['ID'] == $arParams['DEFAULT_BRANCH']){
		$arMain = $arItem;
	}else{
		if($arItem['PROPERTIES']['FILIAL']['VALUE'] == "Y"){
			$arBranches[] = $arItem;
		}else{
			$arDealers[$arItem['NAME']] = $arItem;
		}
	}


	if(!empty($arItem['PROPERTIES']['LOCATION']['VALUE'])){
		$arCoords[] = array(
			"NAME"    => $arItem['NAME'],
			"COORDS"  => $arItem['PROPERTIES']['LOCATION']['VALUE'],
			"ADDRESS" => $arItem['PROPERTIES']['ADDRESS']['VALUE'],
			"URL"     => $arItem['DETAIL_PAGE_URL'],
            "PHONE"   => $arItem['PROPERTIES']['PHONE']['VALUE'][0],
		);
	}
	unset($arResult['ITEMS'][$key]);
}
$arResult['COORDS']      = $arCoords;
$arResult['MAIN_BRANCH'] = $arMain;
$arResult['BRANCHES']    = $arBranches;

array_unshift($arResult['BRANCHES'], $arResult['MAIN_BRANCH']);
ksort($arDealers);
$arResult['DEALERS'] = $arDealers;
?>