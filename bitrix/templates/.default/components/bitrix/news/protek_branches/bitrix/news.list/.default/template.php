<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="res-address">
	<div class="res-address__map res-address__map--single" id="representations_main_map"></div>
</div>
<?if(empty($arResult['MAIN_BRANCH'])){
	$arResult['MAIN_BRANCH'] = $arResult['BRANCHES'][0];
	unset($arResult['BRANCHES'][0]);
}?>
<section class="section" id="representations_main">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<ul class="nav section__nav">
					<li>
						<a data-scroll href="#representations_main" class="active"><?=$arResult['MAIN_BRANCH']['NAME']?></a>
					</li>
					<li>
						<a data-scroll href="#representations_branches"><?=$arParams['BRANCHES_CAPTION']?></a>
					</li>
					<?if(count($arResult['DEALERS']) > 0){?><li>
						<a data-scroll href="#representations_dealers"><?=$arParams['DEALERS_CAPTION']?></a>
					</li>
					<?}?>
				</ul>
			</div>

			<div class="col-12">
				<div class="row">
					<div class="col-12 col-lg-7">
						<h2 class="section__title2"><?=(!empty($arResult["MAIN_BRANCH"]["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) ? $arResult["MAIN_BRANCH"]["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] : $arResult["MAIN_BRANCH"]["NAME"])?></h2>

						<div class="rep">
							<ul class="rep__list rep__list--single">
								<li><span><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["ADDRESS"]["NAME"]?>:</span><p><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["ADDRESS"]["VALUE"]?></p></li>
								<?if(!empty($arResult["MAIN_BRANCH"]["PROPERTIES"]["PHONE"]["VALUE"])){
									?><li><span><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["PHONE"]["NAME"]?>:</span><?
									$count = 0;
									foreach($arResult["MAIN_BRANCH"]["PROPERTIES"]["PHONE"]["VALUE"] as $item){
										echo ($count == 0 ? "" : ",&nbsp;");
										?><a href="tel:+<?=preg_replace("/[^0-9]/", '', $item)?>"><?=$item?></a><?
										$count++;
									}?></li><?
								}?>
								<?if(!empty($arResult["MAIN_BRANCH"]["PROPERTIES"]["FAX"]["VALUE"])){?><li><span><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["FAX"]["NAME"]?>:</span><p><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["FAX"]["VALUE"]?></p></li><?}?>
								<?if(!empty($arResult["MAIN_BRANCH"]["PROPERTIES"]["EMAIL"]["VALUE"])){
									?><li><span><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["EMAIL"]["NAME"]?>:</span><?
									$count = 0;
									foreach($arResult["MAIN_BRANCH"]["PROPERTIES"]["EMAIL"]["VALUE"] as $item){
										echo ($count == 0 ? "" : ",&nbsp;");
										?><a href="mailto:<?=$item?>"><?=$item?></a><?
										$count++;
									}?></li><?
								}?>
								<?if(!empty($arResult["MAIN_BRANCH"]["PROPERTIES"]["TIME"]["VALUE"])){?><li><span><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["TIME"]["NAME"]?>:</span><p><?=$arResult["MAIN_BRANCH"]["PROPERTIES"]["TIME"]["VALUE"]?></p></li><?}?>
							</ul>
						</div>
					</div>

					<div class="col-12 col-lg-5">
						<div class="res-address">
							<div class="res-address__map res-address__map--mini" id="representations_second_map"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?if(count($arResult['BRANCHES']) > 0){?>
<section class="section section--top0 <?=(count($arResult['DEALERS']) > 0 ? "section--bottom0" : "")?>" id="representations_branches">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title2"><?=$arParams["BRANCHES_TITLE"]?></h2>
			</div>

			<div class="col-12">
				<table class="branches-desk">
					<thead>
						<tr>
							<th><?=$arParams['CITY_CAPTION']?></th>
							<th><?=$arResult['MAIN_BRANCH']['PROPERTIES']['PHONE']['NAME']?></th>
							<th><?=$arResult['MAIN_BRANCH']['PROPERTIES']['ADDRESS']['NAME']?></th>
							<th><?=$arResult['MAIN_BRANCH']['PROPERTIES']['EMAIL']['NAME']?></th>
							<th><?=$arResult['MAIN_BRANCH']['PROPERTIES']['SCHEME']['NAME']?></th>
						</tr>
					</thead>

					<tbody>
						<?foreach($arResult['BRANCHES'] as $arItem){?>
						<tr>
							<td><b><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></b></td>
							<td class="branches-desk__phone"><?=$arItem['PROPERTIES']['PHONE']['VALUE'][0]?></td>
							<td><?=$arItem['PROPERTIES']['ADDRESS']['VALUE']?></td>
							<td><?=$arItem['PROPERTIES']['EMAIL']['VALUE'][0]?></td>
							<td><?if(!empty($arItem['PROPERTIES']['SCHEME']['VALUE'])){?><a download href="<?=CFile::GetPath($arItem['PROPERTIES']['SCHEME']['VALUE'])?>"><?=$arItem['PROPERTIES']['SCHEME']['NAME']?></a><?}?></td>
						</tr>
						<?}?>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="branches-mob">
		<?foreach($arResult['BRANCHES'] as $arItem){?>
		<div class="branches-mob__item">
			<div class="branches-mob__wrap">
				<span><b><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></b> <?=$arItem['PROPERTIES']['PHONE']['VALUE'][0]?></span>
				<span><?=$arItem['PROPERTIES']['ADDRESS']['VALUE']?></span>
				<?if(!empty($arItem['PROPERTIES']['EMAIL']['VALUE'])){?><span><?=$arItem['PROPERTIES']['EMAIL']['VALUE'][0]?></span><?}?>
				<?if(!empty($arItem['PROPERTIES']['SCHEME']['VALUE'])){?><a download href="<?=CFile::GetPath($arItem['PROPERTIES']['SCHEME']['VALUE'])?>"><?=$arItem['PROPERTIES']['SCHEME']['NAME']?></a><?}?>
			</div>
		</div>
		<?}?>
	</div>
</section>
<?}?>
<?if(count($arResult['DEALERS']) > 0){?>
<section class="section" id="representations_dealers">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title2"><?=$arParams['DEALERS_TITLE']?></h2>
			</div>

			<div class="col-12">
				<ul class="dealers">
				<?foreach($arResult['DEALERS'] as $arItem){?>
					<li><?=$arItem['NAME']?></li>
				<?}?>
				</ul>
			</div>
		</div>
	</div>
</section>
<?}
//getDebug($arResult['COORDS'][0]);?>
<script>
var arCoords = [
	<?foreach($arResult['COORDS'] as $arItem){?>
	{
		name: '<?=$arItem['NAME']?>',
		coords: [<?=$arItem['COORDS']?>],
		address: '<?=$arItem['ADDRESS']?>',
		url: '<?=$arItem['URL']?>',
        phone: '<?=$arItem['PHONE']?>',
	},
	<?}?>
];
$(document).ready(function(){
	ymaps.ready(function () {
		var mainBranchMap = new ymaps.Map('representations_second_map', {
				center: [<?=$arResult['MAIN_BRANCH']['PROPERTIES']['LOCATION']['VALUE']?>],
				zoom: 14
			}, {
				searchControlProvider: 'yandex#search'
			})
		,
		mainBranchPlacemark = new ymaps.Placemark([<?=$arResult['MAIN_BRANCH']['PROPERTIES']['LOCATION']['VALUE']?>], {
			hintContent: '<?=$arResult['MAIN_BRANCH']['NAME']?>',
			balloonContent: '<?=$arResult['MAIN_BRANCH']['PROPERTIES']['ADDRESS']['VALUE']?>'
				},{
			iconLayout: 'default#image',
			//iconImageHref: '/bitrix/templates/ru/img/marker.png',
			//iconImageSize: [161, 61],
			//iconImageOffset: [-161, -61]
		});
		mainBranchMap.behaviors.disable('scrollZoom');
		mainBranchMap.geoObjects.add(mainBranchPlacemark);

		var branchesMap = new ymaps.Map('representations_main_map', {
			center: [55.751574, 37.573856],
			zoom: 4,
			controls: ["zoomControl"]
		}, {
			searchControlProvider: 'yandex#search'
		});

		branchesMap.behaviors.disable('scrollZoom');


		arCoords.forEach(function(item, i, arr) {
			var branchesPlacemark = new ymaps.Placemark(item.coords, {
				hintContent: item.name,
				balloonContentHeader: '<a href = "' + item.url + '">' + item.name + '</a>',
				balloonContent: item.address + '<br/><a href="tel:'+ item.phone +'">' + item.phone + '<br/><br/>',
				balloonContentFooter: '<a href="' + item.url + '" class="section__btn small"><?=$arParams['MAP_BUTTON']?></a>',
			});
			branchesMap.geoObjects.add(branchesPlacemark);
		});


	});
});
</script>