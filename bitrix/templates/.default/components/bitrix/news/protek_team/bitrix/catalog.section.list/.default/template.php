<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<?
if(count($arResult['SECTIONS']) > 0){
	?><div class="section__region">
		<span class="section__region_title"><?=$arParams['REGION_CAPTION']?></span>
		<select id="select_ajax_hack_container">
			<?foreach($arResult['SECTIONS'] as $key => $arSection){?>
				<?if($arSection['ELEMENT_CNT'] > 0){?><option <?=($_REQUEST['SECTION_ID'] == $arSection['ID'] ? "selected" : "")?> data-href="<?=$arSection['SECTION_PAGE_URL']?>" value="#select_ajax_hack_<?=$key?>"><?=$arSection['NAME']?></option><?}?>
			<?}?>
		</select>
	</div>
<?}?>
