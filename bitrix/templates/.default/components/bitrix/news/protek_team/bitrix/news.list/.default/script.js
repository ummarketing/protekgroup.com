$(document).ready(function(){

	// ---------- Ajax пагинация -----------
	$(document).on('click', '.members-container .pagination a', function(){
		var url   	  = $(this).attr('href');
		var container = $('[data-ajax="container"]');

		container.addClass('loaded');

		if (url.length)
		{
			$.ajax({
				url: url,
				type: 'POST',
				data: {ajax: 'Y'},
				dataType: 'HTML',
			}).done(function(data){
				var new_block = $(data).find('[data-ajax="block"]');

				window.history.pushState("object or string", "Title", url);
				container.find('[data-ajax="block"]').remove();
				container.append(new_block);

				setTimeout(function(){
					container.removeClass('loaded');
					$('html, body').animate({scrollTop: container.offset().top - 150}, 60 * 15);
				}, 60 * 5);
			});
		}
		return false;
	});
	// ---------- /Ajax пагинация -----------



	// --------- Ajax смена региона -----------
	$(document).on('change', '#select_ajax_hack_container', function(){
		var url 	  = $(this).find('option[value="'+ $(this).val() +'"]').attr('data-href');
		var container = $('[data-ajax="container"]');

		container.addClass('loaded');

		if (url.length)
		{
			if (url.indexOf('?SECTION_ID=1') > -1) {
				url = url.replace('?SECTION_ID=1', '');
			}

			$.ajax({
				url: url,
				type: 'POST',
				data: {ajax: 'Y'},
				dataType: 'HTML',
			}).done(function(data){
				var new_block = $(data).find('[data-ajax="block"]');

				window.history.pushState("object or string", "Title", url);
				container.find('[data-ajax="block"]').remove();
				container.append(new_block);

				setTimeout(function(){
					container.removeClass('loaded');
				}, 60 * 5);
			});
		}

		return false;
	});
	// --------- /Ajax смена региона -----------
});