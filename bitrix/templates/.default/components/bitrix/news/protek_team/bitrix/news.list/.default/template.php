<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?foreach($arResult["ITEMS"] as $arItem){
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>
<div class="col-12 col-md-4 col-xl-3 memb-fix-cont">
	<div class="member">
		<img src="<?$arPhotoSmall = CFile::ResizeImageGet(
				$arItem['PREVIEW_PICTURE']['ID'], 
				array(
					'width'=>300,
					'height'=>300
				), 
				BX_RESIZE_IMAGE_EXACT,
				false,
				80
			);
			echo $arPhotoSmall['src'];?>" alt="<?=$arItem['NAME']?>">
		<h3><?=$arItem['NAME']?></h3>
		<p><?=$arItem['DISPLAY_PROPERTIES']['POSITION']['DISPLAY_VALUE']?></p>
		<?if(isset($arItem['DISPLAY_PROPERTIES']['PHONE']) && !empty($arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE'])){?><a href="tel:+<?=preg_replace("/[^0-9]/", '', $arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE']);?>"><?=$arItem['DISPLAY_PROPERTIES']['PHONE']['VALUE']?></a><?}?>
		<?if(isset($arItem['DISPLAY_PROPERTIES']['EMAIL']) && !empty($arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])){?><a href="mailto:<?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?>"><?=$arItem['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?></a><?}?>
	</div>
</div>
<?}?>
<div class="col-12 memb-fix-cont"><?=$arResult["NAV_STRING"]?></div>
