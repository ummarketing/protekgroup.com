<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arSelect = Array('ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE');
$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID']);
$arSort = Array($arParams["SORT_BY_1"] => $arParams["SORT_ORDER_1"], $arParams["SORT_BY_2"] => $arParams["SORT_ORDER_2"]);
$res = CIBlockElement::GetList(
	$arSort, $arFilter, false,
	array('nElementID' => $arResult['ID'], 'nPageSize' => 1),
	$arSelect
);
$count = 0;
while($arRes = $res -> GetNext()){
	$arRes['PREVIEW_PICTURE'] = CFile::GetPath($arRes['PREVIEW_PICTURE']);
	if($count == 0){
		if($arRes['ID'] != $arResult['ID']){
			$arResult['PREW_POST'] = $arRes;
		}
	}else{
		if($arRes['ID'] != $arResult['ID']){
			$arResult['NEXT_POST'] = $arRes;
		}
	}
$count++;
}

$this->__component->SetResultCacheKeys(array("CACHED_TPL"));






// --------------- Содержание страницы ----------------
$html_links = '';
if (!empty($arResult['PROPERTIES']['LINKS']['VALUE']))
{
    ob_start();
        $APPLICATION->IncludeFile(
            $this->GetFolder().'/links.php',
            Array('result' => $arResult),
            Array("SHOW_BORDER"=>false,"MODE"=>"php",)
        );
        $html_links = ob_get_contents();
    ob_end_clean();
}
$arResult['~DETAIL_TEXT'] = str_replace('#Содержание_страницы#', $html_links, $arResult['DETAIL_TEXT']);
// --------------- /Содержание страницы ----------------
?>