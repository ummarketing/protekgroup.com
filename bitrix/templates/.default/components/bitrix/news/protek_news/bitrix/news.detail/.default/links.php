<?
$arResult = $arParams['result'];
$arLinks  = $arResult['PROPERTIES']['LINKS']['VALUE'];
?>


<? if (!empty($arLinks)): ?>
    <div class="articleDetail__links customBlock__links">
        <div class="customBlock__links-title" data-collapse-title>
            <?= GetMessage('LINKS'); ?>

            <div class="customBlock__links-arrow">
                <svg width="21" height="13" viewBox="0 0 21 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M2 2L10.5 10L19 2" stroke="#58BA47" stroke-width="3"/>
                </svg>
            </div>
        </div>
        <div class="customBlock__links-list" data-collapse-block>
            <? foreach ($arLinks as $i => $link): ?>
                <div class="customBlock__links-item">
                    <?= ($i + 1); ?>.
                    <span data-article-link><?= $link ?></span>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? endif ?>
