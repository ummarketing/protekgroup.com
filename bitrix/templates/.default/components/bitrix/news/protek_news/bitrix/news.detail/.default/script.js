$(document).ready(function(){
	$('.slider').owlCarousel({
		mouseDrag: $('.slider__item').length > 1,
		touchDrag: $('.slider__item').length > 1,
		dots: true,
		loop: $('.slider__item').length > 1,
		autoplay: true,
		smartSpeed: 600,
		margin: 0,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		items: 1,
		responsive : {
			1200 : {
				dots: false,
			},
		}
	});

	$('.slider-nav__btn--next').on('click', function() {
		let slider = $(this).parents('.slider-nav').siblings('.slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('next.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	$('.slider-nav__btn--prev').on('click', function() {
		let slider = $(this).parents('.slider-nav').siblings('.slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('prev.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});


	// ---------- COLLAPSE ------------
	$('[data-collapse-title]').on('click', function(){
		var collapseTitle  = $(this);
		var collapseParent = $(this).parent();
		var collapseBlock  = collapseParent.find('[data-collapse-block]');

		collapseTitle.toggleClass('active');
		collapseBlock.slideToggle();
	});
	// ---------- /COLLAPSE ------------


	// ---------- ЯКОРЬ ---------------
	$('[data-article-link]').on('click', function(){
		var scroll = false
		var h2 	   = $('.news_article_container h2:contains('+ $(this).text() +')');
		var h3 	   = $('.news_article_container h3:contains('+ $(this).text() +')');
		var p  	   = $('.news_article_container p:contains('+ $(this).text() +')');
		var strong = $('.news_article_container strong:contains('+ $(this).text() +')');

		if (h2.length) {
			scroll = h2;
		} else if (h3.length) {
			scroll = h3;
		} else if (p.length) {
			scroll = p;
		} else if (strong.length) {
			scroll = strong;
		}

		if (scroll) {
			$('body, html').animate({scrollTop: scroll.offset().top - 165}, 60 * 15);
			return false;
		}
	});
	// ---------- /ЯКОРЬ ---------------
});