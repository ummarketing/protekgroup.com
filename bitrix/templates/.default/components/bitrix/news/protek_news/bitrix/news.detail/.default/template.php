<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->AddHeadScript('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$APPLICATION->AddHeadScript('//yastatic.net/share2/share.js');
$upperMargin = 1;
if(count($arResult['DISPLAY_PROPERTIES']['BANNERS']['VALUE']) > 1 || $arResult['DISPLAY_PROPERTIES']['SHOW_DETAIL']['VALUE'] == 'Y'){
	$upperMargin = 0;
}
ob_start();
?>


<section class="section section--top0 <?=($upperMargin ? "section--bottom0" : "")?>">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title section__title--article" style="margin-bottom: 0;">
                    <?=$arResult['NAME']?>
                </h2>
                <div class="post__date">
                    <?= $arResult['DISPLAY_ACTIVE_FROM']; ?>
                </div>
			</div>
		</div>
	</div>
</section>


<?if($arResult['DISPLAY_PROPERTIES']['SHOW_DETAIL']['VALUE'] == 'Y'){?>
    <section class="section section--top0 <?=(count($arResult['DISPLAY_PROPERTIES']['BANNERS']['VALUE']) > 1 ? "" : "section--bottom0")?>">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img class="img-fluid" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" title="<?=$arResult['DETAIL_PICTURE']['TITLE']?>" alt="<?=$arResult['DETAIL_PICTURE']['ALT']?>">
                </div>
            </div>
        </div>
    </section>
<?}?>


<?if(count($arResult['DISPLAY_PROPERTIES']['BANNERS']['VALUE']) > 0){?>
    <div class="slider-wrap">
        <div class="slider slider--article owl-carousel">
        <?foreach($arResult['DISPLAY_PROPERTIES']['BANNERS']['VALUE'] as $fileId){?>
            <div class="slider__item">
                <?
                $arWaterMark = Array(
                    array(
                        "name"        => "watermark",
                        "position"    => "center",
                        "type"        => "image",
                        "size"        => "real",
                        "file"        => WATERMARK_PATH,
                        "fill"        => "exact",
                        "alpha_level" => 26
                    )
                );
                ?>
                <img src="<?
                $arPhotoSmall = CFile::ResizeImageGet(
                    $fileId,
                    array(
                        'width'  => 1140,
                        'height' => 430
                    ),
                    BX_RESIZE_IMAGE_EXACT,
                    false,
                    $arWaterMark,
                    80
                );
                echo $arPhotoSmall['src'];
                ?>" alt="<?=(isset($arResult['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'] : $arResult['NAME'])?>">
            </div>
        <?}?>
        </div>
        <?if(count($arResult['DISPLAY_PROPERTIES']['BANNERS']['VALUE']) > 1){?>
        <div class="slider-nav">
            <button class="slider-nav__btn slider-nav__btn--prev" type="button">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
            </button>
            <button class="slider-nav__btn slider-nav__btn--next" type="button">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
            </button>
        </div>
        <?}?>
    </div>
<?}?>


<div class="article">
	<div class="container">
		<div class="row">
			<div class="col-12 news_article_container">
				<?= $arResult['~DETAIL_TEXT'] ?>
			</div>

			<div class="col-12">
				<div class="article__share">
					<span><?=$arParams['SHARE_CAPTION']?></span>
				</div>
				<div class="text-center">
					<div class="ya-share2" data-services="vkontakte,facebook,twitter,telegram"></div>
				</div>
			</div>

			<div class="col-12 col-md-6">
				<?if(isset($arResult['PREW_POST'])){?>
				<div class="article__link article__link--prev">
					<a href="<?=$arResult['PREW_POST']['DETAIL_PAGE_URL']?>">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.494 31.494" style="enable-background:new 0 0 31.494 31.494;" xml:space="preserve"><path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554 c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587 c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/></svg>
					</a>
					<div>
						<span><?=$arParams['PREW_CAPTION']?></span>
						<a href="<?=$arResult['PREW_POST']['DETAIL_PAGE_URL']?>"><?=$arResult['PREW_POST']['NAME']?></a>
					</div>
				</div>
				<?}?>
			</div>

			<div class="col-12 col-md-6">
				<?if(isset($arResult['NEXT_POST'])){?>
				<div class="article__link article__link--next">
					<div>
						<span><?=$arParams['NEXT_CAPTION']?></span>
						<a href="<?=$arResult['NEXT_POST']['DETAIL_PAGE_URL']?>"><?=$arResult['NEXT_POST']['NAME']?></a>
					</div>
					<a href="<?=$arResult['NEXT_POST']['DETAIL_PAGE_URL']?>"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve"><path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111 C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587 c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/></svg></a>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</div>


<?
$this->__component->arResult["CACHED_TPL"] = @ob_get_contents();
ob_get_clean()?>