<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row posts-slider">
	<?foreach($arResult["ITEMS"] as $arItem){
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="post margin-bottom-30 col-lg-4 col-sm-6 col-xs-12" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="post__img">
			<img src="<?
				$arPhotoSmall = CFile::ResizeImageGet(
					$arItem['~PREVIEW_PICTURE'],
					array(
						'width'=>280,
						'height'=>185
					),
					BX_RESIZE_IMAGE_EXACT,
					false,
					70
				);
				echo $arPhotoSmall['src'];
			?>" alt="">
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['PREVIEW_TEXT']?></a>
		</div>
		<span class="post__title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></span>
        <div class="post__date">
            <?= $arItem['DISPLAY_ACTIVE_FROM']; ?>
        </div>
		<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="post__btn"><?=$arParams['LINK_DEFAULT_TEXT']?></a>
	</div>
	<?}?>
</div>
<div class="row">
	<div class="col-12"><?=$arResult["NAV_STRING"]?></div>
</div>