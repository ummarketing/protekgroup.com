<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<script data-skip-moving="true">
var basketData = {
	basketTemplateUrl : "<?=$arResult['ITEMS'][0]['DETAIL_PAGE_URL']?>?action=ADD2BASKET&id=#ID#",
	quantity : "<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>",
};
$(document).ready(function(){
	var compareCarousel = $('.compare__slider');

	compareCarousel.owlCarousel({
		mouseDrag: true,
		touchDrag: true,
		dots: false,
		loop: $('.compare__item').length > 1,
		autoplay: false,
		smartSpeed: 600,
		margin: 10,
		autoplayTimeout: 2000,
		items: 2,
		responsive : {
			992 : {
				items: 3
			},
			1200 : {
				items: 4,
				margin: 20
			},
		},
	});

	$('.compare-nav__btn--next').on('click', function() {
		compareCarousel.trigger('next.owl.carousel');
	});
	$('.compare-nav__btn--prev').on('click', function() {
		compareCarousel.trigger('prev.owl.carousel');
	});
});
</script>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title"><?$APPLICATION->ShowTitle()?></h1>
			</div>

			<div class="col-12 col-md-8 offset-md-4 col-lg-9 offset-lg-3 col-xl-10 offset-xl-2">
				<div class="compare">
					<?if(count($arResult['ITEMS']) > 1){?>
					<div class="compare-nav">
						<button class="compare-nav__btn compare-nav__btn--prev" type="button">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
						</button>
						<button class="compare-nav__btn compare-nav__btn--next" type="button">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
						</button>
					</div>

					<div class="compare__check">
						<input id="compare1" name="compare1" type="checkbox">
						<label for="compare1"><?=$arParams['COMPARE_ONLY_DIFFERENCES']?></label>
					</div>
					<?}?>
					<div class="compare__slider owl-carousel">
						<?foreach($arResult['ITEMS'] as $key => $item){?>
						<div class="compare__item" data-cyber_id="<?=$item['ID']?>" data-carousel_counter="<?=$key?>">
							<h2 class="compare__title">
								<a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a>
							</h2>
							<img class="compare__img" src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['PREVIEW_PICTURE']['ALT']?>">
							<?if(!empty($item['PRICES'])){?>
							<div class="compare__quantity">
								<button type="button" class="catitem__quantity_down quantity-control minus">-</button>
								<input type="text" class="quantity" value="1" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>">
								<button type="button" class="catitem__quantity_up quantity-control plus">+</button>
							</div>
							<button class="compare__add add2basket_btn" data-cyber_id="<?=$item['ID']?>" type="button"><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></button>
							<?}?>
							<ul class="compare__list">
							<?foreach($arResult['SHOW_PROPERTIES'] as $prop){?>
								<li class="<?=($prop['NO_DIFFERENCES'] ? "no-diff" : "diff")?>"><?
								if(!empty($item['DISPLAY_PROPERTIES'][$prop['CODE']]['VALUE'])){
									if(is_array($item['DISPLAY_PROPERTIES'][$prop['CODE']]['DISPLAY_VALUE'])){
										echo implode(", ", $item['DISPLAY_PROPERTIES'][$prop['CODE']]['DISPLAY_VALUE']);
									}else{
										echo $item['DISPLAY_PROPERTIES'][$prop['CODE']]['DISPLAY_VALUE'];
									}
								}else{
									echo "-";
								}?></li>
							<?}?>
							</ul>
							<a href="<?=$item['~DELETE_URL']?>" class="compare__delete">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></svg>
							</a>
						</div>
						<?}?>
					</div>

					<ul class="compare__table">
						<?foreach($arResult['SHOW_PROPERTIES'] as $prop){?>
							<li class="<?=($prop['NO_DIFFERENCES'] ? "no-diff" : "diff")?>"><?=$prop['NAME']?></li>
						<?}?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>