$(document).ready(function(){

	$('body').on('input change', '.quantity', function() { //Только цифры в инпутах
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		if(this.value == 0){
			this.value = 1;
		}
	});
	$('body').on('click', '.quantity-control', function(){ //Изменение количества (клики по плюсу и минусу)
		let quantutyInput = $(this).siblings('input'),
			quantity = 1;
		if($(this).hasClass('minus')) {
			
			if(quantutyInput.val() > 1) { //Не изменяем количество если после нажатия на минус в поле должен появиться ноль
				quantutyInput.val(Number(quantutyInput.val()) - 1);
			}
			
		} else {
			quantutyInput.val(Number(quantutyInput.val()) + 1);
		}
		
	});
	$('body').on('click', '.add2basket_btn', function(){ // add2basket
		let basketParams = {
				'ajax_basket': 'Y'
			},
			basketUrl = basketData['basketTemplateUrl'].replace("#ID#", $(this).data('cyber_id'));
		basketParams[basketData.quantity] = $(this).parents('.catitem__content').find('.quantity').val();

		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: basketUrl,
			data: basketParams,
			success: function(data) {
				console.log(data);
				$('body').trigger('cyber.change.basket.result');
			}
		});
	});
	$('body').on('click', '.add2compare_btn', function(){ // add to compare or remove
		
		let compareUrl = basketData['compareAddTemplate'].replace("#ID#", $(this).data('cyber_id')) + (basketData['compareAddTemplate'].indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y',
			elementInCompare = false,
			compareAddMessage = basketData['compareAddMessage'],
			compareRemoveMessage = basketData['compareRemoveMessage'];
		if($(this).hasClass('in-compare')){
			compareUrl = basketData['compareDeleteTemplate'].replace("#ID#", $(this).data('cyber_id')) + (basketData['compareDeleteTemplate'].indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y';
			elementInCompare = true;
		}

		let self = this;
		$.ajax({
			method: 'POST',
			dataType: elementInCompare ? 'json' : 'html',
			url: compareUrl,
			success: function(data) {
				obj = {};
				if(typeof data === 'object' && data !== null){
					obj = data;
				}else{
					obj = $.parseJSON(data);
				}
				
				if(obj.STATUS == 'OK'){
					$('body').trigger('cyber.change.compare.result');
					if(!elementInCompare){
						$(self).addClass('in-compare');
						$(self).find('span').text(compareRemoveMessage);
					}else{
						$(self).removeClass('in-compare');
						$(self).find('span').text(compareAddMessage);
					}
				}
			}
		});
	});
	$('body').on('click', '.add2favorites_btn', function(){ // add to favorites or remove
		
		let elementInFavorites = false,
			favoritesAddMessage = basketData['favoritesAddMessage'],
			favoritesRemoveMessage = basketData['favoritesRemoveMessage'],
			favoritesData = "id=" + $(this).data('cyber_id') + "&destination=" + basketData['favoritesName'] + "&action=";
			
		if($(this).hasClass('in-favorites')){
			elementInFavorites = true;
			favoritesData += "remove";
		}else{
			favoritesData += "add";
		}

		
		let self = this;
		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: '/ajax/ajax_add2favorites.php',
			data: favoritesData,
			success: function(data) {
				
				if(data.STATUS == 'OK'){
					$('body').trigger('cyber.change.favorites.result');
					if(!elementInFavorites){
						$(self).addClass('in-favorites');
						$(self).find('span').text(favoritesRemoveMessage);
					}else{
						$(self).removeClass('in-favorites');
						$(self).find('span').text(favoritesAddMessage);
					}
				}
			}
		});
	});
});