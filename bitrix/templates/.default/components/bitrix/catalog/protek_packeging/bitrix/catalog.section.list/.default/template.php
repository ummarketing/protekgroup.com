<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

?>
<section class="section section--padding">
	<div class="container">
		<div class="row">
			<?foreach($arResult['SECTIONS'] as $key => $arSection){?>
			<div class="col-12 col-md-6 col-lg-4" id="<? echo $this->GetEditAreaId($arSection['ID']);?>">
				<a href="<?=$arSection['SECTION_PAGE_URL']?>" class="packaging">
					<img class="packaging__img" src="<?
					$arPhotoSmall = CFile::ResizeImageGet(
						$arSection['PICTURE']['ID'], 
						array(
							'width'=>360,
							'height'=>200
						), 
						BX_RESIZE_IMAGE_EXACT,
						false,
						false,
						75
					);
					echo $arPhotoSmall['src'];?>" alt="<?=$arSection['NAME']?>">
					<span class="packaging__title">
						<h3><?=$arSection['NAME']?></h3>
					</span>
				</a>
			</div>
			<?}?>
		</div>
	</div>
</section>