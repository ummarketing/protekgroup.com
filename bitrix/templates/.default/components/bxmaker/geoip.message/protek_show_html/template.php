<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main\Localization\Loc as Loc;

	$COMPONENT_NAME = 'BXMAKER.GEOIP.MESSAGE';

	$randString = $this->randString();


	if ($arParams['AJAX'] != 'Y') { ?>

        <span class="bxmaker__geoip__message bxmaker__geoip__message--default js-bxmaker__geoip__message"
             id="bxmaker__geoip__message__id<?= $randString; ?>"
             data-timeoffset="<?= date('Z'); ?>"
             data-template="<?= $templateName; ?>"
             data-type="<?= $arParams['TYPE']; ?>"
             data-debug="<?= $arResult['DEBUG']; ?>"
             data-cookie-prefix="<?= $arParams['COOKIE_PREFIX']; ?>"
             data-key="<?= $randString; ?>">

			<? $frame = $this->createFrame('bxmaker__geoip__message__id' . $randString, false)->begin(); ?>

			<? if ($arResult['CURRENT']['TIME'] == 'Y'): ?><?
			if($arParams['TYPE'] == "PHONE"){
				?><a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['DEFAULT']['MESSAGE']);?>" class="js-bxmaker__geoip__message-value-default"><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
				<a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['CURRENT']['MESSAGE']);?>" class="js-bxmaker__geoip__message-value-current"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
			}elseif ($arParams['TYPE'] == "EMAIL"){
				?><a href="mailto:<?=$arResult['DEFAULT']['MESSAGE'];?>" class="js-bxmaker__geoip__message-value-default"><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
				<a href="mailto:<?=$arResult['CURRENT']['MESSAGE'];?>" class="js-bxmaker__geoip__message-value-current"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
			}else{
				?><span class="js-bxmaker__geoip__message-value-default"><?= $arResult['DEFAULT']['MESSAGE']; ?></span>
				<span class="js-bxmaker__geoip__message-value-current"><?= $arResult['CURRENT']['MESSAGE']; ?></span><br /><?
			}?>
			<?endif; ?>

            <span class="bxmaker__geoip__message-value js-bxmaker__geoip__message-value v1"
                 data-location="<?= $arParams['LOCATION']; ?>"
                 data-city="<?= $arParams['CITY']; ?>"
                 data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"
                 data-time="<?= $arResult['CURRENT']['TIME']; ?>"
                 data-timestart="<?= $arResult['CURRENT']['START']; ?>"
                 data-timestop="<?= $arResult['CURRENT']['STOP']; ?>">
				<?
				if($arParams['TYPE'] == "PHONE"){
					?><a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['CURRENT']['MESSAGE']);?>"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
				}else if($arParams['TYPE'] == "EMAIL"){
					?><a href="mailto:<?=$arResult['CURRENT']['MESSAGE'];?>"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
				}else{
					?><span><?= $arResult['CURRENT']['MESSAGE']; ?></span><br /><?
				}?>
            </span>


			<? $frame->beginStub(); ?>

            <span class="bxmaker__geoip__message-value  js-bxmaker__geoip__message-value v2"
                 data-time="N">
				<?
				if($arParams['TYPE'] == "PHONE"){
					?><a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['DEFAULT']['MESSAGE']);?>"><?= $arResult['DEFAULT']['MESSAGE']; ?></a><br /><?
				}else if($arParams['TYPE'] == "EMAIL"){
					?><a href="mailto:<?=$arResult['DEFAULT']['MESSAGE'];?>"><?= $arResult['DEFAULT']['MESSAGE']; ?></a><br /><?
				}else{
					?><span><?= $arResult['DEFAULT']['MESSAGE']; ?></span><br /><?
				}?>
            </span>

			<? $frame->end(); ?>
        </span>
		<?
	}
	else {
		?>
		<? if ($arResult['CURRENT']['TIME'] == 'Y'): ?>
		
            <?
			if($arParams['TYPE'] == "PHONE"){
				?><a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['DEFAULT']['MESSAGE']);?>" class="js-bxmaker__geoip__message-value-default"><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
				<a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['CURRENT']['MESSAGE']);?>" class="js-bxmaker__geoip__message-value-current"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
			}else if($arParams['TYPE'] == "EMAIL"){
				?><a href="mailto:<?=$arResult['DEFAULT']['MESSAGE'];?>" class="js-bxmaker__geoip__message-value-default"><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
				<a href="mailto:<?=$arResult['CURRENT']['MESSAGE'];?>" class="js-bxmaker__geoip__message-value-current"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
			}else{
				?><span class="js-bxmaker__geoip__message-value-default"><?= $arResult['DEFAULT']['MESSAGE']; ?></span>
				<span class="js-bxmaker__geoip__message-value-current"><?= $arResult['CURRENT']['MESSAGE']; ?></span><br /><?
			}?>
		<?endif; ?>

        <span class="bxmaker__geoip__message-value  js-bxmaker__geoip__message-value v3"
             data-location="<?= $arParams['LOCATION']; ?>"
             data-city="<?= $arParams['CITY']; ?>"
             data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"
             data-time="<?= $arResult['CURRENT']['TIME']; ?>"
             data-timestart="<?= $arResult['CURRENT']['START']; ?>"
             data-timestop="<?= $arResult['CURRENT']['STOP']; ?>">
			<?
			if($arParams['TYPE'] == "PHONE"){
				?><a href="tel:<?=preg_replace("/[^0-9]/", '', $arResult['CURRENT']['MESSAGE']);?>"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
			}else if($arParams['TYPE'] == "EMAIL"){
				?><a href="mailto:<?=$arResult['CURRENT']['MESSAGE'];?>"><?= $arResult['CURRENT']['MESSAGE']; ?></a><br /><?
			}else{
				?><span><?= $arResult['CURRENT']['MESSAGE']; ?></span><br /><?
			}?>
        </span>
		<?
	}
