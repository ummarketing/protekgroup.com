<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main\Localization\Loc as Loc;

	$COMPONENT_NAME = 'BXMAKER.GEOIP.MESSAGE';

	$randString = $this->randString();


	if ($arParams['AJAX'] != 'Y') { ?>

        <div class="bxmaker__geoip__message bxmaker__geoip__message--default js-bxmaker__geoip__message"
             id="bxmaker__geoip__message__id<?= $randString; ?>"
             data-timeoffset="<?= date('Z'); ?>"
             data-template="<?= $templateName; ?>"
             data-type="<?= $arParams['TYPE']; ?>"
             data-debug="<?= $arResult['DEBUG']; ?>"
             data-cookie-prefix="<?= $arParams['COOKIE_PREFIX']; ?>"
             data-key="<?= $randString; ?>">

			<? $frame = $this->createFrame('bxmaker__geoip__message__id' . $randString, false)->begin(); ?>

			<? if ($arResult['CURRENT']['TIME'] == 'Y'): ?>	
                <div class="js-bxmaker__geoip__message-value-default">
					<a href="mailto:<?= $arResult['DEFAULT']['MESSAGE']; ?>" class="header__contacts-mail">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
					</svg><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
                </div>
                <div class="js-bxmaker__geoip__message-value-current">
					<a href="mailto:<?= $arResult['CURRENT']['MESSAGE']; ?>" class="header__contacts-mail">
					<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
					</svg><?= $arResult['CURRENT']['MESSAGE']; ?></a>
                </div>
			<?endif; ?>

            <div class="bxmaker__geoip__message-value js-bxmaker__geoip__message-value v1"
                 data-location="<?= $arParams['LOCATION']; ?>"
                 data-city="<?= $arParams['CITY']; ?>"
                 data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"
                 data-time="<?= $arResult['CURRENT']['TIME']; ?>"
                 data-timestart="<?= $arResult['CURRENT']['START']; ?>"
                 data-timestop="<?= $arResult['CURRENT']['STOP']; ?>">
				<a href="mailto:<?= $arResult['CURRENT']['MESSAGE']; ?>" class="header__contacts-mail">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
				</svg><?= $arResult['CURRENT']['MESSAGE']; ?></a>
            </div>


			<? $frame->beginStub(); ?>

            <div class="bxmaker__geoip__message-value  js-bxmaker__geoip__message-value v2"
                 data-time="N">
				<a href="mailto:<?= $arResult['DEFAULT']['MESSAGE']; ?>" class="header__contacts-mail">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
				</svg><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
            </div>

			<? $frame->end(); ?>
        </div>
		<?
	}
	else {
		?>
		<? if ($arResult['CURRENT']['TIME'] == 'Y'): ?>
		
            <div class="js-bxmaker__geoip__message-value-default">
				<a href="mailto:<?= $arResult['DEFAULT']['MESSAGE']; ?>" class="header__contacts-mail">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
				</svg><?= $arResult['DEFAULT']['MESSAGE']; ?></a>
            </div>
            <div class="js-bxmaker__geoip__message-value-current">
				<a href="mailto:<?= $arResult['CURRENT']['MESSAGE']; ?>" class="header__contacts-mail">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
				</svg><?= $arResult['CURRENT']['MESSAGE']; ?></a>
            </div>
		<?endif; ?>

        <div class="bxmaker__geoip__message-value  js-bxmaker__geoip__message-value v3"
             data-location="<?= $arParams['LOCATION']; ?>"
             data-city="<?= $arParams['CITY']; ?>"
             data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"
             data-time="<?= $arResult['CURRENT']['TIME']; ?>"
             data-timestart="<?= $arResult['CURRENT']['START']; ?>"
             data-timestop="<?= $arResult['CURRENT']['STOP']; ?>">
			<a href="mailto:<?= $arResult['CURRENT']['MESSAGE']; ?>" class="header__contacts-mail">
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 14 14" xml:space="preserve"><path d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/><path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/><polygon points="0,2.878 0,11.186 4.833,7.079"/><polygon points="9.167,7.079 14,11.186 14,2.875"/>
				</svg><?= $arResult['CURRENT']['MESSAGE']; ?></a>
        </div>
		<?
	}
