$(document).ready(function(){

	ymaps.ready(function () {
		var myMap = new ymaps.Map('map2', {
			center: $('.yamap_data_container').find('span[style]').text().split(','),
			zoom: 13
		}, {
			searchControlProvider: 'yandex#search'
		});
		myMap.behaviors.disable('scrollZoom');
		var placemark = new ymaps.Placemark(myMap.getCenter(), {});
		myMap.geoObjects.add(placemark);
		
		var myMapMobile = new ymaps.Map('map', {
		center: $('.yamap_data_container').find('span[style]').text().split(','),
		zoom: 9,
		controls: []
		}, {
			searchControlProvider: 'yandex#search'
		});
		myMapMobile.behaviors.disable('scrollZoom');
		var placemarkMobile = new ymaps.Placemark(myMapMobile.getCenter(), {});
		myMapMobile.geoObjects.add(placemarkMobile);
			
		$(document).on('bxmaker.geoip.message.reload.after', function () {
			
			let coord = $('.yamap_data_container').find('span[style]').text();
			coord = coord.split(',');
			
			myMap.geoObjects.removeAll();
			myMap.setCenter(coord, 13, {
				checkZoomRange: true
			});
			myMapMobile.geoObjects.removeAll();
			myMapMobile.setCenter(coord, 9, {
				checkZoomRange: true
			});

			var placemark = new ymaps.Placemark(myMap.getCenter(), {});
			myMap.geoObjects.add(placemark);
			var placemarkMobile = new ymaps.Placemark(myMapMobile.getCenter(), {});
			myMapMobile.geoObjects.add(placemarkMobile);
			
		});
	});
});