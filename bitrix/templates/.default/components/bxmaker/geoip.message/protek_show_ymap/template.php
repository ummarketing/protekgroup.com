<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

	use Bitrix\Main\Localization\Loc as Loc;

	$COMPONENT_NAME = 'BXMAKER.GEOIP.MESSAGE';

	$randString = $this->randString();


	if ($arParams['AJAX'] != 'Y') { ?>

        <span class="bxmaker__geoip__message yamap_data_container bxmaker__geoip__message--default js-bxmaker__geoip__message preloader"
             id="bxmaker__geoip__message__id<?= $randString; ?>"
             data-timeoffset="<?= date('Z'); ?>"
             data-template="<?= $templateName; ?>"
             data-type="<?= $arParams['TYPE']; ?>"
             data-debug="<?= $arResult['DEBUG']; ?>"
             data-cookie-prefix="<?= $arParams['COOKIE_PREFIX']; ?>"
             data-key="<?= $randString; ?>">

			<? $frame = $this->createFrame('bxmaker__geoip__message__id' . $randString, false)->begin(); ?>

			<? if ($arResult['CURRENT']['TIME'] == 'Y'): ?>
			
				<span class="js-bxmaker__geoip__message-value-default"  style="display:none;"><?= $arResult['DEFAULT']['MESSAGE']; ?></span>
				<span class="js-bxmaker__geoip__message-value-current"  style="display:none;"><?= $arResult['CURRENT']['MESSAGE']; ?></span>
			
			<?endif; ?>

            <span class="bxmaker__geoip__message-value js-bxmaker__geoip__message-value v1"
                 data-location="<?= $arParams['LOCATION']; ?>"
                 data-city="<?= $arParams['CITY']; ?>"
                 data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"
                 data-time="<?= $arResult['CURRENT']['TIME']; ?>"
                 data-timestart="<?= $arResult['CURRENT']['START']; ?>"
                 data-timestop="<?= $arResult['CURRENT']['STOP']; ?>">
				 
				<span style="display:none;"><?= $arResult['CURRENT']['MESSAGE']; ?></span>
            </span>


			<? $frame->beginStub(); ?>

            <span class="bxmaker__geoip__message-value  js-bxmaker__geoip__message-value v2"
                 data-time="N">
				 
				<span style="display:none;"><?= $arResult['DEFAULT']['MESSAGE']; ?></span><br />
            </span>

			<? $frame->end(); ?>
        </span>
		<?
	}
	else {
		?>
		<? if ($arResult['CURRENT']['TIME'] == 'Y'): ?>
		
            <span class="js-bxmaker__geoip__message-value-default"  style="display:none;"><?= $arResult['DEFAULT']['MESSAGE']; ?></span>
			<span class="js-bxmaker__geoip__message-value-current"  style="display:none;"><?= $arResult['CURRENT']['MESSAGE']; ?></span>
		<?endif; ?>

        <span class="bxmaker__geoip__message-value  js-bxmaker__geoip__message-value v3"
             data-location="<?= $arParams['LOCATION']; ?>"
             data-city="<?= $arParams['CITY']; ?>"
             data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"
             data-time="<?= $arResult['CURRENT']['TIME']; ?>"
             data-timestart="<?= $arResult['CURRENT']['START']; ?>"
             data-timestop="<?= $arResult['CURRENT']['STOP']; ?>">
			 
			<span style="display:none;"><?= $arResult['CURRENT']['MESSAGE']; ?></span>
        </span>
		<?
	}
