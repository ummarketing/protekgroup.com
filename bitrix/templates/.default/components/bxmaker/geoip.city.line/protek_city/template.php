<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

    use Bitrix\Main\Localization\Loc as Loc;


    $this->setFrameMode(true);

    $randString = $this->randString();
    $BXMAKER_COMPONENT_NAME = 'BXMAKER.GEOIP.CITY.LINE';

    $oManager = \Bxmaker\GeoIP\Manager::getInstance();
	$arResult['CITY_DEFAULT'] = $oManager->getParam('DEFAULT_CITY');
	$arResult['CITY_CURRENT'] = $oManager->getCity();

?>


<div class="bxmaker__geoip__city__line  bxmaker__geoip__city__line--default js-bxmaker__geoip__city__line"
     id="bxmaker__geoip__city__line-id<?= $randString; ?>"
     data-question-show="<?= $arParams['QUESTION_SHOW']; ?>"
     data-info-show="<?= $arParams['INFO_SHOW']; ?>"
     data-debug="<?= $arResult['DEBUG']; ?>"
     data-cookie-prefix="<?= $arParams['COOKIE_PREFIX']; ?>"
     data-fade-timeout="200"
     data-tooltip-timeout="500"
     data-key="<?= $randString; ?>">



    <div class="bxmaker__geoip__city__line__params__id" id="bxmaker__geoip__city__line__params__id<?= $randString; ?>">
        <? $frame = $this->createFrame('bxmaker__geoip__city__line__params__id' . $randString, false)->begin(); ?>
        <div class="js-bxmaker__geoip__city__line__params" data-cookie-domain="<?=$arParams['COOKIE_DOMAIN'];?>"></div>
        <? $frame->beginStub(); ?>
        <div class="js-bxmaker__geoip__city__line__params" data-cookie-domain=""></div>
        <? $frame->end(); ?>
    </div>
	<div class="header__contacts-city">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 80 80" xml:space="preserve"><path d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path></svg>
		<span class="bxmaker__geoip__city__line-label"><?= $arParams['~CITY_LABEL']; ?></span>
		<a href="#modal-location" class="open-modal bxmaker__geoip__city__line-name js-bxmaker__geoip__city__line-name js-bxmaker__geoip__city__line-city"><?=(!empty($arResult['CITY_CURRENT']) ? $arResult['CITY_CURRENT'] : $arResult['CITY_DEFAULT'])?></a>
	</div>
</div>