<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

    use Bitrix\Main\Localization\Loc as Loc;

    $this->setFrameMode(true);

    $randString = $this->randString();

    $COMPONENT_NAME = 'BXMAKER.GEOIP.CITY';

    $oManager = \Bxmaker\GeoIP\Manager::getInstance();

	$arResult['CITY_DEFAULT'] = $oManager->getParam('DEFAULT_CITY');
	$arResult['CITY_CURRENT'] = $oManager->getCity();

?>

<div class="bxmaker__geoip__city bxmaker__geoip__city--default js-bxmaker__geoip__city" id="bxmaker__geoip__city-id<?= $randString; ?>"
     data-debug="<?= $arResult['DEBUG']; ?>"
     data-data-use-domain-redirect="<?= $arParams['USE_DOMAIN_REDIRECT']; ?>"
     data-cookie-prefix="<?= $arParams['COOKIE_PREFIX']; ?>"
     data-reload="<?= $arParams['RELOAD_PAGE']; ?>"
     data-search-show="<?= $arParams['SEARCH_SHOW']; ?>"
     data-favorite-show="<?= $arParams['FAVORITE_SHOW']; ?>"
     data-use-yandex="<?= $arResult['USE_YANDEX']; ?>"
     data-use-yandex-search="<?= $arResult['USE_YANDEX_SEARCH']; ?>"
     data-yandex-search-skip-words="<?= $oManager->getPreparedForHtmlAttr($arResult['YANDEX_SEARCH_SKIP_WORDS']); ?>"
     data-msg-empty-result="<?= $oManager->getPreparedForHtmlAttr($arParams['MSG_EMPTY_RESULT']); ?>"
     data-key="<?= $randString; ?>">

    <div class="bxmaker__geoip__city__composite__params" id="bxmaker__geoip__city__composite__params__id<?= $randString; ?>">
        <? $frame = $this->createFrame('bxmaker__geoip__city__composite__params__id' . $randString, false)->begin(); ?>
        <div class="js-bxmaker__geoip__city__composite__params"
             data-location-domain="<?= $arParams['LOCATION_DOMAIN']; ?>"
             data-cookie-domain="<?= $arParams['COOKIE_DOMAIN']; ?>"></div>
        <? $frame->beginStub(); ?>
        <div class="js-bxmaker__geoip__city__composite__params"
             data-location-domain=""
             data-cookie-domain=""></div>
        <? $frame->end(); ?>
    </div>

	<div id="modal-location" class="zoom-anim-dialog mfp-hide modal modal--location">
		<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
		<div class="modal__content">
			<h6 class="modal__title"><?= str_ireplace('%CITY%', '<span class="bxmaker__geoip__cityname_show">'.($arResult['CITY_CURRENT'] ? $arResult['CITY_CURRENT'] : $arResult['CITY_DEFAULT']).'</span>', $arParams['QUESTION_TEXT'])?></h6>

			<div class="modal__btns">
				<a href="#" class="modal__btn modal__btn--green"><?=$arParams['BTN_CLOSE']?></a>
				<a href="#modal-city" class="modal__btn modal__btn--grey open-modal"><?=$arParams['BTN_EDIT']?></a>
			</div>
		</div>
	</div>

    <div id="modal-city" class="zoom-anim-dialog mfp-hide modal modal--city  <?/*bxmaker__geoip__popup js-bxmaker__geoip__popup*/?>">
		<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
		<div class="modal__content">
			<h6 class="modal__title"><?=$arParams['POPUP_LABEL'];?></h6>

			<div class="row">
			<?
			$tempArray = array();
			foreach($arResult['ITEMS'] as $item){
				$tempArray[$item['NAME']] = $item;
			}
			ksort($tempArray);
			$count = 0;

			$iColRows = ceil(count($tempArray) / 4);
			$arResult['PREPARED_ITEMS'] = array_chunk($tempArray, $iColRows);
			foreach($arResult['PREPARED_ITEMS'] as $column){
				echo '<div class="col-12 col-md-6 col-lg-3">';
				echo '<span class="modal__abc">' . substr($column[0]['NAME'], 0, 1) . ' - ' . substr($column[key(array_slice($column, -1, 1, TRUE))]['NAME'], 0, 1) . '</span>';
				echo '<ul class="modal__city">';
				foreach($column as $el){?>
                    <li>
                        <span class="js-bxmaker__geoip__popup-option" data-id="<?= $el['ID'] ?>">
                            <?= $el['NAME'] ?>
                        </span>
                    </li>
                <?}
				echo '</ul></div>';
			}

			?>
			</div>
		</div>
	</div>
</div>