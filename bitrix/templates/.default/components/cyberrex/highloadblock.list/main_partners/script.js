$(document).ready(function(){
	$('.partners-slider').owlCarousel({
		mouseDrag: true,
		touchDrag: true,
		dots: true,
		loop: true,
		autoplay: true,
		smartSpeed: 600,
		margin: 16,
		autoplayTimeout: 5000,
		autoplayHoverPause: true,
		responsive : {
			0 : {
				items: 2,
			},
			768 : {
				items: 3,
			},
			992 : {
				items: 4,
			},
			1200 : {
				items: 6,
				dots: false,
			},
		}
	});
	$('.partners-nav__btn--next').on('click', function() {
		let slider = $('.partners-slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('next.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
	$('.partners-nav__btn--prev').on('click', function() {
		let slider = $('.partners-slider');
		$(slider).trigger('stop.owl.autoplay');
		$(slider).trigger('prev.owl.carousel');
		$(slider).trigger('play.owl.autoplay');
	});
});