<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}?>
<section class="section section--grey">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title"><?=$arParams["BLOCK_TITLE"]?></h2>
			</div>
			<div class="col-12">
				<div class="partners-slider owl-carousel">
<?
foreach($arResult['rows'] as $item){
	if(!empty($item['UF_LINK'])){
		?><a class="partner" target="_blank" href="<?=$item['UF_LINK']?>">
			<img src="<?
			$arPhotoSmall = CFile::ResizeImageGet(
				$item['UF_ICON_ID'], 
				array(
					'width'=>135,
					'height'=>90
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL,
				false,
				70
			);
			echo $arPhotoSmall['src'];
			?>" alt="<?=$item['UF_TITLE']?>" />
		</a><?
	}else{
		?><span class="partner">
			<img src="<?
			$arPhotoSmall = CFile::ResizeImageGet(
				$item['UF_ICON_ID'], 
				array(
					'width'=>135,
					'height'=>90
				), 
				BX_RESIZE_IMAGE_PROPORTIONAL,
				false,
				70
			);
			echo $arPhotoSmall['src'];
			?>" alt="<?=$item['UF_TITLE']?>" />
		</span><?
	}
}?>
				</div>
				<?if(count($arResult['rows']) > 1){?>
				<div class="partners-nav">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<button class="partners-nav__btn partners-nav__btn--prev" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M1.2,48.8c-1.3-1.3-1.3-3.4,0-4.6L44.3,1c1.3-1.3,3.4-1.3,4.6,0c1.3,1.3,1.3,3.4,0,4.6L8.2,46.5L49,87.3 c1.3,1.3,1.3,3.4,0,4.6c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1L1.2,48.8z M44.3,91.9"/></svg>
								</button>
								<button class="partners-nav__btn partners-nav__btn--next" type="button">
									<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 49.9 93.8" xml:space="preserve"><path class="st0" d="M5.6,91.9c-0.6,0.6-1.4,1-2.3,1c-0.9,0-1.7-0.3-2.3-1c-1.3-1.3-1.3-3.4,0-4.6l40.8-40.8L1,5.7 C-0.3,4.4-0.3,2.3,1,1c1.3-1.3,3.4-1.3,4.6,0l43.1,43.1c1.3,1.3,1.3,3.4,0,4.6L5.6,91.9z M5.6,91.9"/></svg>
								</button>
							</div>
						</div>
					</div>
				</div>
				<?}?>
			</div>
		</div>
	</div>
</section>