<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}?>
<section class="section section--features">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title"><?=$arParams["BLOCK_TITLE"]?></h2>
			</div>

<?
foreach($arResult['rows'] as $item){
	?><div class="col-12 col-md-6 col-lg-3">
			<div class="feature">
				<img src="<?=$item['UF_ICON']?>" alt="<?=$item['UF_TITLE']?>" />
				<p><?=$item['UF_TITLE']?></p>
			</div>
		</div><?
}?>
		</div>
	</div>
</section>