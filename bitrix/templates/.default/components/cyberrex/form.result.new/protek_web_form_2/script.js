$(document).ready(function(){
	$('.form__file-input').on('change', function() {
		
		let taskLabel = $(this).siblings('.form__file-label'),
			fileNote = $(this).parent().parent().siblings('.form__file-note');
			fileCount = $('.form__file-input').parent().siblings('.form__file-hidden').find('input').length + 1;
			
		if ($(this).val() != '') {
			
			let filesArray = $(this)[0].files,
				nameArray = [];
				
			if(filesArray.length == 1){
				taskLabel.text($(this)[0].files[0].name);
				$(fileNote).find('span').text(fileCount);
			}else{
				for (let i = 0; i < filesArray.length; i++) {
					nameArray[nameArray.length] = filesArray[i].name;
				}
				$(fileNote).find('span').text(fileCount);
				$(fileNote).show(400);
				taskLabel.text(nameArray.join(', '));
			}
			
		}else{
			taskLabel.text(taskLabel.data('caption'));
		}
	});
	$('body').on('change', '.cityselection_select_container', function(){
		$(this).siblings(".selected_city").val($(this).find("option:selected").data("email"));
	});
});