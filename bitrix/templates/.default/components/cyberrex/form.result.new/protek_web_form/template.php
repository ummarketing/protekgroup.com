<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<section class="section section--grey">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-8 col-xl-6">
			<?
			$arTempCounters = array();
			foreach($arResult["QUESTIONS"] as $arItem){
				$arTempCounters[] = $arItem['STRUCTURE'][0]['FIELD_TYPE'];
			}
			if ($arResult["isFormTitle"]){?>
				<h6 class="modal__title"><?=$arResult["FORM_TITLE"]?></h6>
			<?}

			if($arResult["isFormErrors"] == "Y"){
				echo '<div class="text-center">' . $arResult["FORM_ERRORS_TEXT"] . '</div>';
			}
			echo '<div class="text-center">' . $arResult["FORM_NOTE"] . '</div>';

				if ($arResult["isFormNote"] != "Y"){
					echo $arResult["FORM_HEADER"];
						?><div class="row"><?
						$count = $group_count = 0;
					foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
						if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'){
							echo $arQuestion["HTML_CODE"];
						}else{
							if($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == "textarea" || $arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == "file"){
								?><div class="col-12"><?
							}else{

								if($group_count == 1){
									?><div class="col-12 col-md-6"><?
								}else if($arTempCounters[$count + 1] == "text" || $arTempCounters[$count + 1] == "email" || $arTempCounters[$count + 1] == "dropdown"){
									?><div class="col-12 col-md-6"><?
								}else{
									?><div class="col-12"><?
								}
								if($group_count == 1){
									$group_count = 0;
								}else{
									$group_count++;
								}
							}

							switch ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']) {
								case "text":
									if(stristr($FIELD_SID, "PHONE")){
										?><input type="tel" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="masked_phone form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>">
                                    <?}elseif(stristr($FIELD_SID, "CITYSELECT")){?>
                                        <!--noindex-->
                                        <select <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="cityselection_select_container form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error";?>">
											<option selected disabled><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></option>
											<?foreach($arResult['CITIES_SELECT'] as $arCity){
												?><option value="<?=$arCity['UF_NAME']?>" data-email="<?=$arCity['UF_EMAIL']?>"><?=(SITE_ID == "s1" ? $arCity['UF_NAME'] : $arCity['UF_NAME_EN'])?></option><?
											}?>
										</select>
                                        <!--/noindex-->
                                    <?}elseif(stristr($FIELD_SID, "SEND_TO_EMAIL")){
										?><input type="hidden" name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="selected_city"><?
									}else{
										?><input type="text" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>"><?
									}
									break;
								case "email":
									?><input type="email" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_email_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>"><?
									break;
								case "textarea":
									?><textarea <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_textarea_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="form__textarea<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>"></textarea><?
									break;
								case "file":
									if(count($arQuestion['STRUCTURE']) == 1){
										?><div class="form__file">
										<span><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></span>
										<div class="form__file-wrap">
											<label for="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-label" data-caption="<?=$arParams['FORM_FILE_CHOOSE']?>"><?=$arParams['FORM_FILE_CHOOSE']?></label>
											<input id="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-input <?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" name="form_file_<?=$arQuestion['STRUCTURE'][0]['ID']?>" type="file" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> >
										</div>
									</div><?
									}else{
									?><div class="form__file">
										<span><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></span>
										<div class="form__file-wrap">
											<label for="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-label" data-caption="<?=$arParams['FORM_FILE_CHOOSE_MULTY']?>"><?=$arParams['FORM_FILE_CHOOSE_MULTY']?></label>
											<input multiple id="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-input <?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" name="form_file_<?=$arQuestion['STRUCTURE'][0]['ID']?>[]" type="file" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> >
										</div>
										<div class="form__file-hidden" style="display:none"><?
										for ($i = 1; $i < count($arQuestion['STRUCTURE']); $i++){
											?><input name="form_file_<?=$arQuestion['STRUCTURE'][$i]['ID']?>" type="file" ><?
										}?></div>
									</div>
									<div class="form__file-note" style="display:none"><?=$arParams['FORM_FILE_CHOOSE_NOTE']?>: <span></span></div><?
									}
									break;
								case "dropdown":
									?><select class="form__select" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_dropdown_<?=$FIELD_SID?>" id="form_dropdown_<?=$FIELD_SID?>">
										<option disabled selected><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></option>
										<?foreach($arQuestion['STRUCTURE'] as $item){
											?><option value="<?=$item['ID']?>"><?=$item['MESSAGE']?></option><?
										}?>
									</select><?
									break;
							}
							?></div><?
						}
						$count++;
					}
					if($arResult["isUseCaptcha"] == "Y"){?>
						<div class="col-12">
							<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
							<?/*<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];*/?>
							<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
						</div>
					<?}?>

						<div class="col-12">
							<br />
							<button <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> class="form__btn 123" name="web_form_submit" type="submit"><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></button>
							<input type="hidden" name="web_form_apply" value="Y" />
							<p class="form__text"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/agreement.php"), false);?></p>
						</div>
					</div><?
					echo $arResult["FORM_FOOTER"];
				}
			?>
			</div>
		</div>
	</div>
</section>
<?if($_REQUEST['formresult'] == 'addok'){
	?><script>
	setTimeout(function() {
		window.location.replace(<?=$APPLICATION->GetCurPage();?>);
    }, 5000);
	</script><?
}?>