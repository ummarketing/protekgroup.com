<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

CModule::IncludeModule('iblock');
CModule::IncludeModule('highloadblock');

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();
foreach($arResult['ITEMS'] as $key => $item){
	if($item['DISPLAY_PROPERTIES']['PACK_TYPE']){
		$hlblock = HL\HighloadBlockTable::getById(9)->fetch();
		   $entity = HL\HighloadBlockTable::compileEntity($hlblock);
		   $entityClass = $entity->getDataClass();

		   $res = $entityClass::getList(array(
			   'select' => array('*'),
			   'filter' => array('UF_XML_ID' => $item['DISPLAY_PROPERTIES']['PACK_TYPE']['VALUE'])
		   ));

		$row = $res->fetch();
		$arResult['ITEMS'][$key]['DISPLAY_PROPERTIES']['PACK_TYPE']['VALUE'] = ($row['UF_LENGTH'] * $row['UF_WIDTH'] * $row['UF_HEIGHT']) / 1000000000;
	}
}