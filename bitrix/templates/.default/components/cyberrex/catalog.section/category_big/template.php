<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);

if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$arMessages['ADD_TO_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arMessages['ADD_TO_CART'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arMessages['ADD_TO_FAVORITES'] = $arParams['~BUTTON_ADD_TO_FAVORITES'];
$arMessages['DELETE_FROM_COMPARE'] = $arParams['~BUTTON_DELETE_FROM_COMPARE'];
$arMessages['DELETE_FROM_FAVORITES'] = $arParams['~BUTTON_DELETE_FROM_FAVORITES'];?>
<script data-skip-moving="true">
var basketData = {
	basketTemplateUrl : "<?=$arResult['~ADD_URL_TEMPLATE']?>",
	quantity : "<?=$arResult['ORIGINAL_PARAMETERS']['PRODUCT_QUANTITY_VARIABLE']?>",
	compareAddTemplate : "<?=$arResult['~COMPARE_URL_TEMPLATE']?>",
	compareDeleteTemplate : "<?=$arResult['~COMPARE_DELETE_URL_TEMPLATE']?>",
	compareAddMessage : "<?=$arMessages['ADD_TO_COMPARE']?>",
	compareRemoveMessage : "<?=$arMessages['DELETE_FROM_COMPARE']?>",
	favoritesAddMessage : "<?=$arMessages['ADD_TO_FAVORITES']?>",
	favoritesRemoveMessage : "<?=$arMessages['DELETE_FROM_FAVORITES']?>",
	favoritesName : "<?=FAVORITES_NAME.SITE_ID?>"
};
</script>
<div class="row">
	<div class="col-12">
		<div class="category__grid">
			<div class="row">
	<?
	if (!empty($arResult['ITEMS'])){
		$areaIds = array();
		?><!-- items-container --><?
		foreach ($arResult['ITEMS'] as $item){
			$uniqueId = $item['ID'].'_'.md5($this->randString().$component->getAction());
			$this->AddEditAction($uniqueId, $item['EDIT_LINK'], $elementEdit);
			$this->AddDeleteAction($uniqueId, $item['DELETE_LINK'], $elementDelete, $elementDeleteParams);
		?>
			<div class="col-12 col-md-6 col-xl-4" id="<?=$areaId?>">
				<div class="catitem">
					<a href="<?=$item['DETAIL_PAGE_URL']?>" class="catitem__img">
						<img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" alt="<?=$item['DETAIL_PICTURE']['ALT']?>">
					</a>
					
					<div class="catitem__content">
						<h2 class="catitem__title"><a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a></h2>
						<ul class="catitem__list">
							<?foreach($item['DISPLAY_PROPERTIES'] as $arProp){?>
							<li><?=$arProp['NAME']?>: <?=$arProp['VALUE']?></li>
							<?}?>
						</ul>
						<?if(!empty($item['PRICES'])){?>
						<div class="catitem__quantity">
							<button type="button" class="catitem__quantity_down quantity-control minus">-</button>
							<input type="text" class="quantity" value="1" name="<?=$arParams['PRODUCT_QUANTITY_VARIABLE']?>">
							<button type="button" class="catitem__quantity_up quantity-control plus">+</button>
						</div>
						<?}?>
						<div class="catitem__action">
							<?if(!empty($item['PRICES'])){?><button class="catitem__add add2basket_btn" data-cyber_id="<?=$item['ID']?>" type="button"><?=$arMessages['ADD_TO_CART']?></button><?}?>
							<?if(!empty($_SESSION[FAVORITES_NAME.SITE_ID][$item['ID']])){?>
								<button class="catitem__favorites in-favorites add2favorites_btn" data-cyber_id="<?=$item['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path></svg><span><?=$arMessages['DELETE_FROM_FAVORITES']?></span></button>
							<?}else{?>
								<button class="catitem__favorites add2favorites_btn" data-cyber_id="<?=$item['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 510 510" style="enable-background:new 0 0 510 510;" xml:space="preserve"><path d="M255,489.6l-35.7-35.7C86.7,336.6,0,257.55,0,160.65C0,81.6,61.2,20.4,140.25,20.4c43.35,0,86.7,20.4,114.75,53.55 C283.05,40.8,326.4,20.4,369.75,20.4C448.8,20.4,510,81.6,510,160.65c0,96.9-86.7,175.95-219.3,293.25L255,489.6z"></path></svg><span><?=$arMessages['ADD_TO_FAVORITES']?></span></button>
							<?}?>
							<?if(!empty($_SESSION[$arResult['ORIGINAL_PARAMETERS']['COMPARE_NAME']][$arResult['ORIGINAL_PARAMETERS']['IBLOCK_ID']]['ITEMS'][$item['ID']])){?>
								<button class="catitem__compare add2compare_btn in-compare" data-cyber_id="<?=$item['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve"><path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path><path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path><path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path><path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path><path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path></svg><span><?=$arMessages['DELETE_FROM_COMPARE']?></span></button>
							<?}else{?>
							<button class="catitem__compare add2compare_btn in-compare" data-cyber_id="<?=$item['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 511.626 511.627" style="enable-background:new 0 0 511.626 511.627;" xml:space="preserve"><path d="M63.953,401.992H9.135c-2.666,0-4.853,0.855-6.567,2.567C0.859,406.271,0,408.461,0,411.126v54.82 c0,2.665,0.855,4.855,2.568,6.563c1.714,1.718,3.905,2.57,6.567,2.57h54.818c2.663,0,4.853-0.853,6.567-2.57 c1.712-1.708,2.568-3.898,2.568-6.563v-54.82c0-2.665-0.856-4.849-2.568-6.566C68.806,402.848,66.616,401.992,63.953,401.992z"></path><path d="M283.225,292.36h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.57,3.901-2.57,6.563v164.456 c0,2.665,0.856,4.855,2.57,6.563c1.713,1.718,3.899,2.57,6.567,2.57h54.818c2.665,0,4.855-0.853,6.563-2.57 c1.711-1.708,2.573-3.898,2.573-6.563V301.49c0-2.662-0.862-4.853-2.573-6.563C288.081,293.216,285.89,292.36,283.225,292.36z"></path><path d="M173.589,365.451h-54.818c-2.667,0-4.854,0.855-6.567,2.566c-1.711,1.711-2.568,3.901-2.568,6.563v91.358 c0,2.669,0.854,4.859,2.568,6.57c1.713,1.711,3.899,2.566,6.567,2.566h54.818c2.663,0,4.853-0.855,6.567-2.566 c1.709-1.711,2.568-3.901,2.568-6.57v-91.358c0-2.662-0.859-4.853-2.568-6.563C178.442,366.307,176.251,365.451,173.589,365.451z"></path><path d="M392.857,182.725h-54.819c-2.666,0-4.856,0.855-6.57,2.568c-1.708,1.714-2.563,3.901-2.563,6.567v274.086 c0,2.665,0.855,4.855,2.563,6.563c1.714,1.718,3.904,2.57,6.57,2.57h54.819c2.666,0,4.856-0.853,6.563-2.57 c1.718-1.708,2.57-3.898,2.57-6.563V191.86c0-2.666-0.853-4.853-2.57-6.567C397.713,183.58,395.523,182.725,392.857,182.725z"></path><path d="M509.06,39.115c-1.718-1.714-3.901-2.568-6.57-2.568h-54.816c-2.662,0-4.853,0.854-6.567,2.568 c-1.714,1.709-2.569,3.899-2.569,6.563v420.268c0,2.665,0.855,4.855,2.569,6.563c1.715,1.718,3.905,2.57,6.567,2.57h54.816 c2.669,0,4.853-0.853,6.57-2.57c1.711-1.708,2.566-3.898,2.566-6.563V45.679C511.626,43.015,510.771,40.825,509.06,39.115z"></path></svg><span><?=$arMessages['ADD_TO_COMPARE']?></span></button>
							<?}?>
						</div>
					</div>
				</div>
			</div>
		<?}?><!-- items-container --><?
	}?>
			</div>
		</div>
	</div>
	<div class="col-12" data-pagination-num="<?=$navParams['NavNum']?>">
	<!-- pagination-container -->
	<?=$arResult['NAV_STRING']?>
	<!-- pagination-container -->
	</div>
</div>

<?
$signer = new \Bitrix\Main\Security\Sign\Signer;
$signedTemplate = $signer->sign($templateName, 'catalog.section');
$signedParams = $signer->sign(base64_encode(serialize($arResult['ORIGINAL_PARAMETERS'])), 'catalog.section');
?>
<!-- component-end -->