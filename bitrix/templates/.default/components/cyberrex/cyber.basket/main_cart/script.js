$(document).ready(function(){
	initPopup();
	$('body').on('input change', '.cyber_quantity', function() { //Только цифры в инпутах
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		if(this.value == 0){
			this.value = 1;
		}
		changeQuantity($(this));
	});
	$('body').on('click', '.cyber_quantity-control', function(){ //Изменение количества (клики по плюсу и минусу)
		let quantutyInput = $(this).siblings('input'),
			quantity = 1;
		if($(this).hasClass('minus')) {
			
			if(quantutyInput.val() > 1) { //Не изменяем количество если после нажатия на минус в поле должен появиться ноль
				quantutyInput.val(Number(quantutyInput.val()) - 1);
				changeQuantity($(this));
			}
			
		} else {
			quantutyInput.val(Number(quantutyInput.val()) + 1);
			changeQuantity($(this));
		}
		
	});
	
	$('body').on('click', '.cart__clean', function(){ // Очистить
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "clear_basket=true&cyber_ajax=y",
			success: function(data) {
				
				$('.cyber_maincart-container').html($(data).find('.cyber_maincart-container').html());
				initPopup();
				$('body').trigger('cyber.change.basket.basket');
			}
		});
	});
	
	$('body').on('click', '.cart__item-delete', function(){ // Удалить конкретный товар
		
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "remove_from_basket=" + $(this).data('cyber_id') + "&cyber_ajax=y",
			success: function(data) {
				
				$('.cyber_maincart-container').html($(data).find('.cyber_maincart-container').html());
				initPopup();
				$('body').trigger('cyber.change.basket.basket');
			}
		});
	});
	
	
	function changeQuantity(element){ // Изменение количества в коризне
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "cyber_ajax=y&change_quantity=" + element.parents('.cart__item-quantity').find('.cyber_quantity').val() + "&changed_item=" + element.parents('.cart__item-quantity').data('cyber_id'),
			success: function(data) {
				
				$('.cyber_maincart-container').html($(data).find('.cyber_maincart-container').html());
				initPopup();
				$('body').trigger('cyber.change.basket.basket');
			}
		});
	}
	
	function initPopup(){
		$('.cartwrap-open-modal').magnificPopup({
			fixedContentPos: true,
			fixedBgPos: true,
			overflowY: 'auto',
			type: 'inline',
			preloader: false,
			modal: false,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in',
			callbacks: {
				open: function() {
					if ($(window).width() > 991) {
						$('.nav--scroll').css('margin-left', "-" + (getScrollBarWidth()/2) + "px");
					}

					if ($(window).width() > 1199) {
						$('.cart-btn-wrap').css('margin-right', "-" + ( 705 - (getScrollBarWidth()/2)) + "px");
					}
				},
				close: function() {
					if ($(window).width() > 991) {
						$('.nav--scroll').css('margin-left', 0);
					}

					if ($(window).width() > 1199) {
						$('.cart-btn-wrap').css('margin-right', -705);
					}
				}
			}
		});
		$('.cartwrap-open-modal').on('click', function(){
			$('body').trigger('cyber.open.basket.modal');
		});
	}
	function getScrollBarWidth () {
		var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
			widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
		$outer.remove();
		return 100 - widthWithScroll;
	};
	
});