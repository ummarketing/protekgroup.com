<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if(!empty($arResult['BASKET'])){?>
<section class="section section--top0">
	<div class="container">
		<div class="row cyber_maincart-container">
			<div class="col-12">
				<div class="cart">
					<div class="cart__info">
						<div class="cart__values cart__values--head">
							<p><?=$arParams["BASKET_SUM_ITEMS"]?>: <b class="cyberbasket_sum_items"><?=count($arResult['BASKET'])?></b></p>
							<p><?=$arParams["BASKET_SUM_VOLUME"]?>: <b class="cyberbasket_sum_volume"><?
							$sumVolume = 0;
							foreach($arResult['BASKET'] as $item){
								$sumVolume += $item['QUANTITY'] * ($item['PROPERTIES']['PACK_TYPE']['VALUE'] ? $item['PROPERTIES']['PACK_TYPE']['VALUE'] : (floatval($item['PROPERTIES']['PACK_VOLUME']['VALUE']) ? floatval($item['PROPERTIES']['PACK_VOLUME']['VALUE']) : 0));
							}
							echo $sumVolume;
							?></b></p>
							<p><?=$arParams["BASKET_SUM_WEIGHT"]?>: <b class="cyberbasket_sum_weight"><?
							$sumWeight = 0;
							foreach($arResult['BASKET'] as $item){
								$sumWeight += $item['QUANTITY']* $item['PROPERTIES']['PACK_WEIGHT']['VALUE'];
							}
							echo $sumWeight;
							?></b></p>
							<p><?=$arParams["BASKET_SUM_PACKS"]?>: <b class="cyberbasket_sum_packs"><?
							$sumPacks = 0;
							foreach($arResult['BASKET'] as $item){
								$sumPacks += $item['QUANTITY'];
							}
							echo $sumPacks;?></b></p>
						</div>

						<button class="cart__clean" type="button"><?=$arParams['BASKET_CLEAR']?></button>
					</div>

					<div class="cart__items">
						<?foreach($arResult['BASKET'] as $key => $item){?>
						<div class="cart__item">
							<img class="cart__item-img" src="<?
								$arWaterMark = Array(
									array(
										"name" => "watermark",
										"position" => "center",
										"type" => "image",
										"size" => "medium",
										"file" => WATERMARK_PATH,
										"fill" => "exact",
										"alpha_level" => 26
									)
								);
								$arPhotoSmall = CFile::ResizeImageGet(
								$item['PREVIEW_PICTURE'], 
								array(
									'width'=>640,
									'height'=>450
								), 
								BX_RESIZE_IMAGE_EXACT,
								false,
								$arWaterMark,
								85
							);
							echo $arPhotoSmall['src'];?>" alt="<?=$item['NAME']?>">

							<div class="cart__item-meta">
								<a href="<?=$item['DETAIL_PAGE_URL']?>"><h3><?=$item['NAME']?></h3></a>
								<?$arDisplayedProps = array(
									"ART",
									"MATERIAL",
									"PACK_QUANTITY",
									"PACK_TYPE",
									"PACK_VOLUME",
									"OUTTER_SIZES"
								);
								?><ul><?
								foreach($arDisplayedProps as $key){
									if(!empty($item['PROPERTIES'][$key]['VALUE'])){
										?><li><?=$item['PROPERTIES'][$key]['NAME']?>: <?=(is_array($item['PROPERTIES'][$key]['VALUE']) ? implode($item['PROPERTIES'][$key]['VALUE'], ", ") : $item['PROPERTIES'][$key]['VALUE'])?></li><?
									}
								}
								?></ul>
							</div>

							<div class="cart__item-action">
								<span><?=$arParams["PACKS_QUANTITY_TITLE"]?></span>
								<div class="cart__item-quantity"  data-cyber_id="<?=$item['ID']?>">
									<button type="button cyber_quantity-control minus">-</button>
									<input type="text" class="cyber_quantity" value="<?=$item['QUANTITY']?>">
									<button type="button" class="cyber_quantity-control plus">+</button>
								</div>
								<?if(!empty($item['PROPERTIES']['PACK_QUANTITY']['VALUE'])){?><p><?=$arParams["PACKS_STUCKS_TITLE"]?>: <span class="cyberbasket_item_stucks"><?=intVal($item['PROPERTIES']['PACK_QUANTITY']['VALUE'])*intVal($item['QUANTITY'])?></span></p><?}?>
							</div>
							
							<button class="cart__item-delete" type="button" data-cyber_id="<?=$item['ID']?>">
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></svg>
							</button>
						</div>
						<?}?>
					</div>
				</div>
			</div>

			<div class="col-12 col-md-5 offset-md-7 col-lg-4 offset-lg-8">
				<div class="cart__total">
					<div class="cart__values">
						<p><?=$arParams["BASKET_SUM_ITEMS"]?>: <b class="cyberbasket_sum_items"><?=count($arResult['BASKET'])?></b></p>
						<p><?=$arParams["BASKET_SUM_VOLUME"]?>: <b class="cyberbasket_sum_volume"><?
						$sumVolume = 0;
						foreach($arResult['BASKET'] as $item){
							$sumVolume += $item['QUANTITY'] * ($item['PROPERTIES']['PACK_TYPE']['VALUE'] ? $item['PROPERTIES']['PACK_TYPE']['VALUE'] : (floatval($item['PROPERTIES']['PACK_VOLUME']['VALUE']) ? floatval($item['PROPERTIES']['PACK_VOLUME']['VALUE']) : 0));
						}
						echo $sumVolume;
						?></b></p>
						<p><?=$arParams["BASKET_SUM_WEIGHT"]?>: <b class="cyberbasket_sum_weight"><?
						$sumWeight = 0;
						foreach($arResult['BASKET'] as $item){
							$sumWeight += $item['QUANTITY']* $item['PROPERTIES']['PACK_WEIGHT']['VALUE'];
						}
						echo $sumWeight;
						?></b></p>
						<p><?=$arParams["BASKET_SUM_PACKS"]?>: <b class="cyberbasket_sum_packs"><?
						$sumPacks = 0;
						foreach($arResult['BASKET'] as $item){
							$sumPacks += $item['QUANTITY'];
						}
						echo $sumPacks;?></b></p>
					</div>
					<a href="#modal-price" class="cart__btn open-modal cartwrap-open-modal"><?=$arParams['BASKET_ORDER_PRICES']?></a>
				</div>
			</div>
		</div>
	</div>
</section>
<?}else{?>
<section class="section section--top0">
	<div class="container">
		<div class="row cyber_maincart-container">
			<div class="col-12">
				<h2 class="section__title"><?=$arParams['BASKET_TITLE']?></h2>
				<p class=" news_article_container"><?=$arParams['BASKET_IS_EMPTY']?></p>
			</div>
		</div>
	</div>
</section>
<?}?>