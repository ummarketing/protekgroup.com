<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="section section--top0 section--bottom0">
	<div class="container">
		<div class="row post-slider">
			<?foreach($arResult['SECTIONS'] as $arSect){?>
				<div class="post col-lg-4 col-sm-6 col-xs-12" title="<?=$arSect['NAME']?>">
					<h3 class="post__title scroll-to" data-scroll-direction="#<?=$arSect['CODE']?>"><?=$arSect['NAME']?></h3>
					<div class="post__img">
						<img src="<?
							$arPhotoSmall = CFile::ResizeImageGet(
								$arSect['PICTURE']['ID'], 
								array(
									'width'=>280,
									'height'=>185
								), 
								BX_RESIZE_IMAGE_EXACT,
								false,
								70
							);
							echo $arPhotoSmall['src'];	
						?>" alt="<?=$arSect['NAME']?>">
						<a class="scroll-to" data-scroll-direction="#<?=$arSect['CODE']?>"><?=$arSect['DESCRIPTION']?></a>
					</div>
				</div>
			<?}?>
		</div>
	</div>
</section>
<hr />
<?foreach($arResult['SECTIONS'] as $arSect){
	if(count($arSect['ELEMENTS']) > 0){?>
	<section class="section" id="<?=$arSect['CODE']?>">
		<div class="container">
			<div class="row products-slider">
				<div class="col-12">
					<h3 class="section__title"><?=$arSect['NAME']?></h3>
				</div>
				<?foreach($arSect['ELEMENTS'] as $arEl){?>
					<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
						<div class="product">
							<a class="product__img" href="<?=SITE_TEMPLATE_PATH?>/components/bitrix/catalog.section.list/lp4_main/ajax.php?product=<?=$arEl['ID']?>">
								<img src="<?
									$arPhotoSmall = CFile::ResizeImageGet(
										$arEl['PREVIEW_PICTURE'], 
										array(
											'width'=>520,
											'height'=>340
										), 
										BX_RESIZE_IMAGE_EXACT,
										false,
										70
									);
									echo $arPhotoSmall['src'];
								?>" alt="<?=$arEl['NAME']?>">
							</a>
							<div class="product__content">
								<div class="product__title">
									<a href="<?=$arEl['DETAIL_PAGE_URL']?>"><?=$arEl['NAME']?></a>
								</div>
							</div>
						</div>
					</div>
				<?}?>
			</div>
		</div>
	</section>
	<?}
}?>