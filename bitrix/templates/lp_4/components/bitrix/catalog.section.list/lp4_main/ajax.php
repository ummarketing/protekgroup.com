<?define("NO_KEEP_STATISTIC", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

if($_REQUEST["product"]){
	
	$ID = htmlspecialchars((string)$_REQUEST["product"]);
	$arElement = array();
	$arDisplayProps = array( // Codes of properties for showing in modal
		"ART",
		"MATERIAL",
		//"USING",
		//"PACK_QUANTITY",
		"PACK_TYPE",
		"PACK_VOLUME",
		"OUTER_LENGTH",
		"OUTER_WIDTH",
		"OUTER_HEIGHT",
		"OUTER_DIAMETER"
	);
	if(!CModule::IncludeModule('iblock'))
		return;
	$arWaterMark = Array(
		array(
			"name" => "watermark",
			"position" => "center",
			"type" => "image",
			"size" => "real",
			"file" => WATERMARK_PATH,
			"fill" => "exact",
			"alpha_level" => 26
		)
	);
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PREVIEW_PICTURE", "DETAIL_PICTURE", "DETAIL_PAGE_URL");
	$arFilter = Array("IBLOCK_ID"=>40, "ID"=>$ID, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()){ 
		$arElement = $ob->GetFields();  
		$arElement['PROPERTIES'] = $ob->GetProperties();
	}
	?>
<div id="custom-content" class="white-popup-block">
	<div class="row">
		<div class="col-12">
			<h3 class="section__title2"><a href="<?=$arElement['DETAIL_PAGE_URL']?>"><?=$arElement['NAME']?></a></h3>
		</div>
		<div class="d-none d-sm-block col-12 col-md-3 col-sm-3 lp4_modal-thumbs">
			<?if(!empty($arElement['PROPERTIES']['MORE_PHOTO']['VALUE']) && count($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'] > 0)){
				?><img class="img-fluid" data-slick_slide="0" src="<?
					$arPhotoSmall = CFile::ResizeImageGet(
						$arElement['DETAIL_PICTURE'], 
						array(
							'width'=>200,
							'height'=>200
						), 
						BX_RESIZE_IMAGE_EXACT,
						false,
						70
					);
					echo $arPhotoSmall['src'];
				?>" /><?
				$i = 1;
				foreach($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo){
					?><img class="img-fluid" data-slick_slide="<?=$i?>" src="<?
					$arPhotoSmall = CFile::ResizeImageGet(
						$photo, 
						array(
							'width'=>200,
							'height'=>200
						), 
						BX_RESIZE_IMAGE_EXACT,
						false,
						70
					);
					echo $arPhotoSmall['src'];
					?>" /><?
					$i++;
				}
			}?>
		</div>
		<div class="col-12 <?=(!empty($arElement['PROPERTIES']['MORE_PHOTO']['VALUE']) && count($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'] > 0) ? "col-md-9 col-sm-9" : "")?>">
			<div class="lp4_modal-slider">
				<div class="lp4_modal-slide">
					<img class="img-fluid" src="<?
						$arPhotoSmall = CFile::ResizeImageGet(
							$arElement['DETAIL_PICTURE'], 
							array(
								'width'=>600,
								'height'=>400
							), 
							BX_RESIZE_IMAGE_EXACT,
							false,
							$arWaterMark,
							80
						);
						echo $arPhotoSmall['src'];
					?>" />
				</div>
				<?if(!empty($arElement['PROPERTIES']['MORE_PHOTO']['VALUE']) && count($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'] > 0)){
					foreach($arElement['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo){
						?><div class="lp4_modal-slide">
						<img class="img-fluid" src="<?
							$arPhotoSmall = CFile::ResizeImageGet(
								$photo, 
								array(
									'width'=>600,
									'height'=>400
								), 
								BX_RESIZE_IMAGE_EXACT,
								false,
								$arWaterMark,
								80
							);
							echo $arPhotoSmall['src'];
						?>" />
					</div><?
					}
				}?>
			</div>
			
			<div class="lp4_product-info">
				<ul>
					<?foreach($arDisplayProps as $key){
						if(isset($arElement['PROPERTIES'][$key]) && !empty($arElement['PROPERTIES'][$key]['VALUE'])){
							?><li> <?
							echo $arElement['PROPERTIES'][$key]['NAME'] . ": ";
							
							switch ($key) {
								case "MATERIAL":
									echo implode (" / ", $arElement['PROPERTIES'][$key]['VALUE']);
									break;
								case "USING":
									$arS = $arElement['PROPERTIES'][$key]['VALUE'];
									unset($arElement['PROPERTIES'][$key]['VALUE']);
									
									$rsSections = CIBlockSection::GetList(array("SORT"=>"ASC"), array('IBLOCK_ID' => intVal($arElement['PROPERTIES'][$key]['LINK_IBLOCK_ID']), 'ID' => $arS));
									while($arSection = $rsSections->Fetch()){
										$arElement['PROPERTIES'][$key]['VALUE'][] = $arSection['NAME'];
									}
									echo implode (", ", $arElement['PROPERTIES'][$key]['VALUE']);
									break;
								case "PACK_TYPE":
									CModule::IncludeModule('highloadblock');
									
									$hlblock = HL\HighloadBlockTable::getById(9)->fetch();
									$entity = HL\HighloadBlockTable::compileEntity($hlblock);
									$entityClass = $entity->getDataClass();
									$res1 = $entityClass::getList(array(
									   'select' => array('*'),
									   'filter' => array('UF_XML_ID' => $arElement['PROPERTIES'][$key]['VALUE'])
									));

									$row = $res1->fetch();
									echo ($row['UF_LENGTH'] * $row['UF_WIDTH'] * $row['UF_HEIGHT']) / 1000000000;
									break;
								default: 
									echo $arElement['PROPERTIES'][$key]['VALUE'];
								
							}
							?></li><?
						}
					}?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?
	die();
	
}else{
	header( "HTTP/1.1 404 Not Found" );
	exit();
}
?>