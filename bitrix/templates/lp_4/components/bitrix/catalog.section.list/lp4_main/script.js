$(document).ready(function() {
	$('.product__img').magnificPopup({
		type: 'ajax',
		alignTop: true,
		overflowY: 'scroll',
		mainClass: 'mfp-fade',
		callbacks: {
			ajaxContentAdded: function() {
				//console.log('ajax-modal init');
				$('.lp4_modal-slider').slick({
					autoplay: false,
					dots: false,
					arrows: false,
					draggable: true,
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					adaptiveHeight: true
					
				});
				$('.lp4_modal-thumbs img').click(function(e) {
					var slideno = $(this).data('slick_slide');
					$('.lp4_modal-slider').slick('slickGoTo', slideno);
				});
			}
		}
	});
});