<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSelect   = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "DETAIL_PICTURE", "PROPERTY_LP4_SHOW", "PROPERTY_LP4_SORT");
$arFilter   = Array("IBLOCK_ID" => 40, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "!PROPERTY_LP4_SHOW" => false);
$res        = CIBlockElement::GetList(Array('PROPERTY_LP4_SORT' => 'ASC'), $arFilter, false, Array(), $arSelect);
$arElements = array();


while($ob = $res->GetNextElement()){
	$arElement                                           = $ob->GetFields();
	$arElement['PROPERTIES']                             = $ob->GetProperties();
	$arElements[$arElement['PROPERTY_LP4_SHOW_VALUE']][] = $arElement;
}


foreach($arResult['SECTIONS'] as $key => $arSect){
	$arResult['SECTIONS'][$key]['ELEMENTS'] = $arElements[$arSect['ID']];
}
?>