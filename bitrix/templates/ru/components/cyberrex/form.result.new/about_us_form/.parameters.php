<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"FORM_UNIQUE_CLASS" => Array(
		"NAME" => "Уникальный идентификатор формы",
		"TYPE" => "STRING",
	),
	"FORM_REQUIRE_NOTE" => Array(
		"NAME" => "Обозначение обязательных полей",
		"TYPE" => "STRING",
	),
	"FORM_FILE_CHOOSE" => Array(
		"NAME" => "Подпись Выбрать файл, для полей типа Файл",
		"TYPE" => "STRING",
	),
	"FORM_FILE_CHOOSE_MULTY" => Array(
		"NAME" => "Подпись Выбрать файлы, для множественных полей типа Файл",
		"TYPE" => "STRING",
	),
	"FORM_FILE_CHOOSE_NOTE" => Array(
		"NAME" => "Пометка Не более пяти файлов",
		"TYPE" => "STRING",
	),
);
?>
