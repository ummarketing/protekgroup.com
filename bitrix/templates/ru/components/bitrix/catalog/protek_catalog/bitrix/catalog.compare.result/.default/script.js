$(document).ready(function(){
	var compareCarousel = $('.compare__slider');

	compareCarousel.owlCarousel({
		mouseDrag: true,
		touchDrag: true,
		dots: false,
		loop: $('.compare__item').length > 1,
		autoplay: false,
		smartSpeed: 600,
		margin: 10,
		autoplayTimeout: 2000,
		items: 2,
		responsive : {
			992 : {
				items: 3
			},
			1200 : {
				items: 4,
				margin: 20
			},
		},
	});

	$('.compare-nav__btn--next').on('click', function() {
		compareCarousel.trigger('next.owl.carousel');
	});
	$('.compare-nav__btn--prev').on('click', function() {
		compareCarousel.trigger('prev.owl.carousel');
	});
	
	$('body').on('change', '#compare1', function() {

		if (this.checked) {
			$('.no-diff').fadeOut(100);
		}else{
			$('.no-diff').fadeIn(100);
		}
	});
});