<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(count($arResult['ITEMS']) > 1){
	$arMatches = array();
	foreach($arResult['SHOW_PROPERTIES'] as $prop){
		foreach($arResult['ITEMS'] as $key => $item){
			if(isset($arMatches[$prop['CODE']]['VALUE'])){
				if($arMatches[$prop['CODE']]['VALUE'] != $item['PROPERTIES'][$prop['CODE']]['VALUE']){
					$arMatches[$prop['CODE']]['DIFF'] = true;
				}
			}else{
				$arMatches[$prop['CODE']]['VALUE'] = $item['PROPERTIES'][$prop['CODE']]['VALUE'];
			}
			if(!empty($item['PREVIEW_PICTURE']['SRC'])){
				$arPhotoSmall = CFile::ResizeImageGet(
					$item['PREVIEW_PICTURE']['ID'], 
					array(
						'width'=>220,
						'height'=>155
					), 
					BX_RESIZE_IMAGE_EXACT,
					false,
					70
				);
				$arResult['ITEMS'][$key]['PREVIEW_PICTURE']['SRC'] =  $arPhotoSmall['src'];
			}
		}
	}
	foreach($arMatches as $key => $match){
		if(!$match['DIFF']){
			$arResult['SHOW_PROPERTIES'][$key]['NO_DIFFERENCES'] = 1;
		}
	}
}else{
	foreach($arResult['SHOW_PROPERTIES'] as $key => $prop){
		$arResult['SHOW_PROPERTIES'][$key]['NO_DIFFERENCES'] = 1;
	}
}
