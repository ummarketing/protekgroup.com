<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach($arResult['SECTIONS'] as $key => $arSect){
	
	if($arSect['RELATIVE_DEPTH_LEVEL'] > 1){
		
		foreach($arResult['SECTIONS'] as $key1 => $arSect1){
			
			if($arSect1['ID'] == $arSect['IBLOCK_SECTION_ID']){
				
				$arResult['SECTIONS'][$key1]['SECTIONS'][] = $arSect;
				unset($arResult['SECTIONS'][$key]);
			}
		}
	}
}
?>