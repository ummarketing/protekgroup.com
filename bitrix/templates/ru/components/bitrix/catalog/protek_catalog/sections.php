<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */


?>


<section class="section section--top0 section--bottom0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title">
                    <? $APPLICATION->ShowTitle(false); ?>
                </h1>
                <? if (0): ?>
                    <? if (!empty($arParams['DOWNLOAD_PRICELIST_FILE'])): ?>
                        <div class="section__download2">
                            <a href="<?= $arParams['DOWNLOAD_PRICELIST_FILE'] ?>" class="download__link" download>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512"
                                     style="enable-background:new 0 0 512 512;" xml:space="preserve"><path
                                            d="M498.966,339.946c-7.197,0-13.034,5.837-13.034,13.034v49.804c0,28.747-23.388,52.135-52.135,52.135H78.203 c-28.747,0-52.135-23.388-52.135-52.135V352.98c0-7.197-5.835-13.034-13.034-13.034C5.835,339.946,0,345.782,0,352.98v49.804 c0,43.121,35.082,78.203,78.203,78.203h355.594c43.121,0,78.203-35.082,78.203-78.203V352.98 C512,345.782,506.165,339.946,498.966,339.946z"></path>
                                    <path d="M419.833,391.3H92.167c-7.197,0-13.034,5.837-13.034,13.034s5.835,13.034,13.034,13.034h327.665 c7.199,0,13.034-5.835,13.034-13.034C432.866,397.137,427.031,391.3,419.833,391.3z"></path>
                                    <path d="M387.919,207.93c-4.795-5.367-13.034-5.834-18.404-1.038l-100.482,89.765V44.048c0-7.197-5.835-13.034-13.034-13.034 c-7.197,0-13.034,5.835-13.034,13.034v252.609l-100.482-89.764c-5.367-4.796-13.607-4.328-18.404,1.038 c-4.794,5.369-4.331,13.609,1.037,18.404l109.174,97.527c6.187,5.529,13.946,8.292,21.708,8.292 c7.759,0,15.519-2.763,21.708-8.289l109.174-97.53C392.25,221.537,392.714,213.297,387.919,207.93z"></path>
                                </svg><?= $arParams['DOWNLOAD_PRICELIST'] ?></a>
                            <a href="/pdf_catalog/" class="download__link" target="_blank">
                                <img src="/local/assets/img/icon-eye.png" alt="">Смотреть каталог
                            </a>
                        </div>
                    <? endif; ?>
                <? endif; ?>
			</div>
		</div>
	</div>
</section>



<? $APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		'DOWNLOAD_PRICELIST' => $arParams['DOWNLOAD_PRICELIST'],
		'DOWNLOAD_PRICELIST_FILE' => $arParams['DOWNLOAD_PRICELIST_FILE'],
		'MORE_CAPTION' => $arParams['MORE_CAPTION'],
		"SECTION_USER_FIELDS" => array("UF_SHORT_DESCRIPTION"),
	),
	$component,
	($arParams["SHOW_TOP_ELEMENTS"] !== "N" ? array("HIDE_ICONS" => "Y") : array())
);