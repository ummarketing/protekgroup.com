<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
$arRowParams = array(
	"LOCKER",
	"PERFORATION",
	"BRANDING",
	"SEALING",
	"FREEZING",
	"WARMING",
	"SPIKES",
);
$arRowType = array(
	"MATERIAL",
	"USING",
	"FORM",
	"COLOR",
	"REGION",
);?>
<div class="filter bx-filter">
	<div class="collapse show" id="collapseFilter">
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<div class="row">
				<?foreach($arResult["HIDDEN"] as $arItem){?>
				<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
				<?}?>
				<div class="col-12 col-md-4">
					<span class="filter__title"><?=$arParams["FILTER_TYPE_CAPTION"]?></span>
					<input type="text" class="filter__search" name="search_name" id="search_name" placeholder="<?=$arParams["FILTER_SEARCH_NAME"]?>">
					<input type="text" class="filter__search" name="search_art" id="search_art" placeholder="<?=$arParams["FILTER_SEARCH_ART"]?>">
					<?foreach($arResult["ITEMS"] as $key=>$arItem){
						if(!in_array($arItem["CODE"], $arRowType))
							continue;
						if(empty($arItem["VALUES"]))
							continue;

						if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
							continue;
						?>
						<div class="bx-filter-parameters-box">
							<span class="bx-filter-container-modef" style="display:none"></span>
							<div class="bx-filter-block filter__collapse_container dropdown" data-role="bx_filter_block" title="<?=$arItem['NAME']?>">
								<a id="filter_collapse_<?=$key?>" class="filter_select dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$arItem['NAME']?></a>
								<div class="filter__collapse dropdown-menu" aria-labelledby="filter_collapse_<?=$key?>">
									<?foreach($arItem["VALUES"] as $val => $ar){?>
									<div class="category__check">
										<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
											/>
										<label for="<? echo $ar["CONTROL_ID"] ?>" data-role="label_<?=$ar["CONTROL_ID"]?>" <? echo $ar["DISABLED"] ? 'disabled': '' ?>>
										<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?></span></label>
									</div>
									<?}?>
								</div>
							</div>
						</div>
					<?}?>
					
					
				</div>
				<div class="col-12 col-md-4">
					<span class="filter__title"><?=$arParams["FILTER_SIZES_CAPTION"]?></span>
					<?foreach($arResult["ITEMS"] as $key=>$arItem){
					if(empty($arItem["VALUES"]))
						continue;

					if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
						continue;
					$arCur = current($arItem["VALUES"]);
						if($arItem["DISPLAY_TYPE"] == "B" || $arItem["DISPLAY_TYPE"] == "A"){?>
							<div class="bx-filter-parameters-box">
								<span class="bx-filter-container-modef" style="display:none"></span>
								<div class="bx-filter-block" data-role="bx_filter_block">
									<span class="filter_price_title"><?=$arItem['NAME']?></span>
									<div class="row">
										<div class="col-6 bx-filter-parameters-box-container-block">
											<i class="bx-ft-sub"><?=$arParams['FILTER_NUM_FROM']?></i>
											<div class="bx-filter-input-container">
												<input
													class="min-price"
													type="text"
													name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
													id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
													value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
													size="5"
													onkeyup="smartFilter.keyup(this)"
												/>
											</div>
										</div>
										<div class="col-6 bx-filter-parameters-box-container-block">
											<i class="bx-ft-sub"><?=$arParams['FILTER_NUM_TO']?></i>
											<div class="bx-filter-input-container">
												<input
													class="max-price"
													type="text"
													name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
													id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
													value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
													size="5"
													onkeyup="smartFilter.keyup(this)"
												/>
											</div>
										</div>

										<div class="col-12 bx-ui-slider-track-container">
											<div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
												<?
												$precision = $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0;
												$step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
												$value1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
												$value2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
												$value3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
												$value4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
												$value5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
												?>
												<div class="bx-ui-slider-part p1"><span><?=$value1?></span></div>
												<div class="bx-ui-slider-part p2"><span><?=$value2?></span></div>
												<div class="bx-ui-slider-part p3"><span><?=$value3?></span></div>
												<div class="bx-ui-slider-part p4"><span><?=$value4?></span></div>
												<div class="bx-ui-slider-part p5"><span><?=$value5?></span></div>

												<div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
												<div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
												<div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
												<div class="bx-ui-slider-range" 	id="drag_tracker_<?=$key?>"  style="left: 0;right: 0;">
													<a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
													<a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
												</div>
											</div>
										</div>
									</div>
									<?
									$arJsParams = array(
										"leftSlider" => 'left_slider_'.$key,
										"rightSlider" => 'right_slider_'.$key,
										"tracker" => "drag_tracker_".$key,
										"trackerWrap" => "drag_track_".$key,
										"minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
										"maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
										"minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
										"maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
										"curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
										"curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
										"fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"] ,
										"fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
										"precision" => $arItem["DECIMALS"]? $arItem["DECIMALS"]: 0,
										"colorUnavailableActive" => 'colorUnavailableActive_'.$key,
										"colorAvailableActive" => 'colorAvailableActive_'.$key,
										"colorAvailableInactive" => 'colorAvailableInactive_'.$key,
									);
									?>
									<script type="text/javascript">
										BX.ready(function(){
											window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
										});
									</script>									
								</div>
							</div>
						<?}
					}?>
				</div>
				<div class="col-12 col-md-4">
					<span class="filter__title"><?=$arParams["FILTER_PARAMS_CAPTION"]?></span>
					<?foreach($arResult["ITEMS"] as $key=>$arItem){
						if(!in_array($arItem["CODE"], $arRowParams))
							continue;
						if(empty($arItem["VALUES"]))
							continue;

						if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
							continue;
						?>
						<div class="bx-filter-parameters-box">
							<span class="bx-filter-container-modef" style="display:none"></span>
							<div class="bx-filter-block filter__collapse_container dropdown" data-role="bx_filter_block" title="<?=$arItem['NAME']?>">
								<a id="filter_collapse_<?=$key?>" class="filter_select dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$arItem['NAME']?></a>
								<div class="filter__collapse dropdown-menu" aria-labelledby="filter_collapse_<?=$key?>">
									<?foreach($arItem["VALUES"] as $val => $ar){?>
									<div class="category__check">
										<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												onclick="smartFilter.click(this)"
											/>
										<label for="<? echo $ar["CONTROL_ID"] ?>" data-role="label_<?=$ar["CONTROL_ID"]?>" <? echo $ar["DISABLED"] ? 'disabled': '' ?>>
										<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?></span></label>
									</div>
									<?}?>
								</div>
							</div>
						</div>
					<?}?>
				</div>
				<div class="col-12 col-md-8 offset-md-4">
					<div class="filter__btns">
						<button
							type="submit"
							class="filter__apply" 
							id="set_filter"
							name="set_filter"
						><?=$arParams['SUBMIT_FILTER']?></button>
						<button
							class="filter__clear"
							type="submit"
							id="del_filter"
							name="del_filter"
						><?=$arParams["CLEAR_FILTER"]?></button>
					</div>
				</div>
				
				<div class="bx-filter-popup-result <?echo $arParams["POPUP_POSITION"]?>" id="modef" style="display:none;">
					<a href="<?echo $arResult["FILTER_URL"]?>" target="">
						<?=$arParams['SHOW_CAPTION']?>
						<?echo '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>';?>
					</a>
				</div>
			</div>
		</form>
	</div>
	<button class="filter-btn" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="true" aria-controls="collapseFilter" data-caption="<?=$arParams["OPEN_FILTER"]?>"><?=$arParams["CLOSE_FILTER"]?></button>
</div>
<script type="text/javascript">
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>