<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters['SHOW_CAPTION'] = array(
	'NAME' => "Подпись кнопки Показать",
	'TYPE' => 'STRING',
);
$arTemplateParameters['SUBMIT_FILTER'] = array(
	'NAME' => "Подпись кнопки Применить фильтр",
	'TYPE' => 'STRING',
);
$arTemplateParameters['FILTER_NUM_FROM'] = array(
	'NAME' => "От",
	'TYPE' => 'STRING',
);
$arTemplateParameters['FILTER_NUM_TO'] = array(
	'NAME' => "До",
	'TYPE' => 'STRING',
);
$arTemplateParameters['CATEGORY_FILTER'] = array(
    'NAME' => "Категории фильтра",
    'TYPE' => 'STRING',
);
?>