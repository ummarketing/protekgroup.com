$(document).ready(function () {
	"use strict";

	/*==============================
	Header
	==============================*/
	$('.header__mobmenu-search').on('click', function() {
		$('.header__mobfade').removeClass('active');

		if ( $('.header__mobmenu-phone').hasClass('active') ) {
			$('.header__mobmenu-phone').removeClass('active');
			$('.header__mobcontent').removeClass('header__mobcontent--active');

			$(this).toggleClass('active');
			$('.header__mobsearch').toggleClass('header__mobsearch--active');
		} else {
			$(this).toggleClass('active');
			$('.header__mobmenu').toggleClass('header__mobmenu--active');
			$('.header__mobsearch').toggleClass('header__mobsearch--active');
		}
	});

	$('.header__mobmenu-phone').on('click', function() {
		$('.header__mobfade').removeClass('active');

		if ( $('.header__mobmenu-search').hasClass('active') ) {
			$('.header__mobmenu-search').removeClass('active');
			$('.header__mobsearch').removeClass('header__mobsearch--active');

			$(this).toggleClass('active');
			$('.header__mobcontent').toggleClass('header__mobcontent--active');
		} else {
			$(this).toggleClass('active');
			$('.header__mobmenu').toggleClass('header__mobmenu--active');
			$('.header__mobcontent').toggleClass('header__mobcontent--active');
		}
	});
	/*==============================
	Nav
	==============================*/
	$('.nav__btn--head').on('click', function() {
		if($('.nav--footer').hasClass('active')){
			$('#collapsed_mobile_menu').collapse('hide');
			let buttonCaption = $('.nav__btn--footer').find('span'),
				toggleCaption = $(buttonCaption).text();
			$(buttonCaption).text($(buttonCaption).data('toggle-caption'));
			$(buttonCaption).data('toggle-caption', toggleCaption);
			$('.nav__btn--footer').toggleClass('active');
			$('.nav--footer').toggleClass('active');
		}

		let buttonCaption = $(this).find('span'),
			toggleCaption = $(buttonCaption).text();
		$(buttonCaption).text($(buttonCaption).data('toggle-caption'));
		$(buttonCaption).data('toggle-caption', toggleCaption);

		if(!$(this).siblings('#collapsed_mobile_menu').length){
			let temp = $('#collapsed_mobile_menu');
			$('#collapsed_mobile_menu').remove();
			$(this).after(temp);
		}

		$(this).toggleClass('active');
		$('.nav--head').toggleClass('active');
	});

	$('.nav__btn--footer').on('click', function() {
		if($('.nav--head').hasClass('active')){
			$('#collapsed_mobile_menu').collapse('hide');
			let buttonCaption = $('.nav__btn--head').find('span'),
				toggleCaption = $(buttonCaption).text();
			$(buttonCaption).text($(buttonCaption).data('toggle-caption'));
			$(buttonCaption).data('toggle-caption', toggleCaption);
			$('.nav__btn--head').toggleClass('active');
			$('.nav--head').toggleClass('active');
		}

		let buttonCaption = $(this).find('span'),
			toggleCaption = $(buttonCaption).text();
		$(buttonCaption).text($(buttonCaption).data('toggle-caption'));
		$(buttonCaption).data('toggle-caption', toggleCaption);

		if(!$(this).siblings('#collapsed_mobile_menu').length){
			let temp = $('#collapsed_mobile_menu');
			$('#collapsed_mobile_menu').remove();
			$(this).before(temp);
		}

		$(this).toggleClass('active');
		$('.nav--footer').toggleClass('active');
	});

	$(window).on('scroll', function () {
		if ($(window).scrollTop() >= 230 && $(window).width() >= 1200) {
			$('.nav--head').addClass('nav--scroll');
		} else {
			$('.nav--head').removeClass('nav--scroll');
			$('.nav--head .search-block__result').fadeOut();
		}

		if ($(window).scrollTop() >= $(window).height()) {
			$('.sideback').show();
		} else {
			$('.sideback').hide();
		}

		if ($(window).scrollTop() >= 230 && $(window).scrollTop() >= Math.abs( $(document).height() - 1200 ) ) {
			$('.nav--head').addClass('nav--hidden');
		} else {
			$('.nav--head').removeClass('nav--hidden');
		}
	});
	$(window).trigger('scroll');

	/*==============================
	Collapse stuff
	==============================*/
	$(".collapse_shower").click(function(e){
		e.preventDefault();
		let tempCaption = $(this).data('caption');
		$($(this).attr('href')).collapse('toggle');
		$(this).data('caption', $(this).text());
		$(this).text(tempCaption);
	});

	/*==============================
	Smooth scroll
	==============================*/
	var scroll = new SmoothScroll('[data-scroll]', {
		ignore: '[data-scroll-ignore]',
		topOnEmptyHash: true,
		updateURL: false,
		speed: 500,
		speedAsDuration: true,
		offset: 100,
		easing: 'easeInOutQuad',
	});

	/*==============================
	Back to anywhere
	==============================*/
	$('body').on('click', '.scroll-to', function(){
        let elementClick = $(this).data("scroll-direction");
        let destination = Math.max(0, $(elementClick).offset().top - $('.nav--head').innerHeight() - 70); // - $(window).innerHeight() / 8
            $('html,body').animate({scrollTop: destination}, 700);
        return false;
    });


    /*==============================
	Modal
	==============================*/

	$('.open-modal').magnificPopup({
		fixedContentPos: true,
		fixedBgPos: true,
		overflowY: 'auto',
		type: 'inline',
		preloader: false,
		modal: false,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in',
		callbacks: {
			open: function() {
				if ($(window).width() > 991) {
					$('.nav--scroll').css('margin-left', "-" + (getScrollBarWidth()/2) + "px");
				}

				if ($(window).width() > 1199) {
					$('.cart-btn-wrap').css('margin-right', "-" + ( 705 - (getScrollBarWidth()/2)) + "px");
				}
			},
			close: function() {
				if ($(window).width() > 991) {
					$('.nav--scroll').css('margin-left', 0);
				}

				if ($(window).width() > 1199) {
					$('.cart-btn-wrap').css('margin-right', -705);
				}
			}
		}
	});

	$('.modal__close, .modal__btn--green').on('click', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	function getScrollBarWidth () {
		var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
			widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
		$outer.remove();
		return 100 - widthWithScroll;
	};

	/*==============================
	Mask
	==============================*/
	$('input.masked_phone').mask('+9 (999) 999-99-99');


	/*==============================
	Tables
	===============================*/
	if($('table').length > 0){
		$('table').wrap('<div class="table-responsive"></div>');
	}
	/*==============================
	Ajax
	===============================*/

	$('body').on('cyber.change.compare.result cyber.change.favorites.result cyber.change.basket.basket', function(){ // перезагрузка формы по событию (добавление товара в корзину)
		$.ajax({
			method: 'POST',
			url: '/ajax/ajax_smallBasket.php',
			data: "cyber_ajax=y&SITE_ID=" + SITE_ID,
			dataType: 'json',
			success: function(data) {
				//console.log("change small basket", data);
				if(data.compare){
					$('.cart-btn-wrap .cart-btn-wrap-compare').text('(' + data.compare + ')');
					// $('.header__mob_fav_comp .cart-btn-wrap-compare').text('(' + data.compare + ')');
					$('.mobile-header__compare-item.compare [data-mobile="numbers"]').text(data.compare);
				}else{
					$('.cart-btn-wrap .cart-btn-wrap-compare').text("");
					// $('.header__mob_fav_comp .cart-btn-wrap-compare').text("");
					$('.mobile-header__compare-item.compare [data-mobile="numbers"]').text('');
				}

				if(data.favorites){
					$('.cart-btn-wrap .cart-btn-wrap-favorites').text('(' + data.favorites + ')');
					// $('.header__mob_fav_comp .cart-btn-wrap-favorites').text('(' + data.favorites + ')');
					$('.mobile-header__compare-item.favorites [data-mobile="numbers"]').text(data.favorites);
				}else{
					$('.cart-btn-wrap .cart-btn-wrap-favorites').text("");
					// $('.header__mob_fav_comp .cart-btn-wrap-favorites').text("");
					$('.mobile-header__compare-item.favorites [data-mobile="numbers"]').text('');
				}

				if(data.basket){
					$('.cart-btn-wrap .cart-btn-wrap-basket').text(data.basket);
					$('.header__mobmenu-cart .cart-btn-wrap-basket').text(data.basket);
				}else{
					$('.cart-btn-wrap .cart-btn-wrap-basket').text("");
					$('.header__mobmenu-cart .cart-btn-wrap-basket').text("");
				}
				if(data.compare || data.favorites || data.basket){
					$('.cart-btn-wrap').fadeIn("500");
				}else{
					$('.cart-btn-wrap').fadeOut("500");
				}

				initShowCompareItems();
			}
		});
	});
	//-------- Кнопки избранное и срвнение -------
    function initShowCompareItems() {
        setTimeout(function () {
            var mobile_num = $('[data-mobile="numbers"]');

            mobile_num.each(function (i, elem) {

                if (parseInt($(elem).text()) > 0) {
                    $(elem).parent().parent().addClass('active');
                }else{
                	$(elem).parent().parent().removeClass('active');
				}
            });
        }, 60 * 10);
    }
    initShowCompareItems();
    //-------- /Кнопки избранное и срвнение -------
	$('#cookie_approve_modal .modal__close').click(function(){
		$('#cookie_approve_modal').fadeOut('500', function(){
			this.remove();
		});
	});
	$('#cookie_approve_modal .form__btn').on('click', function(){ // Кнопка Принять Cookie
		$.ajax({
			method: 'POST',
			url: '/ajax/ajax_cookieApprove.php',
			data: "cookie_approve=true",
			dataType: 'json',
			success: function(data) {
				//console.log("cookie_approve");
				$('#cookie_approve_modal').fadeOut('500', function(){
					this.remove();
				});
			}
		});
	});

});