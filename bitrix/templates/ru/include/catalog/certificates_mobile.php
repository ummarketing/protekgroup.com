<div class="certificates">
    <div class="certificates__wrapper">
        <a href="/upload/iblock/280/Protek-_-VVMTrans-_-ISO-22000_page_0001.jpg" data-title="Сертификат" data-lightbox="certificates_mobile" class="certificates__item">
            <img src="/upload/iblock/280/Protek-_-VVMTrans-_-ISO-22000_page_0001.jpg" alt="Сертификат" class="certificates__image">
        </a>
        <a href="/upload/iblock/1ce/Protek-_-IP_2001-_-ISO-22000_page_0001.jpg" data-title="Сертификат" data-lightbox="certificates_mobile" class="certificates__item">
            <img src="/upload/iblock/1ce/Protek-_-IP_2001-_-ISO-22000_page_0001.jpg" alt="Сертификат" class="certificates__image">
        </a>
        <a href="/upload/iblock/7b7/sertifikat_2014.jpg" data-title="Сертификат" data-lightbox="certificates_mobile" class="certificates__item">
            <img src="/upload/iblock/7b7/sertifikat_2014.jpg" alt="Сертификат" class="certificates__image">
        </a>
        <a href="/upload/iblock/e45/ros_upak_uchastie.jpg" data-title="Сертификат" data-lightbox="certificates_mobile" class="certificates__item">
            <img src="/upload/iblock/e45/ros_upak_uchastie.jpg" alt="Сертификат" class="certificates__image">
        </a>
        <a href="/upload/iblock/cd0/iso_22_000_2005.jpg" data-title="Сертификат" data-lightbox="certificates_mobile" class="certificates__item">
            <img src="/upload/iblock/cd0/iso_22_000_2005.jpg" alt="Сертификат" class="certificates__image">
        </a>
        <a href="/upload/iblock/0aa/iso_22000_vvmtrans_jpeg.jpg" data-title="Сертификат" data-lightbox="certificates_mobile" class="certificates__item">
            <img src="/upload/iblock/0aa/iso_22000_vvmtrans_jpeg.jpg" alt="Сертификат" class="certificates__image">
        </a>
    </div>
    <a href="/certificates/" class="certificates__link">Все сертификаты</a>
</div>