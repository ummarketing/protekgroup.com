<div class="nav nav--footer">
    <div class="container nav__wrap">
        <div class="row">
            <div class="col-12">
                <div class="nav__content">
                    <button class="nav__btn nav__btn--footer"
                            type="button"
                            data-toggle="collapse"
                            href="#collapsed_mobile_menu"
                            aria-expanded="false"
                            aria-controls="collapse01">
                        <svg version="1.1"
                             xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             x="0px"
                             y="0px"
                             viewBox="0 0 92.833 92.833"
                             xml:space="preserve"><path
                                    d="M89.834,1.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V4.75 C92.834,3.096,91.488,1.75,89.834,1.75z"/>
                            <path d="M89.834,36.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V39.75 C92.834,38.096,91.488,36.75,89.834,36.75z"/>
                            <path d="M89.834,71.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V74.75 C92.834,73.095,91.488,71.75,89.834,71.75z"/></svg>
                        <span data-toggle-caption="<?= GetMessage("PROTEK_TEMPLATE_CLOSE_MENU") ?>"><?= GetMessage("PROTEK_TEMPLATE_OPEN_MENU") ?></span>
                    </button>

                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "protek_main_menu",
                        Array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "1",
                            "MENU_CACHE_GET_VARS" => array(0 => "",),
                            "MENU_CACHE_TIME" => "36000",
                            "MENU_CACHE_TYPE" => "A",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "top",
                            "USE_EXT" => "N"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contacts">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contacts__mobile">
                    <button class="contacts__collapse-btn"
                            type="button"
                            data-toggle="collapse"
                            href="#collapse-con1"
                            aria-expanded="false"
                            aria-controls="collapse-con1">
                        <span><?= GetMessage("PROTEK_TEMPLATE_OUR_CONTACTS") ?></span>
                        <svg version="1.1"
                             xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 129 129"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             enable-background="new 0 0 129 129">
                            <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
                        </svg>
                    </button>

                    <div class="collapse contacts__collapse"
                         id="collapse-con1">
                        <div class="contacts__info">
                            <a href="#modal-city"
                               class="contacts__place open-modal">
                                <span class="cyber_location_name"><?= $arCity['NAME'] ?></span>
                                <svg version="1.1"
                                     xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 129 129"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     enable-background="new 0 0 129 129">
                                    <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
                                </svg>
                            </a>

                            <p>
                                <?= GetMessage("PROTEK_TEMPLATE_CONTACTS_PHONE") ?>
                                <br/>
                                <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                                   class="cyber_location_string phone">
                                    <span><?= $arCity['PHONE']; ?></span>
                                </a><br/>
                                <?= GetMessage("PROTEK_TEMPLATE_CONTACTS_EMAIL") ?>
                                <br/>
                                <a href="mailto:<?= $arCity['EMAIL'] ?>"
                                   class="cyber_location_string email">
                                    <span><?= $arCity['EMAIL'] ?></span></a><br/>
                                <?= GetMessage("PROTEK_TEMPLATE_CONTACTS_TIME") ?>
                                <br/>
                                <span class="cyber_location_string time"><?= $arCity['TIME'] ?></span><br/>
                            </p>

                            <a href="#modal-question"
                               class="contacts__btn open-modal"><?= GetMessage("PROTEK_TEMPLATE_QUESTION") ?></a>

                            <p class="cyber_location_string address"><?= $arCity['ADDRESS'] ?></p>

                            <?if($arCity['LOCATION']):?>
                                <div class="contacts__map"
                                     id="map"></div>
                            <?endif;?>
                        </div>
                    </div>

                    <div class="contacts__separator"></div>

                    <button class="contacts__collapse-btn"
                            type="button"
                            data-toggle="collapse"
                            href="#collapse-con2"
                            aria-expanded="false"
                            aria-controls="collapse-con2">
                        <span><?= GetMessage("PROTEK_TEMPLATE_ABOUT") ?></span>
                        <svg version="1.1"
                             xmlns="http://www.w3.org/2000/svg"
                             viewBox="0 0 129 129"
                             xmlns:xlink="http://www.w3.org/1999/xlink"
                             enable-background="new 0 0 129 129">
                            <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
                        </svg>
                    </button>

                    <div class="collapse contacts__collapse"
                         id="collapse-con2">
                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/include/advantages.php"), false); ?>
                    </div>
                </div>

                <div class="contacts__desk">
                    <div class="row">
                        <div class="col-lg-8 col-xl-7">
                            <div class="contacts__title"><?= GetMessage("PROTEK_TEMPLATE_OUR_CONTACTS") ?></div>
                        </div>

                        <div class="col-lg-4 col-xl-5">
                            <div class="contacts__title contacts__title--last"><?= GetMessage("PROTEK_TEMPLATE_ABOUT") ?></div>
                        </div>

                        <div class="col-lg-4 col-xl-3">
                            <div class="contacts__info">
                                <a href="#modal-city"
                                   class="contacts__place open-modal">
                                    <span class="cyber_location_name"><?= $arCity['NAME'] ?></span>
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         viewBox="0 0 129 129"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         enable-background="new 0 0 129 129">
                                        <path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/>
                                    </svg>
                                </a>

                                <p>
                                    <?= GetMessage("PROTEK_TEMPLATE_CONTACTS_PHONE") ?>
                                    <br/>
                                    <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                                       class="cyber_location_string phone">
                                        <span class="ya-phone"><?= $arCity['PHONE']; ?></span>
                                    </a><br/>
                                    <?= GetMessage("PROTEK_TEMPLATE_CONTACTS_EMAIL") ?>
                                    <br/>
                                    <a href="mailto:<?= $arCity['EMAIL'] ?>"
                                       class="cyber_location_string email">
                                        <span><?= $arCity['EMAIL'] ?></span></a><br/>
                                    <?= GetMessage("PROTEK_TEMPLATE_CONTACTS_TIME") ?>
                                    <br/>
                                    <span class="cyber_location_string time"><?= $arCity['TIME'] ?></span><br/>
                                </p>

                                <a href="#modal-question"
                                   class="contacts__btn open-modal"
                                   onclick="_gaq.push(['_trackEvent', 'form','user_click5']);yaCounter30699088.reachGoal('goal5'); return true;"><?= GetMessage("PROTEK_TEMPLATE_QUESTION") ?></a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-4">
                            <div class="contacts__info">
                                <p class="cyber_location_string address"><?= $arCity['ADDRESS'] ?></p>
                                <?if($arCity['LOCATION']):?>
                                <div class="contacts__map"
                                     id="map2"></div>
                                <?endif;?>
                            </div>
                        </div>

                        <div class="col-lg-4 col-xl-5">
                            <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/include/advantages.php"), false); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <span class="footer__copyright"><? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH . "/include/copyright.php"), false); ?></span>

                <div class="footer__links">
                    <a href="#modal-map"
                       class="open-modal"><?= GetMessage("PROTEK_TEMPLATE_SITEMAP") ?></a>
                    <a href="/politics/"
                       target="_blank"><?= GetMessage("PROTEK_TEMPLATE_PRIVACY") ?></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ADD NEW -->
<? if (!$APPLICATION->GetPageProperty("no_sticky_menu")) {
    $APPLICATION->IncludeComponent(
        "bitrix:menu",
        "protek_sticky",
        Array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "left",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => array(0 => "",),
            "MENU_CACHE_TIME" => "36000",
            "MENU_CACHE_TYPE" => "A",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "left",
            "USE_EXT" => "Y",
            "TOP_CAPTION" => GetMessage("PROTEK_TEMPLATE_UP"),
            "MORE_CAPTION" => GetMessage("PROTEK_TEMPLATE_MORE"),
        )
    );
} ?>
<? $APPLICATION->IncludeComponent(
    "cyberrex:form.result.basket",
    ".default",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "N",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "AJAX_MODE" => "N",
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "WEB_FORM_ID" => "22",
        "COMPONENT_TEMPLATE" => ".default",
        "FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
        "FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
        "FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
        "FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
        "ITEMS_TITLE" => GetMessage("PROTEK_TEMPLATE_CART_ORDER_ITEMS_TITLE"),
        "FORM_SEND_MORE" => GetMessage("PROTEK_TEMPLATE_FORM_SEND_AGAIN"),
        "FORM_UNIQUE_CLASS" => "",
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
); ?>
<? if (!stristr($APPLICATION->GetCurPage(), "/cart/")) { ?>
    <div id="modal-cart"
         class="zoom-anim-dialog mfp-hide modal modal--cart ">
        <button class="modal__close" type="button">
            <svg version="1.1"
                 xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px"
                 y="0px"
                 viewBox="0 0 47.971 47.971"
                 style="enable-background:new 0 0 47.971 47.971;"
                 xml:space="preserve"><path
                        d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg>
        </button>
        <div class="modal__content">
            <? $APPLICATION->IncludeComponent(
                "cyberrex:cyber.basket",
                "",
                Array(
                    "IBLOCK_ID" => "40",
                    "IBLOCK_TYPE" => "catalog",
                    "AJAX_MODE" => "N",
                    "BASKET_TITLE" => GetMessage("PROTEK_TEMPLATE_CART_TITLE"),
                    "BASKET_SUM_ITEMS" => GetMessage("PROTEK_TEMPLATE_CART_ITEMS_IN"),
                    "BASKET_SUM_VOLUME" => GetMessage("PROTEK_TEMPLATE_CART_SUM_VOLUME"),
                    "BASKET_SUM_WEIGHT" => GetMessage("PROTEK_TEMPLATE_CART_SUM_WEIGHT"),
                    "BASKET_SUM_PACKS" => GetMessage("PROTEK_TEMPLATE_CART_SUM_BOXES"),
                    "BASKET_CLEAR" => GetMessage("PROTEK_TEMPLATE_CART_CLEAR"),
                    "BASKET_ORDER_PRICES" => GetMessage("PROTEK_TEMPLATE_CART_ORDER_BUTTON_CAPTION"),
                    "BASKET_IS_EMPTY" => GetMessage("PROTEK_TEMPLATE_CART_EMPTY"),
                    "LINK_TO_CART" => "/cart/",
                    "LINK_TO_CART_TITLE" => GetMessage("PROTEK_TEMPLATE_CART_LINK_TO_MAIN"),
                    "BASKET_OUTTER_SIZES" => GetMessage("PROTEK_TEMPLATE_CART_OUTTER_SIZES"),
                )
            ); ?>
        </div>
    </div>
    <?
    $arCompare = array();
    $showCart = false;
    if (reset($_SESSION[COMPARE_NAME . SITE_ID])) {
        $arCompare = reset($_SESSION[COMPARE_NAME . SITE_ID]);
    }
    if (count($arCompare['ITEMS']) > 0 || count($_SESSION[FAVORITES_NAME . SITE_ID]) > 0 || count($_SESSION['CYBER_BASKET_' . SITE_ID]) > 0) {
        $showCart = true;
    }
    ?>
    <div class="cart-btn-wrap"
         style="<?= ($showCart ? "" : "display:none") ?>">
        <div class="catalog-links">
            <a href="<?= SITE_DIR ?>catalog/compare.php"
               onclick="_gaq.push(['_trackEvent', 'form','user_click3']);"><?= GetMessage('PROTEK_TEMPLATE_COMPARE') ?>
                <span class="cart-btn-wrap-compare"><? if (isset($_SESSION[COMPARE_NAME . SITE_ID])) {
                        if (reset($_SESSION[COMPARE_NAME . SITE_ID])) {
                            if (count($arCompare['ITEMS']) > 0) {
                                echo "(" . count($arCompare['ITEMS']) . ")";
                            }
                        }
                    } ?></span></a>
            <a href="<?= SITE_DIR ?>catalog/favorites/"><?= GetMessage('PROTEK_TEMPLATE_FAVORITES') ?>
                <span class="cart-btn-wrap-favorites"><? if (isset($_SESSION[FAVORITES_NAME . SITE_ID])) {
                        if (count($_SESSION[FAVORITES_NAME . SITE_ID]) > 0) {
                            echo "(" . count($_SESSION[FAVORITES_NAME . SITE_ID]) . ")";
                        }
                    } ?></span></a>
        </div>
        <a href="#modal-cart"
           class="cart-btn open-modal">
            <span class="cart-btn-wrap-basket"><?= (count($_SESSION['CYBER_BASKET_' . SITE_ID]) > 0 ? count($_SESSION['CYBER_BASKET_' . SITE_ID]) : "") ?></span>
            <svg version="1.1"
                 xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px"
                 y="0px"
                 viewBox="0 0 260.293 260.293"
                 xml:space="preserve"><path
                        d="M258.727,57.459c-1.42-1.837-3.612-2.913-5.934-2.913H62.004l-8.333-32.055c-0.859-3.306-3.843-5.613-7.259-5.613H7.5 c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h33.112l8.333,32.055c0,0.001,0,0.001,0.001,0.002l29.381,112.969 c0.859,3.305,3.843,5.612,7.258,5.612h137.822c3.415,0,6.399-2.307,7.258-5.612l29.385-112.971 C260.636,61.687,260.147,59.295,258.727,57.459z M117.877,167.517H91.385l-5.892-22.652h32.384V167.517z M117.877,129.864H81.592 l-5.895-22.667h42.18V129.864z M117.877,92.197H71.795l-5.891-22.651h51.973V92.197z M176.119,167.517h-43.242v-22.652h43.242 V167.517z M176.119,129.864h-43.242v-22.667h43.242V129.864z M176.119,92.197h-43.242V69.546h43.242V92.197z M217.609,167.517 h-26.49v-22.652h32.382L217.609,167.517z M227.403,129.864h-36.284v-22.667h42.18L227.403,129.864z M237.201,92.197h-46.081V69.546 h51.974L237.201,92.197z"/>
                <path d="M105.482,188.62c-15.106,0-27.396,12.29-27.396,27.395c0,15.108,12.29,27.4,27.396,27.4 c15.105,0,27.395-12.292,27.395-27.4C132.877,200.91,120.588,188.62,105.482,188.62z M105.482,228.415 c-6.835,0-12.396-5.563-12.396-12.4c0-6.835,5.561-12.395,12.396-12.395c6.834,0,12.395,5.561,12.395,12.395 C117.877,222.853,112.317,228.415,105.482,228.415z"/>
                <path d="M203.512,188.62c-15.104,0-27.392,12.29-27.392,27.395c0,15.108,12.288,27.4,27.392,27.4 c15.107,0,27.396-12.292,27.396-27.4C230.908,200.91,218.618,188.62,203.512,188.62z M203.512,228.415 c-6.833,0-12.392-5.563-12.392-12.4c0-6.835,5.559-12.395,12.392-12.395c6.836,0,12.396,5.561,12.396,12.395 C215.908,222.853,210.347,228.415,203.512,228.415z"/></svg>
        </a>
    </div>
<? } ?>
<button class="sideback scroll-to"
        style="display:none"
        data-scroll-direction="body"
        type="button">
    <svg version="1.1"
         xmlns="http://www.w3.org/2000/svg"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         x="0px"
         y="0px"
         viewBox="0 0 32.634 32.634"
         style="enable-background:new 0 0 32.634 32.634;"
         xml:space="preserve"><path
                d="M16.317,32.634c-0.276,0-0.5-0.224-0.5-0.5V0.5c0-0.276,0.224-0.5,0.5-0.5s0.5,0.224,0.5,0.5v31.634 C16.817,32.41,16.594,32.634,16.317,32.634z"></path>
        <path d="M28.852,13.536c-0.128,0-0.256-0.049-0.354-0.146L16.319,1.207L4.135,13.39c-0.195,0.195-0.512,0.195-0.707,0 s-0.195-0.512,0-0.707L15.966,0.146C16.059,0.053,16.186,0,16.319,0l0,0c0.133,0,0.26,0.053,0.354,0.146l12.533,12.536 c0.195,0.195,0.195,0.512,0,0.707C29.108,13.487,28.98,13.536,28.852,13.536z"></path></svg>
</button>
<!-- END ADD NEW -->
<? $APPLICATION->IncludeComponent(
    "bitrix:menu",
    "protek_sitemap_menu",
    Array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "2",
        "MENU_CACHE_GET_VARS" => array(0 => "",),
        "MENU_CACHE_TIME" => "36000",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "sitemap",
        "USE_EXT" => "N"
    )
); ?>
<? $APPLICATION->IncludeComponent(
    "cyberrex:form.result.new",
    "protek_modal_web_form",
    array(
        "CACHE_TIME" => "36000",
        "CACHE_TYPE" => "Y",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "AJAX_MODE" => "N",  // режим AJAX
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "WEB_FORM_ID" => "1",
        "COMPONENT_TEMPLATE" => "protek_modal_web_form",
        "FORM_UNIQUE_CLASS" => "modal-call",
        "FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
        "FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
        "FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
        "FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
        "FORM_SEND_MORE" => GetMessage("PROTEK_TEMPLATE_FORM_SEND_AGAIN"),
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
); ?>
<? $APPLICATION->IncludeComponent(
    "cyberrex:form.result.new",
    "protek_modal_web_form",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "AJAX_MODE" => "N",  // режим AJAX
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "WEB_FORM_ID" => "2",
        "COMPONENT_TEMPLATE" => "protek_modal_web_form",
        "FORM_UNIQUE_CLASS" => "modal-request",
        "FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
        "FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
        "FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
        "FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
        "FORM_SEND_MORE" => GetMessage("PROTEK_TEMPLATE_FORM_SEND_AGAIN"),
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
); ?>
<? $APPLICATION->IncludeComponent(
    "cyberrex:form.result.new",
    "protek_modal_web_form",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "AJAX_MODE" => "N",  // режим AJAX
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "WEB_FORM_ID" => "3",
        "COMPONENT_TEMPLATE" => "protek_modal_web_form",
        "FORM_UNIQUE_CLASS" => "modal-question",
        "FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
        "FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
        "FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
        "FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
        "FORM_SEND_MORE" => GetMessage("PROTEK_TEMPLATE_FORM_SEND_AGAIN"),
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
); ?>

<? $APPLICATION->IncludeComponent(
    "cyberrex:form.result.new",
    "protek_modal_web_form",
    array(
        "CACHE_TIME" => "3600",
        "CACHE_TYPE" => "A",
        "CHAIN_ITEM_LINK" => "",
        "CHAIN_ITEM_TEXT" => "",
        "EDIT_URL" => "",
        "AJAX_MODE" => "N",  // режим AJAX
        "IGNORE_CUSTOM_TEMPLATE" => "N",
        "LIST_URL" => "",
        "SEF_MODE" => "N",
        "SUCCESS_URL" => "",
        "USE_EXTENDED_ERRORS" => "N",
        "WEB_FORM_ID" => "34",
        "COMPONENT_TEMPLATE" => "protek_modal_web_form",
        "FORM_UNIQUE_CLASS" => "catalog-file",
        "FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
        "FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
        "FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
        "FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
        "FORM_SEND_MORE" => GetMessage("PROTEK_TEMPLATE_FORM_SEND_AGAIN"),
        "VARIABLE_ALIASES" => array(
            "WEB_FORM_ID" => "WEB_FORM_ID",
            "RESULT_ID" => "RESULT_ID",
        )
    ),
    false
); ?>
<? if (!$APPLICATION->get_cookie('CYBER_COOKIE_APPROVE')) { ?>
    <div id="cookie_approve_modal">
        <button class="modal__close"
                type="button">
            <svg version="1.1"
                 xmlns="http://www.w3.org/2000/svg"
                 xmlns:xlink="http://www.w3.org/1999/xlink"
                 x="0px"
                 y="0px"
                 viewBox="0 0 47.971 47.971"
                 style="enable-background:new 0 0 47.971 47.971;"
                 xml:space="preserve"><path
                        d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg>
        </button>
        <div class="modal__content">
            <div class="text-center"><?= GetMessage("PROTEK_TEMPLATE_COOKIE_TEXT") ?></div>
            <button class="form__btn"
                    name="web_form_submit"
                    type="submit"><?= GetMessage("PROTEK_TEMPLATE_COOKIE_BUTTON") ?></button>
        </div>
    </div>
<? } ?>


<script data-skip-moving="true"
        type="text/javascript">
    const SITE_ID = "<?=SITE_ID?>";
</script>
<script data-skip-moving="true"
        type="text/javascript">!function () {
        var t = document.createElement("script");
        t.type = "text/javascript", t.async = !0, t.src = "https://vk.com/js/api/openapi.js?162", t.onload = function () {
            VK.Retargeting.Init("VK-RTRG-410429-1EMdi"), VK.Retargeting.Hit()
        }, document.head.appendChild(t)
    }();</script>
<noscript>
    <img src="https://vk.com/rtrg?p=VK-RTRG-410429-1EMdi"
         style="position:fixed; left:-999px;"
         alt=""/>
</noscript>
<!-- Facebook Pixel Code -->
<script>
    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return;
        n = f.fbq = function () {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq) f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '911110725989927');
    fbq('track', 'PageView');
</script>
<noscript>
    <img height="1"
         width="1"
         style="display:none"
         src="https://www.facebook.com/tr?id=911110725989927&ev=PageView&noscript=1"
    />
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(30699088, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div>
        <img src="https://mc.yandex.ru/watch/30699088"
             style="position:absolute; left:-9999px;"
             alt=""/>
    </div>
</noscript>
<!-- /Yandex.Metrika counter -->


<!-- ASSETS -->
<script src="/local/assets/plugins/mCustomScrollbar/jquery.mCustomScrollbar.js"></script>
<script src="/local/assets/js/fancybox.min.js"></script>
<script src="/local/assets/js/script_new.js"></script>
<!-- /ASSETS -->
</body>
</html>


<? //------------- Отправка целей ------------?>
<? if (isset($_GET['WEB_FORM_ID']) && $_GET['formresult'] == 'addok'): ?>
    <? $event = array(); ?>
    <? if ($_SESSION['GOAL'] !== $_GET['WEB_FORM_ID']): ?>
        <? $_SESSION['GOAL'] = $_GET['WEB_FORM_ID']; ?>
        <? switch ($_GET['WEB_FORM_ID']) {
            case '1':
                $event[0] = 'send_zvonok_header';
                $event[1] = 'send_call_header';
                break;

            case '2':
                $event[0] = 'send_zayavka_header';
                $event[1] = 'send_request_header';
                break;

            case '3':
                $event[0] = 'send_vopros_footer';
                $event[1] = 'send_question_footer';
                if (strpos($APPLICATION->GetCurPage(), '/about/') !== false) {
                    $event[0] = 'send_vopros_about';
                    $event[1] = 'send_question_company';
                }
                break;

            case '4':
                $event[0] = 'send_zayavka_vacancies';
                $event[1] = 'send_vacancy';
                break;

            case '22':
                $event[0] = 'send_korzina';
                $event[1] = 'form_uznat_cenu';
                break;
        } ?>
        <script>
            $(document).ready(function () {
                trackDuble('<?= $event[0]?>', '<?=$event[1] ?>');
            });
        </script>
    <? endif ?>
<? endif ?>
<? //------------- Отправка целей ------------?>




<?
//----------- Настройка мета тегов ----------
global $arGlobeMeta;


$h1 = $arGlobeMeta['H1'];
$title = $arGlobeMeta['TITLE'];
$desc = $arGlobeMeta['DESC'];

if (!empty($h1)) {
    $APPLICATION->SetTitle($h1);
}
if (!empty($title)) {
    $APPLICATION->SetPageProperty('title', $title);
}
if (!empty($desc)) {
    $APPLICATION->SetPageProperty('description', $desc);
}

$APPLICATION->SetPageProperty('title',replacer($APPLICATION->GetPageProperty('title')));

$APPLICATION->SetPageProperty('description',replacer($APPLICATION->GetPageProperty('description')));

$APPLICATION->SetPageProperty('keywords',replacer($APPLICATION->GetPageProperty('keywords')));

$APPLICATION->SetTitle(replacer($APPLICATION->GetTitle()));
//----------- /Настройка мета тегов ----------
?>