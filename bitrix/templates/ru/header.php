<?
//---------- Редирект с иньекции ----------
if (preg_match("/\WUNION.{1,}/", $_SERVER['REQUEST_URI'])) {
    $good_url = preg_replace('/\WUNION.{1,}/', '', $_SERVER['REQUEST_URI']);
    LocalRedirect($good_url);
}
//---------- /Редирект с иньекции ----------
?>


<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? IncludeTemplateLangFile(__FILE__); ?>


    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><? $APPLICATION->ShowTitle(); ?></title>
        <?
        if($APPLICATION->GetCurPage() === '/ty_interplastic/' || $APPLICATION->GetCurPage() === '/protek_concern/'){
            echo '<meta name="robots" content="noindex, nofollow"/>';
        }
        $APPLICATION->ShowMeta("keywords");
        $APPLICATION->ShowMeta("description");
        $APPLICATION->SetAdditionalCss(DEFAULT_TEMPLATE_PATH . "/css/styles.css");
        $APPLICATION->SetAdditionalCss(DEFAULT_TEMPLATE_PATH . "/css/template_styles.css");
        $APPLICATION->AddHeadScript(DEFAULT_TEMPLATE_PATH . "/js/scripts.min.js");
        $APPLICATION->AddHeadScript("//api-maps.yandex.ru/2.1/?lang=ru_RU");
        $APPLICATION->AddHeadScript(DEFAULT_TEMPLATE_PATH . "/js/main.js");
        $APPLICATION->ShowHead(); ?>
        <meta property="og:title" content="<? $APPLICATION->ShowTitle() ?>"/>
        <meta property="og:type" content="<? $APPLICATION->ShowProperty("og_type", "website") ?>"/>
        <meta property="og:url" content="<? $APPLICATION->ShowProperty("canonical", $_SERVER['REQUEST_SCHEME'] . "https://" . $_SERVER['SERVER_NAME'] . $APPLICATION->GetCurDir()) ?>"/>
        <meta property="og:image" content="<? $APPLICATION->ShowProperty("og_image", $_SERVER['REQUEST_SCHEME'] . "https://" . $_SERVER['SERVER_NAME'] . DEFAULT_TEMPLATE_PATH . "/img/logo.svg") ?>"/>
        <meta property="og:description" content="<? $APPLICATION->ShowProperty("description") ?>"/>
        <? $APPLICATION->IncludeFile("/include/canonical.php") ?>

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script data-skip-moving=true async src="https://www.googletagmanager.com/gtag/js?id=UA-63692499-1"></script>
        <script data-skip-moving=true>

            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-63692499-1');

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-63692499-1']);
            _gaq.push(['_trackPageview']);
            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (m, e, t, r, i, k, a) {
                m[i] = m[i] || function () {
                    (m[i].a = m[i].a || []).push(arguments)
                };
                m[i].l = 1 * new Date();
                k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
            })
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(30699088, "init", {
                clickmap: true,
                trackLinks: true,
                accurateTrackBounce: true,
                webvisor: true
            });
        </script>
        <noscript>
            <div>
                <img src="https://mc.yandex.ru/watch/30699088" style="position:absolute; left:-9999px;" alt=""/>
            </div>
        </noscript>
        <!-- /Yandex.Metrika counter -->
        <!-- dsafsdf--->



        <!-- ASSETS -->
        <link rel="stylesheet" href="/local/assets/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" href="/local/assets/css/fancybox.min.css">
        <link rel="stylesheet" href="/local/assets/css/grid.css">
        <link rel="stylesheet" href="/local/assets/css/style_new.css">
 		<link rel="stylesheet" href="/local/assets/css/css.css">
        <!-- /ASSETS -->
    </head>
<body>

<? $APPLICATION->ShowPanel(); ?>
    <header class="header">
        <div class="header__submenu hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="header__submenu-wrap">
                            <div class="header__submenu-lang">
                                <span><?= GetMessage("PROTEK_TEMPLATE_LANGUAGE") ?></span>
                                <span class="active">RU</span>
                                <a href="/en/">EN</a>
                                <a href="/de/">DE</a>
                                <a href="/cn/">CN</a>
                                <a href="/es/">ES</a>
                            </div>

                            <div class="header__submenu-links">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "protek_top_menu",
                                    array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "left",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(),
                                        "MENU_CACHE_TIME" => "36000",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "first",
                                        "USE_EXT" => "Y",
                                        "COMPONENT_TEMPLATE" => "protek_top_menu"
                                    ),
                                    false
                                ); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?
        $CITY_ID = !empty($APPLICATION->get_cookie('MY_CITY')) ? $APPLICATION->get_cookie('MY_CITY') : 27;
        $arComponentOutput = $APPLICATION->IncludeComponent(
            "cyberrex:city.selection",
            ".default",
            array(
                "ELEMENT_ID" => $CITY_ID,
                "IBLOCK_ID" => "8",
                "IBLOCK_TYPE" => "content",
                "COMPONENT_TEMPLATE" => ".default",
                "BTN_EDIT" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_NO"),
                "BTN_CLOSE" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_YES"),
                "POPUP_LABEL" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_CHOOSE_CLOSEST"),
                "QUESTION_TEXT" => GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_QUESTION"),
                "BLOCKED" => GetMessage("PROTEK_TEMPLATE_CITY_CHANGING_BLOCK"),
            ),
            false
        );
        global $arCity;
        $arCity = array(
            'ID' => $arComponentOutput['CURRENT_CITY']['ID'],
            'NAME' => $arComponentOutput['CURRENT_CITY']['NAME'],
            'ADDRESS' => $arComponentOutput['CURRENT_CITY']['ADDRESS'],
            'EMAIL' => $arComponentOutput['CURRENT_CITY']['EMAIL'],
            'PHONE' => $arComponentOutput['CURRENT_CITY']['PHONE'],
            'TIME' => $arComponentOutput['CURRENT_CITY']['TIME'],
            'LOCATION' => $arComponentOutput['CURRENT_CITY']['LOCATION'],
        );
        unset($arComponentOutput);
        ?>







        <!-- NEW MOBILE MENU -->
        <? include ($_SERVER['DOCUMENT_ROOT'].'/local/include_area/mobile_menu.php'); ?>
        <!-- /NEW MOBILE MENU -->




        <div class="header__content hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-3 col-xl-4">
                        <div class="header__mobmenu">
                            <div class="header__mobmenu-nav">
                                <button class="header__mobmenu-phone"
                                        type="button">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 348.077 348.077"
                                         xml:space="preserve"><path
                                                d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"/></svg>
                                </button>
                                <button class="header__mobmenu-search"
                                        type="button">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 56.966 56.966"
                                         xml:space="preserve"><path
                                                d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23 s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92 c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17 s-17-7.626-17-17S14.61,6,23.984,6z"/></svg>
                                </button>
                                <a href="#modal-cart"
                                   class="header__mobmenu-cart<?if ($APPLICATION->GetCurPage() !== '/cart/'){echo ' open-modal';}?>">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 260.293 260.293"
                                         xml:space="preserve"><path
                                                d="M258.727,57.459c-1.42-1.837-3.612-2.913-5.934-2.913H62.004l-8.333-32.055c-0.859-3.306-3.843-5.613-7.259-5.613H7.5 c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h33.112l8.333,32.055c0,0.001,0,0.001,0.001,0.002l29.381,112.969 c0.859,3.305,3.843,5.612,7.258,5.612h137.822c3.415,0,6.399-2.307,7.258-5.612l29.385-112.971 C260.636,61.687,260.147,59.295,258.727,57.459z M117.877,167.517H91.385l-5.892-22.652h32.384V167.517z M117.877,129.864H81.592 l-5.895-22.667h42.18V129.864z M117.877,92.197H71.795l-5.891-22.651h51.973V92.197z M176.119,167.517h-43.242v-22.652h43.242 V167.517z M176.119,129.864h-43.242v-22.667h43.242V129.864z M176.119,92.197h-43.242V69.546h43.242V92.197z M217.609,167.517 h-26.49v-22.652h32.382L217.609,167.517z M227.403,129.864h-36.284v-22.667h42.18L227.403,129.864z M237.201,92.197h-46.081V69.546 h51.974L237.201,92.197z"/>
                                        <path d="M105.482,188.62c-15.106,0-27.396,12.29-27.396,27.395c0,15.108,12.29,27.4,27.396,27.4 c15.105,0,27.395-12.292,27.395-27.4C132.877,200.91,120.588,188.62,105.482,188.62z M105.482,228.415 c-6.835,0-12.396-5.563-12.396-12.4c0-6.835,5.561-12.395,12.396-12.395c6.834,0,12.395,5.561,12.395,12.395 C117.877,222.853,112.317,228.415,105.482,228.415z"/>
                                        <path d="M203.512,188.62c-15.104,0-27.392,12.29-27.392,27.395c0,15.108,12.288,27.4,27.392,27.4 c15.107,0,27.396-12.292,27.396-27.4C230.908,200.91,218.618,188.62,203.512,188.62z M203.512,228.415 c-6.833,0-12.392-5.563-12.392-12.4c0-6.835,5.559-12.395,12.392-12.395c6.836,0,12.396,5.561,12.396,12.395 C215.908,222.853,210.347,228.415,203.512,228.415z"/></svg>
                                    <span class="cart-btn-wrap-basket"><?= (count($_SESSION['CYBER_BASKET_' . SITE_ID]) > 0 ? count($_SESSION['CYBER_BASKET_' . SITE_ID]) : "") ?></span>
                                </a>
                            </div>




                            <div class="header__mob_fav_comp">
                                <a href="<?= SITE_DIR ?>catalog/compare.php">
                                    <?= GetMessage('PROTEK_TEMPLATE_COMPARE') ?>
                                    <span class="cart-btn-wrap-compare">
                                        <? if (isset($_SESSION[COMPARE_NAME . SITE_ID])) {
                                            if (reset($_SESSION[COMPARE_NAME . SITE_ID])) {
                                                $arCompare = reset($_SESSION[COMPARE_NAME . SITE_ID]);
                                                if (count($arCompare['ITEMS']) > 0) {
                                                    echo "(" . count($arCompare['ITEMS']) . ")";
                                                }
                                            }
                                        } ?>
                                    </span>
                                </a>
                                <a href="<?= SITE_DIR ?>catalog/favorites/">
                                    <?= GetMessage('PROTEK_TEMPLATE_FAVORITES') ?>
                                    <span class="cart-btn-wrap-favorites">
                                        <? if (isset($_SESSION[FAVORITES_NAME . SITE_ID])) {
                                            if (count($_SESSION[FAVORITES_NAME . SITE_ID]) > 0) {
                                                echo "(" . count($_SESSION[FAVORITES_NAME . SITE_ID]) . ")";
                                            }
                                        } ?>
                                    </span>
                                </a>
                            </div>



                            <?$APPLICATION->IncludeComponent("bitrix:search.form", "protek_search_input", Array(
                                "PAGE" => "#SITE_DIR#search/",
                                "SEARCH_CSS_CLASS" => "header__mobsearch",
                                "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
                            ),
                                false
                            ); ?>

                            <div class="header__mobcontent">
                                <div class="header__contacts">
                                    <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                                       class="header__contacts-phone cyber_location_string phone">
                                        <svg version="1.1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px"
                                             y="0px"
                                             viewBox="0 0 348.077 348.077"
                                             xml:space="preserve"><path
                                                    d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"></path></svg>
                                        <span class="ya-phone"><?= $arCity['PHONE']; ?></span>
                                    </a>
                                    <div class="header__contacts-city">
                                        <svg version="1.1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px"
                                             y="0px"
                                             viewBox="0 0 80 80"
                                             xml:space="preserve"><path
                                                    d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path></svg>
                                        <span><?= GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_TITLE") ?></span>

                                        <!--noindex-->
                                        <a href="#modal-location" class="open-modal cyber_location_name"><?= $arCity['NAME'] ?></a>
                                        <!--/noindex-->
                                    </div>
                                    <a href="mailto:<?= $arCity['EMAIL'] ?>"
                                       class="header__contacts-mail cyber_location_string email">
                                        <svg version="1.1"
                                             xmlns="http://www.w3.org/2000/svg"
                                             xmlns:xlink="http://www.w3.org/1999/xlink"
                                             x="0px"
                                             y="0px"
                                             viewBox="0 0 14 14"
                                             xml:space="preserve"><path
                                                    d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                                            <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/>
                                            <polygon
                                                    points="0,2.878 0,11.186 4.833,7.079"/>
                                            <polygon
                                                    points="9.167,7.079 14,11.186 14,2.875"/>
									</svg>
                                        <span><?= $arCity['EMAIL'] ?></span></a>
                                </div>

                                <div class="header__btns">
                                    <a href="#modal-call"
                                       class="header__btn open-modal"
                                       onclick="yaCounter30699088.reachGoal('goal1'); return true;"><?= GetMessage("PROTEK_TEMPLATE_ORDER_BUTTON") ?></a>
                                    <a href="#modal-request"
                                       class="header__btn open-modal"
                                       onclick="yaCounter30699088.reachGoal('goal3'); return true;"><?= GetMessage("PROTEK_TEMPLATE_CALLBACK") ?></a>
                                </div>

                                <div class="header__social">
                                    <div class="header__social-list">
                                        <span><?= GetMessage("PROTEK_TEMPLATE_SOCIAL_TITLE") ?></span>
                                        <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/socnet.php"), false); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="header__logo">
                            <a href="/">
                                <svg enable-background="new 0 0 443.2 155.5"
                                     version="1.1"
                                     viewBox="0 0 443.2 155.5"
                                     xml:space="preserve"
                                     xmlns="http://www.w3.org/2000/svg">
									<style type="text/css">
                                        .cyber_super_class_for_svg0 {
                                            fill: #45A2D0
                                        }

                                        .cyber_super_class_for_svg1 {
                                            fill: #595651
                                        }
                                    </style>
                                    <path class="cyber_super_class_for_svg0"
                                          d="m228.1 45.2h-15.2v13.8h15.2c3 0 5.3-0.6 7.2-1.7 2-1.2 3-2.9 3-5 0-4.7-3.4-7.1-10.2-7.1m10.2 16.5c-3 1.7-6.8 2.5-11.3 2.5h-14.1v16.8h-5.9v-41h20.7c11 0 16.6 4.1 16.6 12.3-0.1 4.1-2.1 7.3-6 9.4"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="m287.3 49.3c-3.1-2.6-7.4-3.9-13-3.9s-9.9 1.3-13 3.9c-3 2.6-4.5 6.4-4.5 11.2s1.5 8.6 4.6 11.4 7.4 4.2 12.9 4.2c5.6 0 9.9-1.4 13-4.1 3.1-2.8 4.6-6.6 4.6-11.5 0-4.8-1.5-8.6-4.6-11.2m4.2 26.5c-4.2 3.8-9.9 5.6-17.1 5.6s-12.9-1.9-17.1-5.6-6.3-8.8-6.3-15.3 2.1-11.5 6.4-15.1c4.1-3.5 9.8-5.3 17-5.3s12.9 1.8 17 5.3c4.3 3.6 6.4 8.7 6.4 15.1s-2.1 11.5-6.3 15.3"/>
                                    <polygon
                                            class="cyber_super_class_for_svg0"
                                            points="323.3 45.2 323.3 81 317.4 81 317.4 45.2 301.5 45.2 301.5 40 339.2 40 339.2 45.2"/>
                                    <polygon
                                            class="cyber_super_class_for_svg0"
                                            points="433.4 40 404.7 59.8 404.7 40 398.8 40 398.8 81 404.7 81 404.7 66.3 412 61.1 434.8 81 443.2 81 416.8 57.9 443.1 40"/>
                                    <polygon
                                            class="cyber_super_class_for_svg0"
                                            points="198.6 42.4 198.6 40 160.9 40 160.9 43.4 160.9 45.2 160.9 81 166.8 81 166.8 45.2 176.8 45.2 182.7 45.2 192.7 45.2 192.7 81 198.6 81"/>
                                    <polygon
                                            class="cyber_super_class_for_svg1"
                                            points="188.7 98.4 182.6 98.4 164.6 110.8 164.6 98.4 160.9 98.4 160.9 124.2 164.6 124.2 164.6 114.9 169.2 111.7 183.5 124.2 188.8 124.2 172.2 109.7"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="m226.1 118.5c-1.9 1.7-4.6 2.6-8.2 2.6-3.5 0-6.2-0.9-8.1-2.6s-2.9-4.1-2.9-7.2c0-3 1-5.4 2.9-7s4.6-2.5 8.1-2.5 6.2 0.8 8.2 2.4c1.9 1.6 2.9 4 2.9 7s-1 5.6-2.9 7.3m2.6-16.7c-2.6-2.2-6.2-3.3-10.7-3.3s-8.1 1.1-10.7 3.3c-2.7 2.3-4 5.4-4 9.5 0 4 1.3 7.2 3.9 9.6 2.6 2.3 6.2 3.5 10.8 3.5 4.5 0 8.1-1.2 10.8-3.5 2.7-2.4 4-5.5 4-9.6-0.1-4-1.4-7.2-4.1-9.5"/>
                                    <polygon
                                            class="cyber_super_class_for_svg1"
                                            points="271.7 109.1 254.4 109.1 254.4 98.4 250.7 98.4 250.7 124.2 254.4 124.2 254.4 112.4 271.7 112.4 271.7 124.2 275.4 124.2 275.4 98.4 271.7 98.4"/>
                                    <polygon
                                            class="cyber_super_class_for_svg1"
                                            points="341.7 112.4 357.5 112.4 357.5 109.1 341.7 109.1 341.7 101.7 359.6 101.7 359.6 98.4 338 98.4 338 124.2 359.8 124.2 359.8 120.9 341.7 120.9"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="m394.9 109.3c-1.2 0.7-2.7 1.1-4.5 1.1h-9.6v-8.6h9.5c4.3 0 6.4 1.5 6.4 4.4 0.1 1.3-0.5 2.3-1.8 3.1m-4.8-10.9h-13v25.7h3.7v-10.5h8.8c2.8 0 5.2-0.5 7.1-1.6 2.5-1.3 3.7-3.3 3.7-6 0.1-5-3.3-7.6-10.3-7.6"/>
                                    <polygon
                                            class="cyber_super_class_for_svg1"
                                            points="439.5 98.4 439.5 109.1 422.2 109.1 422.2 98.4 418.5 98.4 418.5 124.2 422.2 124.2 422.2 112.4 439.5 112.4 439.5 124.2 443.2 124.2 443.2 98.4"/>
                                    <polygon
                                            class="cyber_super_class_for_svg1"
                                            points="319 120.8 319 98.4 315.3 98.4 315.3 120.8 298.1 120.8 298.1 98.4 294.4 98.4 294.4 124.2 298.1 124.2 308.3 124.2 308.3 124.2 320.5 124.2 320.5 126.5 324.2 126.5 324.2 120.8 320.5 120.8"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="m359.3 81c-7.2 0-13.6-0.8-13.6-10.7v-3.3h5.5v2.8c0 4.9 1.4 5.8 8.2 5.8h18.7c6.2 0 6.7-3 6.7-12.2v-1.1h-30.6v-5h30.6v-4.7c0-6.5-2.5-7.5-11.2-7.5h-16c-5.1 0-6 1.7-6 6.8v1.4h-5.5v-1.5c0-8.6 3-12 10.9-12h17.2c8.6 0 16.3 1 16.3 13v15.2c0 8.2-4.3 13-11.6 13h-19.6z"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="m97.7 79s-19.3-36.4-43.4-22.6c-24.2 13.8-46.2 28.6-21.5 53 0 0-12.3-7.6-21-12.3-8.7-4.6-25.3-13.7 8.4-34.9 33.7-21.3 33.6-22.3 44.8-22.3 11.1 0.1 30 19.5 32.7 39.1"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="m74.6 106.6s37.7-17.5 24.7-41.9-27.1-46.6-52.8-23.3c0 0 8.2-11.8 13.3-20.2 5-8.4 14.9-24.4 35 9.7s21.2 34 20.7 45c-0.5 11.1-21 28.9-40.9 30.7"/>
                                    <path class="cyber_super_class_for_svg1"
                                          d="m72.6 58.9s-37.9 17.1-25.2 41.6 26.5 47 52.5 24c0 0-8.4 11.7-13.5 20-5.2 8.3-15.2 24.2-34.9-10.1-19.8-34.4-20.9-34.4-20.2-45.4 0.7-10.9 21.4-28.5 41.3-30.1"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="m48.6 83.1s17 37.5 42 25.2 47.8-25.8 24.7-51.6c0 0 11.8 8.3 20.2 13.5 8.4 5.1 24.4 15.1-10.5 34.4-35 19.1-35 20.1-46.2 19.4-11.1-0.8-28.7-21.3-30.2-40.9"/>
                                    <path class="cyber_super_class_for_svg0"
                                          d="m144.8 12.9c-0.7 3.6-2 7-3.8 10.3-2.1 3.8-4.8 7.2-7.9 10.3-3.2 3.2-6.7 6.1-10.4 8.7l-3.9 2.7c-0.1 0.1-0.2 0.1-0.3 0.2h-0.1c0.1-0.7 0.1-1.4 0.3-2 0.5-2.5 1.6-4.6 3.2-6.6 1.5-1.9 3.3-3.6 5.1-5.3 2-1.8 4.1-3.6 6-5.5 2.7-2.7 5.1-5.6 7.4-8.6 1.5-2 3-4.1 4.5-6.1 0-0.1 0.1-0.1 0.1-0.2h0.1c-0.1 0.8-0.2 1.4-0.3 2.1m4.5-9.8l-0.3-2.7c0-0.1-0.1-0.3-0.2-0.4-0.1 0.1-0.3 0.2-0.4 0.3-0.2 0.2-0.4 0.6-0.6 0.8-1.1 1-2.2 2-3.3 2.9-2.9 2.3-6.1 4.1-9.4 5.9-2.9 1.6-5.8 3.1-8.7 4.7-2.5 1.4-4.9 3-7.1 5-4.6 4.4-7.2 9.6-7.4 16-0.1 2.2 0 4.3 0.7 6.4 0.4 1.3 0.6 2.6 0.9 3.9 0.3 1.7 0.1 3.3-0.6 4.8-0.2 0.3-0.3 0.7-0.5 1l2.4 3.5c0.3-2 1.1-3.7 2.6-5.1 0.2-0.2 0.4-0.3 0.6-0.3h1.2c4.3-0.2 8.5-1 12.4-2.8 2.9-1.3 5.5-3.1 7.7-5.3 3.3-3.3 5.5-7.2 7.1-11.5 1.4-3.9 2.2-7.8 2.7-11.9 0.6-5 0.6-10.1 0.2-15.2"
                                          fill-opacity="0"/>
								</svg>
                            </a>
                        </div>
                    </div>

                    <div class="col-12 col-md-12 col-lg-9 col-xl-8">
                        <div class="header__content-wrap">
                            <div class="header__contacts">
                                <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                                   class="header__contacts-phone cyber_location_string phone">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 348.077 348.077"
                                         xml:space="preserve"><path
                                                d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"></path></svg>
                                    <span class="ya-phone"><?= $arCity['PHONE']; ?></span>
                                </a>
                                <div class="header__contacts-city">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 80 80"
                                         xml:space="preserve"><path
                                                d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path></svg>
                                    <span><?= GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_TITLE") ?></span>

                                    <!--noindex-->
                                    <a href="#modal-location" class="open-modal cyber_location_name"><?= $arCity['NAME'] ?></a>
                                    <!--/noindex-->
                                </div>
                                <a href="mailto:<?= $arCity['EMAIL'] ?>"
                                   class="header__contacts-mail cyber_location_string email">
                                    <svg version="1.1"
                                         xmlns="http://www.w3.org/2000/svg"
                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                         x="0px"
                                         y="0px"
                                         viewBox="0 0 14 14"
                                         xml:space="preserve"><path
                                                d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                                        <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/>
                                        <polygon
                                                points="0,2.878 0,11.186 4.833,7.079"/>
                                        <polygon
                                                points="9.167,7.079 14,11.186 14,2.875"/>
								</svg>
                                    <span><?= $arCity['EMAIL'] ?></span></a>
                            </div>

                            <div class="header__btns">
                                <a href="#modal-call"
                                   class="header__btn open-modal"
                                   style="display: none;"
                                   onclick="yaCounter30699088.reachGoal('goal1'); return true;"><?= GetMessage("PROTEK_TEMPLATE_ORDER_BUTTON") ?></a>
                                <a href="#modal-request"
                                   class="header__btn open-modal"
                                   onclick="yaCounter30699088.reachGoal('goal3'); return true;"><?= GetMessage("PROTEK_TEMPLATE_CALLBACK") ?></a>
                            </div>

                            <div class="header__social">
                                <div class="header__lang">
                                    <span class="active">RU</span>
                                    <a href="/en/">EN</a>
                                    <a href="/de/">DE</a>
                                    <a href="/cn/">CN</a>
                                    <a href="/es/">ES</a>
                                </div>

                                <div class="header__social-list">
                                    <span><?= GetMessage("PROTEK_TEMPLATE_SOCIAL_TITLE") ?></span>
                                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/socnet.php"), false); ?>
                                </div>
                            </div>
                        </div>
                        <div class="search__block">
                            <div class="search-block__form">
                                <?  $APPLICATION->IncludeComponent(
                                    "bitrix:search.form",
                                    "protek_search_input",
                                    Array(
                                        "PAGE"               => "#SITE_DIR#search/",
                                        "SEARCH_CSS_CLASS"   => "header__search",
                                        "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
                                    ),
                                    false
                                ); ?>
                            </div>
                            <div class="search-block__result">
                                <div class="search-result__list" data-scroll>
                                    <div class="search-result__item">
                                        <div class="search-item__name">
                                            Пожалуйста, введите не менее 3 символов
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>



    <div class="nav nav--head hidden-xs">
        <div class="container nav__wrap">
            <div class="row">
                <div class="col-12">
                    <div class="nav__content">
                        <button class="nav__btn nav__btn--head"
                                type="button"
                                data-toggle="collapse"
                                href="#collapsed_mobile_menu"
                                aria-expanded="false"
                                aria-controls="collapse0">
                            <svg version="1.1"
                                 xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 x="0px"
                                 y="0px"
                                 viewBox="0 0 92.833 92.833"
                                 xml:space="preserve"><path
                                        d="M89.834,1.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V4.75 C92.834,3.096,91.488,1.75,89.834,1.75z"/>
                                <path d="M89.834,36.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V39.75 C92.834,38.096,91.488,36.75,89.834,36.75z"/>
                                <path d="M89.834,71.75H3c-1.654,0-3,1.346-3,3v13.334c0,1.654,1.346,3,3,3h86.833c1.653,0,3-1.346,3-3V74.75 C92.834,73.095,91.488,71.75,89.834,71.75z"/></svg>
                            <span data-toggle-caption="<?= GetMessage("PROTEK_TEMPLATE_CLOSE_MENU") ?>"><?= GetMessage("PROTEK_TEMPLATE_OPEN_MENU") ?></span>
                        </button>



                        <div class="collapse nav__mob" id="collapsed_mobile_menu">
                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "protek_mobile_menu",
                                Array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "2",
                                    "MENU_CACHE_GET_VARS" => array(0 => "",),
                                    "MENU_CACHE_TIME" => "36000",
                                    "MENU_CACHE_TYPE" => "A",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "top",
                                    "USE_EXT" => "Y",
                                    "MENU_SHOW_LESS" => GetMessage("PROTEK_TEMPLATE_SHOW_LESS"),
                                )
                            ); ?>
                            <div class="nav__submenu">
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:menu",
                                    "protek_top_menu",
                                    Array(
                                        "ALLOW_MULTI_SELECT" => "N",
                                        "CHILD_MENU_TYPE" => "left",
                                        "DELAY" => "N",
                                        "MAX_LEVEL" => "1",
                                        "MENU_CACHE_GET_VARS" => array(0 => "",),
                                        "MENU_CACHE_TIME" => "36000",
                                        "MENU_CACHE_TYPE" => "A",
                                        "MENU_CACHE_USE_GROUPS" => "Y",
                                        "ROOT_MENU_TYPE" => "first",
                                        "USE_EXT" => "N"
                                    )
                                ); ?>
                            </div>
                        </div>



                        <? $APPLICATION->IncludeComponent("bitrix:menu", "protek_main_menu1_new", Array(
	"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
		"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
		"DELAY" => "N",	// Откладывать выполнение шаблона меню
		"MAX_LEVEL" => "1",	// Уровень вложенности меню
		"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
			0 => "",
		),
		"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
		"MENU_CACHE_TYPE" => "A",	// Тип кеширования
		"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
		"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
		"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
	),
	false
); ?>

                    </div>
                </div>
            </div>
        </div>
         <div class="search__block scroll">
            <div class="search-block__form">
                <? $APPLICATION->IncludeComponent("bitrix:search.form", "protek_search_input", Array(
                    "PAGE" => "#SITE_DIR#search/",
                    "SEARCH_CSS_CLASS" => "nav__search",
                    "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
                    "SEARCH_USE_CONTAINER" => "Y",
                ),
                    false
                ); ?>
            </div>
            <div class="container relative">
                <div class="search-block__result">
                    <div class="search-result__list" data-scroll>
                        <div class="search-result__item">
                            <div class="search-item__name">
                                Пожалуйста, введите не менее 3 символов
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>




<? if ($APPLICATION->GetCurPage() != SITE_DIR) { ?>
    <div class="section section--bottom0 <? $APPLICATION->ShowProperty('Breadcrumb')?>">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "main",
                        Array(
                            "PATH" => "",
                            "SITE_ID" => SITE_ID,
                            "START_FROM" => "0"
                        )
                    ); ?>
                </div>
            </div>
        </div>
    </div>
<? } ?>