<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => "Cyber basket",
	"DESCRIPTION" => "Cyber basket",
	
	"SORT" => 20,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "cyberrex",
		"CHILD" => array(
			"ID" => "basket",
			"NAME" => "Cyber Basket",
			"SORT" => 10,
		),
	),
);

?>