<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arCurrentValues */

if(!CModule::IncludeModule("iblock"))
	return;

$arTypesEx = CIBlockParameters::GetIBlockTypes(array("-"=>" "));

$arIBlocks=array();
$db_iblock = CIBlock::GetList(array("SORT"=>"ASC"), array("SITE_ID"=>$_REQUEST["site"], "TYPE" => ($arCurrentValues["IBLOCK_TYPE"]!="-"?$arCurrentValues["IBLOCK_TYPE"]:"")));
while($arRes = $db_iblock->Fetch())
	$arIBlocks[$arRes["ID"]] = "[".$arRes["ID"]."] ".$arRes["NAME"];

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"IBLOCK_TYPE" => array(
			"PARENT" => "BASE",
			"NAME" => "Type Инфоблока каталога",
			"TYPE" => "LIST",
			"VALUES" => $arTypesEx,
			"DEFAULT" => "catalog",
			"REFRESH" => "Y",
		),
		"IBLOCK_ID" => array(
			"PARENT" => "BASE",
			"NAME" => "ID Инфоблока каталога",
			"TYPE" => "LIST",
			"VALUES" => $arIBlocks,
			"DEFAULT" => "",
			"ADDITIONAL_VALUES" => "Y",
			"REFRESH" => "Y",
		),
		"BASKET_TITLE" => array(
			"NAME" => "Заголовок блока Коризны",
			"TYPE" => "STRING",
		),
		
		"BASKET_SUM_ITEMS" => array(
			"NAME" => "Всего выбрано",
			"TYPE" => "STRING",
		),
		"BASKET_SUM_VOLUME" => array(
			"NAME" => "Всего объем",
			"TYPE" => "STRING",
		),
		"BASKET_SUM_WEIGHT" => array(
			"NAME" => "Всего вес",
			"TYPE" => "STRING",
		),
		"BASKET_SUM_PACKS" => array(
			"NAME" => "Всего коробов",
			"TYPE" => "STRING",
		),
		"BASKET_CLEAR" => array(
			"NAME" => "Очистить коризну",
			"TYPE" => "STRING",
		),
		"BASKET_ORDER_PRICES" => array(
			"NAME" => "Узнать цены",
			"TYPE" => "STRING",
		),
		"BASKET_IS_EMPTY" => array(
			"NAME" => "Корзина пуста",
			"TYPE" => "STRING",
		),
		"BASKET_OUTTER_SIZES" => array(
			"NAME" => "Внешние размеры, мм",
			"TYPE" => "STRING",
		),
		
	),
);
