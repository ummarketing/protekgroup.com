<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */

use Bitrix\Main\Loader,
	Bitrix\Iblock,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;

$arResult = $arID = array();
session_start();

if (!CModule::IncludeModule("sale")){
	ShowError("SALE_MODULE_NOT_INSTALL");
	return;
}


if($_REQUEST["clear_basket"] && $_REQUEST["clear_basket"] == 'true'){
	CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
	unset($arResult, $_SESSION['CYBER_BASKET_'.SITE_ID]);
}

$dbBasketItems = CSaleBasket::GetList(
	 array(
		"NAME" => "ASC",
		"ID" => "ASC"
	),
	 array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL"
	),
	false,
	false,
	array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS")
);
while ($arItems = $dbBasketItems->Fetch()){
	if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"]){
		
		if($_REQUEST["remove_from_basket"] && intVal($_REQUEST["remove_from_basket"]) == $arItems["PRODUCT_ID"]){
			CSaleBasket::Delete($arItems["ID"]);
		}else{
			$quantity = $arItems["QUANTITY"];
			
			if($_REQUEST["change_quantity"] && intVal($_REQUEST["changed_item"]) == $arItems["PRODUCT_ID"]){
				$quantity = intVal($_REQUEST["change_quantity"]);
			}
			CSaleBasket::UpdatePrice($arItems["ID"],
				$arItems["CALLBACK_FUNC"],
				"N",
				$arItems["PRODUCT_ID"],
				$quantity,
				"N",
				$arItems['PRODUCT_PROVIDER_CLASS']
			);
			$arID[] = $arItems["ID"];
		}
	}
}
if (!empty($arID)){
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
		array(
			"ID" => $arID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "DELAY", "CAN_BUY", "PRICE", "WEIGHT", "PRODUCT_PROVIDER_CLASS", "NAME")
	);
	while ($arItems = $dbBasketItems->Fetch()){
		$arResult['BASKET'][] = $arItems;
	}
}

if(!empty($arResult['BASKET']) && CModule::IncludeModule("iblock")){
	
	$arBasket = $arItems = array();
	foreach($arResult['BASKET'] as $item){
		$arBasket[$item['PRODUCT_ID']] = $item['QUANTITY'];
	}

	$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "ACTIVE", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID"=>IntVal($arParams['IBLOCK_ID']), "ID" => array_keys($arBasket), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	while($ob = $res->GetNextElement()){
		$arFields = $ob->GetFields();
		$arFields['QUANTITY'] = $arBasket[$arFields['ID']];
		$arFields['PROPERTIES'] = $ob->GetProperties();
		
		if(!empty($arFields['PROPERTIES']['PACK_TYPE']['VALUE']) && CModule::IncludeModule('highloadblock')){
			$hlblock = HL\HighloadBlockTable::getById(9)->fetch();
			$entity = HL\HighloadBlockTable::compileEntity($hlblock);
			$entityClass = $entity->getDataClass();

			$resHL = $entityClass::getList(array(
			   'select' => array('*'),
			   'filter' => array('UF_XML_ID' => $arFields['PROPERTIES']['PACK_TYPE']['VALUE'])
			));

			$row = $resHL->fetch();
			$arFields['PROPERTIES']['PACK_TYPE']['VALUE'] = ($row['UF_LENGTH'] * $row['UF_WIDTH'] * $row['UF_HEIGHT']) / 1000000000;
			unset($arFields['PROPERTIES']['PACK_VOLUME']);
			
		}else{
			$arFields['PROPERTIES']['PACK_TYPE']['VALUE'] = 0;
		}
		if(!empty($arFields['PROPERTIES']['OUTER_LENGTH']['VALUE']) && !empty($arFields['PROPERTIES']['OUTER_WIDTH']['VALUE']) && !empty($arFields['PROPERTIES']['OUTER_HEIGHT']['VALUE'])){
			$arFields['PROPERTIES']['OUTTER_SIZES'] = array(
				'NAME' => $arParams['BASKET_OUTTER_SIZES'],
				'VALUE' => $arFields['PROPERTIES']['OUTER_LENGTH']['VALUE'] . "x" . $arFields['PROPERTIES']['OUTER_WIDTH']['VALUE'] . "x" . $arFields['PROPERTIES']['OUTER_HEIGHT']['VALUE']
			);
		}
		$arItems[] = $arFields;
	}
	
	$arResult['BASKET'] = $arItems;
}


unset($_SESSION['CYBER_BASKET_'.SITE_ID]);

if(!empty($arResult['BASKET'])){
	foreach($arResult['BASKET'] as $item){
		$_SESSION['CYBER_BASKET_'.SITE_ID][$item['ID']] = array(
			"ID" => $item['ID'],
			"IBLOCK_ID" => IntVal($arParams['IBLOCK_ID']),
			"NAME" => $item['NAME'],
			"QUANTITY" => $item['QUANTITY'],
			"DETAIL_PAGE_URL" => $item['DETAIL_PAGE_URL']
		);
	}
}

if ($_REQUEST["cyber_ajax"] && ($_REQUEST["cyber_ajax"] == "y")) {
	$APPLICATION->RestartBuffer();
	$this->IncludeComponentTemplate();
	die();
}else{
	$this->IncludeComponentTemplate();
}
