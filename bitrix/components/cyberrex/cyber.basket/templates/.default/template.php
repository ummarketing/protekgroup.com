<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? if ($_REQUEST['collapse_open'] == 'true') {
    $arResult['COLLAPSE_IS_OPEN'] = true;
} else {
    $arResult['COLLAPSE_IS_OPEN'] = false;
} ?>
    <h6 class="modal__title"><?= $arParams['BASKET_TITLE'] ?></h6>

<? if (!empty($arResult['BASKET'])): ?>
    <div class="cartmodal__items" id="cyber_basket_modal">
        <? foreach ($arResult['BASKET'] as $key => $item):
            //        if($key == 2){?>
            <!--    <div class="collapse --><? //=($arResult['COLLAPSE_IS_OPEN'] ? "show" : "")?><!--" id="cartmodal__items">--><? //}?>
            <div class="cartmodal__item">
                <div>
                    <img class="cartmodal__item-img" src="<?
                    $arWaterMark = array(
                        array(
                            "name" => "watermark",
                            "position" => "center",
                            "type" => "image",
                            "size" => "medium",
                            "file" => WATERMARK_PATH,
                            "fill" => "exact",
                            "alpha_level" => 26
                        )
                    );
                    $arPhotoSmall = CFile::ResizeImageGet(
                        $item['PREVIEW_PICTURE'],
                        array(
                            'width' => 200,
                            'height' => 141
                        ),
                        BX_RESIZE_IMAGE_EXACT,
                        false,
                        $arWaterMark,
                        85
                    );
                    echo $arPhotoSmall['src']; ?>" alt="<?= $item['NAME'] ?>">

                    <div class="cartmodal__item-quantity" data-cyber_id="<?= $item['ID'] ?>">
                        <button class="cyber_quantity-control minus" type="button">-</button>
                        <input class="cyber_quantity" type="text" value="<?= $item['QUANTITY'] ?>">
                        <button class="cyber_quantity-control plus" type="button">+</button>
                    </div>
                </div>

                <div class="cartmodal__item-meta">
                    <a href="<?= $item['DETAIL_PAGE_URL'] ?>"><h3><?= $item['NAME'] ?></h3></a>
                    <? $arDisplayedProps = array(
                        "ART",
                        "MATERIAL",
                        "PACK_QUANTITY",
                        "PACK_TYPE",
                        "PACK_VOLUME",
                        "OUTTER_SIZES"
                    );
                    ?>
                    <ul><?
                        foreach ($arDisplayedProps as $key) {
                            if (!empty($item['PROPERTIES'][$key]['VALUE'])) {
                                ?>
                                <li><?= $item['PROPERTIES'][$key]['NAME'] ?>
                                : <?= (is_array($item['PROPERTIES'][$key]['VALUE']) ? implode($item['PROPERTIES'][$key]['VALUE'], ", ") : $item['PROPERTIES'][$key]['VALUE']) ?></li><?
                            }
                        }
                        ?></ul>
                </div>

                <button class="cartmodal__item-delete" type="button" data-cyber_id="<?= $item['ID'] ?>">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px"
                         y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;"
                         xml:space="preserve"><path
                                d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"/></svg>
                </button>
            </div>
        <? endforeach; ?>
        <!--        --><? // if(count($arResult['BASKET']) > 2){?><!--</div>--><? // }?>
        <div class="cartmodal__wrap-clear">
            <button class="cartmodal__clear" type="button"><?= $arParams['BASKET_CLEAR'] ?></button>
        </div>
        <div class="cartmodal__values">
            <p><?= $arParams["BASKET_SUM_ITEMS"] ?>: <b
                        class="cyberbasket_sum_items"><?= count($arResult['BASKET']) ?></b></p>
            <p><?= $arParams["BASKET_SUM_VOLUME"] ?>: <b class="cyberbasket_sum_volume"><?
                    $sumVolume = 0;
                    foreach ($arResult['BASKET'] as $item) {
                        $sumVolume += $item['QUANTITY'] * ($item['PROPERTIES']['PACK_TYPE']['VALUE'] ? $item['PROPERTIES']['PACK_TYPE']['VALUE'] : (floatval($item['PROPERTIES']['PACK_VOLUME']['VALUE']) ? floatval($item['PROPERTIES']['PACK_VOLUME']['VALUE']) : 0));
                    }
                    echo $sumVolume;
                    ?></b></p>
            <p><?= $arParams["BASKET_SUM_WEIGHT"] ?>: <b class="cyberbasket_sum_weight"><?
                    $sumWeight = 0;
                    foreach ($arResult['BASKET'] as $item) {
                        $sumWeight += $item['QUANTITY'] * $item['PROPERTIES']['PACK_WEIGHT']['VALUE'];
                    }
                    echo $sumWeight;
                    ?></b></p>
            <p><?= $arParams["BASKET_SUM_PACKS"] ?>: <b class="cyberbasket_sum_packs"><?
                    $sumPacks = 0;
                    foreach ($arResult['BASKET'] as $item) {
                        $sumPacks += $item['QUANTITY'];
                    }
                    echo $sumPacks; ?></b></p>
        </div>
    </div>
    <? if (0): ?>
        <? if (count($arResult['BASKET']) > 2): ?>
            <a class="cartmodal__more" data-toggle="collapse" href="#cartmodal__items" role="button"
               aria-expanded="<?= ($arResult['COLLAPSE_IS_OPEN'] ? "true" : "false") ?>"
               aria-controls="cartmodal__items">Показать
                еще</a>
        <? endif; ?>
    <? endif; ?>
    <div class="cartmodal__total">
        <div class="cartmodal__actions">
            <a href="#modal-price" class="cartmodal__prices cartwrap-open-modal"
               onclick="_gaq.push(['_trackEvent', 'form','user_click11']);yaCounter30699088.reachGoal('goal10'); return true;"><?= $arParams['BASKET_ORDER_PRICES'] ?></a>
            <a href="<?= $arParams['LINK_TO_CART'] ?>" class="cartmodal__maincart "
               onclick="_gaq.push(['_trackEvent', 'form','user_click14']);yaCounter30699088.reachGoal('goal8'); return true;"><?= $arParams['LINK_TO_CART_TITLE'] ?></a>
        </div>
    </div>
<? else: ?>
    <br/><p class="text-center"><?= $arParams['BASKET_IS_EMPTY'] ?></p>
<? endif; ?>