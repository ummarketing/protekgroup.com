$(document).ready(function(){
	initPopup();
	$('body').on('input change', '.cyber_quantity', function() { //Только цифры в инпутах
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		if(this.value == 0){
			this.value = 1;
		}
		changeQuantity($(this), $('#cartmodal__items').hasClass('show'));
	});
	$('body').on('click', '.cyber_quantity-control', function(){ //Изменение количества (клики по плюсу и минусу)
		let quantutyInput = $(this).siblings('input'),
			quantity = 1;
		if($(this).hasClass('minus')) {
			
			if(quantutyInput.val() > 1) { //Не изменяем количество если после нажатия на минус в поле должен появиться ноль
				quantutyInput.val(Number(quantutyInput.val()) - 1);
				changeQuantity($(this), $('#cartmodal__items').hasClass('show'));
			}
			
		} else {
			quantutyInput.val(Number(quantutyInput.val()) + 1);
			changeQuantity($(this), $('#cartmodal__items').hasClass('show'));
		}
		
	});
	
	$('body').on('click', '.cartmodal__clear', function(){ // Очистить
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "clear_basket=true&cyber_ajax=y",
			success: function(data) {
				
				$('#modal-cart .modal__content').html(data);
				initPopup();
				$('body').trigger('cyber.change.basket.basket');
			}
		});
	});
	
	$('body').on('click', '.cartmodal__item-delete', function(){ // Удалить конкретный товар
		
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "remove_from_basket=" + $(this).data('cyber_id') + "&cyber_ajax=y",
			success: function(data) {
				
				$('#modal-cart .modal__content').html(data);
				initPopup();
				$('body').trigger('cyber.change.basket.basket');
			}
		});
	});
	
	$('body').on('cyber.change.basket.result', function(){ // перезагрузка формы по событию (добавление товара в корзину)
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "cyber_ajax=y",
			success: function(data) {
				
				$('#modal-cart .modal__content').html(data);
				initPopup();
				$('body').trigger('cyber.change.basket.basket');
			}
		});
	});
	
	function changeQuantity(element, collapseOpen){ // Изменение количества в коризне
		$.ajax({
			method: 'POST',
			url: window.location.href,
			data: "cyber_ajax=y&change_quantity=" + element.parents('.cartmodal__item-quantity').find('.cyber_quantity').val() + "&changed_item=" + element.parents('.cartmodal__item-quantity').data('cyber_id') + "&collapse_open=" + collapseOpen,
			success: function(data) {
				
				$('#modal-cart .modal__content').html(data);
				initPopup();
				$('body').trigger('cyber.change.basket.quantity');
			}
		});
	}
	
	function initPopup(){
		$('.cartwrap-open-modal').magnificPopup({
			fixedContentPos: true,
			fixedBgPos: true,
			overflowY: 'auto',
			type: 'inline',
			preloader: false,
			modal: false,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in',
			callbacks: {
				open: function() {
					if ($(window).width() > 991) {
						$('.nav--scroll').css('margin-left', "-" + (getScrollBarWidth()/2) + "px");
					}

					if ($(window).width() > 1199) {
						$('.cart-btn-wrap').css('margin-right', "-" + ( 705 - (getScrollBarWidth()/2)) + "px");
					}
				},
				close: function() {
					if ($(window).width() > 991) {
						$('.nav--scroll').css('margin-left', 0);
					}

					if ($(window).width() > 1199) {
						$('.cart-btn-wrap').css('margin-right', -705);
					}
				}
			}
		});
		$('.cartwrap-open-modal').on('click', function(){
			$('body').trigger('cyber.open.basket.modal');
		});
	}
	
});