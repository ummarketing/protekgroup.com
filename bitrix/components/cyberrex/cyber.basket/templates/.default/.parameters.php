<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"LINK_TO_CART" => Array(
		"NAME" => "Ссылка на корзину",
		"TYPE" => "STRING",
	),
	"LINK_TO_CART_TITLE" => Array(
		"NAME" => "Подпись ссылки на корзину",
		"TYPE" => "STRING",
	),
);
?>