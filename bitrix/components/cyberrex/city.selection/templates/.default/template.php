<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
$this->setFrameMode(true);
global $APPLICATION;
?>
<script type="text/javascript" data-skip-moving="true">
var yandex_map_coordinates = [<?=(!empty($arResult['CURRENT_CITY']['LOCATION']) ? $arResult['CURRENT_CITY']['LOCATION'] : $arResult['DEFAULT_CITY']['LOCATION'])?>],
	city_change_blocked = "<?=$arParams['BLOCKED']?>";
</script>
<div id="modal-location" class="zoom-anim-dialog mfp-hide modal modal--location">
	<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
	<div class="modal__content">
		<h6 class="modal__title"><?= str_ireplace('%CITY%', '<span class="cyber_location_name">' . ($arResult['CURRENT_CITY']['NAME'] ? $arResult['CURRENT_CITY']['NAME'] : $arResult['DEFAULT_CITY']['NAME']) . '</span>', $arParams['QUESTION_TEXT'])?></h6>

		<div class="modal__btns">
			<a href="#" class="modal__btn modal__btn--green"><?=$arParams['BTN_CLOSE']?></a>
			<a href="#modal-city" class="modal__btn modal__btn--grey open-modal"><?=$arParams['BTN_EDIT']?></a>
		</div>
	</div>
</div>

<div id="modal-city" class="zoom-anim-dialog mfp-hide modal modal--city">
	<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
	<div class="modal__content">
		<h6 class="modal__title"><?=$arParams['POPUP_LABEL'];?></h6>

        <!--noindex-->
		<div class="row">
		<?
		$iColRows = ceil(count($arResult['CITIES_LIST']) / 4);
		$preparedItems = array_chunk($arResult['CITIES_LIST'], $iColRows);
		foreach($preparedItems as $column){
			echo '<div class="col-12 col-md-6 col-lg-3">';
			echo '<span class="modal__abc">' . substr($column[0]['NAME'], 0, 1) . ' - ' . substr($column[key(array_slice($column, -1, 1, TRUE))]['NAME'], 0, 1) . '</span>';
			echo '<ul class="modal__city">';
			foreach($column as $el){
				?><li><span class="cyber_location_click" data-cyber-location="<?=$el['ID']?>"><?=$el['NAME']?></span></li><?
			}
			echo '</ul></div>';
		}

		?>
		</div>
        <!--/noindex-->
	</div>
</div>