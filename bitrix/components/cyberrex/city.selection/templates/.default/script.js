$(document).ready(function(){
	$('.cyber_location_click').click(function(){
		let id = $(this).data('cyber-location'),
			name = $(this).text();
		$.magnificPopup.close();
		
		$.ajax({
			url: "/bitrix/components/cyberrex/city.selection/ajax.php",
			type: "POST",
			dataType: "JSON",
			data: {
				"CITY": id,
				"SITE_ID": SITE_ID
			},
			success: function(response){
				
				if(!response.BLOCK){
					$('.cyber_location_name').text(name);
					
					if(response.LOCATION.length > 5){
						yandex_map_coordinates = response.LOCATION.split(",");
						$(document).trigger('cyber_location_change');
					}
					
					if(response.EMAIL.length > 5){
						
						let cyberEmail = $('.cyber_location_string.email');
						$(cyberEmail).attr('href', 'mailto:' + response.EMAIL);
						$(cyberEmail).find('span').text(response.EMAIL);
						
						
					}
					
					if(response.PHONE.length > 5){
						let cyberPhone = $('.cyber_location_string.phone');
						$(cyberPhone).attr('href', 'tel:+' + response.PHONE.replace(/\D+/g,""));
						$(cyberPhone).find('span').text(response.PHONE);
					}
					
					if(response.ADDRESS.length > 5){
						$('.cyber_location_string.address').text(response.ADDRESS);
					}
					
					if(response.TIME.length > 5){
						$('.cyber_location_string.time').text(response.TIME);
					}
					
				}else{
					console.log('block');
					$('#modal-location .modal__title').html(city_change_blocked);
					$('#modal-location .modal__btns').hide();
					
				}
			}
		});
	});
	
	ymaps.ready(function () {
		
		var myMap = new ymaps.Map('map2', {
			center: yandex_map_coordinates,
			zoom: 13
		}, {
			searchControlProvider: 'yandex#search'
		});
		myMap.behaviors.disable('scrollZoom');
		var placemark = new ymaps.Placemark(myMap.getCenter(), {});
		myMap.geoObjects.add(placemark);
		
		var myMapMobile = new ymaps.Map('map', {
		center: yandex_map_coordinates,
		zoom: 9,
		controls: []
		}, {
			searchControlProvider: 'yandex#search'
		});
		myMapMobile.behaviors.disable('scrollZoom');
		var placemarkMobile = new ymaps.Placemark(myMapMobile.getCenter(), {});
		myMapMobile.geoObjects.add(placemarkMobile);
		
		$(document).on('cyber_location_change', function () {
			
			myMap.setCenter(yandex_map_coordinates, 13, {
				checkZoomRange: true
			});
			myMap.geoObjects.removeAll();
			
			myMapMobile.setCenter(yandex_map_coordinates, 9, {
				checkZoomRange: true
			});
			myMapMobile.geoObjects.removeAll();

			var placemark = new ymaps.Placemark(yandex_map_coordinates, {});
			myMap.geoObjects.add(placemark);
			
			var placemarkMobile = new ymaps.Placemark(yandex_map_coordinates, {});
			myMapMobile.geoObjects.add(placemarkMobile);
			
		});	
		
	});
});