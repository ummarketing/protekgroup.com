<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Loader;

if(Loader::includeModule("iblock")){
	global $APPLICATION;
	$DEFAULT = $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_ID');
	$CURRENT = $APPLICATION->get_cookie('CYBER_CURRENT_CITY_ID');
	
	if($this->startResultCache(36000000, array($DEFAULT, $CURRENT))){
		
		if(!$APPLICATION->get_cookie('CYBER_DEFAULT_CITY_ID') || ($APPLICATION->get_cookie('CYBER_DEFAULT_CITY_ID') != intVal($arParams['ELEMENT_ID']))){
			$arFilter = array(
				"IBLOCK_ID" => intVal($arParams['IBLOCK_ID']),
				"IBLOCK_TYPE" => trim($arParams['IBLOCK_TYPE']),
				"ID" => intVal($arParams['ELEMENT_ID']),
				"ACTIVE" => "Y",
			);
			$arSelect = array(
				"ID",
				"NAME",
				"IBLOCK_ID",
				"PROPERTY_ADDRESS",
				"PROPERTY_EMAIL",
				"PROPERTY_PHONE",
				"PROPERTY_LOCATION",
				"PROPERTY_TIME",
			);
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			if($ar_fields = $res->GetNext()){
				$arResult['DEFAULT_CITY'] = array(
					'ID' => $ar_fields['ID'],
					'NAME' => $ar_fields['NAME'],
					'ADDRESS' => $ar_fields['PROPERTY_ADDRESS_VALUE'],
					'EMAIL' => $ar_fields['PROPERTY_EMAIL_VALUE'],
					'PHONE' => $ar_fields['PROPERTY_PHONE_VALUE'],
					'TIME' => $ar_fields['PROPERTY_TIME_VALUE'],
					'LOCATION' => $ar_fields['PROPERTY_LOCATION_VALUE'],
				);
				
				foreach($arResult['DEFAULT_CITY'] as $key => $value){
					$APPLICATION->set_cookie("CYBER_DEFAULT_CITY_" . $key, $value, time()+60*60*24*30*12*2);
				}
			}
		}else{
			$arResult['DEFAULT_CITY'] = array(
				'ID' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_ID'),
				'NAME' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_NAME'),
				'ADDRESS' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_ADDRESS'),
				'EMAIL' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_EMAIL'),
				'PHONE' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_PHONE'),
				'TIME' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_TIME'),
				'LOCATION' => $APPLICATION->get_cookie('CYBER_DEFAULT_CITY_LOCATION'),
				'FROM_COOKIE' => 'TRUE',
			);
		}
		
		if($APPLICATION->get_cookie('CYBER_CURRENT_CITY_ID') && ($APPLICATION->get_cookie('CYBER_DEFAULT_CITY_ID') == intVal($arParams['ELEMENT_ID']))){
			$arResult['CURRENT_CITY'] = array(
				'ID' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_ID'),
				'NAME' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_NAME'),
				'ADDRESS' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_ADDRESS'),
				'EMAIL' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_EMAIL'),
				'PHONE' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_PHONE'),
				'TIME' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_TIME'),
				'LOCATION' => $APPLICATION->get_cookie('CYBER_CURRENT_CITY_LOCATION'),
				'FROM_COOKIE' => 'TRUE',
			);
		}else{
			$arFilter = array(
				"IBLOCK_ID" => intVal($arParams['IBLOCK_ID']),
				"IBLOCK_TYPE" => trim($arParams['IBLOCK_TYPE']),
				"ID" => intVal($arParams['ELEMENT_ID']),
				"ACTIVE" => "Y",
			);
			$arSelect = array(
				"ID",
				"NAME",
				"IBLOCK_ID",
				"PROPERTY_ADDRESS",
				"PROPERTY_EMAIL",
				"PROPERTY_PHONE",
				"PROPERTY_LOCATION",
				"PROPERTY_TIME",
			);
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			if($ar_fields = $res->GetNext()){
				$arResult['CURRENT_CITY'] = array(
					'ID' => $ar_fields['ID'],
					'NAME' => $ar_fields['NAME'],
					'ADDRESS' => $ar_fields['PROPERTY_ADDRESS_VALUE'],
					'EMAIL' => $ar_fields['PROPERTY_EMAIL_VALUE'],
					'PHONE' => $ar_fields['PROPERTY_PHONE_VALUE'],
					'TIME' => $ar_fields['PROPERTY_TIME_VALUE'],
					'LOCATION' => $ar_fields['PROPERTY_LOCATION_VALUE'],
				);
				
				foreach($arResult['CURRENT_CITY'] as $key => $value){
					$APPLICATION->set_cookie("CYBER_CURRENT_CITY_" . $key, $value, time()+60*60*24*30*12*2);
				}
			}
		}
		
		$arFilter = array(
			"IBLOCK_ID" => intVal($arParams['IBLOCK_ID']),
			"IBLOCK_TYPE" => trim($arParams['IBLOCK_TYPE']),
			"ACTIVE" => "Y",
			"PROPERTY_FILIAL_VALUE" => "Y"
		);
		$arSelect = array(
			"ID",
			"NAME",
		);
		
		$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
		while($ar_fields = $res->GetNext()){
			$arResult['CITIES_LIST'][$ar_fields['NAME']] = $ar_fields;
		}
		ksort($arResult['CITIES_LIST']);
		
		$this->includeComponentTemplate();
	}
	return $arResult;
}else{
	return;
}