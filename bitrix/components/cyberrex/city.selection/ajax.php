<?define("NO_KEEP_STATISTIC", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;?>
<?

$timesToBlock = 10;
$blockTime = 60*5;
$iblocks = array(
	"s1" => array(
		"ID" => "8",
		"TYPE" => "content",
	),
	"s2" => array(
		"ID" => "12",
		"TYPE" => "content_en",
	),
	"s3" => array(
		"ID" => "21",
		"TYPE" => "content_de",
	),
	"s4" => array(
		"ID" => "30",
		"TYPE" => "content_cn",
	),
	"s5" => array(
		"ID" => "36",
		"TYPE" => "content_es",
	),
);


global $APPLICATION;

if($APPLICATION->get_cookie('CYBER_LOCATION_BLOCK') == $timesToBlock){
	$arResult['CITY']['BLOCK'] = true;
	echo json_encode($arResult['CITY'], JSON_UNESCAPED_UNICODE);
}else{
	if(Loader::includeModule("iblock")){

		$times   = intVal($APPLICATION->get_cookie('CYBER_LOCATION_BLOCK'));
		$APPLICATION->set_cookie("CYBER_LOCATION_BLOCK", ($times ? $times : 0) + 1, time() + $blockTime);
		$SITE_ID = htmlspecialchars((string)$_REQUEST['SITE_ID']);

		if(isset($_POST['CITY']) || intVal($_POST['CITY'])){

			$ID = intVal($_POST['CITY']);
			$APPLICATION->set_cookie("MY_CITY", $ID, time()+60*60*24*30*12*2);

			$arFilter = array(
				"IBLOCK_ID" => $iblocks[$SITE_ID]["ID"],
				"IBLOCK_TYPE" => $iblocks[$SITE_ID]["TYPE"],
				"ID" => $ID,
				"ACTIVE" => "Y",
			);
			$arSelect = array(
				"ID",
				"NAME",
				"IBLOCK_ID",
				"PROPERTY_ADDRESS",
				"PROPERTY_EMAIL",
				"PROPERTY_PHONE",
				"PROPERTY_LOCATION",
				"PROPERTY_TIME",
			);
			$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
			if($ar_fields = $res->GetNext()){
				$arResult['CITY'] = array(
					'ID' => $ar_fields['ID'],
					'NAME' => $ar_fields['NAME'],
					'ADDRESS' => $ar_fields['PROPERTY_ADDRESS_VALUE'],
					'EMAIL' => $ar_fields['PROPERTY_EMAIL_VALUE'],
					'PHONE' => $ar_fields['PROPERTY_PHONE_VALUE'],
					'TIME' => $ar_fields['PROPERTY_TIME_VALUE'],
					'LOCATION' => $ar_fields['PROPERTY_LOCATION_VALUE'],
				);
			}

			foreach($arResult['CITY'] as $key => $value){
				$APPLICATION->set_cookie("CYBER_CURRENT_CITY_" . $key, $value, time()+60*60*24*30*12*2);
			}
			$arResult['CITY']['BLOCK'] = false;
			echo json_encode($arResult['CITY'], JSON_UNESCAPED_UNICODE);
		}
		CMain::FinalActions();
	}
}
?>