<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if($arParams['AJAX_MODE'] != 'Y'){

	if($_GET['WEB_FORM_ID'] == $arResult['arForm']['ID'] || !empty($arResult['FORM_ERRORS_TEXT'])){?>
		<script>
		$(document).ready(function(){
			$.magnificPopup.open({
				items: {
					src: '#modal-price'
				},
				fixedContentPos: true,
				fixedBgPos: true,
				overflowY: 'auto',
				type: 'inline',
				preloader: false,
				modal: false,
				removalDelay: 300,
				mainClass: 'my-mfp-zoom-in',
			}, 0);
			$('body').on('change', '.cityselection_select_container', function(){
				$(this).siblings(".selected_city").val($(this).find("option:selected").data("email"));
			});
		});
		</script>
	<?}
}?>

<div id="modal-price" class="zoom-anim-dialog mfp-hide modal modal--cart">
	<button class="modal__close" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button>
	<div class="modal__content">
		<?if ($arResult["isFormTitle"]){?>
		<h6 class="modal__title">
            <?=$arResult["FORM_TITLE"]?>
        </h6>
		<?}

		if($arResult["isFormErrors"] == "Y"){
			echo '<div class="text-center">' . $arResult["FORM_ERRORS_TEXT"] . '</div>';
		}
		echo '<div class="text-center">' . $arResult["FORM_NOTE"] . '</div>';
		if($arResult["isFormErrors"] != "Y" && $arResult["isFormNote"] == "Y"){
			?><a href="<?=$APPLICATION->GetCurPage()?>" class="form__btn cyber_link_back"><?=$arParams['FORM_SEND_MORE']?></a><?
		}
		if ($arResult["isFormNote"] != "Y"){
			echo $arResult["FORM_HEADER"];
			foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion){
				if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden'){
					echo $arQuestion["HTML_CODE"];
				}else{
					switch ($arQuestion['STRUCTURE'][0]['FIELD_TYPE']) {
						case "text":
							if(stristr($FIELD_SID, "PHONE")){
								?><input type="tel" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="masked_phone form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>">
                            <?}elseif(stristr($FIELD_SID, "CITYSELECT")){?>
                                <!--noindex-->
                                <select <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="cityselection_select_container form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error";?>">
                                        <option selected disabled><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></option>
                                        <?foreach($arResult['CITIES_SELECT'] as $arCity){
                                            ?><option value="<?=$arCity['UF_NAME']?>" data-email="<?=$arCity['UF_EMAIL']?>"><?=(SITE_ID == "s1" ? $arCity['UF_NAME'] : $arCity['UF_NAME_EN'])?></option><?
                                        }?>
                                </select>
                                <!--/noindex-->
                            <?}elseif(stristr($FIELD_SID, "SEND_TO_EMAIL")){
								?><input type="hidden" name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="selected_city"><?
							}else{
								?><input type="text" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_text_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>"><?
							}
							break;
						case "email":
							?><input type="email" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_email_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="form__input<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>"><?
							break;
						case "textarea":
							if(stristr($FIELD_SID, "BASKET")){
								?><textarea style="display:none" name="form_textarea_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="cyber_basket_order_conteiner form__textarea"><?
								if(!empty($_SESSION['CYBER_BASKET_'.SITE_ID])){
									foreach($_SESSION['CYBER_BASKET_'.SITE_ID] as $item){
										echo $item['NAME'] . " Количество: " . $item['QUANTITY'] . " (ID: " . $item['ID'] . ") \n" . SITE_SERVER_NAME . $item['DETAIL_PAGE_URL'] + "\n\n";
										?>
									<?}
								}
								?></textarea><?
							}else{
								?><textarea <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_textarea_<?=$arQuestion['STRUCTURE'][0]['ID']?>" class="form__textarea<?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" placeholder="<?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?>"></textarea><?
							}
							break;
						case "file":
							if(count($arQuestion['STRUCTURE']) == 1){
								?><div class="form__file">
									<span><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></span>
									<div class="form__file-wrap">
										<label for="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-label" data-caption="<?=$arParams['FORM_FILE_CHOOSE']?>"><?=$arParams['FORM_FILE_CHOOSE']?></label>
										<input id="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-input <?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" name="form_file_<?=$arQuestion['STRUCTURE'][0]['ID']?>" type="file" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> >
									</div>
								</div><?
							}else{
							?><div class="form__file">
								<span><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></span>
								<div class="form__file-wrap">
									<label for="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-label" data-caption="<?=$arParams['FORM_FILE_CHOOSE_MULTY']?>"><?=$arParams['FORM_FILE_CHOOSE_MULTY']?></label>
									<input multiple id="formFile_<?=$arParams['FORM_UNIQUE_CLASS']?>" class="form__file-input <?if(is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])) echo " error"?>" name="form_file_<?=$arQuestion['STRUCTURE'][0]['ID']?>[]" type="file" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> >
								</div>
								<div class="form__file-hidden" style="display:none"><?
								for ($i = 1; $i < count($arQuestion['STRUCTURE']); $i++){
									?><input name="form_file_<?=$arQuestion['STRUCTURE'][$i]['ID']?>" type="file" ><?
								}?></div>
							</div>
							<div class="form__file-note" style="display:none"><?=$arParams['FORM_FILE_CHOOSE_NOTE']?>: <span></span></div><?
							}
							break;
						case "dropdown":
							?><select class="form__select" <?=($arQuestion["REQUIRED"] == "Y" ? "required" : "")?> name="form_dropdown_<?=$FIELD_SID?>" id="form_dropdown_<?=$FIELD_SID?>">
								<option disabled selected><?=$arQuestion["CAPTION"] . " " .($arQuestion["REQUIRED"] == "Y" ? $arParams['FORM_REQUIRE_NOTE'] : "")?></option>
								<?foreach($arQuestion['STRUCTURE'] as $item){
									?><option value="<?=$item['ID']?>"><?=$item['MESSAGE']?></option><?
								}?>
							</select><?
							break;
					}
				}
			}
			if(!empty($_SESSION['CYBER_BASKET_'.SITE_ID])){?>
			<span class="modal__cart-title"><?=$arParams['ITEMS_TITLE']?>:</span>
			<ul class="modal__cart-list">
			<?foreach($_SESSION['CYBER_BASKET_'.SITE_ID] as $item){?>
				<li><?=$item['NAME']?> <button class="delete_item modalcart-delete_item" data-cyber_id="<?=$item['ID']?>" type="button"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;" xml:space="preserve"><path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88 c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242 C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879 s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path></svg></button></li>
			<?}?>
			</ul>
			<?}else{?>
			<span class="modal__cart-title" style="display:none"><?=$arParams['ITEMS_TITLE']?>:</span>
			<ul class="modal__cart-list" style="display:none">
			</ul>
			<?}
			if($arResult["isUseCaptcha"] == "Y"){?>
				<input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" />
				<img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
				<?/*<?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];*/?>
				<input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
			<?}?>
			<br />
			<button <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> class="form__btn" name="web_form_submit" type="submit" onclick="yaCounter30699088.reachGoal('goal11'); return true;"><?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?></button>
			<input type="hidden" name="web_form_apply" value="Y" />
			<p class="form__text"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/agreement.php"), false);?></p>
			<?
			echo $arResult["FORM_FOOTER"];
		}
	?>
	</div>
</div>