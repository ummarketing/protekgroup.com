<?

use Bitrix\Main\EventManager,
    Bitrix\Main\Loader;

CModule::IncludeModule('sale');
CModule::includeModule("iblock");


//----------- Проверка на спам ------------
function shield_onBeforeResultAdd($WEB_FORM_ID, &$arFields, &$arrVALUES)
{
    global $APPLICATION;

    if ($WEB_FORM_ID == 22) {
        if (
            strpos($arrVALUES['form_text_142'], '#') !== false
            ||
            strpos($arrVALUES['form_text_142'], "C:") !== false
            ||
            strpos($arrVALUES['form_text_142'], '[') !== false
        ) {
            $APPLICATION->ThrowException('Поля формы не должны содержать символы (# [ : ] / \ )');
        }

        if (strpos($arrVALUES['form_textarea_146'], 'http') !== false) {
            $APPLICATION->ThrowException('Поля формы не должны содержать символы (# [ : ] / \ )');
        }

        if (empty($arrVALUES['form_text_145'])) {
            $APPLICATION->ThrowException('Необходимо указать город');
        }
        if (!preg_match('/\+[0-9] \([0-9]{3}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/', $arrVALUES['form_text_143'])) {
            $APPLICATION->ThrowException('Неверно указан номер');
        }
        if (preg_match('/@[\.A-z]{1,}/', $arrVALUES['form_textarea_146'])) {
            $APPLICATION->ThrowException('В комментарии присутствуют недопустимые символы');
        }
        if (strlen($arrVALUES['form_text_145']) < 2) {
            $APPLICATION->ThrowException('Город указан с ошибкой');
        }
    }


    if ($WEB_FORM_ID == 3) {
        if (!preg_match('/\+[0-9]\s\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}/', $arrVALUES['form_text_16'])) {
            $APPLICATION->ThrowException('Неверно указан номер');
        }
    }
}

AddEventHandler('form', 'onBeforeResultAdd', 'shield_onBeforeResultAdd');
//----------- /Проверка на спам ------------


define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"] . "/log_Rus2Others.txt");
define("DEFAULT_TEMPLATE_PATH", "/bitrix/templates/ru"); // Default template contains all styles, scripts and images
define("FAVORITES_NAME", "CYBER_FAVORITES_"); // for keep favorites in $_SESSION
define("COMPARE_NAME", "CYBER_COMPARE_"); // the same for compare
define("WATERMARK_PATH", $_SERVER["DOCUMENT_ROOT"] . DEFAULT_TEMPLATE_PATH . "/img/watermark.png"); // path to watermark

$rsSites = CSite::GetByID(SITE_ID);
$arSite = $rsSites->Fetch();
define('SITE_NAME', $arSite['NAME']);


$eventManager = EventManager::getInstance();
$eventManager->addEventHandlerCompatible("iblock", "OnAfterIBlockElementAdd", function (&$fields) {

    $arBranchToTeam = array( // Branch IBLOCK_ID => Team IBLOCK_ID
        "8" => "6",
        "12" => "17",
        "21" => "18",
        "30" => "29",
        "36" => "35",
    );

    $arRusToOthers = array( // rus IBLOCK_ID => array of IBLOCK_ID of other languages
        "2" => array(16, 19, 27, 33), // main slider
        "3" => array(13, 37, 26, 38), // downloads
        "4" => array(15, 23, 25, 32), // articles
        "5" => array(11, 22, 39, 31), // news
        "6" => array(17, 18, 29, 35), // team
        "7" => array(14, 20, 28, 34), // team slider
        "8" => array(12, 21, 30, 36), // branches

        "44" => array(47, 50, 53, 56), // gallery
        "46" => array(48, 51, 54, 57), // certificates
        "45" => array(49, 52, 55, 58), // reviews
        "59" => array(60, 61, 62, 63), // about slider

        "42" => array(72, 73, 74, 75), // packaging slider

        "40" => array(64, 65, 66, 67), // catalog

    );

    if ($arBranchToTeam[$fields['IBLOCK_ID']] && $fields['RESULT']) { //After add new branch - create section in Team and mark it in new branch parameters
        if (Loader::includeModule("iblock")) {

            $db_props = CIBlockElement::GetProperty($fields['IBLOCK_ID'], $fields['ID'], array("sort" => "asc"), array("CODE" => "FILIAL"));

            if ($ar_props = $db_props->Fetch()) {
                //AddMessage2Log($ar_props, "ar Props");
                if ($ar_props["VALUE_ENUM"] == 'Y') {

                    $bs = new CIBlockSection;
                    $arFields = array(
                        "ACTIVE" => 'Y',
                        "IBLOCK_ID" => $arBranchToTeam[$fields['IBLOCK_ID']],
                        "NAME" => $fields['NAME'],
                        "SORT" => $fields['SORT'],
                    );
                    $ID = $bs->Add($arFields);

                    CIBlockElement::SetPropertyValuesEx($fields['ID'], $fields['IBLOCK_ID'], array('TEAM' => $ID));
                }

            }
        }
    }

    if ($arRusToOthers[$fields['IBLOCK_ID']] && $fields['RESULT']) { // After add new element in all of rus iblocks, clone this element to iblocks other languages.
        if (Loader::includeModule("iblock")) {
            //AddMessage2Log($fields, "success add to rus");

            global $USER;
            if (!is_object($USER)) $USER = new CUser;

            $el = new CIBlockElement;

            $props = array();
            $sections = array();
            $listProps = array(); //   Different logic for list properties

            foreach ($fields["PROPERTY_VALUES"] as $prop_id => $values) {

                $res = CIBlockProperty::GetByID($prop_id);
                $ar_res = $res->GetNext();

                if (is_array($values)) {
                    if ($ar_res["PROPERTY_TYPE"] != 'L') {
                        foreach ($values as $value) {
                            $props[$ar_res["CODE"]][] = $value["VALUE"];
                        }
                    } else {

                        foreach ($values as $value) {
                            $db_enum_list = CIBlockProperty::GetPropertyEnum($prop_id, array(), array("IBLOCK_ID" => $fields["IBLOCK_ID"], "ID" => $value["VALUE"]));
                            if ($ar_enum_list = $db_enum_list->GetNext()) {
                                $listProps[$ar_res["CODE"]][] = $ar_enum_list["VALUE"];
                            }
                        }
                    }
                }
            }

            if (!empty($fields["IBLOCK_SECTION"]) && isset($fields["IBLOCK_SECTION"])) { // If element is in section
                $arFilter = array('IBLOCK_ID' => $fields['IBLOCK_ID'], 'ID' => $fields["IBLOCK_SECTION"], 'GLOBAL_ACTIVE' => 'Y');
                $db_list = CIBlockSection::GetList(array(), $arFilter, false, array("UF_TRANSLATION_ID"));

                while ($uf_value = $db_list->GetNext()) {
                    $sections[] = $uf_value["UF_TRANSLATION_ID"];
                }
            }


            $arPreparedProductArray = array(   // Prepared properties array
                "CODE" => $fields["CODE"],
                "NAME" => $fields["NAME"],
                "IBLOCK_SECTION_ID" => false,   // element have not parent section
                "IBLOCK_ID" => "",
                "ACTIVE" => "Y",   // active
                "DATE_ACTIVE_FROM" => $fields["ACTIVE_FROM"],
                "DATE_ACTIVE_TO" => $fields["ACTIVE_TO"],
                "SORT" => $fields["SORT"],
                "MODIFIED_BY" => $USER->GetID(),
                "PREVIEW_TEXT" => $fields["PREVIEW_TEXT"],
                "DETAIL_TEXT" => $fields["DETAIL_TEXT"],
                "PREVIEW_TEXT_TYPE" => $fields["PREVIEW_TEXT_TYPE"],
                "DETAIL_TEXT_TYPE" => $fields["DETAIL_TEXT_TYPE"],
                "DETAIL_PICTURE" => CFile::MakeFileArray(CFile::GetPath($fields["DETAIL_PICTURE_ID"])),   // new element should be with new picture
                "PREVIEW_PICTURE" => CFile::MakeFileArray(CFile::GetPath($fields["PREVIEW_PICTURE_ID"])),
                "IPROPERTY_TEMPLATES" => $fields["IPROPERTY_TEMPLATES"],
                "PROPERTY_VALUES" => $props,
            );

            foreach ($arRusToOthers[$fields['IBLOCK_ID']] as $ID) {

                $arLoadProductArray = $arPreparedProductArray;
                $arLoadProductArray["IBLOCK_ID"] = $ID;

                foreach ($listProps as $code => $values) {
                    foreach ($values as $val) {
                        $db_enum_list = CIBlockProperty::GetPropertyEnum($code, array(), array("IBLOCK_ID" => $ID, "VALUE" => $val));

                        if ($ar_enum_list = $db_enum_list->GetNext()) {

                            $arLoadProductArray["PROPERTY_VALUES"][$code][] = $ar_enum_list['ID'];
                        }
                    }
                }
                $arSections = array();

                if (!empty($sections)) { // compare sections from old and new iblocks by UF_TRANSLATION_ID
                    $arFilter = array('IBLOCK_ID' => $ID, 'UF_TRANSLATION_ID' => $sections, 'GLOBAL_ACTIVE' => 'Y');
                    $db_list = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "UF_TRANSLATION_ID"));

                    while ($uf_value = $db_list->GetNext()) {
                        $arSections[] = $uf_value["ID"];
                    }
                }
                if (!empty($arSections)) {
                    if (count($arSections) > 1) {
                        $arLoadProductArray["IBLOCK_SECTION"] = $arSections;
                    } else {
                        $arLoadProductArray["IBLOCK_SECTION_ID"] = $arSections[0];
                    }

                }


                if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {

                    if ($fields['IBLOCK_ID'] == '40' && Loader::includeModule("catalog")) { // this stuff is only for catalog
                        $productID = CCatalogProduct::add(array("ID" => $PRODUCT_ID));
                        if ($productID) {
                            CPrice::Add(array(
                                "CURRENCY" => "RUB",
                                "PRICE" => 0, // Price = 0
                                "CATALOG_GROUP_ID" => 1, // BASE Price type
                                "PRODUCT_ID" => $PRODUCT_ID
                            ));
                        }
                    }
                } else {
                    AddMessage2Log("Error: " . $el->LAST_ERROR, "add to lang");
                }

            }

        }
    }


});


// ============= Функции по работе с разделами ==============

// -------- Получение раздела по ID -----------
function getSectionById($section_id)
{
    $arResult = CIBlockSection::GetById($section_id);
    if ($arSection = $arResult->Fetch()) {
        return $arSection;
    }
}

// -------- Получение раздела по CODE -----------
function getSectionByCode($iblock_id, $elem_code)
{
    $arFilter = array('IBLOCK_ID' => $iblock_id, 'CODE' => $elem_code);
    $arResult = CIBlockSection::GetList(array('ID' => 'ASC'), $arFilter, false, array());
    if ($arElement = $arResult->Fetch()) {
        return $arElement;
    }
}

// -------- Получение раздела по URL -------------
function getSectionByUrl($iblock_id, $url)
{
    $arUrl = explode('/', $url);
    $sect_code = $arUrl[count($arUrl) - 2];
    $arResult = getSectionByCode($iblock_id, $sect_code);

    return $arResult;
}

// -------- Получение раздела по фильтру -----------
function getSectionList($filter, $select)
{
    $arValues = array();
    $arResult = CIBlockSection::GetList(array('ID' => 'ASC'), $filter, false, $select);

    while ($arSection = $arResult->Fetch()) {
        $arValues[] = $arSection;
    }

    return $arValues;
}

// --------- Иерархический вывод разделов ----------
function getSectionTreeList($filter, $select)
{
    $dbSection = CIBlockSection::GetList(
        array(
            'LEFT_MARGIN' => 'ASC',
        ),
        array_merge(
            array(
                'ACTIVE' => 'Y',
                'GLOBAL_ACTIVE' => 'Y'
            ),
            is_array($filter) ? $filter : array()
        ),
        false,
        array_merge(
            array(
                'ID',
                'IBLOCK_SECTION_ID'
            ),
            is_array($select) ? $select : array()
        )
    );

    while ($arSection = $dbSection->GetNext(true, false)) {

        $SID = $arSection['ID'];
        $PSID = (int)$arSection['IBLOCK_SECTION_ID'];

        $arLincs[$PSID]['CHILDS'][$SID] = $arSection;

        $arLincs[$SID] = &$arLincs[$PSID]['CHILDS'][$SID];
    }

    return array_shift($arLincs);
}

// ============= /Функции по работе с разделами ==============


// ============ Функции по работе с элементами ==============

//--------- Получение элемента по ID ---------
function getElementById($element_id)
{
    $arResult = CIBlockElement::GetById($element_id);
    if ($arElement = $arResult->Fetch()) {
        return $arElement;
    }
}

//--------- Получение элемента по CODE ---------
function getElementByCode($iblock_id, $elem_code)
{
    $arFilter = array('IBLOCK_ID' => $iblock_id, 'CODE' => $elem_code);
    $arResult = CIBlockElement::GetList(array('ID' => 'ASC'), $arFilter, false, false, array());
    if ($arElement = $arResult->Fetch()) {
        return $arElement;
    }
}

// -------- Получение раздела по URL -------------
function getElementByUrl($iblock_id, $url)
{
    $arUrl = explode('/', $url);
    $elem_code = $arUrl[count($arUrl) - 2];
    $arResult = getElementByCode($iblock_id, $elem_code);

    return $arResult;
}

// -------- Получение элемента по фильтру -----------
function getElementList($filter, $select)
{
    $arValues = array();
    $arResult = CIBlockElement::GetList(array('ID' => 'ASC'), $filter, false, false, $select);

    while ($arElement = $arResult->Fetch()) {
        $arValues[] = $arElement;
    }

    return $arValues;
}

// ============ /Функции по работе с элементами ==============


// ============ Функции по работе со свойствами =============

// -------- Получение свойства по ID ---------
function getElementPropertyByID($iblock_id, $elem_id, $prop_id)
{

    $arElemProp = array();
    $arPropResult = CIBlockElement::GetProperty($iblock_id, $elem_id, array(), array("ID" => $prop_id));

    while ($arProp = $arPropResult->Fetch()) {
        array_push($arElemProp, $arProp);
    }

    return $arElemProp;
}

// -------- Получение свойства по CODE ---------
function getElementPropertyByCode($iblock_id, $elem_id, $prop_code)
{

    $arElemProp = array();
    $arPropResult = CIBlockElement::GetProperty($iblock_id, $elem_id, array(), array("CODE" => $prop_code));

    while ($arProp = $arPropResult->Fetch()) {
        array_push($arElemProp, $arProp);
    }

    unset($arPropResult);
    return $arElemProp;
}

// --------- Получение значений типа справочник,
/*
===== Массив для использования функцией можно получить, например, через функцию getElementPropertyByID() ====
*/

use \Bitrix\Highloadblock as HL;

function getHiloadEnum($arProp)
{

    /**
     * @var array Массив описывающий свойство типа справочник
     */
    $arHighloadProperty = $arProp;

    /**
     * @var string название таблицы справочника
     */
    $sTableName = $arHighloadProperty['USER_TYPE_SETTINGS']['TABLE_NAME'];

    /**
     * Работаем только при условии, что
     *    - модуль highloadblock подключен
     *    - в описании присутствует таблица
     *    - есть заполненные значения
     */
    if (Loader::IncludeModule('highloadblock') && !empty($sTableName) && !empty($arHighloadProperty["VALUE"])) {
        /**
         * @var array Описание Highload-блока
         */
        $hlblock = HL\HighloadBlockTable::getRow([
            'filter' => [
                '=TABLE_NAME' => $sTableName
            ],
        ]);

        if ($hlblock) {
            /**
             * Магия highload-блоков компилируем сущность, чтобы мы смогли с ней работать
             *
             */
            $entity = HL\HighloadBlockTable::compileEntity($hlblock);
            $entityClass = $entity->getDataClass();

            $arRecords = $entityClass::getList([
                'filter' => [
                    //'UF_XML_ID' => $arHighloadProperty["VALUE"]
                ],
            ]);
            foreach ($arRecords as $record) {
                /**
                 * Тут любые преобразования с записью, полученной из таблицы.
                 * Я транслировал почти все напрямую.
                 *
                 * Нужно помнить, что например в UF_FILE возвращается ID файла,
                 * а не полный массив описывающий файл
                 */
                $arRecord = [
                    'ID' => $record['ID'],
                    'UF_NAME' => $record['UF_NAME'],
                    'UF_SORT' => $record['UF_SORT'],
                    'UF_XML_ID' => $record['UF_XML_ID'],
                    'UF_LINK' => $record['UF_LINK'],
                    'UF_DESCRIPTION' => $record['UF_DESCRIPTION'],
                    'UF_FULL_DESCRIPTION' => $record['UF_FULL_DESCRIPTION'],
                    'UF_DEF' => $record['UF_DEF'],
                    'UF_FILE' => [],
                    '~UF_FILE' => $record['UF_FILE'],
                ];

                /**
                 * Не очень быстрое решение - сколько записей в инфоблоке, столько файлов и получим
                 * Хорошо было бы вынести под код и там за 1 запрос все получить, а не плодить
                 * по дополнительному запросу на каждый файл
                 */
                if (!empty($arRecord['~UF_FILE'])) {
                    $arRecord['UF_FILE'] = \CFile::getById($arRecord['~UF_FILE'])->fetch();
                }

                $arHighloadProperty['EXTRA_VALUE'][] = $arRecord;
            }
        }

        return $arHighloadProperty;
    }
}

// ============ /Функции по работе со свойствами =============


// ============= Дополнительные функции ===========

// -------- Удобный дебаг --------
function getDebug($str)
{
    global $USER;

    if ($USER->isAdmin()) {
        echo "<pre>";
        print_r($str);
        echo "</pre>";
    }
}

// ----------- Проверка наличия пагинации ------------
function checkPagen()
{
    $pager = false;
    foreach ($_GET as $key => $arElem) {
        if (strpos($key, 'PAGEN_') !== false) {
            $pager = $_GET[$key];
            break;
        }
    }
    return $pager;
}

// -------- Скачивание картинок ---------
function downloadImg($img_name, $img_url)
{
    $ch = curl_init($img_url);
    $f_img = fopen($_SERVER['DOCUMENT_ROOT'] . '/test/downloads/' . $img_name, 'wb');

    curl_setopt($ch, CURLOPT_FILE, $f_img);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);

    fclose($f_img);
}

// ------- Функция установки заглавной первой буквы --------
function firstLetUp($str)
{
    $first = mb_substr($str, 0, 1, 'UTF-8');
    $last = mb_substr($str, 1);
    $first = strtoupper($first);
    $last = strtolower($last);
    $str = $first . $last;

    return $str;
}

// --------- Замена имени на новое ----------
function replaceItemName($old_name, $new_name)
{

    if (!empty($new_name) && !empty($old_name)) {

        $count = 0;
        $word_pos = 0;

        $arOldName = explode(' ', $old_name);
        $arNewName = explode(' ', $new_name);

        foreach ($arOldName as $key => $item) {
            foreach ($arNewName as $key2 => $subitem) {
                if (mb_strtolower($item) == mb_strtolower($subitem)) {
                    $word_pos = $key;
                    $count++;

                    break;
                }
            }
        }

        if ($count > 0) {
            $arOldName[$word_pos] = $new_name;
        } else {
            $arOldName[0] = mb_strtolower($arOldName[0]);
            array_unshift($arOldName, $new_name);
        }

        return implode(' ', $arOldName);
    } else {
        return $old_name;
    }
}

// --------- Вывод числа в виде цены ---------
function setPrice($price)
{
    return number_format($price, 0, '', ' ');
}

// --------- Транслит слова в понятный ЧПУ ---------
function translit($s)
{
    $s = (string)$s;
    $s = strip_tags($s);
    $s = str_replace(array("\n", "\r"), " ", $s);
    $s = preg_replace("/\s+/", ' ', $s);
    $s = trim($s);
    $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s);
    $s = strtr($s, array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => ''));
    $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s);
    $s = str_replace(" ", "_", $s);

    return $s;
}

//-------- Получение полного пути до категории ----------
function makeFullPath($id)
{

    CModule::IncludeModule('iblock');
    $path = '/catalog/';
    $rs = CIBlockSection::GetNavChain(IBLOCK_ID, $id, array('ID', 'CODE'));

    while ($ar = $rs->Fetch()) {
        $path .= $ar['CODE'] . '/';
    }

    return $path;
}

// ============= /Дополнительные функции ===========


// ------ АГЕНТ: Установка пользовательской сортировки ------
function AgentSetUserSort()
{
    $arSelect = array();
    $arFilter = array('IBLOCK_ID' => 40, 'ACTIVE' => 'Y');
    $arResult = getElementList($arFilter, $arSelect);

    foreach ($arResult as $key => $arItem) {
        $sort = 500;
        $arProps['PRODUCE'] = getElementPropertyByCode($arItem['IBLOCK_ID'], $arItem['ID'], 'PRODUCER')[0];
        $arProps['LABEL'] = getElementPropertyByCode($arItem['IBLOCK_ID'], $arItem['ID'], 'LABEL')[0];


        // ------ Сортировка по новинке ------
        (!empty($arProps['LABEL']['VALUE']) && $arProps['LABEL']['VALUE'] == 'new') ? $sort = 1 : $sort = 2;
        // ------ /Сортировка по новинке ------


        // ------ Сортировка по производителю ------
        if (!empty($arProps['PRODUCE']['VALUE_XML_ID']) && (int)$arProps['PRODUCE']['VALUE_XML_ID'] == 1) {
            $sort .= '1';
        } elseif (!empty($arProps['PRODUCE']['VALUE_XML_ID']) && (int)$arProps['PRODUCE']['VALUE_XML_ID'] == 2) {
            $sort .= '2';
        } else {
            $sort .= '3';
        }
        // ------ /Сортировка по производителю ------


        // ------ Установка нового значения ------
        if (!empty($sort)) {
            CIBlockElement::SetPropertyValuesEx($arItem['ID'], $arItem['IBLOCK_ID'], array('USER_SORT' => (int)$sort));
        }
        // ------ /Установка нового значения ------
    }

    return 'AgentSetUserSort();';
}

// ------ /АГЕНТ: Установка пользовательской сортировки ------


AddEventHandler("main", "OnProlog", "updateFaviconIcon", 50);
function updateFaviconIcon()
{
    global $APPLICATION;
    $APPLICATION->AddHeadString('<link rel="icon" type="image/x-icon" href="/favicon.svg" />');
}


class FilterMain
{
    public $filtered = [
        "SIZE" => [],
        'OTHER_FILTERS'=>[],
        "SPECIAL_ABILITIES" => []
    ];

    function __construct($listId, $listCategory)
    {
       if(!empty($listCategory)) {
           foreach ($listId as $item) {
               $itemCategory = 'OTHER_FILTERS';
               foreach ($listCategory as $category => $ids) {
                   if (in_array($item["ID"], $ids)) {
                       $itemCategory = $category;
                       break;
                   }
                   //TODO calc category
               }
               if (!$this->filtered[$itemCategory]){
                   $this->filtered[$itemCategory] = [];
               }
               array_push($this->filtered[$itemCategory], $item);
           }
       }

    }
/*
    public $familiarCategories = [
        "SIZE" => [139, 166, 164, 165, 138, 162, 137, 136, 176],
        "SPECIAL_ABILITIES" => [175, 142, 140, 141, 143, 144],
    ];*/

    public function getSorted() {
        return $this->filtered;
    }

}