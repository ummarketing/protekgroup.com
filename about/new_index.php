<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "О компании Концерн Протэк. Подробнее читайте на сайте.");
$APPLICATION->SetTitle("О компании");

$APPLICATION->SetAdditionalCss('/local/assets/plugins/wow/css/libs/animate.css');
$APPLICATION->AddHeadScript('/local/assets/plugins/wow/dist/wow.js');
?>
<script>
    new WOW().init();
</script>

<!--О компании-->
<section class="b-company__section b-company__preview">
    <div class="container">
        <h1 class="b-main__title b-company__title">
            О компании
        </h1>
        <div class="b-main__content">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="b-main__text b-company__text">
                        <p>
                            Концерн «Протэк» – крупнейшая российская компания в отрасли создания, производства и реализации упаковочных материалов для пищевой промышленности. Помимо этого мы производим и реализуем одноразовую посуду, столовые приборы и отделочные материалы, работаем в сфере аграрного бизнеса.
                        </p>
                        <br>
                        <p>
                            С момента открытия компании в 1996-м году мы остаемся одной из ведущих фирм
                            на мировом рынке пищевой упаковки. Сегодня в состав Концерна «Протэк» входят
                            более 80 официальных дилеров в России, а также в странах СНГ и Европы.
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="b-main__img b-company__img">
                        <img src="/local/assets/img/about/company.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/О компании-->



<!--Продукция-->
<section class="b-company__section b-company__production">
    <div class="container">
        <div class="b-main__subtitle b-production__title">
            Производство:
        </div>
        <div class="b-production__list">
            <div class="b-production__item wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="200">
                <div class="b-production__item-number">
                    15
                </div>
                <div class="b-production__item-text">
                    собственных промышленных комплексов
                </div>
            </div>
            <div class="b-production__item wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="200">
                <div class="b-production__item-number">
                    5800
                </div>
                <div class="b-production__item-text">
                    тонн различного сырья переработывает наше производство
                </div>
            </div>
            <div class="b-production__item wow fadeInUp" data-wow-delay="0.9s" data-wow-offset="200">
                <div class="b-production__item-number">
                    25
                </div>
                <div class="b-production__item-text">
                    экструдеров включает в себя технический парк основного оборудования
                </div>
            </div>
            <div class="b-production__item wow fadeInUp" data-wow-delay="1.2s" data-wow-offset="200">
                <div class="b-production__item-number">
                    77
                </div>
                <div class="b-production__item-text">
                    формовочных машин
                </div>
            </div>
            <div class="b-production__item wow fadeInUp" data-wow-delay="1.5s" data-wow-offset="200">
                <div class="b-production__item-number">
                    12
                </div>
                <div class="b-production__item-text">
                    литьевых машин
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Продукция-->



<!--Технологии-->
<section class="b-company__section b-company__technology">
    <div class="container">
        <div class="b-main__content">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-6">
                    <div class="b-main__content-block">
                        <div class="b-main__subtitle">
                            Технологии:
                        </div>
                        <div class="b-main__text b-company__text">
                            <p>
                                Высокое качество продукции и ее соответствие мировым стандартам обеспечивается
                                за счет постоянного совершенствования производственных технологий.
                                Беспрерывная научная работа собственных исследовательских подразделений и
                                постоянный мониторинг новшеств отрасли позволяет нам производить сертифицированную упаковку
                                с минимальными затратами. Ключевой показатель эффективности научной работы сотрудников
                                Концерна – наличие собственных запатентованных технологий, которые активно используются
                                в разработке и производстве фирменной продукции.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-md-6">
                    <div class="b-main__img b-company__img wow fadeInRight" data-wow-delay="0.3s" data-wow-offset="250">
                        <img src="/local/assets/img/about/technology1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="b-main__content revert-xs">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-6">
                    <div class="b-main__img b-company__img wow fadeInLeft" data-wow-delay="0.3s" data-wow-offset="250">
                        <img src="/local/assets/img/about/technology2.png" alt="">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-md-6">
                    <div class="b-main__content-block">
                        <div class="b-main__subtitle">
                            Продукция:
                        </div>
                        <div class="b-main__text b-company__text">
                            <p>
                                Ежедневно мы отгружаем с наших заводов свыше 50 фур готовой продукции.
                                Наш ассортимент насчитывает более 6 500 наименований изделий.
                                Мы располагаем собственным парком грузовых автомобилей и развитой службой логистики.
                                Общая площадь складских помещений Концерна «Протэк» превышает
                                50 000 кв. м. Наши склады оборудованы всем необходимым для безопасного
                                и длительного хранения продукции. С момента основания компании
                                и по сегодняшний день мы стремимся к званию «надежного поставщика».
                                Предлагаем сугубо индивидуальный подход к каждому клиенту и поставляем
                                только качественную, сертифицированную и соответствующую требованиям
                                высоких стандартов продукцию.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-main__content">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-9">
                    <div class="b-main__content-block">
                        <div class="b-main__subtitle">
                            Кадры:
                        </div>
                        <div class="b-main__text b-company__text">
                            <p>
                                На протяжении всего времени работы Концерн активно инвестирует в сотрудников,
                                обеспечивая регулярное повышение квалификации кадров. Наши специалисты уделяют
                                особое внимание достижению эффективности работы, качества продукции, безопасности и вопросам экологии
                                в ходе разработки и производства БИО-упаковки.
                            </p>
                            <br>
                            <p>
                                Команда Концерна «Протэк» – это более 1 800 специалистов в области:
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Технологии-->



<!--Тизеры-->
<section class="b-company__section b-company__teasers">
    <div class="container">
        <div class="b-teasers__list">
                <div class="b-teasers__item wow pulse" data-wow-delay="0.3s" data-wow-offset="200">
                    <div class="b-teasers__item-text">
                        Производства
                    </div>
                    <img src="/local/assets/img/about/teaser1.svg" alt="" class="b-teasers__item-img">
                </div>
                <div class="b-teasers__item wow pulse" data-wow-delay="0.9s" data-wow-offset="200">
                    <div class="b-teasers__item-text">
                        Менеджмента
                    </div>
                    <img src="/local/assets/img/about/teaser2.svg" alt="" class="b-teasers__item-img">
                </div>
                <div class="b-teasers__item wow pulse" data-wow-delay="1.2s" data-wow-offset="200">
                    <div class="b-teasers__item-text">
                        Логистики
                    </div>
                    <img src="/local/assets/img/about/teaser3.svg" alt="" class="b-teasers__item-img">
                </div>
                <div class="b-teasers__item wow pulse" data-wow-delay="1.5s" data-wow-offset="200">
                    <div class="b-teasers__item-text">
                        Аграрного бизнеса
                    </div>
                    <img src="/local/assets/img/about/teaser4.svg" alt="" class="b-teasers__item-img">
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Тизеры-->



<!--Галлерея-->
<section class="b-company__section b-company__gallery">
    <div class="container">
        <div class="b-gallery__list">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="b-gallery__item wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="200">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7">
                                <div class="b-gallery__left-content">
                                    <img src="/local/assets/img/about/gallery1-1.png" alt="" class="b-gallery__img b-gallery__img-lg">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <div class="b-gallery__right-content">
                                    <div class="b-galley__title">
                                        Более 1800 сотрудников
                                    </div>
                                    <img src="/local/assets/img/about/gallery1-2.png" alt="" class="b-gallery__img b-gallery__img-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-gallery__item b-gallery__item-v2 wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="200">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5">
                                <div class="b-gallery__right-content">
                                    <div class="b-galley__title">
                                        Командный дух
                                    </div>
                                </div>
                            </div>
                             <div class="col-xs-12 col-sm-7">
                                <div class="b-gallery__left-content">
                                    <img src="/local/assets/img/about/gallery3-1.png" alt="" class="b-gallery__img b-gallery__img-lg">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="b-gallery__item wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="200">
                        <div class="row">
                            <div class="col-xs-12 col-sm-7">
                                <div class="b-gallery__left-content">
                                    <img src="/local/assets/img/about/gallery2-1.png" alt="" class="b-gallery__img b-gallery__img-lg">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <div class="b-gallery__right-content">
                                    <div class="b-galley__title">
                                        15 производственных комплексов
                                    </div>
                                    <img src="/local/assets/img/about/gallery2-2.png" alt="" class="b-gallery__img b-gallery__img-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="b-gallery__item b-gallery__item-v2 wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="200">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5">
                                <div class="b-gallery__right-content">
                                    <div class="b-galley__title">
                                        Собственный логистический парк
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7">
                                <div class="b-gallery__left-content">
                                    <img src="/local/assets/img/about/gallery4-1.png" alt="" class="b-gallery__img b-gallery__img-lg">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Галлерея-->



<!--История компании-->
<section class="b-company__section b-company__history">
    <div class="container">
        <div class="b-main__block-title b-history__title">
            История компании
        </div>
        <div class="b-history__list">
            <ul>
                <li>
                    <div><b>30 декабря </b><b>1996</b><b> г.</b> - Открытие Компании “ПРОТЭК” в г. Москва&nbsp;</div>
                </li>
                <li>
                    <div><b>1998 г. </b>- Начало партнерских отношений с ООО "Приопак" (г. Рязань)&nbsp;</div>
                </li>
                <li>
                    <div><b>2001 г. </b>- Основание производственного комплекса "Интерпластик" в г. Люберцы&nbsp;</div>
                </li>
                <li>
                    <div><b>2002 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Анапа<b>&nbsp;</b></div>
                </li>
                <li>
                    <div><b>2002 г. </b>- Основание производственного комплекса "Мегапласт" в г. Торопец<b>&nbsp;</b></div>
                </li>
                <li>
                    <div><b>2003 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Воронеж&nbsp;</div>
                </li>
                <li>
                    <div><b>2004 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Ярославль&nbsp;</div>
                </li>
                <li>
                    <div><b>2004 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Санкт-Петербург<b>&nbsp;</b></div>
                </li>
                <li>
                    <div><b>2005 г.</b> - Основание производственного комплекса "Интерпластик" в г. Красноярск&nbsp;</div>
                </li>
                <li>
                    <div><b>2006 г.</b> - Основание производственного комплекса "РамУпак" в пос. Томилино (перенесен в г. Раменское)</div>
                </li>
                <li>
                    <div><b>2007 г.</b> - Основание производственного комплекса "РамПласт" в пос. Томилино (перенесен в г. Раменское)&nbsp;</div>
                </li>
                <li>
                    <div><b>2007 г.</b> - Запуск производственного подразделения "Завод упаковочных изделий" ("ЗУИ")&nbsp;</div>
                </li>
                <li><div><b>2010 г. </b>- Основание производственного комплекса "Мегапласт" в г. Пущино&nbsp;</div></li>
                <li><div><b>2011 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Красноярск&nbsp;</div></li>
                <li><div><b>2012 г. </b>- Основание производственного комплекса "Интерпластик 2001" в г. Томилино&nbsp;</div></li>
                <li><div><b>2012 г. </b>- Открытие дилерского отдела Компании "Протэк" в г. Астана, Казахстан&nbsp;</div></li>
                <li><div><b>2012 г. </b>- Открытие дилерского отдела Компании "Протэк" в г. Алматы, Казахстан&nbsp;</div></li>
                <li><div><b>2012 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Киев, Украина&nbsp;</div></li>
                <li><div><b>2013 г.</b> - Основание производственного комплекса "ВВМ- Транс" в г. Пущино&nbsp;</div></li>
                <li><div><b>2013 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Краснодар&nbsp;</div></li>
                <li><div><b>2013 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Нижний Новгород&nbsp;</div></li>
                <li><div><b>2013 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Челябинск&nbsp;</div></li>
                <li><div><b>2013 г.</b> - Открытие дилерского отдела Компании "Протэк" в г. Казань&nbsp;</div></li>
                <li><div><b>2014 г.</b> - Появление торговой марки "Концерн Протэк"&nbsp;</div></li>
                <li><div><b>2014 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Пермь&nbsp;</div></li>
                <li><div><b>2014 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Уфа&nbsp;</div></li>
                <li><div><b>2014 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Симферополь, республика Крым&nbsp;</div></li>
                <li><div><b>2015 г.</b> - Запуск производственного комплекса "Интерпластик 2001" Лакор в пос. Некрасовский&nbsp;</div></li>
                <li><div><b>2015 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Волгоград&nbsp;</div></li>
                <li><div><b>2015 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Белгород&nbsp;</div></li>
                <li><div><b>2016 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Тюмень&nbsp;</div></li>
                <li><div><b>2016 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Ростов-на-Дону&nbsp;</div></li>
                <li><div><b>2016 г.</b> - Запуск производственной площадки в г. Челябинск&nbsp;</div></li>
                <li><div><b>2016 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Минск, Беларусь&nbsp;</div></li>
                <li><div><b>2017 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Екатеринбург&nbsp;</div></li>
                <li><div><b>2017 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Калининград&nbsp;</div></li>
                <li><div><b>2017 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Новосибирск&nbsp;</div></li>
                <li><div><b>2018 г.</b> - Запуск производственной площадки в г. Краснодар&nbsp;</div></li>
                <li><div><b>2018 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Владимир&nbsp;</div></li>
                <li><div><b>2018 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Ижевск&nbsp;</div></li>
                <li><div><b>2018 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Самара&nbsp;</div></li>
                <li><div><b>2018 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Омск&nbsp;</div></li>
                <li><div><b>2018 г.</b> - Запуск производственной площадки в г. Омск&nbsp;</div></li>
                <li><div><b>2018 г. </b>- Открытие дилерского отдела Концерна "Протэк" в г. Липецк&nbsp;</div></li>
                <li><div><b>2018 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Чебоксары&nbsp;</div></li>
                <li><div><b>2018 г.</b> - Открытие дилерского отдела Концерна "Протэк" в г. Псков</div></li>
                <li><div><b>2019 г.</b> - Запуск производственной площадки в с. Гжель</div></li>
            </ul>
        </div>
        <div class="b-history__btn _showMore" data-show="Скрыть часть">
            Смотреть польностью
        </div>
    </div>
</section>
<!--/История компании-->



<!--Преимущества-->
<section class="b-company__section b-company__advantages">
    <div class="container">
        <div class="b-main__block-title b-advantages__title">
            Наши преимущества
        </div>
        <div class="b-advantages__list">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="b-advantages__item wow fadeInUp" data-wow-delay="0.3s" data-wow-offset="200">
                        <div class="b-advantages__item-title">
                            Безопасное производство
                        </div>
                        <div class="b-advantages__item-text">
                            Мы работаем по принципу «Безотходное производство»,
                            то есть материалы перерабатываются вторично, а вода
                            для охлаждения оборудования запущена по замкнутому циклу
                        </div>
                        <img src="/local/assets/img/about/advantage1.svg" alt="" class="b-advantages__item-icon">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="b-advantages__item wow fadeInUp" data-wow-delay="0.6s" data-wow-offset="200">
                        <div class="b-advantages__item-title">
                            Индивидуальный подход
                        </div>
                        <div class="b-advantages__item-text">
                            Гибкая система скидок постоянным клиентам
                        </div>
                        <img src="/local/assets/img/about/advantage2.svg" alt="" class="b-advantages__item-icon">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="b-advantages__item wow fadeInUp b-advantages__item-v2" data-wow-delay="0.9s" data-wow-offset="200">
                        <div class="b-advantages__item-title">
                            Гарантия качества продукции
                        </div>
                        <div class="b-advantages__item-text">
                            Изготовим в сроки, с гарантией качественного исполнения на всех этапах производства.
                        </div>
                        <img src="/local/assets/img/about/advantage3.svg" alt="" class="b-advantages__item-icon">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="b-advantages__item wow fadeInUp" data-wow-delay="1.2s" data-wow-offset="100">
                        <div class="b-advantages__item-title">
                            Создание индивидуальных форм
                        </div>
                        <div class="b-advantages__item-text">
                            Изготовим продукцию под ваши потребности
                        </div>
                        <img src="/local/assets/img/about/advantage4.svg" alt="" class="b-advantages__item-icon">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="b-advantages__item wow fadeInUp" data-wow-delay="1.5s" data-wow-offset="100">
                        <div class="b-advantages__item-title">
                            Собственная брендированная упаковка
                        </div>
                        <div class="b-advantages__item-text">
                        <!--Text here...-->
                        </div>
                        <img src="/local/assets/img/about/advantage5.svg" alt="" class="b-advantages__item-icon v-5">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="b-advantages__item wow fadeInUp" data-wow-delay="1.8s" data-wow-offset="100">
                        <div class="b-advantages__item-title">
                            Собственная логистическая служба
                        </div>
                        <div class="b-advantages__item-text">
                        <!--Text here...-->
                        </div>
                        <img src="/local/assets/img/about/advantage6.svg" alt="" class="b-advantages__item-icon">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--/Преимущества-->


<!--Форма обратной связи-->
<section class="b-company__section b-company__form">
    <div class="container">
        <div class="b-main__block-title b-advantages__title">
            Вы можете задать любой инетерсующий вас вопрос
        </div>
    </div>
    <?$APPLICATION->IncludeComponent(
        "cyberrex:form.result.new",
        "protek_web_form_2",
        Array(
            "AJAX_MODE" => "N",
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "CHAIN_ITEM_LINK" => "",
            "CHAIN_ITEM_TEXT" => "",
            "COMPONENT_TEMPLATE" => "protek_web_form",
            "EDIT_URL" => "",
            "FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
            "FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
            "FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
            "FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
            "FORM_SEND_MORE" => GetMessage("PROTEK_TEMPLATE_FORM_SEND_AGAIN"),
            "FORM_UNIQUE_CLASS" => "contact-call",
            "IGNORE_CUSTOM_TEMPLATE" => "N",
            "LIST_URL" => "",
            "SEF_MODE" => "N",
            "SUCCESS_URL" => "",
            "USE_EXTENDED_ERRORS" => "N",
            "VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
            "WEB_FORM_ID" => "3"
        )
    );?>
</section>
<!--/Форма обратной связи-->

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>
