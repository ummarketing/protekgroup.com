<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetPageProperty("description", "О компании Концерн Протэк. Подробнее читайте на сайте.");
$APPLICATION->SetTitle("Протэк концерн");
?>
<section class="section section--top0" itemscope itemtype="http://schema.org/Organization">
    <div class="container">
        <h1 class="section__title">ПРОТЭК КОНЦЕРН</h1>
        <div class="row">
            <div class="col-12 col-md-6">
                <?
                $APPLICATION->IncludeComponent("bitrix:main.include", "", [
                    "AREA_FILE_SHOW" => 'file',
                    "PATH" => SITE_TEMPLATE_PATH . "/include/index-protek_concern-img.php",
                ]);
                ?>
            </div>
        </div>
    </div>
</section>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
