<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Избранное");
?>
<?
$arParams = array(
	"ACTION_VARIABLE" => "action",
	"ADD_ELEMENT_CHAIN" => "Y",
	"ADD_PICT_PROP" => "MORE_PHOTO",
	"ADD_PROPERTIES_TO_BASKET" => "N",
	"ADD_SECTIONS_CHAIN" => "Y",
	"BASKET_URL" => "/cart/",
	"BIG_DATA_RCM_TYPE" => "personal",
	"CACHE_FILTER" => "N",
	"CACHE_GROUPS" => "Y",
	"CACHE_TIME" => "36000000",
	"CACHE_TYPE" => "A",
	"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
	"COMMON_SHOW_CLOSE_POPUP" => "N",
	"COMPARE_ELEMENT_SORT_FIELD" => "active_from",
	"COMPARE_ELEMENT_SORT_ORDER" => "asc",
	"COMPARE_FIELD_CODE" => array(
		0 => "PREVIEW_PICTURE",
		1 => "",
	),
	"COMPARE_NAME" => COMPARE_NAME.SITE_ID,
	"COMPARE_POSITION" => "top left",
	"COMPARE_POSITION_FIXED" => "Y",
	"COMPARE_PROPERTY_CODE" => array(
		0 => "BRANDING",
		1 => "PACK_WEIGHT",
		2 => "FREEZING",
		3 => "SEALING",
		4 => "WARMING",
		5 => "HEIGHT",
		6 => "DIAMETER",
		7 => "LENGTH",
		8 => "SECTIONS",
		9 => "MATERIAL",
		10 => "SPIKES",
		11 => "VOLUME",
		12 => "REGION",
		13 => "LOCKER",
		14 => "FORM",
		15 => "COLOR",
		16 => "PACK_CONTAINS",
		17 => "PACK_VOLUME",
		18 => "",
	),
	"COMPATIBLE_MODE" => "Y",
	"CONVERT_CURRENCY" => "N",
	"DETAIL_ADD_DETAIL_TO_SLIDER" => "Y",
	"DETAIL_ADD_TO_BASKET_ACTION" => array(
		0 => "ADD",
	),
	"DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => array(
	),
	"DETAIL_BACKGROUND_IMAGE" => "-",
	"DETAIL_BLOG_USE" => "N",
	"DETAIL_BRAND_USE" => "N",
	"DETAIL_BROWSER_TITLE" => "-",
	"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
	"DETAIL_DETAIL_PICTURE_MODE" => array(
	),
	"DETAIL_DISPLAY_NAME" => "Y",
	"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
	"DETAIL_FB_USE" => "N",
	"DETAIL_IMAGE_RESOLUTION" => "16by9",
	"DETAIL_META_DESCRIPTION" => "-",
	"DETAIL_META_KEYWORDS" => "-",
	"DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
	"DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
	"DETAIL_SET_CANONICAL_URL" => "N",
	"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
	"DETAIL_SHOW_POPULAR" => "N",
	"DETAIL_SHOW_SLIDER" => "N",
	"DETAIL_SHOW_VIEWED" => "N",
	"DETAIL_STRICT_SECTION_CHECK" => "Y",
	"DETAIL_USE_COMMENTS" => "N",
	"DETAIL_USE_VOTE_RATING" => "N",
	"DETAIL_VK_USE" => "N",
	"DISABLE_INIT_JS_IN_COMPONENT" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"DISPLAY_ELEMENT_SELECT_BOX" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"ELEMENT_SORT_FIELD" => "propertysort_PRODUCER",
	"ELEMENT_SORT_FIELD2" => "active_from",
	"ELEMENT_SORT_ORDER" => "asc",
	"ELEMENT_SORT_ORDER2" => "asc",
	"FILE_404" => "",
	"FILTER_FIELD_CODE" => array(
		0 => "",
		1 => "",
	),
	"FILTER_HIDE_ON_MOBILE" => "N",
	"FILTER_NAME" => "",
	"FILTER_PRICE_CODE" => array(
	),
	"FILTER_PROPERTY_CODE" => array(
		0 => "BRANDING",
		1 => "FREEZING",
		2 => "SEALING",
		3 => "WARMING",
		4 => "HEIGHT",
		5 => "DIAMETER",
		6 => "LENGTH",
		7 => "SECTIONS",
		8 => "MATERIAL",
		9 => "SPIKES",
		10 => "VOLUME",
		11 => "REGION",
		12 => "USING",
		13 => "LOCKER",
		14 => "FORM",
		15 => "COLOR",
		16 => "",
	),
	"FILTER_VIEW_MODE" => "VERTICAL",
	"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
	"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
	"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
	"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
	"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
	"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
	"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
	"GIFTS_MESS_BTN_BUY" => "Выбрать",
	"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
	"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
	"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
	"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
	"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
	"GIFTS_SHOW_IMAGE" => "Y",
	"GIFTS_SHOW_NAME" => "Y",
	"GIFTS_SHOW_OLD_PRICE" => "Y",
	"HIDE_NOT_AVAILABLE" => "L",
	"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
	"IBLOCK_ID" => "65",
	"IBLOCK_TYPE" => "catalog_de",
	"INCLUDE_SUBSECTIONS" => "Y",
	"INSTANT_RELOAD" => "Y",
	"LABEL_PROP" => array(
			0 => "LABLE",
		),
	"LAZY_LOAD" => "N",
	"LINE_ELEMENT_COUNT" => "3",
	"LINK_ELEMENTS_URL" => "",
	"LINK_IBLOCK_ID" => "",
	"LINK_IBLOCK_TYPE" => "",
	"LINK_PROPERTY_SID" => "",
	"LIST_BROWSER_TITLE" => "-",
	"LIST_ENLARGE_PRODUCT" => "STRICT",
	"LIST_META_DESCRIPTION" => "-",
	"LIST_META_KEYWORDS" => "-",
	"LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
	"LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
	"LIST_SHOW_SLIDER" => "N",
	"LIST_SLIDER_INTERVAL" => "3000",
	"LIST_SLIDER_PROGRESS" => "N",
	"LOAD_ON_SCROLL" => "N",
	"MESSAGE_404" => "",
	"MESS_BTN_ADD_TO_BASKET" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_CART"),
	"MESS_BTN_BUY" => "Купить",
	"MESS_BTN_COMPARE" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_COMPARE"),
	"MESS_BTN_DETAIL" => "Подробнее",
	"MESS_BTN_SUBSCRIBE" => "Подписаться",
	"MESS_COMMENTS_TAB" => "Комментарии",
	"MESS_DESCRIPTION_TAB" => "Описание",
	"MESS_NOT_AVAILABLE" => "Нет в наличии",
	"MESS_PRICE_RANGES_TITLE" => "Цены",
	"MESS_PROPERTIES_TAB" => "Характеристики",
	"PAGER_BASE_LINK_ENABLE" => "N",
	"PAGER_DESC_NUMBERING" => "N",
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
	"PAGER_SHOW_ALL" => "N",
	"PAGER_SHOW_ALWAYS" => "N",
	"PAGER_TEMPLATE" => ".default",
	"PAGER_TITLE" => "Товары",
	"PAGE_ELEMENT_COUNT" => "30",
	"PARTIAL_PRODUCT_PROPERTIES" => "N",
	"PRICE_CODE" => array(
		0 => "BASE",
	),
	"PRICE_VAT_INCLUDE" => "N",
	"PRICE_VAT_SHOW_VALUE" => "N",
	"PRODUCT_ID_VARIABLE" => "id",
	"PRODUCT_PROPS_VARIABLE" => "prop",
	"PRODUCT_QUANTITY_VARIABLE" => "quantity",
	"PRODUCT_SUBSCRIPTION" => "N",
	"SEARCH_CHECK_DATES" => "Y",
	"SEARCH_NO_WORD_LOGIC" => "Y",
	"SEARCH_PAGE_RESULT_COUNT" => "50",
	"SEARCH_RESTART" => "N",
	"SEARCH_USE_LANGUAGE_GUESS" => "N",
	"SECTIONS_SHOW_PARENT_NAME" => "Y",
	"SECTIONS_VIEW_MODE" => "LIST",
	"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
	"SECTION_BACKGROUND_IMAGE" => "-",
	"SECTION_COUNT_ELEMENTS" => "N",
	"SECTION_ID_VARIABLE" => "SECTION_ID",
	"SECTION_TOP_DEPTH" => "2",
	"SEF_FOLDER" => "/de/catalog/",
	"SEF_MODE" => "Y",
	"SET_LAST_MODIFIED" => "N",
	"SET_STATUS_404" => "Y",
	"SET_TITLE" => "Y",
	"SHOW_404" => "Y",
	"SHOW_DEACTIVATED" => "N",
	"SHOW_DISCOUNT_PERCENT" => "N",
	"SHOW_MAX_QUANTITY" => "N",
	"SHOW_OLD_PRICE" => "N",
	"SHOW_PRICE_COUNT" => "1",
	"SHOW_TOP_ELEMENTS" => "N",
	"SIDEBAR_DETAIL_SHOW" => "N",
	"SIDEBAR_PATH" => "",
	"SIDEBAR_SECTION_SHOW" => "N",
	"TEMPLATE_THEME" => "",
	"TOP_ADD_TO_BASKET_ACTION" => "ADD",
	"TOP_ELEMENT_COUNT" => "9",
	"TOP_ELEMENT_SORT_FIELD" => "sort",
	"TOP_ELEMENT_SORT_FIELD2" => "id",
	"TOP_ELEMENT_SORT_ORDER" => "asc",
	"TOP_ELEMENT_SORT_ORDER2" => "desc",
	"TOP_ENLARGE_PRODUCT" => "STRICT",
	"TOP_LINE_ELEMENT_COUNT" => "3",
	"TOP_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
	"TOP_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
	"TOP_SHOW_SLIDER" => "Y",
	"TOP_SLIDER_INTERVAL" => "3000",
	"TOP_SLIDER_PROGRESS" => "N",
	"TOP_VIEW_MODE" => "SECTION",
	"USER_CONSENT" => "N",
	"USER_CONSENT_ID" => "0",
	"USER_CONSENT_IS_CHECKED" => "N",
	"USER_CONSENT_IS_LOADED" => "N",
	"USE_BIG_DATA" => "N",
	"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
	"USE_COMPARE" => "Y",
	"USE_ELEMENT_COUNTER" => "Y",
	"USE_ENHANCED_ECOMMERCE" => "N",
	"USE_FILTER" => "Y",
	"USE_GIFTS_DETAIL" => "N",
	"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
	"USE_GIFTS_SECTION" => "N",
	"USE_MAIN_ELEMENT_SECTION" => "Y",
	"USE_PRICE_COUNT" => "N",
	"USE_PRODUCT_QUANTITY" => "Y",
	"USE_REVIEW" => "N",
	"USE_SALE_BESTSELLERS" => "N",
	"USE_STORE" => "N",
	"COMPONENT_TEMPLATE" => "protek_catalog",
	"MESSAGES_PER_PAGE" => "10",
	"USE_CAPTCHA" => "Y",
	"REVIEW_AJAX_POST" => "Y",
	"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
	"FORUM_ID" => "",
	"URL_TEMPLATES_READ" => "",
	"SHOW_LINK_TO_FORUM" => "N",
	"DOWNLOAD_PRICELIST" => GetMessage("PROTEK_TEMPLATE_CATALOG_DOWNLOAD"),
	"DOWNLOAD_PRICELIST_FILE" => "/robots.txt",
	"MORE_CAPTION" => GetMessage("PROTEK_TEMPLATE_CATALOG_MORE"),
	"CATEGORY_SORT_BY" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY"),
	"CATEGORY_VIEW" => GetMessage("PROTEK_TEMPLATE_CATALOG_VIEW"),
	"CATEGORY_SORT_BY_NAME" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY_NAME"),
	"CATEGORY_SORT_BY_FROM" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY_FROM"),
	"CATEGORY_SORT_BY_SHOWS" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY_SHOWS"),
	"BUTTON_DELETE_FROM_COMPARE" => GetMessage("PROTEK_TEMPLATE_CATALOG_REMOVE"),
	"BUTTON_ADD_TO_FAVORITES" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_FAVORITES"),
	"BUTTON_DELETE_FROM_FAVORITES" => GetMessage("PROTEK_TEMPLATE_CATALOG_REMOVE"),
	
	'ITEMS_SHARE' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_SHARE"),
	'ITEM_INFO' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_TITLE"),
	'ITEM_INFO_PROPS' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_PROPERTIES"),
	'ITEM_INFO_DELIVERY' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_DELIVERY"),
	'ITEM_INFO_DESCR' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_DESCRIPTION"),
	'ITEM_INFO_INSTRUCTION' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_INSRTUCTION"),
	'BOX_QUANTITY' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX_COUNT"),
	'ITEMS_RELEVANT' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_RELEVANT"),
	'ITEMS_SAME' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_SAME"),
	'ITEMS_DOWNLOAD' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_DOWNLOAD_PRISE"),
	'ITEMS_PACK_VOLUME' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX_VOLUME"),
	'ITEMS_PACK_VOLUME_TEXT' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX_VOLUME_TEXT"),
	'ITEMS_PACK_PROPS' => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX"),
	'COMPARE_ONLY_DIFFERENCES' => GetMessage("PROTEK_TEMPLATE_COMPARE_ONLY_DIFFERENCES"),
	'COMPARE_TITLE' => GetMessage("PROTEK_TEMPLATE_COMPARE_TITLE"),
	'COMPARE_NO_RESULT' => GetMessage("PROTEK_TEMPLATE_COMPARE_NO_RESULT"),
	'MENU_LESS' => GetMessage("PROTEK_TEMPLATE_SHOW_LESS"),
	'MENU_MORE' => GetMessage("PROTEK_TEMPLATE_MORE"),
	'MENU_TITLE' => GetMessage("PROTEK_TEMPLATE_CATALOG_MENU_TITLE"),
	'FAVORITES_REMOVE_ALL' => GetMessage("PROTEK_TEMPLATE_CATALOG_CLEAN_FAVORITES"),
	
	"LIST_PROPERTY_CODE_MOBILE" => array(
	),
	"DETAIL_MAIN_BLOCK_PROPERTY_CODE" => array(
	),
	"SEF_URL_TEMPLATES" => array(
		"sections" => "",
		"section" => "#SECTION_CODE#/",
		"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
		"compare" => "compare.php?action=#ACTION_CODE#",
		"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
	),
	"VARIABLE_ALIASES" => array(
		"compare" => array(
			"ACTION_CODE" => "action",
		),
	)
);
if($_POST['forming']) {
	$_SESSION['CYBER_CATALOG_FORMING'] = htmlspecialchars((string)$_POST['forming']);
}
if($_POST['sorting']) {
	$_SESSION['CYBER_CATALOG_SORTING'] = htmlspecialchars((string)$_POST['sorting']);
}?>
<?if(count($_SESSION[FAVORITES_NAME.SITE_ID]) > 0){?>
<div class="category">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-4 col-xl-3">
					<div class="category__sidebar">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"protek_favorites_menu",
					Array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "left",
						"DELAY" => "N",
						"MAX_LEVEL" => "1",
						"MENU_CACHE_GET_VARS" => array(0=>"",),
						"MENU_CACHE_TIME" => "36000",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "sticky",
						"USE_EXT" => "Y",
						"MENU_LESS" => $arParams['MENU_LESS'], 
						"MENU_MORE" => $arParams['MENU_MORE'],
						"MENU_TITLE" => $arParams['MENU_TITLE'],
					)
				);?>
					</div>
				</div>

				<div class="col-12 col-lg-8 col-xl-9">
<div class="category__title">
	<form name="sorting-form" id="sorting-form" action="" method="post"></form>
	<div class="row">
		<div class="col-12">
			<h1 class="section__title"><?$APPLICATION->ShowTitle()?></h1>
			<button type="button" class="category__clear category__clear--desk removeAllfavorites"><?=$arParams['FAVORITES_REMOVE_ALL']?></button>
		</div>
		
		<div class="col-12">
			<div class="category__filter-wrap">
				<div class="category__sort">
					<span><?=$arParams['CATEGORY_SORT_BY']?>:</span>
					<div class="sorting__collapse_container">
						<a data-toggle="collapse" href="#sorting_collapse" role="button" aria-expanded="false" class="category__sort_select">
						<?if($_SESSION['CYBER_CATALOG_SORTING'] == "name"){
							echo $arParams['CATEGORY_SORT_BY_NAME'];
						}elseif($_SESSION['CYBER_CATALOG_SORTING'] == "show_counter"){
							echo $arParams['CATEGORY_SORT_BY_SHOWS'];
						}else{
							echo $arParams['CATEGORY_SORT_BY_FROM'];
						}?>
						</a>
						<div class="sorting__collapse collapse" id="sorting_collapse">
							<button form="sorting-form" class=" <?=($_SESSION['CYBER_CATALOG_SORTING'] == "name" ? "active" : "")?>" type="submit" name="sorting" value="name"><?=$arParams['CATEGORY_SORT_BY_NAME']?></button>
							<button form="sorting-form" class=" <?=($_SESSION['CYBER_CATALOG_SORTING'] == "active_from" ? "active" : "")?>" type="submit" name="sorting" value="active_from"><?=$arParams['CATEGORY_SORT_BY_FROM']?></button>
							<button form="sorting-form" class=" <?=($_SESSION['CYBER_CATALOG_SORTING'] == "show_counter" ? "active" : "")?>" type="submit" name="sorting" value="show_counter"><?=$arParams['CATEGORY_SORT_BY_SHOWS']?></button>
						</div>
					</div>
				</div>

				<div class="category__type">
					<span><?=$arParams['CATEGORY_VIEW']?>:</span>
					<button form="sorting-form" class="products-sort__link <?=($_SESSION['CYBER_CATALOG_FORMING'] == "category_list" ? "active" : "")?>" type="submit" name="forming" value="category_list"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="39px" height="32px" viewBox="0 0 48.098 36.203" style="enable-background:new 0 0 48.098 36.203;" xml:space="preserve"><path d="M45.699,35.567H13.535c-1.324,0-2.398-1.075-2.398-2.399c0-1.328,1.074-2.402,2.398-2.402h32.164 c1.324,0,2.399,1.074,2.399,2.402C48.098,34.492,47.023,35.567,45.699,35.567L45.699,35.567z M45.699,35.567"/><path d="M45.699,20.504H13.535c-1.324,0-2.398-1.074-2.398-2.402c0-1.324,1.074-2.399,2.398-2.399h32.164 c1.324,0,2.399,1.075,2.399,2.399C48.098,19.43,47.023,20.504,45.699,20.504L45.699,20.504z M45.699,20.504"/><path d="M45.699,5.438H13.535c-1.324,0-2.398-1.075-2.398-2.399c0-1.328,1.074-2.402,2.398-2.402h32.164 c1.324,0,2.399,1.074,2.399,2.402C48.098,4.363,47.023,5.438,45.699,5.438L45.699,5.438z M45.699,5.438"/><path d="M6.445,3.223c0,1.781-1.441,3.223-3.222,3.223C1.441,6.446,0,5.004,0,3.223C0,1.442,1.441,0,3.223,0 C5.004,0,6.445,1.442,6.445,3.223L6.445,3.223z M6.445,3.223"/><path d="M6.445,18.102c0,1.781-1.441,3.222-3.222,3.222C1.441,21.324,0,19.883,0,18.102c0-1.778,1.441-3.223,3.223-3.223 C5.004,14.879,6.445,16.324,6.445,18.102L6.445,18.102z M6.445,18.102"/><path d="M6.445,32.981c0,1.781-1.441,3.222-3.222,3.222C1.441,36.203,0,34.762,0,32.981c0-1.778,1.441-3.223,3.223-3.223 C5.004,29.758,6.445,31.203,6.445,32.981L6.445,32.981z M6.445,32.981"/></svg></button>
					<button form="sorting-form" class="products-sort__link <?=(($_SESSION['CYBER_CATALOG_FORMING'] == "category_big"  || empty($_SESSION['CYBER_CATALOG_FORMING'])) ? "active" : "")?>" type="submit" name="forming" value="category_big"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewBox="71.163 261.163 271.673 271.673" style="enable-background:new 71.163 261.163 271.673 271.673;" xml:space="preserve"><path d="M186.102,261.163H81.612c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C196.551,265.841,191.873,261.163,186.102,261.163z"/><path d="M332.388,261.163h-104.49c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C342.837,265.841,338.159,261.163,332.388,261.163z"/><path d="M186.102,407.449H81.612c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C196.551,412.127,191.873,407.449,186.102,407.449z"/><path d="M332.388,407.449h-104.49c-5.771,0-10.449,4.678-10.449,10.449v104.49c0,5.771,4.678,10.449,10.449,10.449h104.49 c5.771,0,10.449-4.678,10.449-10.449v-104.49C342.837,412.127,338.159,407.449,332.388,407.449z"/></svg></button>
					<button form="sorting-form" class="products-sort__link <?=($_SESSION['CYBER_CATALOG_FORMING'] == "category_small" ? "active" : "")?>" type="submit" name="forming" value="category_small"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30" height="30" viewBox="0 0 341.333 341.333" style="enable-background:new 0 0 341.333 341.333;" xml:space="preserve"><rect x="128" y="128" width="85.333" height="85.333"/><rect x="0" y="0" width="85.333" height="85.333"/><rect x="128" y="256" width="85.333" height="85.333"/><rect x="0" y="128" width="85.333" height="85.333"/><rect x="0" y="256" width="85.333" height="85.333"/><rect x="256" y="0" width="85.333" height="85.333"/><rect x="128" y="0" width="85.333" height="85.333"/><rect x="256" y="128" width="85.333" height="85.333"/><rect x="256" y="256" width="85.333" height="85.333"/></svg></a>
				</div>
			</div>
		</div>
	</div>
</div>					
<?
$GLOBALS['arFilterFavorites'] = array("ID" => array_keys($_SESSION[FAVORITES_NAME.SITE_ID]));
$intSectionID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.section",
	"favorites_category",
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ELEMENT_SORT_FIELD" => (!empty($_REQUEST['sorting']) ? htmlspecialchars((string)$_REQUEST['sorting']) : (!empty($_SESSION['CYBER_CATALOG_SORTING']) ? $_SESSION['CYBER_CATALOG_SORTING'] : "active_from")),
		"ELEMENT_SORT_ORDER" => (htmlspecialchars((string)$_REQUEST['sorting']) == "name" || $_SESSION['CYBER_CATALOG_SORTING'] == "name" ? "asc" : "desc"),
		"ELEMENT_SORT_FIELD2" => "PROPERTY_PRODUCER",
		"ELEMENT_SORT_ORDER2" => "asc",
		"PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
		"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
		"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"FILTER_NAME" => "arFilterFavorites",
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_FILTER" => $arParams["CACHE_FILTER"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
		"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
		"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		"PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

		"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
		"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
		"PAGER_TITLE" => $arParams["PAGER_TITLE"],
		"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
		"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
		"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
		"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
		"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
		"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
		"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
		"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
		"LAZY_LOAD" => $arParams["LAZY_LOAD"],
		"MESS_BTN_LAZY_LOAD" => $arParams["MESS_BTN_LAZY_LOAD"],
		"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

		"OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
		"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
		"OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
		"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
		"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
		"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
		"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
		"OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
		'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

		'LABEL_PROP' => $arParams['LABEL_PROP'],
		'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
		'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
		'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
		'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
		'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
		'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
		'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
		'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
		'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
		'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['MESS_SHOW_MAX_QUANTITY']) ? $arParams['MESS_SHOW_MAX_QUANTITY'] : ''),
		'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
		'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['MESS_RELATIVE_QUANTITY_MANY'] : ''),
		'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['MESS_RELATIVE_QUANTITY_FEW'] : ''),
		'MESS_BTN_BUY' => (isset($arParams['MESS_BTN_BUY']) ? $arParams['MESS_BTN_BUY'] : ''),
		'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['MESS_BTN_ADD_TO_BASKET']) ? $arParams['MESS_BTN_ADD_TO_BASKET'] : ''),
		'MESS_BTN_SUBSCRIBE' => (isset($arParams['MESS_BTN_SUBSCRIBE']) ? $arParams['MESS_BTN_SUBSCRIBE'] : ''),
		'MESS_BTN_DETAIL' => (isset($arParams['MESS_BTN_DETAIL']) ? $arParams['MESS_BTN_DETAIL'] : ''),
		'MESS_NOT_AVAILABLE' => (isset($arParams['MESS_NOT_AVAILABLE']) ? $arParams['MESS_NOT_AVAILABLE'] : ''),
		'MESS_BTN_COMPARE' => (isset($arParams['MESS_BTN_COMPARE']) ? $arParams['MESS_BTN_COMPARE'] : ''),

		'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
		'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
		'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

		'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		"ADD_SECTIONS_CHAIN" => "N",
		'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
		'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
		'COMPARE_NAME' => $arParams['COMPARE_NAME'],
		'USE_COMPARE_LIST' => 'Y',
		'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
		'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
		'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
		'BUTTON_DELETE_FROM_COMPARE' => $arParams['BUTTON_DELETE_FROM_COMPARE'],
		'BUTTON_ADD_TO_FAVORITES' => $arParams['BUTTON_ADD_TO_FAVORITES'],
		'BUTTON_DELETE_FROM_FAVORITES' => $arParams['BUTTON_DELETE_FROM_FAVORITES'],
		"MORE_CAPTION" => $arParams['MORE_CAPTION'],
		"CATEGORY_SORT_BY" => $arParams['CATEGORY_SORT_BY'],
		"CATEGORY_VIEW" => $arParams['CATEGORY_VIEW'],
		"CATEGORY_SORT_BY_NAME" => $arParams['CATEGORY_SORT_BY_NAME'],
		"CATEGORY_SORT_BY_FROM" => $arParams['CATEGORY_SORT_BY_FROM'],
		"CATEGORY_SORT_BY_SHOWS" => $arParams['CATEGORY_SORT_BY_SHOWS'],
		'SHOW_ALL_WO_SECTION' => 'Y',
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		'FAVORITES_REMOVE_ALL' => GetMessage("PROTEK_TEMPLATE_CATALOG_CLEAN_FAVORITES"),
	),
	$component
);
?>
				</div>
			</div>
		</div>
	</div>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__about"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/catalog/favorites.php"), false);?></div>
			</div>
		</div>
	</div>
</section>
<?}else{?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title"><?$APPLICATION->ShowTitle()?></h1>
				<p><?=GetMessage("PROTEK_TEMPLATE_FAVORITES_NO_RESULT")?></p>
			</div>
		</div>
	</div>
</section>
<?
}
$APPLICATION->IncludeComponent(
		"bitrix:catalog.compare.list",
		"",
		array(
			"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID" => $arParams["IBLOCK_ID"],
			"NAME" => $arParams["COMPARE_NAME"],
			"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action"),
			"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
			'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
			'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
		),
		$component,
		array("HIDE_ICONS" => "Y")
	);
?>
<script>
$(document).ready(function(){

	$('body').on('input change', '.quantity', function() { //Только цифры в инпутах
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		if(this.value == 0){
			this.value = 1;
		}
	});
	$('body').on('click', '.quantity-control', function(){ //Изменение количества (клики по плюсу и минусу)
		let quantutyInput = $(this).siblings('input'),
			quantity = 1;
		if($(this).hasClass('minus')) {
			
			if(quantutyInput.val() > 1) { //Не изменяем количество если после нажатия на минус в поле должен появиться ноль
				quantutyInput.val(Number(quantutyInput.val()) - 1);
			}
			
		} else {
			quantutyInput.val(Number(quantutyInput.val()) + 1);
		}
		
	});
	$('body').on('click', '.add2basket_btn', function(){ // add2basket
		let basketParams = {
				'ajax_basket': 'Y'
			},
			basketUrl = basketData['basketTemplateUrl'].replace("#ID#", $(this).data('cyber_id'));
		basketParams[basketData.quantity] = $(this).parents('.cyberItemContainer').find('.quantity').val();

		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: basketUrl,
			data: basketParams,
			success: function(data) {
				console.log(data);
				$('body').trigger('cyber.change.basket.result');
			}
		});
	});
	$('body').on('click', '.add2compare_btn', function(){ // add to compare or remove
		
		let compareUrl = basketData['compareAddTemplate'].replace("#ID#", $(this).data('cyber_id')) + (basketData['compareAddTemplate'].indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y',
			elementInCompare = false,
			compareAddMessage = basketData['compareAddMessage'],
			compareRemoveMessage = basketData['compareRemoveMessage'];
		if($(this).hasClass('in-compare')){
			compareUrl = basketData['compareDeleteTemplate'].replace("#ID#", $(this).data('cyber_id')) + (basketData['compareDeleteTemplate'].indexOf('?') !== -1 ? '&' : '?') + 'ajax_action=Y';
			elementInCompare = true;
		}

		let self = this;
		$.ajax({
			method: 'POST',
			dataType: elementInCompare ? 'json' : 'html',
			url: compareUrl,
			success: function(data) {
				obj = {};
				if(typeof data === 'object' && data !== null){
					obj = data;
				}else{
					obj = $.parseJSON(data);
				}
				
				if(obj.STATUS == 'OK'){
					$('body').trigger('cyber.change.compare.result');
					if(!elementInCompare){
						$(self).addClass('in-compare');
						$(self).find('span').text(compareRemoveMessage);
					}else{
						$(self).removeClass('in-compare');
						$(self).find('span').text(compareAddMessage);
					}
				}
			}
		});
	});
	$('body').on('click', '.add2favorites_btn', function(){ // add to favorites or remove
		
		let elementInFavorites = false,
			favoritesAddMessage = basketData['favoritesAddMessage'],
			favoritesRemoveMessage = basketData['favoritesRemoveMessage'],
			favoritesData = "id=" + $(this).data('cyber_id') + "&destination=" + basketData['favoritesName'] + "&action=";
			
		if($(this).hasClass('in-favorites')){
			elementInFavorites = true;
			favoritesData += "remove";
		}else{
			favoritesData += "add";
		}

		
		let self = this;
		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: '/ajax/ajax_add2favorites.php',
			data: favoritesData,
			success: function(data) {
				
				if(data.STATUS == 'OK'){
					$('body').trigger('cyber.change.favorites.result');
					if(!elementInFavorites){
						$(self).addClass('in-favorites');
						$(self).find('span').text(favoritesRemoveMessage);
					}else{
						$(self).removeClass('in-favorites');
						$(self).find('span').text(favoritesAddMessage);
					}
					location.reload();
				}
			}
		});
	});
	
	$('body').on('click', '.removeAllfavorites', function(){ // remove all favorites
		
		let favoritesData = "destination=" + basketData['favoritesName'] + "&action=annihilation";
			

		
		let self = this;
		$.ajax({
			method: 'POST',
			dataType: 'json',
			url: '/ajax/ajax_add2favorites.php',
			data: favoritesData,
			success: function(data) {
				
				if(data.STATUS == 'OK'){
					location.reload();
				}
			}
		});
	});
	
	$('.category-menu-collapse--dropdown').on('click', function (e) {
		e.stopPropagation();
		e.preventDefault();

		var drop = $(this).closest(".dropdown");
		$(drop).addClass("open");

		$('.collapse.in').collapse('hide');
		var col_id = $(this).attr("href");
		$(col_id).collapse('toggle');
	});
});
</script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>