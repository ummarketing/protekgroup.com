<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ÜBER UNS");
?><section class="section section--top0">
<div class="container">
	<h1 class="section__title">Firmengeschichte:</h1>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"simple_slider_2",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("DETAIL_PICTURE", ""),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "61",
		"IBLOCK_TYPE" => "content_de",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("", ""),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N"
	)
);?><br />
	<ul>
		<li><b>30. Dezember</b><b>1996</b><b> г.</b> - Eröffnung der Firma "PROTEK" in Moskau&nbsp;</li>
		<li><b>1998 г. </b>- Beginn der Partnerschaft mit der GmbH "Priopak"(Ryazan)&nbsp;</li>
		<li><b>2001 г. </b>- Gründung des Produktionskomplexes "Interplastic" in Lyubertsy&nbsp;</li>
		<li><b>2002 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Anapa<b>&nbsp;</b></li>
		<li><b>2002 г. </b>- Gründung des Produktionskomplexes "Megaplast" in Toropets <b>&nbsp;</b></li>
		<li><b>2003 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Woronesch&nbsp;</li>
		<li><b>2004 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Jaroslawl &nbsp;</li>
		<li><b>2004 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protec" in St. Petersburg<b>&nbsp;</b></li>
		<li><b>2005 г.</b> - Gründung des Produktionskomplexes "Interplastic" in Krasnojarsk&nbsp;</li>
		<li><b>2006 г.</b> - Gründung des Produktionskomplexes "Ramupak" in der Siedlung Tomilino (verlegt nach Ramenskoje)</li>
		<li><b>2007 г.</b> - Gründung des Produktionskomplexes "Rumplast" in der Siedlung Tomilino (verlegt nach Ramenskoje)&nbsp;</li>
		<li><b>2007 г.</b> - Start der Produktionseinheit "Fabrik der Verpackungsprodukte" ("ZUI") &nbsp;</li>
		<li><b>2010 г. </b>- Gründung des Produktionskomplexes "Megaplast" in Puschtschino &nbsp;</li>
		<li><b>2011 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Krasnojarsk&nbsp;</li>
		<li><b>2012 г. </b>- Gründung des Produktionskomplexes "Interplastic 2001" in Tomilino&nbsp;</li>
		<li><b>2012 г. </b>- Eröffnung der Vertriebsabteilung von protec in Astana, Kasachstan&nbsp;</li>
		<li><b>2012 г. </b>- Eröffnung der Vertriebsabteilung von protec in Almaty, Kasachstan&nbsp;</li>
		<li><b>2012 г.</b> - Eröffnung des Autohauses von protec in Kiew, Ukraine&nbsp;</li>
		<li><b>2013 г.</b> - Gründung des Produktionskomplexes "VVM-Trans" in Puschtschino&nbsp;</li>
		<li><b>2013 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Krasnodar&nbsp;</li>
		<li><b>2013 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Nischni Nowgorod&nbsp;</li>
		<li><b>2013 г.</b> - Eröffnung der Vertriebsabteilung der Firma "Protek" in Tscheljabinsk&nbsp;</li>
		<li><b>2013 г.</b> - Eröffnung des Autohauses der Firma "Protek" in Kazan&nbsp;</li>
		<li><b>2014 г.</b> - das Erscheinen der Handelsmarke "Konzern Protek"&nbsp;</li>
		<li><b>2014 г. </b>- Eröffnung der Vertriebsabteilung des Konzern "Protek" in Perm&nbsp;</li>
		<li><b>2014 г. </b>- Eröffnung der Vertriebsabteilung des Konzern "protec" in Ufa&nbsp;</li>
		<li><b>2014 г.</b> - Eröffnung der Vertriebsabteilung des Konzern "Protek" in Simferopol, Republik Krim&nbsp;</li>
		<li><b>2015 г.</b> - Start Des Produktionskomplexes" Interplastic 2001 " Lakor in der Siedlung Nekrassowski&nbsp;</li>
		<li><b>2015 г.</b> - Eröffnung der Vertriebsabteilung des Konzern "Protek" in Wolgograd&nbsp;</li>
		<li><b>2015 г.</b> - Eröffnung der Vertriebsabteilung des Konzern "Protek" in Belgorod&nbsp;</li>
		<li><b>2016 г.</b> - Eröffnung der autohausabteilung Des Konzern "Protek" in Tjumen&nbsp;</li>
		<li><b>2016 г. </b>- Eröffnung der autohausabteilung Des Konzern "Protek" in Rostow am Don&nbsp;</li>
		<li><b>2016 г.</b> - Start der Produktionsstätte in Tscheljabinsk&nbsp;</li>
		<li><b>2016 г. </b>- Eröffnung der Vertriebsabteilung des Konzern "Protek" in Minsk, Weißrussland&nbsp;</li>
		<li><b>2017 г.</b> - Eröffnung der händlerabteilung Des Konzern "Protek" in Jekaterinburg&nbsp;</li>
		<li><b>2017 г.</b> - Eröffnung der Vertriebsabteilung des Konzern "Protek" in Kaliningrad&nbsp;</li>
		<li><b>2017 г.</b> - Eröffnung der Vertriebsabteilung des Konzern "Protek" in Nowosibirsk&nbsp;</li>
		<li><b>2018 г.</b> - Start der Produktionsstätte in Krasnodar&nbsp;</li>
		<li><b>2018 г. </b>- Eröffnung der Händler-Abteilung Des Konzern "Protek" in Vladimir&nbsp;</li>
		<li><b>2018 г.</b> - Eröffnung der Händler-Abteilung Des Konzern "Protek" in Izhevsk&nbsp;</li>
		<li><b>2018 г.</b> - Eröffnung der Händler-Abteilung Des Konzern "Protek" in Samara&nbsp;</li>
		<li><b>2018 г. </b>- Eröffnung der Händler-Abteilung Des Konzern "Protek" in Omsk&nbsp;</li>
		<li><b>2018 г.</b> - Start der Produktionsstätte in Omsk&nbsp;</li>
		<li><b>2018 г. </b>- Eröffnung der Händler-Abteilung Des Konzern "Protek" in Lipetsk&nbsp;</li>
		<li><b>2018 г.</b> - Eröffnung der Händler-Abteilung Des Konzern "Protek" in Tscheboksary&nbsp;</li>
		<li><b>2018 г.</b> - Eröffnung der Vertriebsabteilung des Konzern "protec" in Pskow</li>
	</ul>
	 <?$APPLICATION->IncludeComponent(
	"cyberrex:highloadblock.list",
	"main_features",
	Array(
		"BLOCK_ID" => "4",
		"BLOCK_TITLE" => "Наши преимущества:",
		"CHECK_PERMISSIONS" => "N",
		"DETAIL_URL" => "",
		"FILTER_NAME" => "",
		"PAGEN_ID" => "",
		"ROWS_PER_PAGE" => "",
		"SORT_FIELD" => "UF_SORT",
		"SORT_ORDER" => "ASC"
	)
);?><br>
	<p>
	</p>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>