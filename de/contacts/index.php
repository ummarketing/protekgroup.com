<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Unsere Kontaktdaten");
?><section class="section section--top0">
<div class="container">
	<h1 class="section__title">Unsere Kontaktdaten</h1>
	<div class="news_article_container">
		<p>
 <b>Adresse:</b> 2, bld. 4, Admirala Makarova str., 125212, Moscow, Russia;
		</p>
		<p>
 <b>Telefon: </b><span class="ya-phone">+7 (495) 775-30-47</span>&nbsp; <b>Fax:</b> +7 (495) 452-34-66&nbsp;
		</p>
		<p>
 <b>Email:</b> <a href="mailto:zayavka@protekgroup.com">zayavka@protekgroup.com</a>&nbsp;
		</p>
		<p>
 <b>Arbeitszeit:</b> Mon.-Fri.: 9 a.m. - 6 p.m. Sat.,Sun.: closed
		</p>
		<p>
 <b>Wie man uns besucht: </b>You should get to Voykovskaya metro station, exit to the Warsaw cinema, then walk along the Metropolis shopping center to the railroad tracks, cross them and turn right and go along the road to the entrance. You should say to the security guard that you are going to PROTEK. You shouldn’t order the pass in advance, as the security gives it right on the place. Our office is in the second building on the right with the white porch.
		</p>
		 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view",
	"",
	Array(
		"API_KEY" => "",
		"CONTROLS" => array("ZOOM"),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.82779308284162;s:10:\"yandex_lon\";d:37.498656513239;s:12:\"yandex_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.498656513239;s:3:\"LAT\";d:55.82779308286;s:4:\"TEXT\";s:27:\"Концерн Протэк\";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "500",
		"OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING")
	)
);?><br>
	</div>
</div>
 </section><br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>