<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Доставка и оплата");
?><section class="section section--top0">
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="section__title">Доставка и оплата</h1>
			<h4>СПОСОБЫ ОПЛАТЫ ДЛЯ ФИЗИЧЕСКИХ ЛИЦ.</h4>
			<p>
				 С физическими лицами мы не работаем.
			</p>
			<p>
			</p>
			<h4>СПОСОБЫ ОПЛАТЫ ДЛЯ ЮРИДИЧЕСКИХ ЛИЦ.</h4>
			<p>
				 Оплату возможно произвести по безналичному расчету, за наличный расчет или через отделение любого банка или онлайн - банка.
			</p>
			<p>
				 Вы можете сделать перевод наличными с Вашей банковской карты на расчетный счет ООО "Холдинг ПРОТЭК", (НДС входит в стоимость товара). Все необходимые для бухгалтерии документы (оригинал счета на оплату, счет-фактура, накладная) выдаются вместе с заказом при получении.
			</p>
			<p>
				 Обратите внимание, что при получении товара необходимо предоставить доверенность по форме М-2 либо поставить круглую печать организации на сопроводительные документы. Оригинал счета, счета — фактуры и товарной накладной (ТОРГ-12) Вы получаете вместе с заказом.
			</p>
			<p>
			</p>
			<h4>МИНИМАЛЬНАЯ СУММА ЗАКАЗА.</h4>
			<p>
				 Минимальная сумма заказа = 1 упаковка/ коробка.
			</p>
			<p>
			</p>
			<h4>ДОСТАВКА</h4>
			<p>
				 Мы осуществляем отправку товаров в любую точку мира.
			</p>
			<p>
				 Для Москвы: мы предоставляем бесплатную доставку своим автотранспортом в черте города при заказе свыше 30 000 рублей. При заказе менее 30 000 рублей стоимость доставки по городу обсуждается индивидуально. Возможен самовывоз. Так же мы предоставляем бесплатную доставку до Транспортной компании в черте города при заказе свыше 30 000 рублей.
			</p>
			<p>
				 Для филиалов: минимальная сумма заказа может разниться, необходимо уточнять в каждом конкретном филиале у менеджеров.
			</p>
			<p>
			</p>
			<h4>САМОВЫВОЗ</h4>
			<p>
				 Для Москвы: Вы можете самостоятельно забрать свой заказ со склада в удобное для Вас время по адресу:
			</p>
			<p>
				 г. Москва, ул. Адмирала Макарова, д.2, стр 4.
			</p>
			<p>
				 Время работы склада — с 9:00 до 18:00 с понедельника по пятницу.
			</p>
			<p>
				 При себе необходимо иметь печать или доверенность.
			</p>
			<p>
				 Для филиалов: время работы складов может разниться, необходимо уточнять в каждом конкретном филиале у менеджеров.
			</p>
			<p>
			</p>
			<h4>ВКЛЮЧЕН ЛИ НДС В СТОИМОСТЬ?</h4>
			<p>
				 Да, НДС включен в стоимость всех наших товаров.
			</p>
			<p>
			</p>
			<h4>ОТСРОЧКА ПЛАТЕЖА.</h4>
			<p>
				 Условия оговариваются индивидуально с каждым клиентом. Уточняйте у наших менеджеров. Звоните!
			</p>
			
			<p class="section__text">
			</p>
			<p>
				 Ваши вопросы вы можете задать, позвонив нам по тел.<b>+7 (495) 775-30-47</b> или написав на эл.почту&nbsp;<a href="mailto:info@protekgroup.com">info@protekgroup.com</a>
			</p>
		</div>
	</div>
</div>
 </section><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>