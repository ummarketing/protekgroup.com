<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Вакансии");
?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title">Вакансии</h1>
				<p class="section__text">Мы являемся динамичной, быстро развивающейся компанией и всегда открыты для общения. Именно поэтому мы заинтересованы в активных, креативных и перспективных сотрудниках. <br>Если Вы инициативны, ответственны и надежны, обращайтесь к нам! Мы ждем ваше резюме.</p>
			</div>
		</div>
	</div>
</section>
<?$APPLICATION->IncludeComponent(
	"cyberrex:form.result.new", 
	"protek_web_form", 
	array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "",
		"AJAX_MODE" => "N",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"WEB_FORM_ID" => "4",
		"COMPONENT_TEMPLATE" => "protek_web_form",
		"FORM_UNIQUE_CLASS" => "job_form",
		"FORM_REQUIRE_NOTE" => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
		"FORM_FILE_CHOOSE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE"),
		"FORM_FILE_CHOOSE_MULTY" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_MULTY"),
		"FORM_FILE_CHOOSE_NOTE" => GetMessage("PROTEK_TEMPLATE_FILE_CHOOSE_NOTE"),
		"VARIABLE_ALIASES" => array(
			"WEB_FORM_ID" => "WEB_FORM_ID",
			"RESULT_ID" => "RESULT_ID",
		)
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>