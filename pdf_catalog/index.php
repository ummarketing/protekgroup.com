<? require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
$arItem     = getElementById(10858);
$arItemProp = getElementPropertyByCode($arItem['IBLOCK_ID'], $arItem['ID'], 'FILE')[0];
$paf_path   = CFile::GetPath($arItemProp['VALUE']);
?>

<meta name="viewport" content="width=device-width, initial-scale=1.0">


<div id="catalog" style="overflow-x: scroll;">
</div>


<link rel="stylesheet" href="/local/assets/plugins/wowbook/css/wow_book.css">
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="/local/assets/plugins/wowbook/pdf.combined.min.js"></script>
<script src="/local/assets/plugins/wowbook/wow_book.min.js"></script>



<script>
var LANG = 'ru';
var files = {
    'ru' : '<?= $paf_path ?>',
    'eng' : '<?= $paf_path ?>'
};
    $(function(){
			var bookOptions = {
                height   : 768,
                // width    : 900,
				maxHeight : 2000

				,centeredWhenClosed : true
				,hardcovers : true
				,pageNumbers: false
				,toolbar : "lastLeft, left, right, lastRight, zoomin, zoomout, slideshow, fullscreen, thumbnails"
				,thumbnailsPosition : 'left'
				,responsiveHandleWidth : 50

				,container: window
				,containerPadding: "0px"
				// ,toolbarContainerPosition: "top" // default "bottom"

				// The pdf and your webpage must be on the same domain
				,pdf: files[LANG]

				// Uncomment the option toc to create a Table of Contents
				// ,toc: [                    // table of contents in the format
				// 	[ "Introduction", 2 ],  // [ "title", page number ]
				// 	[ "First chapter", 5 ],
				// 	[ "Go to codecanyon.net", "http://codecanyon.net" ] // or [ "title", "url" ]
				// ]
			};

			$('#catalog').wowBook( bookOptions ); // create the book

			// How to use wowbook API
			// var book=$.wowBook("#book"); // get book object instance
			// book.gotoPage( 4 ); // call some method
		})
</script>
<style>
    #catalog,
    .wowbook-book-container{
        overflow: hidden !important;
    }
</style>

