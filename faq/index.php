<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Часто задаваемые вопросы");
$APPLICATION->SetPageProperty('title', 'Часто задаваемые вопросы | Протэк Концерн');
$APPLICATION->SetPageProperty('description', 'Часто задаваемые вопросы. Протэк Концерн.');
$APPLICATION->SetAdditionalCSS("/local/assets/css/utils.css");
?>



<? if (!$USER->isAdmin() && false): ?>
    <section class="section section--top0">
        <div class="container">
            <h1 class="section__title">Часто задаваемые вопросы</h1>
            <p>
         <b>НАЛИЧИЕ И АССОРТИМЕНТ НА СКЛАДЕ</b>
            </p>
            <p>
                 Большой ассортимент продукции в наличии на складе в Москве, но необходим предварительный заказ. Более подробную информацию по остаткам на ту или иную позицию, пожалуйста, уточняйте у наших менеджеров.
            </p>
            <p>
         <b>ВОЗМОЖНЫЕ СКИДКИ И АКЦИИ</b>
            </p>
            <p>
                 Условия оговариваются индивидуально с каждым клиентом. Уточняйте у наших менеджеров.
            </p>
            <p>
         <b>УСЛОВИЯ РАБОТЫ В НАШИХ ФИЛИАЛАХ</b>
            </p>
            <p>
                 В наших филиалах индивидуальный подход к каждому клиенту. Условия вы можете уточнить, связавшись <a href="https://www.protekgroup.com/representations/">с ближайшим к вам подразделением</a>. Ваши вопросы вы можете задать, позвонив нам по тел.+7 (495) 775-30-47 или написав на электронную почту <a href="mailto:info@protekgroup.com">info@protekgroup.com</a>
        </p>
        <p>
        <b>ЧТО ОЗНАЧАЮТ ПЛАШКИ НА ФОТО ТОВАРА</b>
            </p>
        <p>
         <b>"Новинка"</b> - упаковка, недавно поступившая в продажу.&nbsp;
            </p>
            <p>
                <b>"Возможный вариант"</b> - спроектированная упаковка, которая прошла тестирование. Доступна к заказу от 200 000 штук наименования.&nbsp;
            </p>
            <p>
                <b>"По заказу"</b> - изделие, реализованное по индивидуальному заказу. Вариант работы с ним обсуждается индивидуально.&nbsp;
            </p>
            <p>
                <b>"В разработке"</b>- изделие находится на этапе проектирования и тестирования.
            </p>
        </div>
     </section>
<? else: ?>
    <div class="container">
        <section class="main__section faq__section">
            <?$APPLICATION->IncludeComponent(
                "bitrix:news",
                "protek_faq",
                Array(
                    "ADD_ELEMENT_CHAIN" => "Y",
                    "ADD_SECTIONS_CHAIN" => "Y",
                    "AJAX_MODE" => "N",
                    "AJAX_OPTION_ADDITIONAL" => "",
                    "AJAX_OPTION_HISTORY" => "N",
                    "AJAX_OPTION_JUMP" => "N",
                    "AJAX_OPTION_STYLE" => "Y",
                    "BROWSER_TITLE" => "-",
                    "CACHE_FILTER" => "N",
                    "CACHE_GROUPS" => "Y",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "CHECK_DATES" => "Y",
                    "DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",
                    "DETAIL_DISPLAY_TOP_PAGER" => "N",
                    "DETAIL_FIELD_CODE" => array("", ""),
                    "DETAIL_PAGER_SHOW_ALL" => "Y",
                    "DETAIL_PAGER_TEMPLATE" => "",
                    "DETAIL_PAGER_TITLE" => "Страница",
                    "DETAIL_PROPERTY_CODE" => array("*", ""),
                    "DETAIL_SET_CANONICAL_URL" => "N",
                    "DISPLAY_BOTTOM_PAGER" => "Y",
                    "DISPLAY_DATE" => "Y",
                    "DISPLAY_NAME" => "Y",
                    "DISPLAY_PICTURE" => "Y",
                    "DISPLAY_PREVIEW_TEXT" => "Y",
                    "DISPLAY_TOP_PAGER" => "N",
                    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                    "IBLOCK_ID" => "84",
                    "IBLOCK_TYPE" => "content",
                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                    "LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",
                    "LIST_FIELD_CODE" => array("", ""),
                    "LIST_PROPERTY_CODE" => array("*", ""),
                    "MESSAGE_404" => "",
                    "META_DESCRIPTION" => "-",
                    "META_KEYWORDS" => "-",
                    "NEWS_COUNT" => "20",
                    "PAGER_BASE_LINK_ENABLE" => "N",
                    "PAGER_DESC_NUMBERING" => "N",
                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                    "PAGER_SHOW_ALL" => "N",
                    "PAGER_SHOW_ALWAYS" => "N",
                    "PAGER_TEMPLATE" => ".default",
                    "PAGER_TITLE" => "Новости",
                    "PREVIEW_TRUNCATE_LEN" => "",
                    "SEF_FOLDER" => "/faq/",
                    "SEF_MODE" => "Y",
                    "SEF_URL_TEMPLATES" => Array(
                        "detail" => "#ELEMENT_CODE#/",
                        "news" => "",
                        "section" => ""
                    ),
                    "SET_LAST_MODIFIED" => "N",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y",
                    "SHOW_404" => "N",
                    "SORT_BY1" => "SORT",
                    "SORT_BY2" => "ID",
                    "SORT_ORDER1" => "ASC",
                    "SORT_ORDER2" => "DESC",
                    "STRICT_SECTION_CHECK" => "N",
                    "USE_CATEGORIES" => "N",
                    "USE_FILTER" => "N",
                    "USE_PERMISSIONS" => "N",
                    "USE_RATING" => "N",
                    "USE_REVIEW" => "N",
                    "USE_RSS" => "N",
                    "USE_SEARCH" => "N",
                    "USE_SHARE" => "N"
                )
            );?>
        </section>
    </div>
<? endif ?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>