<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");
?>

<section class="section section--top0" itemscope itemtype="http://schema.org/Organization">
	<div class="container">
		<h1 class="section__title">Контакты</h1>
		<div class="news_article_container">
            <meta itemprop="name" content="ПРОТЭК Концерн">
			<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                Адрес:
                <span itemprop="postalCode">125212</span>,
                <span itemprop="addressLocality">г. Москва</span>,
                <span itemprop="streetAddress">ул. Адмирала Макарова, д. 2 стр.4&nbsp;</span>
			</p>
			<p>
				Тел:
                <span class="ya-phone" itemprop="telephone">+7 (495) 775-30-47</span>
                Факс:
                <span itemprop="faxNumber">+7 (495) 452-34-66&nbsp;</span>
			</p>
			<p>
				&nbsp;Эл. почта:
                <a href="mailto:zayavka@protekgroup.com">
                    <span itemprop="email">zayavka@protekgroup.com</span>
                </a>&nbsp;
			</p>
			<p>
				&nbsp;Время работы: Пн.-Пт.:c 9:00 до 18:00 Сб.,Вс.: выходной
			</p>
			<?$APPLICATION->IncludeComponent(
				"bitrix:map.yandex.view",
				"",
				Array(
					"API_KEY" => "",
					"CONTROLS" => array("ZOOM"),
					"INIT_MAP_TYPE" => "MAP",
					"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.82779308284162;s:10:\"yandex_lon\";d:37.498656513239;s:12:\"yandex_scale\";i:16;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.498656513239;s:3:\"LAT\";d:55.82779308286;s:4:\"TEXT\";s:27:\"Концерн Протэк\";}}}",
					"MAP_HEIGHT" => "400",
					"MAP_ID" => "",
					"MAP_WIDTH" => "500",
					"OPTIONS" => array("ENABLE_SCROLL_ZOOM","ENABLE_DBLCLICK_ZOOM","ENABLE_DRAGGING")
				)
				);?><br>
			</div>

		</div>
</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>