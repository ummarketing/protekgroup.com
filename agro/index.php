<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Производство агрокассет для рассады, цена от производителя, изготовление на заказ");
?>
<img class="img-fluid lp_3--banner" src="/bitrix/templates/lp_3/images/banner.jpg">
<section class="section section--top0 section--bottom0" id="advantages">
	<div class="container">
		<h4 class="section__title">Преимущества</h4>
	</div>
</section>
<section>
	<div class="container">
		<h4 class="lp_3--title-green">Агрокассета 10 ячеек</h4>
	</div>
</section>
<img class="img-fluid lp_3--banner" src="/bitrix/templates/lp_3/images/page1.jpg">
<section class="container section section--top0">
	<div class="row">
		<div class="d-none d-md-block col-lg-5 col-md-5 col-sm-12">
			<img class="img-fluid" src="/bitrix/templates/lp_3/images/page1textbg.jpg" />
		</div>
		<div class="col-lg-6 col-md-7 col-sm-12 align-self-center">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/main_agrocasset-block.php"), false);?>
		</div>
	</div>
</section>
<section>
	<div class="container">
		<h4 class="lp_3--title-green">Агрокассета 40 ячеек</h4>
	</div>
</section>
<img class="img-fluid lp_3--banner" src="/bitrix/templates/lp_3/images/page2.jpg">
<section class="container section section--top0">
	<div class="row">
		<div class="d-none d-md-block col-lg-5 col-md-5 col-sm-12">
			<img class="img-fluid" src="/bitrix/templates/lp_3/images/page2textbg.jpg" />
		</div>
		<div class="col-lg-6 col-md-7 col-sm-12 align-self-center">
			<div class="lp_3--list"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/main_first-block.php"), false);?></div>
		</div>
	</div>
</section>
<section class="section section--top0 section--bottom0">
	<div class="container">
		<h4 class="section__title">Благодаря агрокассетам Протэк:</h4>
	</div>
</section>
<img class="img-fluid lp_3--banner" src="/bitrix/templates/lp_3/images/page3.jpg">
<section class="container section section--top0">
	<div class="page3text">
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/main_second-block.php"), false);?>
	</div>
</section>
<section class="section section--top0 section--bottom0" id="catalog">
	<div class="container">
		<h4 class="section__title">Каталог товара</h4>
<?$GLOBALS['arLP3Catalog'] = array(
	"!PROPERTY_LP3_SHOW"=>false
);
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"lp3_main", 
	array(
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "40",
		"ELEMENT_SORT_FIELD" => "name",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_FIELD2" => "PROPERTY_PRODUCER",
		"ELEMENT_SORT_ORDER2" => "asc",
		"PROPERTY_CODE" => "",
		"PROPERTY_CODE_MOBILE" => array(
		),
		"META_KEYWORDS" => "",
		"META_DESCRIPTION" => "",
		"BROWSER_TITLE" => "-",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"BASKET_URL" => "",
		"ACTION_VARIABLE" => "",
		"PRODUCT_ID_VARIABLE" => "",
		"SECTION_ID_VARIABLE" => "",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_PROPS_VARIABLE" => "",
		"FILTER_NAME" => "arLP3Catalog",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"SET_TITLE" => "N",
		"MESSAGE_404" => "",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"FILE_404" => "",
		"DISPLAY_COMPARE" => "N",
		"PAGE_ELEMENT_COUNT" => "99",
		"LINE_ELEMENT_COUNT" => "",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"USE_PRICE_COUNT" => "N",
		"SHOW_PRICE_COUNT" => "",
		"PRICE_VAT_INCLUDE" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRODUCT_PROPERTIES" => "",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"CONVERT_CURRENCY" => "N",
		"CURRENCY_ID" => "",
		"HIDE_NOT_AVAILABLE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
		"LABEL_PROP" => array(
		),
		"LABEL_PROP_MOBILE" => "",
		"LABEL_PROP_POSITION" => "",
		"ADD_PICT_PROP" => "-",
		"PRODUCT_DISPLAY_MODE" => "",
		"PRODUCT_BLOCKS_ORDER" => "",
		"PRODUCT_ROW_VARIANTS" => "[]",
		"ENLARGE_PRODUCT" => "STRICT",
		"MESS_BTN_ADD_TO_BASKET" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_CART"),
		"ADD_SECTIONS_CHAIN" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"COMPARE_PATH" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
		"COMPARE_NAME" => $arParams["COMPARE_NAME"],
		"USE_COMPARE_LIST" => "Y",
		"BACKGROUND_IMAGE" => "",
		"COMPATIBLE_MODE" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"BUTTON_DELETE_FROM_COMPARE" => $arParams["BUTTON_DELETE_FROM_COMPARE"],
		"BUTTON_ADD_TO_FAVORITES" => $arParams["BUTTON_ADD_TO_FAVORITES"],
		"BUTTON_DELETE_FROM_FAVORITES" => $arParams["BUTTON_DELETE_FROM_FAVORITES"],
		"MORE_CAPTION" => $arParams["MORE_CAPTION"],
		"CATEGORY_SORT_BY" => $arParams["CATEGORY_SORT_BY"],
		"CATEGORY_VIEW" => $arParams["CATEGORY_VIEW"],
		"CATEGORY_SORT_BY_NAME" => $arParams["CATEGORY_SORT_BY_NAME"],
		"CATEGORY_SORT_BY_FROM" => $arParams["CATEGORY_SORT_BY_FROM"],
		"CATEGORY_SORT_BY_SHOWS" => $arParams["CATEGORY_SORT_BY_SHOWS"],
		"SHOW_ALL_WO_SECTION" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"FAVORITES_REMOVE_ALL" => GetMessage("PROTEK_TEMPLATE_CATALOG_CLEAN_FAVORITES"),
		"COMPONENT_TEMPLATE" => "lp3_main",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"CUSTOM_FILTER" => "",
		"RCM_TYPE" => "personal",
		"RCM_PROD_ID" => "",
		"SHOW_FROM_SECTION" => "N",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SEF_RULE" => "",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_CODE_PATH" => "",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"RESULTS_TITLE" => "",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"TEMPLATE_THEME" => "blue",
		"SHOW_SLIDER" => "Y",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"DETAIL_URL" => "",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"LAZY_LOAD" => "N",
		"LOAD_ON_SCROLL" => "N",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N"
	),
	$component
);?>
	</div>
</section>
<section class="section section--top0 section--bottom0">
	<div class="container">
		<h4 class="section__title">Сертификаты</h4>
	</div>
</section>
<?
$GLOBALS['arLP3Cert'] = array(
	"!PROPERTY_LP3_SHOW"=>false
);

$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"protek_reviews", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "DETAIL_PICTURE",
			2 => "",
		),
		"FILTER_NAME" => "arLP3Cert",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "46",
		"IBLOCK_TYPE" => "photo",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "24",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "protek_reviews"
	),
	false
);?>
<section class="section section--top0 section--bottom0">
	<div class="container">
		<h4 class="section__title">Выставки</h4>
	</div>
</section>
<?
$GLOBALS['arLP3Shows'] = array(
	"SECTION_ID"=>"228",
	"!PROPERTY_LP3_SHOW"=>false
);
$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"protek_shows", 
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
		),
		"FILTER_NAME" => "arLP3Shows",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "44",
		"IBLOCK_TYPE" => "photo",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "24",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "MORE_PHOTO",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "SORT",
		"SORT_BY2" => "ACTIVE_FROM",
		"SORT_ORDER1" => "ASC",
		"SORT_ORDER2" => "DESC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "protek_reviews"
	),
	false
);?>
<section class="section section--grey" id="technologies">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__about">
					<h4 class="section__title">Технологии</h4>
					<p><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_TEMPLATE_PATH."/include/main_tech-block.php"), false);?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>