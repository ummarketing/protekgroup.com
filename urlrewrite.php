<?php
$arUrlRewrite = array(
    1 =>
        array(
            'CONDITION' => '#^/online/([\\.\\-0-9a-zA-Z]+)(/?)([^/]*)#',
            'RULE' => 'alias=$1',
            'ID' => NULL,
            'PATH' => '/desktop_app/router.php',
            'SORT' => 1,
        ),
    2 =>
        array(
            'CONDITION' => '#^/bitrix/services/ymarket/#',
            'RULE' => '',
            'ID' => '',
            'PATH' => '/bitrix/services/ymarket/index.php',
            'SORT' => 2,
        ),
    3 =>
        array(
            'CONDITION' => '#^/online/(/?)([^/]*)#',
            'RULE' => '',
            'ID' => NULL,
            'PATH' => '/desktop_app/router.php',
            'SORT' => 3,
        ),
    4 =>
        array(
            'CONDITION' => '#^/stssync/calendar/#',
            'RULE' => '',
            'ID' => 'bitrix:stssync.server',
            'PATH' => '/bitrix/services/stssync/calendar/index.php',
            'SORT' => 4,
        ),
    5 =>
        array(
            'CONDITION' => '#^/rest/#',
            'RULE' => '',
            'ID' => NULL,
            'PATH' => '/bitrix/services/rest/index.php',
            'SORT' => 5,
        ),
    6 =>
        array(
            'CONDITION' => '#^/catalog/search/filter/(.+?)/apply/\\??(.*)#',
            'RULE' => 'SMART_FILTER_PATH=$1&$2',
            'ID' => NULL,
            'PATH' => '/catalog/search/index.php',
            'SORT' => 60,
        ),
    7 =>
        array(
            'CONDITION' => '#^/catalog/search/filter/(.+?)/apply/\\??(.*)#',
            'RULE' => 'SMART_FILTER_PATH=$1&$2',
            'ID' => NULL,
            'PATH' => '/catalog/search/index.php',
            'SORT' => 65,
        ),
    8 =>
        array(
            'CONDITION' => '#^/de/catalog/search/filter/(.+?)/apply/\\??(.*)#',
            'RULE' => 'SMART_FILTER_PATH=$1&$2',
            'ID' => NULL,
            'PATH' => '/de/catalog/search/index.php',
            'SORT' => 70,
        ),
    9 =>
        array(
            'CONDITION' => '#^/cn/catalog/search/filter/(.+?)/apply/\\??(.*)#',
            'RULE' => 'SMART_FILTER_PATH=$1&$2',
            'ID' => NULL,
            'PATH' => '/cn/catalog/search/index.php',
            'SORT' => 80,
        ),
    10 =>
        array(
            'CONDITION' => '#^/es/catalog/search/filter/(.+?)/apply/\\??(.*)#',
            'RULE' => 'SMART_FILTER_PATH=$1&$2',
            'ID' => NULL,
            'PATH' => '/es/catalog/search/index.php',
            'SORT' => 90,
        ),
    87 =>
        array(
            'CONDITION' => '#^/en/representations/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/en/representations/index.php',
            'SORT' => 100,
        ),
    71 =>
        array(
            'CONDITION' => '#^/representations/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/representations/index.php',
            'SORT' => 100,
        ),
    77 =>
        array(
            'CONDITION' => '#^/de/articles/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/de/articles/index.php',
            'SORT' => 100,
        ),
    85 =>
        array(
            'CONDITION' => '#^/en/catalog/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/en/catalog/index.php',
            'SORT' => 100,
        ),
    75 =>
        array(
            'CONDITION' => '#^/de/catalog/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/de/catalog/index.php',
            'SORT' => 100,
        ),
    54 =>
        array(
            'CONDITION' => '#^/solutions/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/solutions/index.php',
            'SORT' => 100,
        ),
    88 =>
        array(
            'CONDITION' => '#^/catalog/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/catalog/index.php',
            'SORT' => 100,
        ),
    76 =>
        array(
            'CONDITION' => '#^/de/news/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/de/news/index.php',
            'SORT' => 100,
        ),
    11 =>
        array(
            'CONDITION' => '#^/articles/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/articles/index.php',
            'SORT' => 110,
        ),
    12 =>
        array(
            'CONDITION' => '#^/news/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/news/index.php',
            'SORT' => 120,
        ),
    21 =>
        array(
            'CONDITION' => '#^/en/articles/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/en/articles/index.php',
            'SORT' => 210,
        ),
    22 =>
        array(
            'CONDITION' => '#^/en/news/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/en/news/index.php',
            'SORT' => 220,
        ),
    30 =>
        array(
            'CONDITION' => '#^/de/representations/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/de/representations/index.php',
            'SORT' => 300,
        ),
    40 =>
        array(
            'CONDITION' => '#^/cn/representations/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/cn/representations/index.php',
            'SORT' => 400,
        ),
    41 =>
        array(
            'CONDITION' => '#^/cn/articles/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/cn/articles/index.php',
            'SORT' => 410,
        ),
    42 =>
        array(
            'CONDITION' => '#^/cn/news/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/cn/news/index.php',
            'SORT' => 420,
        ),
    50 =>
        array(
            'CONDITION' => '#^/es/representations/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/es/representations/index.php',
            'SORT' => 500,
        ),
    51 =>
        array(
            'CONDITION' => '#^/es/articles/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/es/articles/index.php',
            'SORT' => 510,
        ),
    52 =>
        array(
            'CONDITION' => '#^/es/news/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/es/news/index.php',
            'SORT' => 520,
        ),
    56 =>
        array(
            'CONDITION' => '#^/photo/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/photo/index.php',
            'SORT' => 560,
        ),
    57 =>
        array(
            'CONDITION' => '#^/en/photo/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/en/photo/index.php',
            'SORT' => 570,
        ),
    59 =>
        array(
            'CONDITION' => '#^/cn/photo/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/cn/photo/index.php',
            'SORT' => 590,
        ),
    60 =>
        array(
            'CONDITION' => '#^/es/photo/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/es/photo/index.php',
            'SORT' => 600,
        ),
    62 =>
        array(
            'CONDITION' => '#^/en/solutions/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/en/solutions/index.php',
            'SORT' => 620,
        ),
    64 =>
        array(
            'CONDITION' => '#^/de/solutions/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/de/solutions/index.php',
            'SORT' => 640,
        ),
    66 =>
        array(
            'CONDITION' => '#^/cn/solutions/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/cn/solutions/index.php',
            'SORT' => 660,
        ),
    67 =>
        array(
            'CONDITION' => '#^/cn/catalog/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/cn/catalog/index.php',
            'SORT' => 670,
        ),
    68 =>
        array(
            'CONDITION' => '#^/es/solutions/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/es/solutions/index.php',
            'SORT' => 680,
        ),
    69 =>
        array(
            'CONDITION' => '#^/es/catalog/#',
            'RULE' => '',
            'ID' => 'bitrix:catalog',
            'PATH' => '/es/catalog/index.php',
            'SORT' => 690,
        ),
    300 =>
        array(
            'CONDITION' => '#^/faq/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/faq/index.php',
            'SORT' => 100,
        ),
);
