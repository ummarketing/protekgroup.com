<?define("NO_KEEP_STATISTIC", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
?>
<?
global $APPLICATION;

if(isset($_POST['cookie_approve']) && $_POST['cookie_approve']){
	$APPLICATION->set_cookie("CYBER_COOKIE_APPROVE", true, time()+60*60*24*30*12*2);
	$arResult = array('STATUS' => "OK");
	echo json_encode($arResult, JSON_UNESCAPED_UNICODE);
	CMain::FinalActions();
}else{
	header( "HTTP/1.1 404 Not Found" );
	exit();
}

?>