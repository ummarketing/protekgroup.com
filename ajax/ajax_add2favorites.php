<?define("NO_KEEP_STATISTIC", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");

session_start();
$favoritesTemplate = FAVORITES_NAME;
$arSites = array();

$rsSites = CSite::GetList($by="sort", $order="desc", array());
while ($arSite = $rsSites->Fetch()){
	$arSites[$arSite["ID"]] = $arSite["NAME"];
}


if($_REQUEST['destination']){
	$destination = htmlspecialchars((string)$_REQUEST['destination']);
	if(strstr($destination, $favoritesTemplate) && array_key_exists(substr($destination, -2), $arSites)){
		if(htmlspecialchars((string)$_REQUEST['action']) == "add"){
			$_SESSION[FAVORITES_NAME . substr($destination, -2)][htmlspecialchars((string)$_REQUEST['id'])] = htmlspecialchars((string)$_REQUEST['id']);
		}elseif(htmlspecialchars((string)$_REQUEST['action']) == "annihilation"){
			unset($_SESSION[FAVORITES_NAME . substr($destination, -2)]);
		}else{
			unset($_SESSION[FAVORITES_NAME . substr($destination, -2)][htmlspecialchars((string)$_REQUEST['id'])]);
		}
		$arResult = array('STATUS' => "OK");
		echo json_encode($arResult, JSON_UNESCAPED_UNICODE);
	}
}else{
	header( "HTTP/1.1 404 Not Found" );
	exit();
}
?>