<?define("NO_KEEP_STATISTIC", true);
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
if($_REQUEST["cyber_ajax"] && ($_REQUEST["cyber_ajax"] == "y")){
	session_start();
	$SITE_ID = htmlspecialchars((string)$_REQUEST['SITE_ID']);
	$arResult = array();
	if(isset($_SESSION[COMPARE_NAME.$SITE_ID])){
		$arCompare = reset($_SESSION[COMPARE_NAME.$SITE_ID]);
		if(count($arCompare['ITEMS']) > 0){
			$arResult['compare'] = count($arCompare['ITEMS']);
		}
	}
	if(isset($_SESSION[FAVORITES_NAME.$SITE_ID])){
		if(count($_SESSION[FAVORITES_NAME.$SITE_ID]) > 0){
			$arResult['favorites'] = count($_SESSION[FAVORITES_NAME.$SITE_ID]);
		}
	}
	if(count($_SESSION['CYBER_BASKET_'.$SITE_ID]) > 0){
		$arResult['basket'] = count($_SESSION['CYBER_BASKET_'.$SITE_ID]);
	}
	
	echo json_encode($arResult, JSON_UNESCAPED_UNICODE);
	
	die();
	
}else{
	header( "HTTP/1.1 404 Not Found" );
	exit();
}
?>