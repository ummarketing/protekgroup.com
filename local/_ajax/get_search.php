<? include($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php'); ?>


<?
CModule::IncludeModule("iblock");
//------------ Формирование списка подходящих товаров --------------
$prod_name = $_POST['prod_name'];

if (strlen($prod_name) > 2){
    $arResult  = array();
    $arFilter  = array('IBLOCK_ID' => 40, 'ACTIVE' => 'Y', 'NAME' => '%'.$prod_name.'%');
    $dbResult  = CIBlockElement::GetList(array('SORT' => 'ASC'), $arFilter, false, array('nTopCount' => 15), array());

    while ($arElement = $dbResult->Fetch()){
        if (!empty($arElement['PREVIEW_PICTURE'])) {
            $img_id = $arElement['PREVIEW_PICTURE'];
            $img    = CFile::ResizeImageGet($img_id, array("width" => 100, "height" => 100), BX_RESIZE_IMAGE_PROPORTIONAL, true, false, false, 80 );
            $arElement['PICTURE'] = $img['src'];
        }
        $arElement['URL'] = '/catalog/'.getSectionById($arElement['IBLOCK_SECTION_ID'])['CODE'].'/'.$arElement['CODE'].'/';
        $arResult[]       = $arElement;
    }
}
//------------ /Формирование списка подходящих товаров --------------
?>








<div data-preloader class="active">
    <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
</div>
<div class="search-result__list hide" data-scroll>
    <? if (strlen($prod_name) > 2): ?>
        <? if ($arResult): ?>
            <? foreach ($arResult as $key => $arItem): ?>
                <a href="<?= $arItem['URL'] ?>" class="search-result__item">
                    <? if (!empty($arItem['PICTURE'])): ?>
                        <img src="<?= $arItem['PICTURE'] ?>" alt="<?= $arItem['NAME'] ?>" class="search-item__img">
                    <? endif ?>
                    <div class="search-item__name">
                        <?= $arItem['NAME']; ?>
                    </div>
                </a>
            <? endforeach; ?>
        <? else: ?>
            <div class="search-result__item">
                <div class="search-item__name">
                    К сожалению, по запросу "<?= $prod_name ?>" ничего не найдено
                </div>
            </div>
        <? endif ?>
    <? else: ?>
        <div class="search-result__item">
            <div class="search-item__name">
                Пожалуйста, введите не менее 3 символов
            </div>
        </div>
    <? endif ?>
</div>

<? if ($arResult && count($arResult) == 15): ?>
    <a href="/search/?q=<?= $prod_name ?>" class="search-result__more">
        Показать все результаты
    </a>
<? endif ?>
