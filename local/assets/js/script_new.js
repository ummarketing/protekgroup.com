$(document).ready(function () {
    $('.catitem__add').on('click', function () {
        changeBtnName($(this));
        track('add_korzina');
    });

    BX.addCustomEvent('onAjaxSuccess', function () {
        $('.catitem__add').on('click', function () {
            changeBtnName($(this));
        });
    });


    //--------- Валидация формы -------------
    $('[name="web_form_submit"]').on('click', function () {
        var form = $(this).closest('form[method="POST"]');
        var required = form.find('input[required], select[required], textarea[required]');
        var errors = [];


        //------- Проверка на наличие значений ----------
        required.each(function (i, elem) {
            if ($(elem).val() === '' || $(elem).val() === null || $(elem).val() == 'Другой город') {
                errors.push(elem);
                $(elem).addClass('error');
                $(elem).after('<label class="error__mess">Неверно заполнено поле</label>');
            }
        });
        setTimeout(function () {
            required.removeClass('error');
            $('label.error__mess').remove();
        }, 60 * 60 * 1);
        //------- /Проверка на наличие значений ----------


        if (errors.length > 0) {
            return false;
        }
    });
    $('.cityselection_select_container').on('change', function () {
        var value = $(this).val();

        if (value == 'Другой город') {
            $(this).after('<div class="city__mess"><b>Не нашли ваш город в списке?</b><br>Выберите ближайшее к вашему городу региональное представительство для ускорения обработки заказа.</div>');
        } else {
            setTimeout(function () {
                $('.city__mess').remove();
            }, 60 * 10);
        }
    });
    //--------- /Валидация формы -------------


    //--------- Настройка целей ---------
    $('a[href="#modal-call"]').on('click', function () {
        track('open_zvonok_header');
    });
    $('a[href="#modal-request"]').on('click', function () {
        track('open_zayavka_header');
    });
    $('a[href="#modal-question"]').on('click', function () {
        track('open_vopros_footer');
    });
    $('a[href="#modal-cart"]').on('click', function () {
        modalBasket();
        trackDuble('open_korzina', 'korzina');
        sendBasket(this);
    });

    $('a[href="#modal-price"]').on('click', function () {
        sendBasket(this);
    });
    //--------- /Настройка целей ---------


    //--------- Модальное окно -----------
    if (typeof $('[data-fancybox]') !== 'undefined') {
        $('[data-fancybox]').fancybox({
            beforeShow: function () {
                this.title = $(this.element).data("caption");
            },

            buttons: [
                // "zoom",
                //"share",
                // "slideShow",
                "fullScreen",
                //"download",
                "thumbs",
                "close",
            ],
            clickContent: false,
            loop: true,
            touch: true,
            transitionEffect: "fade",
            animationEffect: "fade",
        });
    }
    //--------- /Модальное окно -----------


    //--------- Custom Scrollbar -----------
    function initCustomScroll() {
        $("[data-scroll]").mCustomScrollbar({
            theme: 'dark'
        });
    }

    initCustomScroll();
    //--------- /Custom Scrollbar -----------


    //--------- Подсказка товаров в поиске -----------
    $('.header__content .header__search input').on('click', function () {
        $('.header__content').find('.search-block__result').fadeIn();
    });
    $('.nav__search input').on('click', function () {
        $('.nav--head').find('.search-block__result').fadeIn();
    });
    $(document).mouseup(function (e) {
        var container = $('.search-block__result');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.fadeOut();
        }
    });
    $('.header__search input, .nav__search input').on('keyup', function () {
        var prod_name = $(this).val();

        $.ajax({
            url: '/local/_ajax/get_search.php',
            type: 'POST',
            data: {prod_name: prod_name}
        }).done(function (data) {
            if (data) {
                $('.search-block__result').html(data);
                initCustomScroll();

                setTimeout(function () {
                    $('[data-preloader]').removeClass('active');
                    $('.search-result__list').fadeIn('slow');
                }, 450)
            }
        });
    });
    //--------- /Подсказка товаров в поиске -----------


    //--------- Расскрытие мобильного меню ------------
    $('[data-mobile="menu-btn"]').on('click', function () {
        $(this).toggleClass('change');
        $('[data-mobile="menu-list"]').slideToggle();
    });
    //--------- /Расскрытие мобильного меню ------------


    //-------- Расскрытие мобильного выбора языков ---------
    $('[data-mobile="lang-btn"]').on('click', function () {
        var search_btn = $('.header__mobmenu-search');
        var search_block = $('.header__mobsearch');
        var phone_btn = $('.header__mobmenu-phone');
        var phone_block = $('.header__mobcontent');


        //------- Скрытие блока с поиском (если активен) ---------
        if (search_btn.hasClass('active')) {
            search_btn.removeClass('active');
            search_block.removeClass('header__mobsearch--active');
        }
        //------- /Скрытие блока с поиском (если активен) ---------


        //------ Скрытие блока с телефоном (если активен) -----------
        if (phone_btn.hasClass('active')) {
            phone_btn.removeClass('active');
            phone_block.removeClass('header__mobcontent--active');
        }
        //------ /Скрытие блока с телефоном (если активен) -----------


        $('[data-mobile="lang-list"]').toggleClass('active');
    });
    //-------- /Расскрытие мобильного выбора языков ---------


    //-------- FAQ: Раскрытие блока с ответом -----------
    $('[data-faq="question"]').on('click', function () {
        $(this).toggleClass('active');
        $(this).parent().find('[data-faq="answer"]').slideToggle();
    });
    //-------- /FAQ: Раскрытие блока с ответом -----------


    //-------- Появление подменю каталога при наведении -----------
    $('[data-link="catalog"], .b-main-menu__submenu')
        .on('mouseenter', function () {
            $('.nav--head .b-main-menu__submenu').addClass('active');
        })
        .on('mouseleave', function () {
            $('.nav--head .b-main-menu__submenu').removeClass('active');
        });
    //-------- /Появление подменю каталога при наведении -----------


    //---------- Прокрутка контента к фильтру в каталоге ------------
    if (window.location.pathname.indexOf('/catalog/search/filter/') > -1 && window.location.pathname.indexOf('/clear/apply/') < 0) {
        setTimeout(function () {
            console.log('Search: scroll to result');
            if ($('.b-catalog-search__result').length > 0) {
                $('body, html').animate({scrollTop: $('.b-catalog-search__result').offset().top - 120}, 1100);
            }
        }, 60 * 15);
    }
    //---------- /Прокрутка контента к фильтру в каталоге ------------


    //---------- Выравнивание каталога -------------
    function initResizeHeight() {

        var max_height = Math.max.apply(null, $(".catitem__title").map(function () {
            return $(this).height();
        }).get());
        $('.catitem__title').css({'height': max_height});
    }

    initResizeHeight();
    BX.addCustomEvent('onAjaxSuccess', function () {
        initResizeHeight();
    });
    //---------- /Выравнивание каталога -------------


    // ------ Скрытие списка сортировки ------
    $(document).mouseup(function (e) {
        var container = $('.sorting__collapse, .category__sort_select');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.removeClass('show');
        }
    });
    // ------ /Скрытие списка сортировки ------


    // ------ Нажатие на кнопку примеить в фильтре ------
    $('#set_filter_custom').on('click', function () {
        $('#set_filter').trigger('click');
        return false;
    });
    $('#del_filter_custom').on('click', function () {
        $('#del_filter').trigger('click');
        return false;
    });
    // ------ /Нажатие на кнопку примеить в фильтре ------


    // -------- ABOUT: Раскрытие списка -----------
    if ($('.b-history__list').length) {
        setTimeout(function () {
            var list = $('.b-history__list');
            var height = (list.find('ul li:eq(1)').outerHeight());

            if ($(window).width() > 767) {
                height = (height * 2) + 60;
            } else {
                height = (height * 4) + 90;
            }
            list.css({'height': height + 'px'});
        }, 60 * 5);
    }


    $('.b-history__btn._showMore').on('click', function () {
        var show_text = $(this).attr('data-show');
        var list = $(this).parent().find('.b-history__list');
        var height = (list.find('ul li:eq(1)').outerHeight());

        if ($(window).width() > 767) {
            height = (height * 2) + 60;
        } else {
            height = (height * 4) + 90;
        }

        if (!$(this).hasClass('_active')) {
            height = list.find('ul').outerHeight();
        } else {
            $('body, html').animate({scrollTop: $('.b-history__title').offset().top - 160}, 1100);
        }

        list.animate({height: height + 'px'}, 60 * 15);
        $(this).attr('data-show', $(this).text()).text(show_text);
        $(this).toggleClass('_active');
    });
    // -------- /ABOUT: Раскрытие списка -----------


    // -------- ABOUT: Выравнивание блока преимуществ -------------
    if ($('.b-advantages__item').length) {
        setTimeout(function () {
            var max_height = Math.max.apply(null, $(".b-advantages__item").map(function () {
                return $(this).outerHeight();
            }).get());
            $('.b-advantages__item').css({'height': max_height});
        }, 60 * 5);
    }
    // -------- /ABOUT: Выравнивание блока преимуществ -------------
});


function changeBtnName(btn) {
    var add_item = btn.find('[data-cart="add_to_cart"]');
    var in_item = btn.find('[data-cart="in_cart"]');

    if (add_item.hasClass('active')) {
        add_item.removeClass('active');
        in_item.addClass('active');
    } else {
        btn.removeClass('add2basket_btn');
        document.location.href = '/cart/';
    }
}


//------------- Функция по отрпавке целей ----------
function track(event) {
    if (event) {
        setTimeout(function () {
            if ('undefined' !== typeof Ya && 'undefined' !== typeof Ya._metrika) {
                var i = 0;
                $.each(Ya._metrika.counters, function (key, counter) {
                    if (i == 0) {
                        counter.reachGoal(event, {URL: document.location.href});
                        console.log('YM tracking: ' + event);
                    }
                    i++;
                });
            }
            if ('undefined' !== typeof ga) {
                ga(ga.getAll()[0].get('name') + '.send', 'pageview', '/virtual/' + event);
                console.log('GA tracking: ' + event);
            }
        }, 550);
    }
}

function sendBasket() {
    $('form').on('submit', function (){
        trackDuble('send_korzina', 'korzina');
    });
}

function modalBasket(){
    $('#modal-cart').parent().addClass('modal-basket');
}

function trackDuble(eventY, eventG) {
    if (eventY || eventG) {
        setTimeout(function () {
            if ('undefined' !== typeof Ya && 'undefined' !== typeof Ya._metrika && eventY) {
                var i = 0;
                $.each(Ya._metrika.counters, function (key, counter) {
                    if (i == 0) {
                        counter.reachGoal(eventY, {URL: document.location.href});
                        console.log('YM tracking: ' + eventY);
                    }
                    i++;
                });
            }
            if ('undefined' !== typeof ga && eventG) {
                ga(ga.getAll()[0].get('name') + '.send', 'pageview', '/virtual/' + eventG);
                console.log('GA tracking: ' + eventG);
            }
        }, 550);
    }
}

//------------- /Функция по отрпавке целей ----------

$('a[href^="http://protekgroup.shop/"]').attr("target", "_blank");