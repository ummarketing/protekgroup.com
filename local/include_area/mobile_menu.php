<?
$home_page = str_replace('//', '/', '/'.explode('/', $APPLICATION->GetCurPage())[1]);
$lang_id   = str_replace('/', '', $home_page);

$home_page = strlen($lang_id) > 2 ? '/' : $home_page;
$lang_id   = empty($lang_id) || strlen($lang_id) > 2 ? 'ru' : $lang_id;
?>




<div class="mobile__header-block visible-xs hidden">
    <div class="container">
        <?//---------- MOBILE: Список кнопок ----------?>
        <div class="mobile-header__nav-list">
            <div class="mobile-header__nav-item mobile__menu-btn" data-mobile="menu-btn">
                <div class="bar1"></div>
                <div class="bar2"></div>
                <div class="bar3"></div>
            </div>
            <div class="mobile-header__nav-item mobile__comp-icon">
                <a href="<?= $home_page; ?>">
                    <? if ($lang_id == 'ru'): ?>
                        <svg enable-background="new 0 0 443.2 155.5" version="1.1" viewBox="0 0 443.2 155.5" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
                                        <style type="text/css">
                                            .cyber_super_class_for_svg0 {
                                                fill: #45A2D0
                                            }

                                            .cyber_super_class_for_svg1 {
                                                fill: #595651
                                            }
                                        </style>
                                        <path class="cyber_super_class_for_svg0" d="m228.1 45.2h-15.2v13.8h15.2c3 0 5.3-0.6 7.2-1.7 2-1.2 3-2.9 3-5 0-4.7-3.4-7.1-10.2-7.1m10.2 16.5c-3 1.7-6.8 2.5-11.3 2.5h-14.1v16.8h-5.9v-41h20.7c11 0 16.6 4.1 16.6 12.3-0.1 4.1-2.1 7.3-6 9.4"></path>
                                        <path class="cyber_super_class_for_svg0" d="m287.3 49.3c-3.1-2.6-7.4-3.9-13-3.9s-9.9 1.3-13 3.9c-3 2.6-4.5 6.4-4.5 11.2s1.5 8.6 4.6 11.4 7.4 4.2 12.9 4.2c5.6 0 9.9-1.4 13-4.1 3.1-2.8 4.6-6.6 4.6-11.5 0-4.8-1.5-8.6-4.6-11.2m4.2 26.5c-4.2 3.8-9.9 5.6-17.1 5.6s-12.9-1.9-17.1-5.6-6.3-8.8-6.3-15.3 2.1-11.5 6.4-15.1c4.1-3.5 9.8-5.3 17-5.3s12.9 1.8 17 5.3c4.3 3.6 6.4 8.7 6.4 15.1s-2.1 11.5-6.3 15.3"></path>
                                        <polygon class="cyber_super_class_for_svg0" points="323.3 45.2 323.3 81 317.4 81 317.4 45.2 301.5 45.2 301.5 40 339.2 40 339.2 45.2"></polygon>
                                        <polygon class="cyber_super_class_for_svg0" points="433.4 40 404.7 59.8 404.7 40 398.8 40 398.8 81 404.7 81 404.7 66.3 412 61.1 434.8 81 443.2 81 416.8 57.9 443.1 40"></polygon>
                                        <polygon class="cyber_super_class_for_svg0" points="198.6 42.4 198.6 40 160.9 40 160.9 43.4 160.9 45.2 160.9 81 166.8 81 166.8 45.2 176.8 45.2 182.7 45.2 192.7 45.2 192.7 81 198.6 81"></polygon>
                                        <polygon class="cyber_super_class_for_svg1" points="188.7 98.4 182.6 98.4 164.6 110.8 164.6 98.4 160.9 98.4 160.9 124.2 164.6 124.2 164.6 114.9 169.2 111.7 183.5 124.2 188.8 124.2 172.2 109.7"></polygon>
                                        <path class="cyber_super_class_for_svg1" d="m226.1 118.5c-1.9 1.7-4.6 2.6-8.2 2.6-3.5 0-6.2-0.9-8.1-2.6s-2.9-4.1-2.9-7.2c0-3 1-5.4 2.9-7s4.6-2.5 8.1-2.5 6.2 0.8 8.2 2.4c1.9 1.6 2.9 4 2.9 7s-1 5.6-2.9 7.3m2.6-16.7c-2.6-2.2-6.2-3.3-10.7-3.3s-8.1 1.1-10.7 3.3c-2.7 2.3-4 5.4-4 9.5 0 4 1.3 7.2 3.9 9.6 2.6 2.3 6.2 3.5 10.8 3.5 4.5 0 8.1-1.2 10.8-3.5 2.7-2.4 4-5.5 4-9.6-0.1-4-1.4-7.2-4.1-9.5"></path>
                                        <polygon class="cyber_super_class_for_svg1" points="271.7 109.1 254.4 109.1 254.4 98.4 250.7 98.4 250.7 124.2 254.4 124.2 254.4 112.4 271.7 112.4 271.7 124.2 275.4 124.2 275.4 98.4 271.7 98.4"></polygon>
                                        <polygon class="cyber_super_class_for_svg1" points="341.7 112.4 357.5 112.4 357.5 109.1 341.7 109.1 341.7 101.7 359.6 101.7 359.6 98.4 338 98.4 338 124.2 359.8 124.2 359.8 120.9 341.7 120.9"></polygon>
                                        <path class="cyber_super_class_for_svg1" d="m394.9 109.3c-1.2 0.7-2.7 1.1-4.5 1.1h-9.6v-8.6h9.5c4.3 0 6.4 1.5 6.4 4.4 0.1 1.3-0.5 2.3-1.8 3.1m-4.8-10.9h-13v25.7h3.7v-10.5h8.8c2.8 0 5.2-0.5 7.1-1.6 2.5-1.3 3.7-3.3 3.7-6 0.1-5-3.3-7.6-10.3-7.6"></path>
                                        <polygon class="cyber_super_class_for_svg1" points="439.5 98.4 439.5 109.1 422.2 109.1 422.2 98.4 418.5 98.4 418.5 124.2 422.2 124.2 422.2 112.4 439.5 112.4 439.5 124.2 443.2 124.2 443.2 98.4"></polygon>
                                        <polygon class="cyber_super_class_for_svg1" points="319 120.8 319 98.4 315.3 98.4 315.3 120.8 298.1 120.8 298.1 98.4 294.4 98.4 294.4 124.2 298.1 124.2 308.3 124.2 308.3 124.2 320.5 124.2 320.5 126.5 324.2 126.5 324.2 120.8 320.5 120.8"></polygon>
                                        <path class="cyber_super_class_for_svg0" d="m359.3 81c-7.2 0-13.6-0.8-13.6-10.7v-3.3h5.5v2.8c0 4.9 1.4 5.8 8.2 5.8h18.7c6.2 0 6.7-3 6.7-12.2v-1.1h-30.6v-5h30.6v-4.7c0-6.5-2.5-7.5-11.2-7.5h-16c-5.1 0-6 1.7-6 6.8v1.4h-5.5v-1.5c0-8.6 3-12 10.9-12h17.2c8.6 0 16.3 1 16.3 13v15.2c0 8.2-4.3 13-11.6 13h-19.6z"></path>
                                        <path class="cyber_super_class_for_svg0" d="m97.7 79s-19.3-36.4-43.4-22.6c-24.2 13.8-46.2 28.6-21.5 53 0 0-12.3-7.6-21-12.3-8.7-4.6-25.3-13.7 8.4-34.9 33.7-21.3 33.6-22.3 44.8-22.3 11.1 0.1 30 19.5 32.7 39.1"></path>
                                        <path class="cyber_super_class_for_svg1" d="m74.6 106.6s37.7-17.5 24.7-41.9-27.1-46.6-52.8-23.3c0 0 8.2-11.8 13.3-20.2 5-8.4 14.9-24.4 35 9.7s21.2 34 20.7 45c-0.5 11.1-21 28.9-40.9 30.7"></path>
                                        <path class="cyber_super_class_for_svg1" d="m72.6 58.9s-37.9 17.1-25.2 41.6 26.5 47 52.5 24c0 0-8.4 11.7-13.5 20-5.2 8.3-15.2 24.2-34.9-10.1-19.8-34.4-20.9-34.4-20.2-45.4 0.7-10.9 21.4-28.5 41.3-30.1"></path>
                                        <path class="cyber_super_class_for_svg0" d="m48.6 83.1s17 37.5 42 25.2 47.8-25.8 24.7-51.6c0 0 11.8 8.3 20.2 13.5 8.4 5.1 24.4 15.1-10.5 34.4-35 19.1-35 20.1-46.2 19.4-11.1-0.8-28.7-21.3-30.2-40.9"></path>
                                        <path class="cyber_super_class_for_svg0" d="m144.8 12.9c-0.7 3.6-2 7-3.8 10.3-2.1 3.8-4.8 7.2-7.9 10.3-3.2 3.2-6.7 6.1-10.4 8.7l-3.9 2.7c-0.1 0.1-0.2 0.1-0.3 0.2h-0.1c0.1-0.7 0.1-1.4 0.3-2 0.5-2.5 1.6-4.6 3.2-6.6 1.5-1.9 3.3-3.6 5.1-5.3 2-1.8 4.1-3.6 6-5.5 2.7-2.7 5.1-5.6 7.4-8.6 1.5-2 3-4.1 4.5-6.1 0-0.1 0.1-0.1 0.1-0.2h0.1c-0.1 0.8-0.2 1.4-0.3 2.1m4.5-9.8l-0.3-2.7c0-0.1-0.1-0.3-0.2-0.4-0.1 0.1-0.3 0.2-0.4 0.3-0.2 0.2-0.4 0.6-0.6 0.8-1.1 1-2.2 2-3.3 2.9-2.9 2.3-6.1 4.1-9.4 5.9-2.9 1.6-5.8 3.1-8.7 4.7-2.5 1.4-4.9 3-7.1 5-4.6 4.4-7.2 9.6-7.4 16-0.1 2.2 0 4.3 0.7 6.4 0.4 1.3 0.6 2.6 0.9 3.9 0.3 1.7 0.1 3.3-0.6 4.8-0.2 0.3-0.3 0.7-0.5 1l2.4 3.5c0.3-2 1.1-3.7 2.6-5.1 0.2-0.2 0.4-0.3 0.6-0.3h1.2c4.3-0.2 8.5-1 12.4-2.8 2.9-1.3 5.5-3.1 7.7-5.3 3.3-3.3 5.5-7.2 7.1-11.5 1.4-3.9 2.2-7.8 2.7-11.9 0.6-5 0.6-10.1 0.2-15.2" fill-opacity="0"></path>
                        </svg>
                    <? else: ?>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 443.2 155.5" style="enable-background:new 0 0 443.2 155.5;" xml:space="preserve">
                                        <style type="text/css">
                                            .cyber_super_class_for_svg0 {
                                                fill: #45A2D0
                                            }

                                            .cyber_super_class_for_svg1 {
                                                fill: #595651
                                            }
                                        </style>
                                        <path class="cyber_super_class_for_svg0" d="M197.8,59.7c-3.3,1.8-6.9,2.5-11.7,2.5h-14.6v17.8h-6.2V36.4h21.5c11.7,0,17.1,4.4,17.1,13.1
                                        C204,53.9,201.8,57.2,197.8,59.7 M186.8,42.2h-15.7v14.6h16c2.9,0,5.5-0.7,7.6-1.8c2.2-1.5,3.3-2.9,3.3-5.1
                                        C197.8,44.4,194.1,42.2,186.8,42.2"></path>
                                        <path class="cyber_super_class_for_svg0" d="M244.4,58.6c3.3,2.2,5.1,5.1,5.1,9.5v11.7h-6.2V68.1c0-4.4-3.6-6.6-10.6-6.6h-15.7v18.2h-6.2V36.4h22.2
                                        c11.3,0,16.8,4,16.8,12C250.2,53.2,248.4,56.4,244.4,58.6 M233.5,42.2h-16.4v14.2h15.3c3.3,0,6.2-0.7,8-1.8c2.2-1.5,3.3-3.3,3.3-5.8
                                        C244,44.4,240.4,42.2,233.5,42.2"></path>
                                        <path class="cyber_super_class_for_svg0" d="M299.4,74.3c-4.4,4-10.2,5.8-17.8,5.8s-13.5-1.8-17.8-5.8c-4.4-4-6.6-9.5-6.6-16c0-6.9,2.2-12,6.6-16
                                        c4.4-3.6,10.2-5.5,17.8-5.5s13.5,1.8,17.8,5.5c4.4,4,6.6,9.1,6.6,16C305.9,65.2,303.7,70.3,299.4,74.3 M295,46.2
                                        c-3.3-2.9-7.6-4-13.5-4s-10.2,1.5-13.5,4c-3.3,2.9-4.7,6.6-4.7,11.7s1.5,9.1,4.7,12c3.3,2.9,7.6,4.4,13.5,4.4s10.2-1.5,13.5-4.4
                                        s4.7-6.9,4.7-12C299.7,53.2,298.3,49.2,295,46.2"></path>
                                        <polygon class="cyber_super_class_for_svg0" points="332.5,42.2 332.5,79.7 326.3,79.7 326.3,42.2 309.9,42.2 309.9,36.4 349.3,36.4 349.3,42.2 "></polygon>
                                        <polygon class="cyber_super_class_for_svg0" points="354.7,79.7 354.7,36.4 390.4,36.4 390.4,42.2 360.9,42.2 360.9,54.6 387.1,54.6 387.1,60.1 360.9,60.1
                                        360.9,74.3 390.8,74.3 390.8,79.7 "></polygon>
                                        <polygon class="cyber_super_class_for_svg0" points="434.5,79.7 410.8,59 403.1,64.5 403.1,79.7 397,79.7 397,36.4 403.1,36.4 403.1,57.5 433,36.4
                                        443.2,36.4 415.9,55.3 443.2,79.7 "></polygon>
                                        <path class="cyber_super_class_for_svg1" d="M188.3,123.8h-12.7c-7.3,0-10.6-1.1-10.6-8.7v-7.6c0-6.6,3.3-8.7,8-8.7h17.1c4,0,7.3,1.5,7.3,5.5v1.8h-3.3v-1.5
                                        c0-2.9-1.1-3.6-5.8-3.6h-13.8c-5.8,0-6.9,1.1-6.9,6.6v8c0,4.7,1.8,5.8,6.9,5.8h12.7c5.5,0,6.6-0.7,6.6-4.4v-2.5h-13.5v-2.5H197v4.7
                                        C197.4,122.7,194.5,123.8,188.3,123.8"></path>
                                        <path class="cyber_super_class_for_svg1" d="M257.9,123.8c-4.4,0-4.4-2.5-4.4-5.8v-1.1c0-3.3-0.4-4.4-4-4.4h-19.3v10.9h-3.3V98.7h23.7
                                        c5.5,0,6.6,3.3,6.6,6.6c0,3.6-1.5,5.8-4,6.2l0,0c2.5,0,3.6,1.5,3.6,4.7v3.3c0,1.1,0,1.8,1.8,1.8h0.7v2.5
                                        C259.3,123.8,257.9,123.8,257.9,123.8z M249.1,101.2h-18.9v9.1H248c4.4,0,5.8-0.4,5.8-4.4C254.2,102.7,253.1,101.2,249.1,101.2"></path>
                                        <path class="cyber_super_class_for_svg1" d="M312.5,123.8h-14.6c-6.6,0-9.1-2.2-9.1-8v-8.7c0-6.2,2.2-8.4,9.1-8.4h14.6c6.9,0,9.1,2.2,9.1,8.4v8.7
                                        C321.2,121.6,319,123.8,312.5,123.8 M318.3,107.1c0-4.7-1.1-5.8-5.8-5.8h-14.6c-4.7,0-5.8,1.1-5.8,5.8v8.7c0,5.1,1.8,5.5,5.8,5.5
                                        h14.6c4,0,5.8-0.7,5.8-5.5V107.1z"></path>
                                        <path class="cyber_super_class_for_svg1" d="M371.8,123.8H362c-6.2,0-10.9,0-10.9-7.3V98.7h2.9v17.8c0,4,1.1,4.7,7.6,4.7h9.8c5.5,0,7.6,0,7.6-4.7V98.7h2.9
                                        v17.8C382.8,123.4,378.4,123.8,371.8,123.8"></path>
                                        <path class="cyber_super_class_for_svg1" d="M436.6,114h-18.9v9.8h-3.3V99h22.2c5.5,0,6.6,3.3,6.6,7.6C443.2,108.5,443.2,114,436.6,114 M435.6,101.6h-17.8
                                        v10.2h16.4c4,0,6.2,0,6.2-4.7C440.3,102,439.2,101.6,435.6,101.6"></path>
                                        <path class="cyber_super_class_for_svg0" d="M98.4,78.7c0,0-19.3-36.4-43.3-22.6S9.1,84.5,33.5,108.9c0,0-12.4-7.6-20.8-12.4C4,91.8-12.3,83,21.2,61.5
                                        C54.7,40.4,54.7,39.3,66,39.3C76.9,39.7,95.8,59,98.4,78.7"></path>
                                        <path class="cyber_super_class_for_svg1" d="M75.4,106.3c0,0,37.5-17.5,24.8-41.9C87.1,40.1,73.2,17.8,47.4,41.1c0,0,8.4-11.7,13.1-20
                                        c5.1-8.4,14.9-24.4,35,9.8c20,34.2,21.1,33.9,20.8,44.8C115.5,86.7,95.1,104.5,75.4,106.3"></path>
                                        <path class="cyber_super_class_for_svg1" d="M73.2,58.6c0,0-37.9,17.1-25.1,41.5c12.7,24.4,26.6,47,52.4,24c0,0-8.4,11.7-13.5,20c-5.1,8.4-15.3,24-35-10.2
                                        s-20.8-34.2-20-45.2C32.8,77.6,53.6,60.1,73.2,58.6"></path>
                                        <path class="cyber_super_class_for_svg0" d="M49.6,82.7c0,0,17.1,37.5,41.9,25.1s47.7-25.9,24.8-51.7c0,0,11.7,8.4,20,13.5c8.4,5.1,24.4,14.9-10.6,34.2
                                        s-35,20.4-45.9,19.7C68.5,122.7,51,102.3,49.6,82.7"></path>
                                        <path class="cyber_super_class_for_svg0" d="M145.3,12.7c-0.7,3.6-1.8,6.9-3.6,10.2c-2.2,3.6-4.7,7.3-8,10.2c-3.3,3.3-6.6,6.2-10.6,8.7
                                        c-1.5,1.1-2.5,1.8-4,2.5h-0.4l0,0c0-0.7,0-1.5,0.4-2.2c0.4-2.5,1.5-4.7,3.3-6.6c1.5-1.8,3.3-3.6,5.1-5.1c2.2-1.8,4-3.6,5.8-5.5
                                        c2.5-2.5,5.1-5.5,7.3-8.4c1.5-1.8,2.9-4,4.4-6.2l0,0l0,0C145.7,11.3,145.7,12,145.3,12.7 M150.1,2.9c0-0.7,0-1.8-0.4-2.5V0
                                        c0,0-0.4,0-0.4,0.4c-0.4,0.4-0.4,0.7-0.7,0.7c-1.1,1.1-2.2,2.2-3.3,2.9c-2.9,2.2-6.2,4-9.5,5.8c-2.9,1.5-5.8,2.9-8.7,4.7
                                        c-2.5,1.5-5.1,2.9-6.9,5.1c-4.7,4.4-7.3,9.5-7.3,16c0,2.2,0,4.4,0.7,6.6c0.4,1.1,0.7,2.5,0.7,4c0.4,1.8,0,3.3-0.7,4.7
                                        c0,0.4-0.4,0.7-0.4,1.1l2.5,3.6c0.4-1.8,1.1-3.6,2.5-5.1c0.4,0,0.4-0.4,0.7-0.4c0.4,0,0.7,0,1.1,0c4.4-0.4,8.4-1.1,12.4-2.9
                                        c2.9-1.5,5.5-2.9,7.6-5.5c3.3-3.3,5.5-7.3,6.9-11.3c1.5-4,2.2-7.6,2.5-12C150.4,13.1,150.4,8,150.1,2.9" fill-opacity="0"></path>
                                    </svg>
                    <? endif ?>
                </a>
            </div>
            <div class="mobile-header__nav-item mobile__phone-btn mobile__svg">
                <button class="header__mobmenu-phone" type="button">
                    <img src="/local/assets/img/phone-icon.svg" alt="">
                </button>
            </div>
            <div class="mobile-header__nav-item mobile__search-btn mobile__svg">
                <button class="header__mobmenu-search" type="button">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 56.966 56.966" xml:space="preserve">
                        <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23 s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92 c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17 s-17-7.626-17-17S14.61,6,23.984,6z"/>
                    </svg>
                </button>
            </div>
            <div class="mobile-header__nav-item mobile__lang-btn" data-mobile="lang-btn">
                <span class="active">
                    <?= mb_strtoupper($lang_id); ?>
                </span>
            </div>
            <div class="mobile-header__nav-item mobile__cart-btn mobile__svg">
                <a href="#modal-cart" class="header__mobmenu-cart <?if ($APPLICATION->GetCurPage() !== '/cart/'){echo ' open-modal';}?>">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 260.293 260.293" xml:space="preserve"><path d="M258.727,57.459c-1.42-1.837-3.612-2.913-5.934-2.913H62.004l-8.333-32.055c-0.859-3.306-3.843-5.613-7.259-5.613H7.5 c-4.142,0-7.5,3.358-7.5,7.5c0,4.142,3.358,7.5,7.5,7.5h33.112l8.333,32.055c0,0.001,0,0.001,0.001,0.002l29.381,112.969 c0.859,3.305,3.843,5.612,7.258,5.612h137.822c3.415,0,6.399-2.307,7.258-5.612l29.385-112.971 C260.636,61.687,260.147,59.295,258.727,57.459z M117.877,167.517H91.385l-5.892-22.652h32.384V167.517z M117.877,129.864H81.592 l-5.895-22.667h42.18V129.864z M117.877,92.197H71.795l-5.891-22.651h51.973V92.197z M176.119,167.517h-43.242v-22.652h43.242 V167.517z M176.119,129.864h-43.242v-22.667h43.242V129.864z M176.119,92.197h-43.242V69.546h43.242V92.197z M217.609,167.517 h-26.49v-22.652h32.382L217.609,167.517z M227.403,129.864h-36.284v-22.667h42.18L227.403,129.864z M237.201,92.197h-46.081V69.546 h51.974L237.201,92.197z"></path>
                        <path d="M105.482,188.62c-15.106,0-27.396,12.29-27.396,27.395c0,15.108,12.29,27.4,27.396,27.4 c15.105,0,27.395-12.292,27.395-27.4C132.877,200.91,120.588,188.62,105.482,188.62z M105.482,228.415 c-6.835,0-12.396-5.563-12.396-12.4c0-6.835,5.561-12.395,12.396-12.395c6.834,0,12.395,5.561,12.395,12.395 C117.877,222.853,112.317,228.415,105.482,228.415z"></path>
                        <path d="M203.512,188.62c-15.104,0-27.392,12.29-27.392,27.395c0,15.108,12.288,27.4,27.392,27.4 c15.107,0,27.396-12.292,27.396-27.4C230.908,200.91,218.618,188.62,203.512,188.62z M203.512,228.415 c-6.833,0-12.392-5.563-12.392-12.4c0-6.835,5.559-12.395,12.392-12.395c6.836,0,12.396,5.561,12.396,12.395 C215.908,222.853,210.347,228.415,203.512,228.415z"></path></svg>
                    <span class="cart-btn-wrap-basket"><?= (count($_SESSION['CYBER_BASKET_' . SITE_ID]) > 0 ? count($_SESSION['CYBER_BASKET_' . SITE_ID]) : "") ?></span>
                </a>
            </div>
        </div>
        <?//---------- /MOBILE: Список кнопок ----------?>
    </div>



    <?//---------- MOBILE: Выпадающие контакты -----------?>
    <div class="header__mobcontent">
        <div class="container">
            <div class="header__contacts">
                <a href="tel:+<?= preg_replace("/[^0-9]/", '', $arCity['PHONE']); ?>"
                   class="header__contacts-phone cyber_location_string phone">
                    <svg version="1.1"
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px"
                         y="0px"
                         viewBox="0 0 348.077 348.077"
                         xml:space="preserve"><path
                                d="M340.273,275.083l-53.755-53.761c-10.707-10.664-28.438-10.34-39.518,0.744l-27.082,27.076 c-1.711-0.943-3.482-1.928-5.344-2.973c-17.102-9.476-40.509-22.464-65.14-47.113c-24.704-24.701-37.704-48.144-47.209-65.257 c-1.003-1.813-1.964-3.561-2.913-5.221l18.176-18.149l8.936-8.947c11.097-11.1,11.403-28.826,0.721-39.521L73.39,8.194 C62.708-2.486,44.969-2.162,33.872,8.938l-15.15,15.237l0.414,0.411c-5.08,6.482-9.325,13.958-12.484,22.02 C3.74,54.28,1.927,61.603,1.098,68.941C-6,127.785,20.89,181.564,93.866,254.541c100.875,100.868,182.167,93.248,185.674,92.876 c7.638-0.913,14.958-2.738,22.397-5.627c7.992-3.122,15.463-7.361,21.941-12.43l0.331,0.294l15.348-15.029 C350.631,303.527,350.95,285.795,340.273,275.083z"></path></svg>
                    <span class="ya-phone"><?= $arCity['PHONE']; ?></span>
                </a>
                <div class="header__contacts-city">
                    <svg version="1.1"
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px"
                         y="0px"
                         viewBox="0 0 80 80"
                         xml:space="preserve"><path
                                d="M40,0C26.191,0,15,11.194,15,25c0,23.87,25,55,25,55s25-31.13,25-55C65,11.194,53.807,0,40,0z M40,38.8c-7.457,0-13.5-6.044-13.5-13.5S32.543,11.8,40,11.8c7.455,0,13.5,6.044,13.5,13.5S47.455,38.8,40,38.8z"></path></svg>
                    <span><?= GetMessage("PROTEK_TEMPLATE_CITY_SELECTION_TITLE") ?></span>
                    <a href="#modal-location" class="open-modal cyber_location_name">
                        <?= $arCity['NAME'] ?>
                    </a>
                </div>
                <a href="mailto:<?= $arCity['EMAIL'] ?>"
                   class="header__contacts-mail cyber_location_string email">
                    <svg version="1.1"
                         xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px"
                         y="0px"
                         viewBox="0 0 14 14"
                         xml:space="preserve"><path
                                d="M7,9L5.268,7.484l-4.952,4.245C0.496,11.896,0.739,12,1.007,12h11.986 c0.267,0,0.509-0.104,0.688-0.271L8.732,7.484L7,9z"/>
                        <path d="M13.684,2.271C13.504,2.103,13.262,2,12.993,2H1.007C0.74,2,0.498,2.104,0.318,2.273L7,8 L13.684,2.271z"/>
                        <polygon
                                points="0,2.878 0,11.186 4.833,7.079"/>
                        <polygon
                                points="9.167,7.079 14,11.186 14,2.875"/>
                </svg>
                    <span>
                        <?= $arCity['EMAIL'] ?>
                    </span>
                </a>
            </div>

            <div class="header__btns">
                <a href="#modal-call"
                   class="header__btn open-modal"
                   onclick="yaCounter30699088.reachGoal('goal1'); return true;"><?= GetMessage("PROTEK_TEMPLATE_CALLBACK") ?></a>
                <a href="#modal-request"
                   class="header__btn open-modal"
                   onclick="yaCounter30699088.reachGoal('goal3'); return true;"><?= GetMessage("PROTEK_TEMPLATE_ORDER_BUTTON") ?></a>
            </div>

            <div class="header__social">
                <div class="header__social-list">
                    <span><?= GetMessage("PROTEK_TEMPLATE_SOCIAL_TITLE") ?></span>
                    <? $APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => "/include/socnet.php"), false); ?>
                </div>
            </div>
        </div>
    </div>
    <?//---------- /MOBILE: Выпадающие контакты -----------?>






    <?//----------- MOBILE: Выпадающий поиск ---------------?>
    <form action="/search/" class="header__mobsearch">
        <div class="container relative">
            <input type="text" name="q" value="" placeholder="<?= GetMessage("PROTEK_TEMPLATE_SEARCH"); ?>">
            <button type="submit">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 56.966 56.966" xml:space="preserve"><path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23 s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92 c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17 s-17-7.626-17-17S14.61,6,23.984,6z"></path>
                </svg>
            </button>
        </div>
    </form>
    <?//----------- /MOBILE: Выпадающий поиск ---------------?>




    <?//---------- MOBILE: Выпадающие языки ----------?>
    <div class="header__moblang header__mobfade" data-mobile="lang-list">
        <div class="container">
            <div class="header-moblang__lang-list">
                <a href="/" class="header-moblang__lang-item <?= ($lang_id == 'ru') ? 'active' : '' ?>" data-mobile="lang-link">
                    RU
                </a>
                <a href="/en/" class="header-moblang__lang-item <?= ($lang_id == 'en') ? 'active' : '' ?>">
                    EN
                </a>
                <a href="/de/" class="header-moblang__lang-item <?= ($lang_id == 'de') ? 'active' : '' ?>">
                    DE
                </a>
                <a href="/cn/" class="header-moblang__lang-item <?= ($lang_id == 'cn') ? 'active' : '' ?>">
                    CN
                </a>
                <a href="/es/" class="header-moblang__lang-item <?= ($lang_id == 'es') ? 'active' : '' ?>">
                    ES
                </a>
            </div>
        </div>
    </div>
    <?//---------- /MOBILE: Выпадающие языки ----------?>





    <?//---------- MOBILE: Сравнение / Избранное -----------?>
    <div class="header__mob_fav_comp">
        <? if (strpos($APPLICATION->GetCurPage(), '/catalog/compare.php') === false): ?>
            <a href="<?= SITE_DIR ?>catalog/compare.php" class="mobile-header__compare-item compare">
                <div class="cart-btn-wrap-compare cart-btn-wrap-item">
                    <img src="/local/assets/img/compare-icon.svg" alt="">
                    <span data-mobile="numbers">
                        <? if (isset($_SESSION[COMPARE_NAME . SITE_ID])) {
                            if (reset($_SESSION[COMPARE_NAME . SITE_ID])) {
                                $arCompare = reset($_SESSION[COMPARE_NAME . SITE_ID]);
                                if (count($arCompare['ITEMS']) > 0) { ?>
                                    <?= count($arCompare['ITEMS']); ?>
                                <? }
                            }
                        } ?>
                    </span>
                </div>
                <?= GetMessage('PROTEK_TEMPLATE_COMPARE'); ?>
            </a>
        <? endif ?>

        <? if (strpos($APPLICATION->GetCurPage(), '/catalog/favorites/') === false): ?>
            <a href="<?= SITE_DIR ?>catalog/favorites/" class="mobile-header__compare-item favorites">
                <div class="cart-btn-wrap-favorites cart-btn-wrap-item">
                    <img src="/local/assets/img/favorite-icon.svg" alt="">
                    <span data-mobile="numbers">
                        <? if (isset($_SESSION[FAVORITES_NAME . SITE_ID])) {
                            if (count($_SESSION[FAVORITES_NAME . SITE_ID]) > 0) { ?>
                                <?= count($_SESSION[FAVORITES_NAME . SITE_ID]); ?>
                            <? }
                        } ?>
                    </span>
                </div>
                <?= GetMessage('PROTEK_TEMPLATE_FAVORITES'); ?>
            </a>
        <? endif ?>
    </div>
    <?//---------- /MOBILE: Сравнение / Избранное -----------?>





    <?//---------- MOBILE: Выпадающее меню ---------?>
    <div class="collapse nav__mob" id="collapsed_mobile_menu" data-mobile="menu-list">
        <? $APPLICATION->IncludeComponent(
            "bitrix:menu",
            "protek_mobile_menu",
            Array(
                "ALLOW_MULTI_SELECT" => "N",
                "CHILD_MENU_TYPE" => "left",
                "DELAY" => "N",
                "MAX_LEVEL" => "2",
                "MENU_CACHE_GET_VARS" => array(0 => "",),
                "MENU_CACHE_TIME" => "36000",
                "MENU_CACHE_TYPE" => "A",
                "MENU_CACHE_USE_GROUPS" => "Y",
                "ROOT_MENU_TYPE" => "top",
                "USE_EXT" => "Y",
                "MENU_SHOW_LESS" => GetMessage("PROTEK_TEMPLATE_SHOW_LESS"),
            )
        ); ?>
        <div class="nav__submenu">
            <? $APPLICATION->IncludeComponent(
                "bitrix:menu",
                "protek_top_menu",
                Array(
                    "ALLOW_MULTI_SELECT" => "N",
                    "CHILD_MENU_TYPE" => "left",
                    "DELAY" => "N",
                    "MAX_LEVEL" => "1",
                    "MENU_CACHE_GET_VARS" => array(0 => "",),
                    "MENU_CACHE_TIME" => "36000",
                    "MENU_CACHE_TYPE" => "A",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "ROOT_MENU_TYPE" => "first",
                    "USE_EXT" => "N"
                )
            ); ?>
        </div>
    </div>
    <?//---------- /MOBILE: Выпадающее меню ---------?>
</div>