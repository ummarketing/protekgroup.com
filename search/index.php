<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результаты поиска");
?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title"><?$APPLICATION->ShowTitle()?></h1>
			</div>
			<div class="col-12">
                <?$APPLICATION->IncludeComponent(
                    "main:search.page",
                    "search",
                    array(
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "Y",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_TIME" => "3600",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "N",
                        "DEFAULT_SORT" => "rank",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FILTER_NAME" => "",
                        "NO_WORD_LOGIC" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => "",
                        "PAGER_TITLE" => "Результаты поиска",
                        "PAGE_RESULT_COUNT" => "50",
                        "RESTART" => "Y",
                        "SHOW_WHEN" => "N",
                        "SHOW_WHERE" => "N",
                        "USE_LANGUAGE_GUESS" => "N",
                        "USE_SUGGEST" => "N",
                        "USE_TITLE_RANK" => "Y",
                        "arrFILTER" => array(
                            0 => "iblock_catalog",
                        ),
                        "arrWHERE" => "",
                        "SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
                        "COMPONENT_TEMPLATE" => "search",
                        "arrFILTER_iblock_content" => array(
                            0 => "3",
                            1 => "4",
                            2 => "5",
                            3 => "8",
                        ),
                        "arrFILTER_iblock_catalog" => array(
                            0 => "40",
                        ),
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                    false
                );?>

                <?/*div class="search__item">
                    <div class="search__item-left">
                        <div class="search__item-img">
                            <img src="http://cf28076-bitrix.tw1.ru/upload/iblock/c2b/c2b827791a4390d56dcb35bf13aa787a.png" alt="">
                        </div>
                        <div class="search__date">24.09.2020</div>
                    </div>
                    <div class="search__item-right">
                        <div class="search__item-title">
                        Сдвоенный лоток ПР-ЛО2-144х95х43
                        </div>
                        <div class="search__item-text">
                        Лотки могут быть изготовлены из материалов: РР(полипропилен), РР/РЕ(соэкструзия полипропилена с полиэтиленом), а так же из АПЭТа (аморфный полиэтилентерефталат). Данная упаковка подойдет для сферы продовольственной торговли, быстрого питания, доставки еды, салатов, полуфабрикатов и многого другого. Лотки под запайку используют совместно с пищевой пленкой и трейсиллером. Возможно брендирование на дне лотка.
                        </div>
                        <div class="search__item-crumbs">
                        <small><a href="/">Пищевая упаковка</a>&nbsp;/&nbsp;<a href="/catalog/">Каталог продукции</a></small>
                        </div>
                    </div>
                </div*/?>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>