<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Упаковка которая станет украшением Ваших блюд | Протэк Концерн");
?><section class="lp4_main">
	<img class="img-fluid" src="/bitrix/templates/lp_4/img/sliderheading.png" alt="" />
</section>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"lp4_main",
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"FILTER_NAME" => "",
		"IBLOCK_ID" => "81",
		"IBLOCK_TYPE" => "catering",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "DESCRIPTION",
			3 => "PICTURE",
			4 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "2",
		"VIEW_MODE" => "LINE",
		"COMPONENT_TEMPLATE" => "lp4_main"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>