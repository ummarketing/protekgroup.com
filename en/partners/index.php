<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("С нами сотрудничают");
?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title"><?$APPLICATION->ShowTitle()?></h1>
				<p class="section__text">С нами работают международные и российские компании, специализирующиеся в области автоматизации торговли, склада и производства.</p>
			</div>
		</div>
	</div>
</section>
<?$APPLICATION->IncludeComponent("cyberrex:highloadblock.list", "main_partners_2", Array(
	"BLOCK_ID" => "2",	// ID highload блока
		"BLOCK_TITLE" => "Наши партнеры",	// Заголовок блока
		"CHECK_PERMISSIONS" => "N",	// Проверять права доступа
		"DETAIL_URL" => "",	// Путь к странице просмотра записи
		"FILTER_NAME" => "",	// Идентификатор фильтра
		"PAGEN_ID" => "",	// Идентификатор страницы
		"ROWS_PER_PAGE" => "",	// Разбить по страницам количеством
		"SORT_FIELD" => "UF_SORT",	// Поле сортировки
		"SORT_ORDER" => "ASC",	// Направление сортировки
	),
	false
);?> <?$APPLICATION->IncludeComponent(
	"cyberrex:highloadblock.list",
	"main_features",
	Array(
		"BLOCK_ID" => "3",
		"BLOCK_TITLE" => GetMessage("PROTEK_TEMPLATE_FEATURES_TITLE"),
		"CHECK_PERMISSIONS" => "N",
		"DETAIL_URL" => "",
		"FILTER_NAME" => "",
		"PAGEN_ID" => "",
		"ROWS_PER_PAGE" => "",
		"SORT_FIELD" => "UF_SORT",
		"SORT_ORDER" => "ASC"
	)
);?>
<section class="section section--grey">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__about">
					<h2>Производство и продажа упаковочных материалов и пищевой пластиковой упаковки</h2>
					<p>
						 Концерн "Протэк" - крупнейшая в России, федеральная, торгово-производственная сеть.
					</p>
					<p>
					<a href="/">Концерн Протэк</a> - крупнейшая в отрасли российская компания, являющаяся специалистом в области разработки, производства и продажи упаковочных материалов и пластиковой упаковки для пищевой промышленности, а также одноразовой посуды, столовых приборов, отделочных материалов и <u>упаковки для агробизнеса</u>.
					</p>
					<div class="section__about-collapse collapse" id="collapse-about">
						<p>
							 Со дня открытия в 1996 году мы являемся одной из ведущих фирм на рынке пищевой упаковки.
						</p>
						<p>
							 Мы неустанно и динамично развиваемся и расширяемся. На сегодняшний день в состав концерна Протэк входят 23 филиала и более 70 дилеров в России , странах СНГ и в Европе.
						</p>
						<p>
							 Концерн Протэк располагает 9 промышленными комплексами общей производственной площадью 24000 кв.м., перерабатывающими более 5000 тонн различного сырья ежемесячно. Парк основного оборудования состоит из 17 экструдеров, 53 формовочных и 9 литьевых машин.
						</p>
						<p>
							 Собственные производственные, сертифицированные, запатентованные технологии позволяют производить качественную продукцию, соответствующую мировым стандартам с минимальными затратами.
						</p>
						<p>
							 Наша дружная и слаженная команда насчитывает более 1500 человек.
						</p>
						<p>
							 Концерн Протэк уверенно занимает одно из лидирующих мест в списке мировых компаний упаковочной отрасли и производит более 5000 наименований продукции.
						</p>
					</div>
				<a class="section__about-btn collapse_shower" href="#collapse-about" role="button" data-caption="Скрыть подробнее">Раскрыть подробнее</a>
				</div>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>