<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Результаты поиска");
?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="section__title"><?$APPLICATION->ShowTitle()?></h1>
			</div>
			<div class="col-12">
<?$APPLICATION->IncludeComponent(
	"bitrix:search.page", 
	"search", 
	array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "Y",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"DEFAULT_SORT" => "rank",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILTER_NAME" => "",
		"NO_WORD_LOGIC" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_TITLE" => "Результаты поиска",
		"PAGE_RESULT_COUNT" => "50",
		"RESTART" => "Y",
		"SHOW_WHEN" => "N",
		"SHOW_WHERE" => "N",
		"USE_LANGUAGE_GUESS" => "N",
		"USE_SUGGEST" => "N",
		"USE_TITLE_RANK" => "Y",
		"arrFILTER" => array(
			0 => "iblock_content_en",
			1 => "iblock_catalog_en",
		),
		"arrWHERE" => "",
		"SEARCH_PLACEHOLDER" => GetMessage("PROTEK_TEMPLATE_SEARCH"),
		"COMPONENT_TEMPLATE" => "search",
		"arrFILTER_iblock_content" => array(
			0 => "3",
			1 => "4",
			2 => "5",
			3 => "8",
		),
		"arrFILTER_iblock_catalog" => array(
			0 => "40",
			1 => "41",
		),
		"arrFILTER_iblock_content_en" => array(
			0 => "11",
			1 => "12",
			2 => "13",
			3 => "15",
		),
		"arrFILTER_iblock_catalog_en" => array(
			0 => "64",
			1 => "68",
		)
	),
	false
);?>	
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>