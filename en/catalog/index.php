<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Каталог");
?><?$APPLICATION->IncludeComponent(
	"bitrix:catalog", 
	"protek_catalog", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_ELEMENT_CHAIN" => "Y",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "N",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BASKET_URL" => "/cart/",
		"BIG_DATA_RCM_TYPE" => "personal",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
		"COMMON_SHOW_CLOSE_POPUP" => "N",
		"COMPARE_ELEMENT_SORT_FIELD" => "active_from",
		"COMPARE_ELEMENT_SORT_ORDER" => "asc",
		"COMPARE_FIELD_CODE" => array(
			0 => "PREVIEW_PICTURE",
			1 => "",
		),
		"COMPARE_NAME" => COMPARE_NAME.SITE_ID,
		"COMPARE_POSITION" => "top left",
		"COMPARE_POSITION_FIXED" => "Y",
		"COMPARE_PROPERTY_CODE" => array(
			0 => "BRANDING",
			1 => "PACK_WEIGHT",
			2 => "OUTER_DIAMETER",
			3 => "OUTER_HEIGHT",
			4 => "OUTER_LENGTH",
			5 => "OUTER_WIDTH",
			6 => "INNER_DIAMETER",
			7 => "INNER_HEIGHT",
			8 => "INNER_LENGTH",
			9 => "FREEZING",
			10 => "SEALING",
			11 => "PERFORATION",
			12 => "WARMING",
			13 => "SECTIONS",
			14 => "MATERIAL",
			15 => "SPIKES",
			16 => "VOLUME",
			17 => "PACK_VOLUME",
			18 => "REGION",
			19 => "LOCKER",
			20 => "FORM",
			21 => "COLOR",
			22 => "HEIGHT",
			23 => "DIAMETER",
			24 => "LENGTH",
			25 => "PACK_CONTAINS",
			26 => "",
		),
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_ADD_DETAIL_TO_SLIDER" => "Y",
		"DETAIL_ADD_TO_BASKET_ACTION" => array(
			0 => "ADD",
		),
		"DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => array(
		),
		"DETAIL_BACKGROUND_IMAGE" => "-",
		"DETAIL_BLOG_USE" => "N",
		"DETAIL_BRAND_USE" => "N",
		"DETAIL_BROWSER_TITLE" => "-",
		"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
		"DETAIL_DETAIL_PICTURE_MODE" => array(
		),
		"DETAIL_DISPLAY_NAME" => "Y",
		"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"DETAIL_FB_USE" => "N",
		"DETAIL_IMAGE_RESOLUTION" => "16by9",
		"DETAIL_META_DESCRIPTION" => "-",
		"DETAIL_META_KEYWORDS" => "-",
		"DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
		"DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
		"DETAIL_SET_CANONICAL_URL" => "N",
		"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
		"DETAIL_SHOW_POPULAR" => "N",
		"DETAIL_SHOW_SLIDER" => "N",
		"DETAIL_SHOW_VIEWED" => "N",
		"DETAIL_STRICT_SECTION_CHECK" => "Y",
		"DETAIL_USE_COMMENTS" => "N",
		"DETAIL_USE_VOTE_RATING" => "N",
		"DETAIL_VK_USE" => "N",
		"DISABLE_INIT_JS_IN_COMPONENT" => "Y",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_ELEMENT_SELECT_BOX" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "active_from",
		"ELEMENT_SORT_FIELD2" => "PROPERTY_PRODUCER",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"FILE_404" => "/404_cat.php",
		"FILTER_FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_HIDE_ON_MOBILE" => "N",
		"FILTER_NAME" => "",
		"FILTER_PRICE_CODE" => array(
		),
		"FILTER_PROPERTY_CODE" => array(
			0 => "BRANDING",
			1 => "FREEZING",
			2 => "SEALING",
			3 => "WARMING",
			4 => "SECTIONS",
			5 => "MATERIAL",
			6 => "SPIKES",
			7 => "VOLUME",
			8 => "REGION",
			9 => "USING",
			10 => "LOCKER",
			11 => "FORM",
			12 => "COLOR",
			13 => "HEIGHT",
			14 => "DIAMETER",
			15 => "LENGTH",
			16 => "",
		),
		"FILTER_VIEW_MODE" => "VERTICAL",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SECTION_LIST_BLOCK_TITLE" => "Подарки к товарам этого раздела",
		"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE" => "L",
		"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
		"IBLOCK_ID" => "64",
		"IBLOCK_TYPE" => "catalog_en",
		"INCLUDE_SUBSECTIONS" => "Y",
		"INSTANT_RELOAD" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LINK_ELEMENTS_URL" => "",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"LIST_BROWSER_TITLE" => "-",
		"LIST_ENLARGE_PRODUCT" => "STRICT",
		"LIST_META_DESCRIPTION" => "-",
		"LIST_META_KEYWORDS" => "-",
		"LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"LIST_SHOW_SLIDER" => "N",
		"LIST_SLIDER_INTERVAL" => "3000",
		"LIST_SLIDER_PROGRESS" => "N",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_CART"),
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_COMPARE" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_COMPARE"),
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_COMMENTS_TAB" => "Комментарии",
		"MESS_DESCRIPTION_TAB" => "Описание",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_PRICE_RANGES_TITLE" => "Цены",
		"MESS_PROPERTIES_TAB" => "Характеристики",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "N",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"SEARCH_CHECK_DATES" => "Y",
		"SEARCH_NO_WORD_LOGIC" => "Y",
		"SEARCH_PAGE_RESULT_COUNT" => "50",
		"SEARCH_RESTART" => "N",
		"SEARCH_USE_LANGUAGE_GUESS" => "N",
		"SECTIONS_SHOW_PARENT_NAME" => "Y",
		"SECTIONS_VIEW_MODE" => "LIST",
		"SECTION_ADD_TO_BASKET_ACTION" => "ADD",
		"SECTION_BACKGROUND_IMAGE" => "-",
		"SECTION_COUNT_ELEMENTS" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_TOP_DEPTH" => "2",
		"SEF_FOLDER" => "/en/catalog/",
		"SEF_MODE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_STATUS_404" => "Y",
		"SET_TITLE" => "Y",
		"SHOW_404" => "Y",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_TOP_ELEMENTS" => "N",
		"SIDEBAR_DETAIL_SHOW" => "N",
		"SIDEBAR_PATH" => "",
		"SIDEBAR_SECTION_SHOW" => "N",
		"TEMPLATE_THEME" => "",
		"TOP_ADD_TO_BASKET_ACTION" => "ADD",
		"TOP_ELEMENT_COUNT" => "9",
		"TOP_ELEMENT_SORT_FIELD" => "sort",
		"TOP_ELEMENT_SORT_FIELD2" => "id",
		"TOP_ELEMENT_SORT_ORDER" => "asc",
		"TOP_ELEMENT_SORT_ORDER2" => "desc",
		"TOP_ENLARGE_PRODUCT" => "STRICT",
		"TOP_LINE_ELEMENT_COUNT" => "3",
		"TOP_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons",
		"TOP_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
		"TOP_SHOW_SLIDER" => "Y",
		"TOP_SLIDER_INTERVAL" => "3000",
		"TOP_SLIDER_PROGRESS" => "N",
		"TOP_VIEW_MODE" => "SECTION",
		"USER_CONSENT" => "N",
		"USER_CONSENT_ID" => "0",
		"USER_CONSENT_IS_CHECKED" => "N",
		"USER_CONSENT_IS_LOADED" => "N",
		"USE_BIG_DATA" => "N",
		"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
		"USE_COMPARE" => "Y",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_FILTER" => "Y",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_GIFTS_SECTION" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "Y",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "Y",
		"USE_REVIEW" => "N",
		"USE_SALE_BESTSELLERS" => "N",
		"USE_STORE" => "N",
		"COMPONENT_TEMPLATE" => "protek_catalog",
		"MESSAGES_PER_PAGE" => "10",
		"USE_CAPTCHA" => "Y",
		"REVIEW_AJAX_POST" => "Y",
		"PATH_TO_SMILE" => "/bitrix/images/forum/smile/",
		"FORUM_ID" => "",
		"URL_TEMPLATES_READ" => "",
		"SHOW_LINK_TO_FORUM" => "N",
		"FILTER_CAPTION" => GetMessage("PROTEK_TEMPLATE_CATALOG_FILTER_CAPTION"),
		"SHOW_CAPTION" => GetMessage("PROTEK_TEMPLATE_CATALOG_USE_FILTER_CAPTION"),
		"SUBMIT_FILTER" => GetMessage("PROTEK_TEMPLATE_CATALOG_USE_FILTER"),
		"FILTER_NUM_FROM" => GetMessage("PROTEK_TEMPLATE_CATALOG_FILTER_NUM_FROM"),
		"FILTER_NUM_TO" => GetMessage("PROTEK_TEMPLATE_CATALOG_FILTER_NUM_TO"),
		"DOWNLOAD_PRICELIST" => GetMessage("PROTEK_TEMPLATE_CATALOG_DOWNLOAD"),
		"DOWNLOAD_PRICELIST_FILE" => "/upload/catalogs/catalog.pdf",
		"MORE_CAPTION" => GetMessage("PROTEK_TEMPLATE_CATALOG_MORE"),
		"CATEGORY_SORT_BY" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY"),
		"CATEGORY_VIEW" => GetMessage("PROTEK_TEMPLATE_CATALOG_VIEW"),
		"CATEGORY_SORT_BY_NAME" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY_NAME"),
		"CATEGORY_SORT_BY_FROM" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY_FROM"),
		"CATEGORY_SORT_BY_SHOWS" => GetMessage("PROTEK_TEMPLATE_CATALOG_SORT_BY_SHOWS"),
		"BUTTON_DELETE_FROM_COMPARE" => GetMessage("PROTEK_TEMPLATE_CATALOG_REMOVE"),
		"BUTTON_ADD_TO_FAVORITES" => GetMessage("PROTEK_TEMPLATE_CATALOG_ADD_TO_FAVORITES"),
		"BUTTON_DELETE_FROM_FAVORITES" => GetMessage("PROTEK_TEMPLATE_CATALOG_REMOVE"),
		"ITEMS_SHARE" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_SHARE"),
		"ITEM_INFO" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_TITLE"),
		"ITEM_INFO_PROPS" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_PROPERTIES"),
		"ITEM_INFO_DELIVERY" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_DELIVERY"),
		"ITEM_INFO_DESCR" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_DESCRIPTION"),
		"ITEM_INFO_INSTRUCTION" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_INSRTUCTION"),
		"BOX_QUANTITY" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX_COUNT"),
		"ITEMS_RELEVANT" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_RELEVANT"),
		"ITEMS_SAME" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_SAME"),
		"ITEMS_DOWNLOAD" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_DOWNLOAD_PRISE"),
		"ITEMS_PACK_VOLUME" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX_VOLUME"),
		"ITEMS_PACK_VOLUME_TEXT" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX_VOLUME_TEXT"),
		"ITEMS_PACK_PROPS" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_BOX"),
		"COMPARE_ONLY_DIFFERENCES" => GetMessage("PROTEK_TEMPLATE_COMPARE_ONLY_DIFFERENCES"),
		"COMPARE_TITLE" => GetMessage("PROTEK_TEMPLATE_COMPARE_TITLE"),
		"COMPARE_NO_RESULT" => GetMessage("PROTEK_TEMPLATE_COMPARE_NO_RESULT"),
		"MENU_LESS" => GetMessage("PROTEK_TEMPLATE_SHOW_LESS"),
		"MENU_MORE" => GetMessage("PROTEK_TEMPLATE_MORE"),
		"MENU_TITLE" => GetMessage("PROTEK_TEMPLATE_CATALOG_MENU_TITLE"),
		"MENU_UP" => GetMessage("PROTEK_TEMPLATE_UP"),
		'REVIEW_IBLOCK_ID' => "77",
		'REVIEW_IBLOCK_TYPE' => "rewiews",
		'REVIEW_NAME' => GetMessage("PROTEK_TEMPLATE_REVIEW_NAME"),
		'REVIEW_TEXT' => GetMessage("PROTEK_TEMPLATE_REVIEW_TEXT"),
		'REVIEW_REQUIRED' => GetMessage("PROTEK_TEMPLATE_REQUIRE_NOTE"),
		'REVIEW_ADD_BUTTON' => GetMessage("PROTEK_TEMPLATE_REVIEW_ADD_BUTTON"),
		'REVIEW_ADD_MESSAGE' => GetMessage("PROTEK_TEMPLATE_REVIEW_ADD_MESSAGE"),
		"ITEM_INFO_REVIEWS" => GetMessage("PROTEK_TEMPLATE_CATALOG_ITEM_INFO_REVIEWS"),
		"LIST_PROPERTY_CODE_MOBILE" => array(
		),
		"DETAIL_MAIN_BLOCK_PROPERTY_CODE" => array(
		),
		"LABEL_PROP_MOBILE" => array(
		),
		"LABEL_PROP_POSITION" => "top-left",
		"SEF_URL_TEMPLATES" => array(
			"sections" => "",
			"section" => "#SECTION_CODE#/",
			"element" => "#SECTION_CODE#/#ELEMENT_CODE#/",
			"compare" => "compare.php?action=#ACTION_CODE#",
			"smart_filter" => "#SECTION_CODE#/filter/#SMART_FILTER_PATH#/apply/",
		),
		"VARIABLE_ALIASES" => array(
			"compare" => array(
				"ACTION_CODE" => "action",
			),
		)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>