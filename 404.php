<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(GetMessage("PROTEK_TEMPLATE_404_TITLE"));
$APPLICATION->AddChainItem(GetMessage("PROTEK_TEMPLATE_404_CHAIN"));?>
<section class="section section--top0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h2 class="section__title"><?$APPLICATION->ShowTitle()?></h2>
			</div>

			<div class="col-12 col-lg-7 col-xl-8">
				<img class="img-404" src="<?=DEFAULT_TEMPLATE_PATH?>/img/404/404.jpg" alt="">
			</div>

			<div class="col-12 col-lg-5 col-xl-4">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu",
					"protek_404_menu",
					Array(
						"ALLOW_MULTI_SELECT" => "N",
						"CHILD_MENU_TYPE" => "left",
						"DELAY" => "N",
						"MAX_LEVEL" => "2",
						"MENU_CACHE_GET_VARS" => array(0=>"",),
						"MENU_CACHE_TIME" => "36000",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_USE_GROUPS" => "Y",
						"ROOT_MENU_TYPE" => "sitemap",
						"USE_EXT" => "N"
					)
				);?>
			</div>
		</div>
	</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>